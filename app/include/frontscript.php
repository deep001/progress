<!-- jQuery 2.0.2 -->
<script src="js/jquery-1.8.3.js"></script>
<!-- Bootstrap -->
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
		
<script type="text/javascript">
$(document).ready(function(){ 
$("#login-form").validate({
rules: {
	txtUser: "required",
	txtPass: "required"
	},
messages: {
	txtUser : "*",
	txtPass : "*"
	}
});
}); 

</script>
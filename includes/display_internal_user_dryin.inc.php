<?php
require_once("../../includes/functions_internal_user_dryin.inc.php");
class display
{

	function title()
	{
		echo ":: Welcome to Seven Oceans Commercials ::";
	}
	
	function favicon()
	{
		echo '<link href="../../favicon.ico" rel="shortcut icon" />'; 
	}
	
	function css()
	{
		echo '<link rel="stylesheet" href="../../css/bootstrap.min.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../../css/font-awesome.min.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../../css/ionicons.min.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../../css/AdminLTE.css" type="text/css"/>';
		echo '<link rel="stylesheet" href="../../css/jClocksGMT.css" type="text/css"/>';
	}
	
	function js()
	{
		echo '<script src="../../js/jquery-1.8.3.js" type="text/javascript"></script>';
		echo '<script src="../../js/bootstrap.min.js" type="text/javascript"></script>';
		echo '<script src="../../js/AdminLTE/app.js" type="text/javascript"></script>';
		echo '<script src="../../js/jquery.validate.js" type="text/javascript"></script>';
		echo '<script src="../../js/jClocksGMT.js" type="text/javascript"></script>';
		echo '<script src="../../js/jquery.rotate.js" type="text/javascript"></script>';
	}
	
	
	function header_tag()
	{
		$obj = new data();
		if($_SESSION['gender'] == 1){$gender_img = "avatar5.png";}else{$gender_img = "avatar3.png";}
		$recent_work_arr = $obj->getLoginidWiseRecentFiveWorks();
		$company_name = $obj->getCompanyNameBasedOnID($_SESSION['company'],"COMPANY_NAME");
	echo '<header class="header">
            <a href="#Header" class="logo" style="font-size:14px;">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->'?>
               ::&nbsp;Seven Oceans' Commercials&nbsp;::
           <?php echo ' </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
						<!--<li><a href="../lockscreen.php" title="lock screen"><i class="fa fa-lock"></i></a></li>-->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-envelope"></i>
                                <span class="label label-success">'.count($recent_work_arr).'</span>
                            </a>';
							if(count($recent_work_arr) == 0)
							{
                            echo '<ul class="dropdown-menu">
                                <li class="header">No recent work log</li> 
							</ul>';
							}
							else
							{	
								echo '<ul class="dropdown-menu">
                                <li class="header">Top '.count($recent_work_arr).' recent work</li> 
								
								<li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">';
									
                                       for($i=0;$i<count($recent_work_arr);$i++)
									   {
									    echo '<li><!-- start message -->
                                            <a href="#">
                                                <div class="pull-left">
                                                    <img src="../../img/'.$gender_img.'" class="img-circle" alt="User Image"/>
                                                </div>
                                                <h4>
                                                    '.ucwords(strtolower($_SESSION['display'])).'
                                                    <small><i class="fa fa-clock-o"></i> '.$recent_work_arr[$i]['datetime'].'</small>
                                                </h4>
                                                <p>'.$recent_work_arr[$i]['work'].'</p>
                                            </a>
                                        </li><!-- end message -->';
                                      } 
                                    echo '</ul>
                                </li>
                               
							  <li class="footer"><a href="#">See All Works</a></li>
								</ul>';
							}
                        echo '</li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-warning"></i>
                                <span class="label label-warning">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have zero notifications</li>                           
                            </ul>
                        </li>
                        <!-- Tasks: style can be found in dropdown.less -->
                        <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-tasks"></i>
                                <span class="label label-danger">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have zero tasks</li>                             
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>';
								$display_short_name = explode(" ",$_SESSION['display']);
                                echo '<span> '.$_SESSION['display'].' <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">';
							//if($_SESSION['gender'] == 1){$gender_img = "avatar5.png";}else{$gender_img = "avatar3.png";}
							       echo'<img src="../../img/logo.png" class="img-circle" alt="User Image" />
                                    <p>'.$_SESSION['display'].'
                                        <small>Internal User , '.$company_name.'</small>
                                    </p>
                                </li>
                               <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="profile.php" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="../../logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header><a href="#" class="back-to-top" title="Back to top"><i class="glyphicon glyphicon-circle-arrow-up" style="font-size:20px;"></i></a>';

	}
	
	function leftmenu($val)
	{
		$obj = new data();
		$display_short_name = explode(" ",$_SESSION['display']);
		$company_name = $obj->getCompanyNameBasedOnID($_SESSION['company'],"COMPANY_NAME");
		$dashclass = $fleets = $coa = "";$masters = $daily_task = $modulerights ="treeview";
		$moduleid = $_SESSION['moduleid'];
		$loginid = $_SESSION['uid']; 
		$linkid =  $obj->getUserAuthenticationModuleWiseForMainMenu($moduleid);
		
		if($val==1){$dashclass ="active";}
		else if($val==2){$fleets ="active";}
		else if($val==3){$masters = "active ".$masters;}
		if($val==4){$coa ="active";}
		if($val==5){$daily_task = "active ".$daily_task;}
			echo '<aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
						<img src="../../img/soctaralogo.jpg" class="img-circle" alt="User Image" style="background-color:#fff;"/>
                         </div>
                        <div class="pull-left info">
                            <p>Welcome, '.$_SESSION['display'].'</p>
							<small>'.$company_name.'</small>
						</div>
                    </div>
                   <!-- sidebar menu: : style can be found in sidebar.less -->
                    
					
					<ul class="sidebar-menu">';
					echo '<li class="'.$dashclass.'"><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>';
							
					echo '<li class="'.$fleets.'"><a href="fleet.php"><i class="fa fa-anchor"></i><span>Fleet</span></a></li>';
							
					echo '<li class="'.$masters.'"><a href="#"><i class="fa fa-folder-open"></i><span>Masters</span><i class="fa fa-angle-left pull-right"></i></a>
						   <ul class="treeview-menu">
							<li><a href="banking_details.php" ><i class="fa fa-angle-double-right"></i>Banking Details</a></li>       
							<li><a href="necessary_approval.php" ><i class="fa fa-angle-double-right"></i>Necessary Approval</a></li> 
							<li><a href="terminal_list.php" ><i class="fa fa-angle-double-right"></i>Terminal</a></li>    
							<li><a href="material_list.php" ><i class="fa fa-angle-double-right"></i>Material</a></li>           
							<li><a href="contract_type_list.php" ><i class="fa fa-angle-double-right"></i>Contract Type</a></li>  
							<li><a href="port_cost_type_list.php" ><i class="fa fa-angle-double-right"></i>Port Cost Type</a>
							<li><a href="agency_fee_record.php" ><i class="fa fa-angle-double-right"></i>Agency Fee Records</a></li>
							<li><a href="other_shipping_cost_master.php" ><i class="fa fa-angle-double-right"></i>Other Shipping Cost</a></li>
				            <li><a href="other_miscellaneous_cost_master.php" ><i class="fa fa-angle-double-right"></i>Other Miscellaneous Cost</a></li>
							<li><a href="owner_related_cost.php" ><i class="fa fa-angle-double-right"></i>Owner Related Cost</a></li>
							<li><a href="cargo_work_stage.php" ><i class="fa fa-angle-double-right"></i>Cargo Work Stage</a></li>
							<li><a href="vendor_list.php" ><i class="fa fa-angle-double-right"></i>Vendor</a></li>
							<li><a href="port_information.php" ><i class="fa fa-angle-double-right"></i>Port Information</a></li>
							<li><a href="tc_deductions.php" ><i class="fa fa-angle-double-right"></i>TC Deductions</a></li>
							<li><a href="vc_deductions.php" ><i class="fa fa-angle-double-right"></i>VC Deductions</a></li>
							<li><a href="bunker_grade.php" ><i class="fa fa-angle-double-right"></i>Bunker Grade</a></li>
							<li><a href="port_data.php" ><i class="fa fa-angle-double-right"></i>Port Data</a></li>
							<li><a href="material_safety_data_list.php" ><i class="fa fa-angle-double-right"></i>Material Safety Data Sheets</a></li>
							<li><a href="vessel_cat_master_list.php" ><i class="fa fa-angle-double-right"></i>Vessel Category Master</a></li>
							<li><a href="reference_category_list.php" ><i class="fa fa-angle-double-right"></i>E-Library Category Master</a></li>
							<li><a href="reference_type_list.php" ><i class="fa fa-angle-double-right"></i>E-Library Reference Type Master</a></li>
							<li><a href="baltic_route_list.php" ><i class="fa fa-angle-double-right"></i>Baltic Route</a></li>
							
						   </ul>
						</li>';		
						if(in_array(7,$linkid)){		
						echo '<li class="'.$coa.'"><a href="coa_list.php"><i class="fa fa-truck"></i><span>COA</span></a></li>';
						}
						
						if(in_array(1,$linkid) || in_array(2,$linkid) || in_array(3,$linkid) || in_array(4,$linkid) || in_array(5,$linkid) || in_array(6,$linkid))
						{ 
						echo '<li class="'.$daily_task.'"><a href="#"><i class="fa fa-book"></i><span>Daily Tasks</span><i class="fa fa-angle-left pull-right"></i></a>
						   <ul class="treeview-menu">';
						   
						if(in_array(1,$linkid)){  
						echo' <li><a href="open_vessel_position.php" ><i class="fa fa-angle-double-right"></i>Open Vessel Position</a></li>';       		
						}
						if(in_array(2,$linkid)){
						echo '<li><a href="open_cargo_position.php" ><i class="fa fa-angle-double-right"></i>Open Cargo Position</a></li>'; 			
						}
						if(in_array(3,$linkid)){
						echo '<li><a href="nomination_at_glance.php" ><i class="fa fa-angle-double-right"></i>Nominations at a glance</a></li>';    
						}
						if(in_array(4,$linkid)){
						echo'<li><a href="in_ops_at_glance.php" ><i class="fa fa-angle-double-right"></i>In Ops at a glance</a></li>';           
						}
						if(in_array(5,$linkid)){
						echo'<li><a href="#" ><i class="fa fa-angle-double-right"></i>Vessels in Post Ops</a></li>';
						}
						if(in_array(6,$linkid)){  
						echo'<li><a href="#" ><i class="fa fa-angle-double-right"></i>Vessels in History</a></li>';
						}
						echo '</ul>
						</li>';
						} else{ }
											
                   echo '</ul>	
                </section>
                <!-- /.sidebar -->
            </aside>';
	}
	

function footer()
	{
		echo '<div class="footer" align="right"><img src="../../img/Logo2.jpg" height="14" width="14">&nbsp;<a href="http://sevenoceansconsulting.com" target="_blank" style="font-weight:bold;">A Seven Oceans Holdings production</a>&nbsp;&nbsp;&nbsp;&nbsp;</div>';
	}

	
	function logout_iu()
	{
		if(!isset($_SESSION['uid'],$_SESSION['username'],$_SESSION['display'],$_SESSION['iutype']))
		{
			header("location:../../logout.php");
		}
	}
	
}
?>
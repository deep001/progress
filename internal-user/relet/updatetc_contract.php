<?php 
session_start();
require_once("../../includes/display_internal_user_relet.inc.php");
require_once("../../includes/functions_internal_user_relet.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->updateTC_ContractDetails($id);
	header('Location : ./tc_contract_list.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']).'?id='.$id;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rigts = $obj->getUserRights($uid,$moduleid,1);
$obj->getTC_ContractData($id);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$(".areasize").autosize({append: "\n"});
$("#selVName").val(<?php echo $obj->getFun2();?>);
$("#selVendor").val('<?php echo $obj->getFun3();?>');
$("#txtCPHire,#txtMinPeriod,#txtMaxPeriod").numeric();

$('#txtCPDate,#txtFromDate,#txtToDate,#txtDeliveryDate,#txtReDeliveryDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
});

$('#txtCPDate,#txtFromDate,#txtToDate,#txtDeliveryDate,#txtReDeliveryDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
});

$('#txtToDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
	}).on('changeDate', function(){  getTimeDiff(); });
	

$("#frm1").validate({
	rules: {
		selVName:"required",
		selVendor:"required",
		txtCPDate:"required",
		txtCPHire:"required",
		txtFromDate:"required",
		txtToDate:"required",
		txtTotalDays: "required"
		},
	messages: {	
		selVName:"*",
		selVendor:"*",
		txtCPDate:"*",
		txtCPHire:"*",
		txtFromDate:"*",
		txtToDate:"*",
		txtTotalDays:"*"
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

function getTimeDiff()
{	
	if($('#txtFromDate').val() != '' &&  $('#txtToDate').val() != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat($('#txtToDate').val());
		start_actual_time    =  getDateWithSpecificFormat($('#txtFromDate').val());
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000 ;
		$('#txtTotalDays').val(days.toFixed(0));
	}
}
});

function getString(var1)
{
  var var2 = var1.split(' ');
  var var3 = var2[0].split('-');  
  return var3[2]+'/'+var3[1]+'/'+var3[0]+' '+var2[1];
}
 

function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' 00:00';
	return currentDate;
}
</script>

<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;TC Contract</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="tc_contract_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             UPDATE TC CONTRACT    
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Vessel Name
                            <address>
                               <select  name="selVName" class="form-control" id="selVName" >
								<?php 
                                $obj->getVesselTypeListMemberWise();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                      <div class="col-sm-4 invoice-col">
                           Vendor Name
                            <address>
                                <select  name="selVendor" class="form-control" id="selVendor" >
									<?php 
                                    $obj->getVendorListNew();
                                    ?>
                                    </select>
                            </address>
                        </div> 
                     
					</div>
                    
                   
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                             TC Contract No.
                            <address>
                               <input type="text" name="txtTCContractNo" id="txtTCContractNo" class="form-control" readonly placeholder="TC Contract No." autocomplete="off" value="<?php echo $obj->getFun4();?>"/>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                             CP Date
                            <address>
                               <input type="text" name="txtCPDate" id="txtCPDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($obj->getFun5()));?>"/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             CP Hire
                            <address>
                               <input type="text" name="txtCPHire" id="txtCPHire" class="form-control"  placeholder="CP Hire" autocomplete="off" value="<?php echo $obj->getFun6();?>"/>
                            </address>
                        </div>  
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                             From Date
                            <address>
                               <input type="text" name="txtFromDate" id="txtFromDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($obj->getFun7()));?>"/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             To Date
                            <address>
                               <input type="text" name="txtToDate" id="txtToDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($obj->getFun8()));?>"/>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                             Total Days
                            <address>
                               <input type="text" name="txtTotalDays" id="txtTotalDays" class="form-control"  placeholder="Total Days" autocomplete="off" value="<?php echo $obj->getFun9();?>"/>
                            </address>
                        </div>
					</div>
			
                    <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                             Delivery Date
                            <address>
							<?php if($obj->getFun13()!="0000-00-00"){$txtDeliveryDate = date('d-m-Y',strtotime($obj->getFun13()));}else{$txtDeliveryDate="";}?>
                               <input type="text" name="txtDeliveryDate" id="txtDeliveryDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtDeliveryDate;?>"/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             Re-delivery Date
                            <address>
							<?php if($obj->getFun14()!="0000-00-00"){$txtReDeliveryDate = date('d-m-Y',strtotime($obj->getFun14()));}else{$txtReDeliveryDate="";}?>
                               <input type="text" name="txtReDeliveryDate" id="txtReDeliveryDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo $txtReDeliveryDate;?>"/>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                            Minimum Period (Days)
                            <address>
                               <input type="text" name="txtMinPeriod" id="txtMinPeriod" class="form-control"  placeholder="Minimum Period (Days)" autocomplete="off" value="<?php echo $obj->getFun15();?>"/>
                            </address>
                        </div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                            Maximum Period (Days)
                            <address>
                               <input type="text" name="txtMaxPeriod" id="txtMaxPeriod" class="form-control"  placeholder="Maximum Period (Days)" autocomplete="off" value="<?php echo $obj->getFun16();?>"/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             Location
                            <address>
                               <input type="text" name="txtLocation" id="txtLocation" class="form-control"  placeholder="Location" autocomplete="off" value="<?php echo $obj->getFun17();?>"/>
                            </address>
                        </div>
					</div>
                    <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                           TC Contract Description
                            <address>
                               <textarea class="form-control areasize" name="txtContractDirection" id="txtContractDirection" rows="3" placeholder="TC Contract Description ..." ><?php echo $obj->getFun12();?></textarea>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    
                     <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
							&nbsp;
                            <address>
							
								<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Inspector's Report">
                                    <i class="fa fa-paperclip"></i> Attachment
                                    <input type="file" class="form-control" name="attach_file" id="attach_file" title="" data-widget="Add Inspector's Report" data-toggle="tooltip" data-original-title="Add"/>
									
                                </div>
							
								<?php if($obj->getFun10() != ""){
									echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../../attachment/'.$obj->getFun10().'" target="_blank" data-toggle="tooltip" data-original-title="View Previous Upload"><i class="fa fa-download" style="font-size:15px;"></i></a>';
									 }?>
							</address>
							
						</div>
                     </div>
				
					<?php 
					 	
					 	//if($rigts == 1)
						//{
					 ?>
					<div class="box-footer" align="right">
						<button type="submit" class="btn btn-primary btn-flat" >Submit</button>
						<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>
					<?php // } ?>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>
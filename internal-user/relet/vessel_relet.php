<?php 
session_start();
require_once("../../includes/display_internal_user_relet.inc.php");
require_once("../../includes/functions_internal_user_relet.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
	$msg = $obj->addTC_ContractSheet();
	header('Location : ./vessel_relet.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Vessel Relet</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Vessel Relet added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating Vessel Relet
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
				<h3 style=" text-align:center;">Vessel Relet List</h3>
				<div align="right"><a href="add_vessel_relet.php" title="Add New"><button class="btn btn-info btn-flat">Add New</button></a>&nbsp;&nbsp;&nbsp;&nbsp;</div>
				<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
					<div style="height:10px;">
						<input name="txttblid" id="txttblid" class="input" size="5" type="hidden" value="" />
						<input name="txttblstatus" id="txttblstatus" class="input" size="5" type="hidden" value="" />
						<input type="hidden" name="action" value="submit" />
					</div>				
					<?php $obj->displayVesselReletTCContractList();?>
				</form>
				</div>
                
                <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="fa fa-file-text-o"></i>Add New Sheet</h4>
                    </div>
						<div class="modal-body">
							<div class="box box-primary">
                                <div class="box-body no-padding" style="overflow:auto;">
                                <form name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                                   <table width="100%" cellpadding="1" cellspacing="1" border="0">
                                    <tr height="60">
									  <td width="40%" align="center" valign="middle"></td>
									  <td width="50%" align="center" valign="middle"></td>
                                      <td width="10%" align="center" valign="middle"></td>
									</tr>
									<tr height="30">
									  <td width="40%" align="center" valign="middle">Sheet Name :</td>
									  <td width="50%" align="left" valign="middle"><input type="text" name="txtSheetName" id="txtSheetName" class="form-control"  placeholder="Sheet Name" autocomplete="off" value=""/></td>
                                      <td width="10%" align="center" valign="middle"></td>
									</tr>
                                    <tr height="60">
									  <td width="40%" align="center" valign="middle"></td>
									  <td width="50%" align="left" valign="middle"><button type="submit" class="btn btn-primary btn-flat" >Submit</button>
						<input type="hidden" name="action" value="submit" /><input type="hidden" name="tc_contractId" id="tc_contractId" value="" /><input type="hidden" name="tc_contractNo" id="tc_contractNo" value="" /><input type="hidden" name="txtvslReletId" id="txtvslReletId" value="" /></td>
                                      <td width="10%" align="center" valign="middle"></td>
									</tr>
                                    <tr height="60">
									  <td width="40%" align="center" valign="middle"></td>
									  <td width="50%" align="center" valign="middle"></td>
                                      <td width="10%" align="center" valign="middle"></td>
								    </tr>
								   </table>
                                   </thead>
                                </div><!-- /.box-body -->
                            </div>
							
						</div>                       
                   
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
 $("#frm2").validate({
	rules: {
		txtSheetName:"required"
		},
	messages: {	
		txtSheetName:"*"
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
 });
});
$(function() {
	$("#vessel_category_list").dataTable();

});
function removeDetails(var1)
{
	jConfirm('Do you want to delete this entry?', 'Confirmation', function(r) {
	if(r){ 
		$("#txttblid").val(var1);
		document.frm1.submit();
	}
	
	});
}


function addSheet(var1,var2,var3)
{
	$("#tc_contractId").val(var1);
	$("#tc_contractNo").val(var3);
	$("#txtvslReletId").val(var2);
}
</script>
		
</body>
</html>
<?php 
session_start();
require_once("../../includes/display_internal_user_relet.inc.php");
require_once("../../includes/functions_internal_user_relet.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$tc_conid = $_REQUEST['tc_conid'];
$vsl_reletid = $_REQUEST['vsl_reletid'];
$sheetid = $_REQUEST['sheetid'];
$sheetExists = $obj->getSheetExistsOrNot($tc_conid, $vsl_reletid, $sheetid);

$sheetExists = explode('@@@',$sheetExists);

if(@$_REQUEST['action'] == 'submit1')
 {
 	$msg = $obj->insertVesselReletSheetDetails();
	header('Location : ./vessel_relet.php?msg='.$msg);
 }
if(@$_REQUEST['action'] == 'submit2')
 {
 	$msg = $obj->updateVesselReletSheetDetails();
	header('Location : ./vessel_relet.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF'])."?tc_conid=".$tc_conid.'&vsl_reletid='.$vsl_reletid.'&sheetid='.$sheetid;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rigts = $obj->getUserRights($uid,$moduleid,1);

$obj->getReletSheetData($tc_conid, $vsl_reletid, $sheetExists[2], $sheetExists[1]);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
$(".areasize").autosize({append: "\n"});
$("#txtDailyHire, [id^=txtConspQtyMT_], [id^=txtPrice_], [id^=txtRedelConspQtyMT_], [id^=txtRedelPrice_], #txtCommission, [id^=txtBrCommPercent_], [id^=txtOnRedelConspQtyMT_], [id^=txtOnRedelPrice_], #txtOutDailyHire, [id^=txtOutConspQtyMT_], [id^=txtOutPrice_], [id^=txtOutRedelConspQtyMT_], [id^=txtOutRedelPrice_], #txtOutCommission, [id^=txtOutOnRedelConspQtyMT_], [id^=txtOutOnRedelPrice_], [id^=txtOutBrCommPercent_], #txtWaitingDays, [id^=txtOutWaitConspQtyMT_], [id^=txtOutWaitPrice_], [id^=txtOutExpConspQtyMT_], [id^=txtOutExpPrice_], #txtBalWaitingDays, [id^=txtOutBalConspQtyMT_], [id^=txtOutBalPrice_], #txtHireLossDays, #txtAddComm,#txtILOHC,#txtBallastBonus,#txtILOHC,#txtCVE,#txtRedelCVE,#txtOnRedelCVE,#txtOutCVE,#txtOutILOHC,#txtOutBallastBonus,#txtOutRedelCVE,#txtOutOnRedelCVE,#txtOutWaitCVE,#txtOutExpCVE,#txtOutBalCVE,[id^=txtOffPer_],[id^=txtOutOffPer_],#txtOutRedelCVEPer,#txtOutOnRedelCVEPerAmt,#txtOutWaitCVEPer,#txtOutExpCVEPer,#txtOutBalCVEPer,#txtCVEPer,#txtRedelCVEPer,#txtOnRedelCVEPer,#txtOutCVEPer").numeric();

$('#txtSheetDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
});

$("#frm1").validate({
	rules: {
		txtSheetDate:"required",
		txtDailyHire:"required",
		selTCINOwner:"required",
		txtDateOfDel:"required",
		txtDateOfReDel:"required",
		selTCOutOwner:"required",
		txtOutDailyHire: "required",
		txtOutDateOfDel:"required",
		txtOutDateOfReDel: "required"
		},
	messages: {	
		txtSheetDate:"*",
		txtDailyHire:"*",
		selTCINOwner:"*",
		txtDateOfDel:"*",
		txtDateOfReDel:"*",
		selTCOutOwner:"*",
		txtOutDailyHire:"*",
		txtOutDateOfDel:"*",
		txtOutDateOfReDel:"*"
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

$('#txtOutDateOfDel,#txtOutDateOfReDel,[id^=txtOutFromOff_],[id^=txtOutToOff_],#txtDateOfDel,#txtDateOfReDel,[id^=txtFromOff_],[id^=txtToOff_]').datetimepicker({
	format: 'dd-mm-yyyy hh:ii',
	autoclose: true,
    todayBtn: true,
    minuteStep: 1
	}).on('changeDate', function(){  getCVETotalAmt(); });

getCVETotalAmt();	
});


function getTimeDiff(date2,date1)
{	
	if(date1 != '' &&  date2 != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat(date2);
		start_actual_time    =  getDateWithSpecificFormat(date1);
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000 + parseInt(1);
		return days.toFixed(2);
		//$('#txtTotalDays').val(days.toFixed(0));
	}
}


function getString(var1)
{
  var var2 = var1.split(' ');
  var var3 = var2[0].split('-');  
  return var3[2]+'/'+var3[1]+'/'+var3[0]+' '+var2[1];
}
 
function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' '+dateSplit1[1];
	return currentDate;
}



function getCVETotalAmt()
{
//---------------in-----------------------------------------------
    var delDays = parseFloat(getTimeDiff($("#txtDateOfReDel").val(),$("#txtDateOfDel").val()));
	var numoff = $("#txtOFFID").val();
	var offhiredays = 0;
	for(var i =1;i <=numoff;i++)
	{
	    var timediff = parseFloat(getTimeDiff($("#txtToOff_"+i).val(),$("#txtFromOff_"+i).val()));
		if($("#txtOffPer_"+i).val()!="" || $("#txtOffPer_"+i).val()!=0)
		{var percent = $("#txtOffPer_"+i).val();}
		else
		{var percent = 100;}
		offhiredays =   parseFloat(parseFloat(offhiredays) + parseFloat(parseFloat(parseFloat(timediff)*parseFloat(percent))/100));
	}
	if(isNaN(offhiredays)){offhiredays = 0.00;}
	var nettperiod = parseFloat(delDays)- parseFloat(offhiredays);
	if(isNaN(nettperiod)){nettperiod = 0.00;}
	$("#txtNettPeriod").val(parseFloat(nettperiod).toFixed(2));
	$("#txtNettHire").val(parseFloat(parseFloat($("#txtDailyHire").val())*parseFloat($("#txtNettPeriod").val())).toFixed(2));
	getBrokageCommission();
	//------------------------------------------------------
	var amt = parseFloat(parseFloat($("#txtCVE").val())/30)*parseFloat($("#txtNettPeriod").val());
	if(isNaN(amt)){amt = 0.00;}
	$("#txtCVEAmt").val(amt.toFixed(2));
	if($("#txtCVEPer").val()=="" || $("#txtCVEPer").val()==0){$("#txtCVEPerAmt").val($("#txtCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtCVEAmt").val())*parseFloat($("#txtCVEPer").val()))/100);
		 if(isNaN(perAmt)){perAmt = 0.00;}$("#txtCVEPerAmt").val(perAmt.toFixed(2));}
	
	var amt = parseFloat(parseFloat($("#txtRedelCVE").val())/30)*parseFloat($("#txtNettPeriod").val());
	if(isNaN(amt)){amt = 0.00;}
	$("#txtRedelCVEAmt").val(amt.toFixed(2));
	if($("#txtRedelCVEPer").val()=="" || $("#txtRedelCVEPer").val()==0){$("#txtRedelCVEPerAmt").val($("#txtRedelCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtRedelCVEAmt").val())*parseFloat($("#txtRedelCVEPer").val()))/100);
		 if(isNaN(perAmt)){perAmt = 0.00;}$("#txtRedelCVEPerAmt").val(perAmt.toFixed(2));}
	
	var amt = parseFloat(parseFloat($("#txtOnRedelCVE").val())/30)*parseFloat($("#txtNettPeriod").val());
	if(isNaN(amt)){amt = 0.00;}
	$("#txtOnRedelCVEAmt").val(amt.toFixed(2));
	if($("#txtOnRedelCVEPer").val()=="" || $("#txtOnRedelCVEPer").val()==0){$("#txtOnRedelCVEPerAmt").val($("#txtOnRedelCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtOnRedelCVEAmt").val())*parseFloat($("#txtOnRedelCVEPer").val()))/100);
		 if(isNaN(perAmt)){perAmt = 0.00;}$("#txtOnRedelCVEPerAmt").val(perAmt.toFixed(2));}
	
	var netHireval = parseFloat($("#txtNettHire").val()) + parseFloat($("#txtCVEAmt").val()) + parseFloat($("#txtRedelCVEAmt").val()) + parseFloat($("[id^=txtConspAmt_]").sum()) + parseFloat($("[id^=txtRedelConspAmt_]").sum()) - parseFloat(parseFloat($("[id^=txtOnRedelConspAmt_]").sum()) + parseFloat($("#txtOnRedelCVEAmt").val()) + parseFloat($("#txtTotalCommission").val()) + parseFloat($("#txtBrComm").val()));
	if(isNaN(netHireval)){netHireval = 0.00;}
	$("#txtTotalBalanceOwner").val(netHireval.toFixed(2));
	
 //------------------out--------------------------------------	-----------------------------------
    var deloutDays = parseFloat(getTimeDiff($("#txtOutDateOfReDel").val(),$("#txtOutDateOfDel").val()));
	var numoutoff = $("#txtOutOFFID").val();
	var outoffhiredays = 0;
	for(var i =1;i <=numoutoff;i++)
	{
		var outtimediff = parseFloat(getTimeDiff($("#txtOutToOff_"+i).val(),$("#txtOutFromOff_"+i).val()));
		if($("#txtOutOffPer_"+i).val()!="" || $("#txtOutOffPer_"+i).val()!=0)
		{var outpercent = $("#txtOutOffPer_"+i).val();}
		else
		{var outpercent = 100;}
		outoffhiredays =   parseFloat(parseFloat(outoffhiredays) + parseFloat(parseFloat(parseFloat(outtimediff)*parseFloat(outpercent))/100));
	}
	if(isNaN(outoffhiredays)){outoffhiredays = 0.00;}
	var outnettperiod = parseFloat(deloutDays)- parseFloat(outoffhiredays);
	if(isNaN(outnettperiod)){outnettperiod = 0.00;}
	$("#txtOutNettPeriod").val(parseFloat(outnettperiod).toFixed(2));
	$("#txtOutNettHire").val(parseFloat(parseFloat($("#txtOutDailyHire").val())*parseFloat($("#txtOutNettPeriod").val())).toFixed(2));
	getOutBrokageCommission();
	//-----------------------------------------------------
	var amt = parseFloat(parseFloat($("#txtOutCVE").val())/30)*parseFloat($("#txtOutNettPeriod").val());
	if(isNaN(amt)){amt = 0.00;}
	$("#txtOutCVEAmt").val(amt.toFixed(2));
	if($("#txtOutCVEPer").val()=="" || $("#txtOutCVEPer").val()==0){$("#txtOutCVEPerAmt").val($("#txtOutCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutCVEAmt").val())*parseFloat($("#txtOutCVEPer").val()))/100);
		 if(isNaN(perAmt)){perAmt = 0.00;}$("#txtOutCVEPerAmt").val(perAmt.toFixed(2));}
	
	var amt = parseFloat(parseFloat($("#txtOutRedelCVE").val())/30)*parseFloat($("#txtOutNettPeriod").val());
	if(isNaN(amt)){amt = 0.00;}
	$("#txtOutRedelCVEAmt").val(amt.toFixed(2));
	if($("#txtOutRedelCVEPer").val()=="" || $("#txtOutRedelCVEPer").val()==0){$("#txtOutRedelCVEPerAmt").val($("#txtOutRedelCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutRedelCVEAmt").val())*parseFloat($("#txtOutRedelCVEPer").val()))/100);
		 if(isNaN(perAmt)){perAmt = 0.00;}$("#txtOutRedelCVEPerAmt").val(perAmt.toFixed(2));}
	
	var amt = parseFloat(parseFloat($("#txtOutOnRedelCVE").val())/30)*parseFloat($("#txtOutNettPeriod").val());
	if(isNaN(amt)){amt = 0.00;}
	$("#txtOutOnRedelCVEAmt").val(amt.toFixed(2));
	if($("#txtOutOnRedelCVEPer").val()=="" || $("#txtOutOnRedelCVEPer").val()==0){$("#txtOutOnRedelCVEPerAmt").val($("#txtOutOnRedelCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutOnRedelCVEAmt").val())*parseFloat($("#txtOutOnRedelCVEPer").val()))/100);
		 if(isNaN(perAmt)){perAmt = 0.00;}$("#txtOutOnRedelCVEPerAmt").val(perAmt.toFixed(2));}
	
	var amt = parseFloat(parseFloat($("#txtOutWaitCVE").val())/30)*parseFloat($("#txtOutNettPeriod").val());
	if(isNaN(amt)){amt = 0.00;}
	$("#txtOutWaitCVEAmt").val(amt.toFixed(2));
	if($("#txtOutWaitCVEPer").val()=="" || $("#txtOutWaitCVEPer").val()==0){$("#txtOutWaitCVEPerAmt").val($("#txtOutWaitCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutWaitCVEAmt").val())*parseFloat($("#txtOutWaitCVEPer").val()))/100);
		 if(isNaN(perAmt)){perAmt = 0.00;}$("#txtOutWaitCVEPerAmt").val(perAmt.toFixed(2));}
	
	
	var amt = parseFloat(parseFloat($("#txtOutExpCVE").val())/30)*parseFloat($("#txtOutNettPeriod").val());
	if(isNaN(amt)){amt = 0.00;}
	$("#txtOutExpCVEAmt").val(amt.toFixed(2));
	if($("#txtOutExpCVEPer").val()=="" || $("#txtOutExpCVEPer").val()==0){$("#txtOutExpCVEPerAmt").val($("#txtOutExpCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutExpCVEAmt").val())*parseFloat($("#txtOutExpCVEPer").val()))/100);
		 if(isNaN(perAmt)){perAmt = 0.00;}$("#txtOutExpCVEPerAmt").val(perAmt.toFixed(2));}
	
	var amt = parseFloat(parseFloat($("#txtOutBalCVE").val())/30)*parseFloat($("#txtOutNettPeriod").val());
	if(isNaN(amt)){amt = 0.00;}
	$("#txtOutBalCVEAmt").val(amt.toFixed(2));
	if($("#txtOutBalCVEPer").val()=="" || $("#txtOutBalCVEPer").val()==0){$("#txtOutBalCVEPerAmt").val($("#txtOutBalCVEAmt").val());}
	else{perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutBalCVEAmt").val())*parseFloat($("#txtOutBalCVEPer").val()))/100);
		 if(isNaN(perAmt)){perAmt = 0.00;}$("#txtOutBalCVEPerAmt").val(perAmt.toFixed(2));}
	
	var netOutHireval = parseFloat($("#txtOutNettHire").val()) + parseFloat($("#txtOutCVEPerAmt").val()) + parseFloat($("#txtOutRedelCVEPerAmt").val()) + parseFloat($("#txtOutILOHC").val()) + parseFloat($("#txtOutBallastBonus").val()) + parseFloat($("[id^=txtOutConspAmt_]").sum()) + parseFloat($("[id^=txtOutRedelConspAmt_]").sum())  - parseFloat( parseFloat($("#txtOutOnRedelCVEPerAmt").val()) + parseFloat($("#txtOutTotalCommission").val()) + parseFloat($("[id^=txtOutOnRedelConspAmt_]").sum()) + parseFloat($("[id^=txtOutWaitConspAmt_]").sum()) + parseFloat($("#txtOutWaitCVEPerAmt").val()) + parseFloat($("[id^=txtOutExpConspAmt_]").sum()) + parseFloat($("#txtOutExpCVEPerAmt").val()) + parseFloat($("[id^=txtOutBalConspPerAmt_]").sum()) + parseFloat($("#txtOutBalCVEPerAmt").val()) + parseFloat($("#txtOutBrComm").val()) );
	if(isNaN(netOutHireval)){netOutHireval = 0.00;}
//----------------hire loss---------------------------
    var hireloss = parseFloat($("#txtDailyHire").val())*parseFloat($("#txtHireLossDays").val());
	if(isNaN(hireloss)){hireloss = 0.00;}
	$("#txtHireLossAmt").val(hireloss.toFixed(2));
//---------------- add comm---------------------------
    var addcomm = parseFloat(parseFloat($("#txtHireLossAmt").val())*parseFloat($("#txtAddComm").val()))/100;
	if(isNaN(addcomm)){addcomm = 0.00;}
	$("#txtAddCommAmt").val(addcomm.toFixed(2));
//---------------- Profit and Loss--------------------------
    
    var profitandLoss = parseFloat(netOutHireval) + parseFloat($("#txtAddCommAmt").val()) - parseFloat(parseFloat(netHireval) + parseFloat($("#txtHireLossAmt").val()));
	if(isNaN(profitandLoss)){profitandLoss = 0.00;}
	$("#txtProfitLoss").val(profitandLoss.toFixed(2));
}




function AddNewOffHire()
{
	var id = $("#txtOFFID").val();
	if($("#txtOffHireReason_"+id).val() != "" && $("#txtFromOff_"+id).val() != "" && $("#txtToOff_"+id).val() != "" && $("#txtOffPer_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="off_Row_'+id+'"><td align="center"><a href="#pr'+id+'" onclick="removeOffRow('+id+');"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtOffHireReason_'+id+'" id="txtOffHireReason_'+id+'" rows="2" placeholder="Off Hire Reason..."></textarea></td><td><input type="text" name="txtFromOff_'+id+'" id="txtFromOff_'+id+'" class="form-control" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""></td><td><input type="text" name="txtToOff_'+id+'" id="txtToOff_'+id+'" class="form-control" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""></td><td><input type="text" name="txtOffPer_'+id+'" id="txtOffPer_'+id+'" class="form-control"  placeholder="%" autocomplete="off" value="" onKeyUp="getCVETotalAmt();"/></td></tr>').appendTo("#tblOFF");
		$("#txtOFFID").val(id);
		$("[id^=txtOffPer_]").numeric();	
		$('#txtFromOff_'+id+',#txtToOff_'+id).datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
	    autoclose: true,
        todayBtn: true,
        minuteStep: 1
		}).on('changeDate', function(){  getCVETotalAmt(); });
	}
}

function removeOffRow(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#off_Row_"+var1).remove();
					getCVETotalAmt();	
				 }
			else{return false;}
			});
}

function getBunkerGradeAmt()
{
	var total = $("#txtbid").val();
	for(var i =1;i <=total;i++)
	{
	    
		var amt = parseFloat($("#txtConspQtyMT_"+i).val())*parseFloat($("#txtPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtConspAmt_"+i).val(amt.toFixed(2));
		if($("#txtConspPer_"+i).val()=="" || $("#txtConspPer_"+i).val()==0)
		{
		   $("#txtConspPerAmt_"+i).val($("#txtConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtConspAmt_"+i).val())*parseFloat($("#txtConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	
	getCVETotalAmt();
}


function getRedelBunkerGradeAmt()
{
	var total = $("#txtRedelbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtRedelConspQtyMT_"+i).val())*parseFloat($("#txtRedelPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtRedelConspAmt_"+i).val(amt.toFixed(2))
		if($("#txtRedelConspPer_"+i).val()=="" || $("#txtRedelConspPer_"+i).val()==0)
		{
		   $("#txtRedelConspPerAmt_"+i).val($("#txtRedelConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtRedelConspAmt_"+i).val())*parseFloat($("#txtRedelConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtRedelConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	getCVETotalAmt();
}


function addBrokerageRow()
{
	var id = $("#txtBRokageCount").val();
	if($("#txtBrCommPercent_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="tbrRow_'+id+'"><td><a href="#tb1" onclick="removeBrokerage('+id+');" ><i class="fa fa-times" style="color:red;"></a></td><td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td><td><input type="text" name="txtBrCommPercent_'+id+'" id="txtBrCommPercent_'+id+'" class="form-control" autocomplete="off" value="0.00" onKeyUp="getBrokageCommission();"  /></td><td><input type="text" name="txtBrComm_'+id+'" id="txtBrComm_'+id+'" class="form-control" readonly value="0.00" /></td><td><select  name="selBrVendor_'+id+'" class="select form-control" id="selBrVendor_'+id+'"></select></td></tr>').appendTo("#tbodyBrokerage");
	    $("#selBrVendor_"+id).html($("#selVendor").html());$("#selBrVendor_"+id).val('');
		$("[id^=txtBrCommPercent_]").numeric();	
		$("#txtBRokageCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
	
}

function removeBrokerage(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#tbrRow_"+var1).remove();
					getCVETotalAmt();
				 }
			else{return false;}
			});
}

function getBrokageCommission()
{
	for(var l = 1;l<=$("#txtBRokageCount").val();l++)
	{
	if($("#txtBrCommPercent_"+l).val() == ""){var brokerage_comm = 0;}else{var brokerage_comm = $("#txtBrCommPercent_"+l).val();}
	if($("#txtNettHire").val() == ""){var t_frt = 0;}else{var t_frt = $("#txtNettHire").val();}
	
	var brokerage_comm_usd = (parseFloat(t_frt) * parseFloat(brokerage_comm))/100;
	$("#txtBrComm_"+l).val(brokerage_comm_usd.toFixed(2));
	}
	$("#txtBrCommPercent").val($("[id^=txtBrCommPercent_]").sum().toFixed(2));
	$("#txtBrComm").val($("[id^=txtBrComm_]").sum().toFixed(2));
	
	var brokerage_comm = (parseFloat($("#txtNettHire").val()) * parseFloat($("#txtCommission").val()))/100;
	$("#txtTotalCommission").val(brokerage_comm.toFixed(2));
}



function getOnRedelBunkerGradeAmt()
{
	var total = $("#txtOnRedelbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtOnRedelConspQtyMT_"+i).val())*parseFloat($("#txtOnRedelPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtOnRedelConspAmt_"+i).val(amt.toFixed(2))
		if($("#txtOnRedelConspPer_"+i).val()=="" || $("#txtOnRedelConspPer_"+i).val()==0)
		{
		   $("#txtOnRedelConspPerAmt_"+i).val($("#txtOnRedelConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtOnRedelConspAmt_"+i).val())*parseFloat($("#txtOnRedelConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtOnRedelConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	getCVETotalAmt();
}


function AddOutNewOffHire()
{
	var id = $("#txtOutOFFID").val();
	if($("#txtOutOffHireReason_"+id).val() != "" && $("#txtOutFromOff_"+id).val() != "" && $("#txtOutToOff_"+id).val() != "" && $("#txtOutOffPer_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="Outoff_Row_'+id+'"><td align="center"><a href="#pr'+id+'" onclick="removeOutOffRow('+id+');"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtOutOffHireReason_'+id+'" id="txtOutOffHireReason_'+id+'" rows="2" placeholder="Off Hire Reason..."></textarea></td><td><input type="text" name="txtOutFromOff_'+id+'" id="txtOutFromOff_'+id+'" class="form-control" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""></td><td><input type="text" name="txtOutToOff_'+id+'" id="txtOutToOff_'+id+'" class="form-control" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""></td><td><input type="text" name="txtOutOffPer_'+id+'" id="txtOutOffPer_'+id+'" class="form-control"  placeholder="%" autocomplete="off" value="" onKeyUp="getCVETotalAmt();"/></td></tr>').appendTo("#tblOutOFF");
		$("#txtOutOFFID").val(id);
		$("[id^=txtOutOffPer_]").numeric();	
		$('#txtOutToOff_'+id+',#txtOutFromOff_'+id).datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		autoclose: true,
		todayBtn: true,
		minuteStep: 1
		}).on('changeDate', function(){  getCVETotalAmt(); });
	}
}

function removeOutOffRow(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#Outoff_Row_"+var1).remove();
					getCVETotalAmt();			
				 }
			else{return false;}
			});
}


function getOutBunkerGradeAmt()
{
	var total = $("#txtOutbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtOutConspQtyMT_"+i).val())*parseFloat($("#txtOutPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtOutConspAmt_"+i).val(amt.toFixed(2))
		if($("#txtOutConspPer_"+i).val()=="" || $("#txtOutConspPer_"+i).val()==0)
		{
		   $("#txtOutConspPerAmt_"+i).val($("#txtOutConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutConspAmt_"+i).val())*parseFloat($("#txtOutConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtOutConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	getCVETotalAmt();
}


function getOutRedelBunkerGradeAmt()
{
	var total = $("#txtOutRedelbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtOutRedelConspQtyMT_"+i).val())*parseFloat($("#txtOutRedelPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtOutRedelConspAmt_"+i).val(amt.toFixed(2))
		if($("#txtOutRedelConspPer_"+i).val()=="" || $("#txtOutRedelConspPer_"+i).val()==0)
		{
		   $("#txtOutRedelConspPerAmt_"+i).val($("#txtOutRedelConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutRedelConspAmt_"+i).val())*parseFloat($("#txtOutRedelConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtOutRedelConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	getCVETotalAmt();
}

function addOutBrokerageRow()
{
	var id = $("#txtOutBRokageCount").val();
	if($("#txtOutBrCommPercent_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="OuttbrRow_'+id+'"><td><a href="#tb1" onclick="removeOutBrokerage('+id+');" ><i class="fa fa-times" style="color:red;"></a></td><td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td><td><input type="text" name="txtOutBrCommPercent_'+id+'" id="txtOutBrCommPercent_'+id+'" class="form-control" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"  /></td><td><input type="text" name="txtOutBrComm_'+id+'" id="txtOutBrComm_'+id+'" class="form-control" readonly value="0.00" /></td><td><select  name="selOutBrVendor_'+id+'" class="select form-control" id="selOutBrVendor_'+id+'"></select></td></tr>').appendTo("#tbodyOutBrokerage");
	    $("#selOutBrVendor_"+id).html($("#selVendor").html());$("#selOutBrVendor_"+id).val('');
		$("[id^=txtOutBrCommPercent_]").numeric();	
		$("#txtOutBRokageCount").val(id);
	}
	else
	{
		jAlert('Please fill previous data', 'Alert');
		return false;
	}
}

function removeOutBrokerage(var1)
{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#OuttbrRow_"+var1).remove();
					getCVETotalAmt();
				 }
			else{return false;}
			});
}


function getOutBrokageCommission()
{
	for(var l = 1;l<=$("#txtOutBRokageCount").val();l++)
	{
	if($("#txtOutBrCommPercent_"+l).val() == ""){var brokerage_comm = 0;}else{var brokerage_comm = $("#txtOutBrCommPercent_"+l).val();}
	if($("#txtOutNettHire").val() == ""){var t_frt = 0;}else{var t_frt = $("#txtOutNettHire").val();}
	
	var brokerage_comm_usd = (parseFloat(t_frt) * parseFloat(brokerage_comm))/100;
	$("#txtOutBrComm_"+l).val(brokerage_comm_usd.toFixed(2));
	}
	$("#txtOutBrCommPercent").val($("[id^=txtOutBrCommPercent_]").sum().toFixed(2));
	$("#txtOutBrComm").val($("[id^=txtOutBrComm_]").sum().toFixed(2));
	
	var brokerage_comm = (parseFloat($("#txtOutNettHire").val()) * parseFloat($("#txtOutCommission").val()))/100;
	$("#txtOutTotalCommission").val(brokerage_comm.toFixed(2));
}


function getOutOnRedelBunkerGradeAmt()
{
	var total = $("#txtOutOnRedelbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtOutOnRedelConspQtyMT_"+i).val())*parseFloat($("#txtOutOnRedelPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtOutOnRedelConspAmt_"+i).val(amt.toFixed(2))
		if($("#txtOutOnRedelConspPer_"+i).val()=="" || $("#txtOutOnRedelConspPer_"+i).val()==0)
		{
		   $("#txtOutOnRedelConspPerAmt_"+i).val($("#txtOutOnRedelConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutOnRedelConspAmt_"+i).val())*parseFloat($("#txtOutOnRedelConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtOutOnRedelConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	getCVETotalAmt();
}

function getOutWaitBunkerGradeAmt()
{
	var total = $("#txtOutWaitbid").val();
	for(var i =1;i <=total;i++)
	{
	    var waitingdays = $("#txtWaitingDays").val();
		var amt = parseFloat(parseFloat($("#txtOutWaitConspQtyMT_"+i).val())*parseFloat($("#txtOutWaitPrice_"+i).val()))*parseFloat(waitingdays);
		if(isNaN(amt)){amt = 0.00;}
		$("#txtOutWaitConspAmt_"+i).val(amt.toFixed(2));
		if($("#txtOutWaitConspPer_"+i).val()=="" || $("#txtOutWaitConspPer_"+i).val()==0)
		{
		   $("#txtOutWaitConspPerAmt_"+i).val($("#txtOutWaitConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutWaitConspAmt_"+i).val())*parseFloat($("#txtOutWaitConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtOutWaitConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	getCVETotalAmt();
}


function getOutExpBunkerGradeAmt()
{
	var total = $("#txtOutExpbid").val();
	for(var i =1;i <=total;i++)
	{
		var amt = parseFloat($("#txtOutExpConspQtyMT_"+i).val())*parseFloat($("#txtOutExpPrice_"+i).val());
		if(isNaN(amt)){amt = 0.00;}
		$("#txtOutExpConspAmt_"+i).val(amt.toFixed(2));
		if($("#txtOutExpConspPer_"+i).val()=="" || $("#txtOutExpConspPer_"+i).val()==0)
		{
		   $("#txtOutExpConspPerAmt_"+i).val($("#txtOutExpConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutExpConspAmt_"+i).val())*parseFloat($("#txtOutExpConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtOutExpConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	getCVETotalAmt();
}


function getOutBalBunkerGradeAmt()
{
	var total = $("#txtOutBalbid").val();
	for(var i =1;i <=total;i++)
	{
	    var ballasdays = $("#txtBalWaitingDays").val();
		var amt = parseFloat(parseFloat($("#txtOutBalConspQtyMT_"+i).val())*parseFloat($("#txtOutBalPrice_"+i).val()))*parseFloat(ballasdays);
		if(isNaN(amt)){amt = 0.00;}
		$("#txtOutBalConspAmt_"+i).val(amt.toFixed(2));
		if($("#txtOutBalConspPer_"+i).val()=="" || $("#txtOutBalConspPer_"+i).val()==0)
		{
		   $("#txtOutBalConspPerAmt_"+i).val($("#txtOutBalConspAmt_"+i).val());
		}
		else
		{
		    perAmt =   parseFloat(parseFloat(parseFloat($("#txtOutBalConspAmt_"+i).val())*parseFloat($("#txtOutBalConspPer_"+i).val()))/100);
			if(isNaN(perAmt)){perAmt = 0.00;}
		    $("#txtOutBalConspPerAmt_"+i).val(perAmt.toFixed(2));
		}
	}
	getCVETotalAmt();
}

</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Vessel Relet</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="allPdf.php?id=1&tc_conid=<?php echo $tc_conid; ?>&vsl_reletid=<?php echo $vsl_reletid; ?>&sheetid=<?php echo $sheetid; ?>" title="Pdf"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a><a href="vessel_relet.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
				<?php if($sheetExists[0]==0 && $sheetExists[1]==0)
				 {?>			
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            VOYAGE FINANCIALS
                            <select  name="selPort" class="form-control" style="display:none;" id="selPort" >
                                <?php 
                                $obj->getPortList();
                                ?>
                               </select>
                               <select  name="selVendor" class="select form-control" style="display:none;" id="selVendor" >
									<?php $obj->getVendorListNewUpdate("");	?>
								</select> 
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Fixture Type
                            <address>
							   <input type="text" name="txtFixtureType" id="txtFixtureType" class="form-control" value="Vessel Re-let" readonly  placeholder="Vessel Name" autocomplete="off"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Main Particulars:
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                            Vessel Name
                            <address>
                               <input type="text" name="txtVesselName" id="txtVesselName" class="form-control" readonly  placeholder="Vessel Name" autocomplete="off" value="<?php echo $obj->getVesselIMOData($obj->insertTCContractDataBasedOnID($tc_conid,"VESSEL_IMO_ID"),"VESSEL_NAME");?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                           Nom ID
                            <address>
                               <input type="text" name="txtNomID" id="txtNomID" class="form-control" readonly  placeholder="Nom ID" autocomplete="off" value="<?php echo $obj->getVesselReletSheetNumber($tc_conid, $vsl_reletid, $sheetid);?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                           Vessel Type
                            <address>
                               <input type="text" name="txtVesselType" id="txtVesselType" class="form-control" readonly  placeholder="Vessel Type" autocomplete="off" value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->insertTCContractDataBasedOnID($tc_conid,"VESSEL_IMO_ID"),"VESSEL_TYPE"),"NAME");?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                           Date
                            <address>
                               <input type="text" name="txtSheetDate" id="txtSheetDate" class="form-control"  placeholder="dd-mm-YYYY" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
					</div>
                   
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            TC IN Details:
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
                            <h3 class="page-header">
                            Due to Owners
                            </h3>                            
                      
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            TC IN Owners
                            <address>
                               <select  name="selTCINOwner" class="form-control" id="selTCINOwner" >
					           </select>
							   <script>$("#selTCINOwner").html($("#selVendor").html());</script>
                               <script>$("#selTCINOwner").val('<?php echo $obj->insertTCContractDataBasedOnID($tc_conid,"VENDORID");?>');</script>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           CP Date
                            <address>
                               <input type="text" name="txtCPDate" id="txtCPDate" class="form-control" readonly  placeholder="dd-mm-YYYY" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($obj->insertTCContractDataBasedOnID($tc_conid,"CP_DATE")));?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Daily Hire(USD/Day)
                            <address>
                               <input type="text" name="txtDailyHire" id="txtDailyHire" class="form-control" placeholder="Daily Hire(USD/Day)" autocomplete="off" value="<?php echo $obj->insertTCContractDataBasedOnID($tc_conid,"CP_HIRE");?>" onKeyUp="getCVETotalAmt();"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Date of Del
                            <address>
                               <input type="text" name="txtDateOfDel" id="txtDateOfDel" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                             Port of Del
                            <address>
                               <select  name="selDelPort" class="form-control" id="selDelPort" >
                               </select>
                               <script>$("#selDelPort").html($("#selPort").html());</script>
                            </address>
                        </div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                             Date of Re-Del
                            <address>
                                <input type="text" name="txtDateOfReDel" id="txtDateOfReDel" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             Port of Re-Del
                            <address>
                               <select  name="selReDelPort" class="form-control" id="selReDelPort" >
                               </select>
                               <script>$("#selReDelPort").html($("#selPort").html());</script>
                            </address>
                        </div>
					</div>
			
                    
                    <div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Off Hire Reason</th>
                                    <th>Off Hire From (Days)</th>
                                    <th>Off Hire To (Days)</th>
									<th>Off Hire %<input type="hidden" name="txtOFFID" id="txtOFFID" value="1"/></th>
                                </tr>
                            </thead>
                            <tbody id="tblOFF">
                                <tr id="off_Row_1">
                                   <td align="center"><a href="#pr1" onClick="removeOffRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtOffHireReason_1" id="txtOffHireReason_1" rows="2" placeholder="Off Hire Reason..."></textarea></td>
                                   <td><input type="text" name="txtFromOff_1" id="txtFromOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/></td>
                                   <td><input type="text" name="txtToOff_1" id="txtToOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/></td>
								   <td><input type="text" name="txtOffPer_1" id="txtOffPer_1" class="form-control"  placeholder="%" autocomplete="off" value="" onKeyUp="getCVETotalAmt();"/></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="left" colspan="5"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewOffHire();">Add</button></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
                    
                   
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Nett Period(Days)
                            <address>
                               <input type="text" name="txtNettPeriod" id="txtNettPeriod" class="form-control" readonly  placeholder="Nett Period(Days)" autocomplete="off" value="0"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                     <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                           
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Nett Hire
                            <address>
                               <input type="text" name="txtNettHire" id="txtNettHire" class="form-control" readonly  placeholder="Nett Period(Days)" autocomplete="off" value="0"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers on Delivery</h3><input type="hidden" name="txtbid" id="txtbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtBunkerID_<?php echo $i;?>" name="txtBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtConspQtyMT_<?php echo $i;?>" id="txtConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtPrice_<?php echo $i;?>" id="txtPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtConspAmt_<?php echo $i;?>" id="txtConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtConspPer_<?php echo $i;?>" id="txtConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getBunkerGradeAmt();"/><input type="Hidden" name="txtConspPerAmt_<?php echo $i;?>" id="txtConspPerAmt_<?php echo $i;?>" value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtCVE" id="txtCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtCVEAmt" id="txtCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtCVEPer" id="txtCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtCVEPerAmt" id="txtCVEPerAmt" value="0.00"/></td>
                                </tr>
                                <tr>
                                    <td></td>
								    <td></td>
									<td></td>
                                    <td>ILOHC</td>
									<td><input type="text" name="txtILOHC" id="txtILOHC" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
									
                                </tr>
                                <tr>
                                    <td></td>
								    <td></td>
									<td></td>
                                    <td>Ballast Bonus</td>
                                    <td><input type="text" name="txtBallastBonus" id="txtBallastBonus" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
									
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
                  
                  
                  
                  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers Consp(Estimated) till redelivery</h3><input type="hidden" name="txtRedelbid" id="txtRedelbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtRedelBunkerID_<?php echo $i;?>" name="txtRedelBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtRedelConspQtyMT_<?php echo $i;?>" id="txtRedelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtRedelPrice_<?php echo $i;?>" id="txtRedelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtRedelConspAmt_<?php echo $i;?>" id="txtRedelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtRedelConspPer_<?php echo $i;?>" id="txtRedelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getRedelBunkerGradeAmt();"/><input type="Hidden" name="txtRedelConspPerAmt_<?php echo $i;?>" id="txtRedelConspPerAmt_<?php echo $i;?>" value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtRedelCVE" id="txtRedelCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtRedelCVEAmt" id="txtRedelCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtRedelCVEPer" id="txtRedelCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtRedelCVEPerAmt" id="txtRedelCVEPerAmt" value="0.00"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
                    
                    
                  <div class="box">
								<div class="box-header">
									<h3 class="box-title">Less</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<tbody>
											<tr>
												<td width="11%"></td>
												<td width="26%">Add. Commission</td>
												<td width="20%"><input type="text" name="txtCommission" id="txtCommission" class="form-control" onKeyUp="getBrokageCommission();" autocomplete="off" value="0.00" /></td>
												<td width="20%"><input type="text" name="txtTotalCommission" id="txtTotalCommission" readonly class="form-control" autocomplete="off" value="0.00" /></td>
												<td width="20%"></td>
											</tr>
										</tbody>
										<tbody id="tbodyBrokerage">
										  <tr id="tbrRow_1">
                                                <td><a href="#tb1'" onClick="removeBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getBrokageCommission();"  /></td>
												<td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="form-control" readonly value="0.00" /></td>
												<td><select  name="selBrVendor_1" class="select form-control" id="selBrVendor_1"></select>
													<script>$("#selBrVendor_1").html($("#selVendor").html());$("#selBrVendor_1").val('');</script>
												</td>
											</tr>
									
                                            </tbody>
                                            <tbody>
											<tr>
                                                <td><button type="button" class="btn btn-primary btn-flat" onClick="addBrokerageRow()">Add</button><input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="1"/></td>
												<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												<td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="form-control" autocomplete="off" value="0.00" readonly/></td>
												<td><input type="text" name="txtBrComm" id="txtBrComm" class="form-control" readonly value="0.00" /></td>
												<td>
												</td>
											</tr>
											</tbody>
                                            
                                            
									</table>
								</div>
							</div>
                            
                            
                   <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers on Redelivery</h3><input type="hidden" name="txtOnRedelbid" id="txtOnRedelbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOnRedelBunkerID_<?php echo $i;?>" name="txtOnRedelBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOnRedelConspQtyMT_<?php echo $i;?>" id="txtOnRedelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOnRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOnRedelPrice_<?php echo $i;?>" id="txtOnRedelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOnRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOnRedelConspAmt_<?php echo $i;?>" id="txtOnRedelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOnRedelConspPer_<?php echo $i;?>" id="txtOnRedelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOnRedelBunkerGradeAmt();"/><input type="Hidden" name="txtOnRedelConspPerAmt_<?php echo $i;?>" id="txtOnRedelConspPerAmt_<?php echo $i;?>" value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOnRedelCVE" id="txtOnRedelCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOnRedelCVEAmt" id="txtOnRedelCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOnRedelCVEPer" id="txtOnRedelCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOnRedelCVEPerAmt" id="txtOnRedelCVEPerAmt" value="0.00"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div> 
				  
				  <div class="box">
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tbody>
								<tr>
									<td width="26%">Balance to Owners</td>
									<td width="20%"><input type="text" name="txtTotalBalanceOwner" id="txtTotalBalanceOwner" readonly class="form-control" autocomplete="off" value="0.00" /></td>
									<td width="20%"><select  name="selOwnerVendor" class="select form-control" id="selOwnerVendor"></select>
											<script>$("#selOwnerVendor").html($("#selVendor").html());$("#selOwnerVendor").val('');</script>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				  </div>   
						
						
				  <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              TC OUT(Relet) Details
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
                            <h3 class="page-header">
                             Due to Charterers
                            </h3> 
							    
                       <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            TC Out - Charterers
                            <address>
                               <select  name="selTCOutOwner" class="form-control" id="selTCOutOwner" >
                               </select>
							   <script>$("#selTCOutOwner").html($("#selVendor").html());$("#selTCOutOwner").val('');</script>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           CP Date
                            <address>
                               <input type="text" name="txtCPOutDate" id="txtCPOutDate" class="form-control" readonly  placeholder="dd-mm-YYYY" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($obj->insertTCContractDataBasedOnID($tc_conid,"CP_DATE")));?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Daily Hire(USD/Day)
                            <address>
                               <input type="text" name="txtOutDailyHire" id="txtOutDailyHire" class="form-control" placeholder="Daily Hire(USD/Day)" autocomplete="off" value="<?php echo $obj->insertTCContractDataBasedOnID($tc_conid,"CP_HIRE");?>" onKeyUp="getCVETotalAmt();"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                         
                     
					 <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Date of Del
                            <address>
                               <input type="text" name="txtOutDateOfDel" id="txtOutDateOfDel" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                             Port of Del
                            <address>
                               <select  name="selOutDelPort" class="form-control" id="selOutDelPort" >
                               </select>
                               <script>$("#selOutDelPort").html($("#selPort").html());</script>
                            </address>
                        </div>
					</div>
					
					<div class="row invoice-info">
					  <div class="col-sm-4 invoice-col">
                             Date of Re-Del
                            <address>
                                <input type="text" name="txtOutDateOfReDel" id="txtOutDateOfReDel" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             Port of Re-Del
                            <address>
                               <select  name="selOutReDelPort" class="form-control" id="selOutReDelPort" >
                               </select>
                               <script>$("#selOutReDelPort").html($("#selPort").html());</script>
                            </address>
                        </div>
					</div>
					
					<div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Off Hire Reason</th>
                                    <th>Off Hire From (Days)</th>
                                    <th>Off Hire To (Days)<input type="hidden" name="txtOutOFFID" id="txtOutOFFID" value="1"/></th>
									<th>Off Hire (%)</th>
                                </tr>
                            </thead>
                            <tbody id="tblOutOFF">
                                <tr id="Outoff_Row_1">
                                   <td align="center"><a href="#pr1" onClick="removeOutOffRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtOutOffHireReason_1" id="txtOutOffHireReason_1" rows="2" placeholder="Off Hire Reason..."></textarea></td>
                                   <td><input type="text" name="txtOutFromOff_1" id="txtOutFromOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/></td>
                                   <td><input type="text" name="txtOutToOff_1" id="txtOutToOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/></td>
								   <td><input type="text" name="txtOutOffPer_1" id="txtOutOffPer_1" class="form-control"  placeholder="%" autocomplete="off" value="" onKeyUp="getCVETotalAmt();"/></td>
								   
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="left" colspan="5"><button type="button" class="btn btn-primary btn-flat" onClick="AddOutNewOffHire();">Add</button></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
				  
				  <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Nett Period(Days)
                            <address>
                               <input type="text" name="txtOutNettPeriod" id="txtOutNettPeriod" class="form-control" readonly  placeholder="Nett Period(Days)" autocomplete="off" value="0"/>
                            </address>
                        </div><!-- /.col -->
					</div>
				  <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Nett Hire
                            <address>
                               <input type="text" name="txtOutNettHire" id="txtOutNettHire" class="form-control" readonly  placeholder="Nett Period(Days)" autocomplete="off" value="0"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                     <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                           
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              Nett Hire
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    
					
					<?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers on Delivery</h3><input type="hidden" name="txtOutbid" id="txtOutbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutBunkerID_<?php echo $i;?>" name="txtOutBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutConspQtyMT_<?php echo $i;?>" id="txtOutConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutPrice_<?php echo $i;?>" id="txtOutPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutConspAmt_<?php echo $i;?>" id="txtOutConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutConspPer_<?php echo $i;?>" id="txtOutConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutBunkerGradeAmt();"/><input type="Hidden" name="txtOutConspPerAmt_<?php echo $i;?>" id="txtOutConspPerAmt_<?php echo $i;?>" value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutCVE" id="txtOutCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutCVEAmt" id="txtOutCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutCVEPer" id="txtOutCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutCVEPerAmt" id="txtOutCVEPerAmt" value="0.00"/></td>
                                </tr>
                                <tr>
                                    <td></td>
								    <td></td>
									<td></td>
                                    <td>ILOHC</td>
                                    <td><input type="text" name="txtOutILOHC" id="txtOutILOHC" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00"onKeyUp="getCVETotalAmt();"/></td>
									
                                </tr>
                                <tr>
                                    <td></td>
								    <td></td>
									<td></td>
                                    <td>Ballast Bonus</td>
                                    <td><input type="text" name="txtOutBallastBonus" id="txtOutBallastBonus" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
									
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
				  
				      
				  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers Consp(Estimated) till redelivery</h3><input type="hidden" name="txtOutRedelbid" id="txtOutRedelbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutRedelBunkerID_<?php echo $i;?>" name="txtOutRedelBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutRedelConspQtyMT_<?php echo $i;?>" id="txtOutRedelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutRedelPrice_<?php echo $i;?>" id="txtOutRedelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutRedelConspAmt_<?php echo $i;?>" id="txtOutRedelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutRedelConspPer_<?php echo $i;?>" id="txtOutRedelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutRedelBunkerGradeAmt();"/><input type="hidden" name="txtOutRedelConspPerAmt_<?php echo $i;?>" id="txtOutRedelConspPerAmt_<?php echo $i;?>"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutRedelCVE" id="txtOutRedelCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutRedelCVEAmt" id="txtOutRedelCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutRedelCVEPer" id="txtOutRedelCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutRedelCVEPerAmt" id="txtOutRedelCVEPerAmt" value="0.00"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
				  
				  
				  <div class="box">
					<div class="box-header">
						<h3 class="box-title">Less</h3>
					</div>
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tbody>
								<tr>
									<td width="11%"></td>
									<td width="26%">Add. Commission (%)</td>
									<td width="20%"><input type="text" name="txtOutCommission" id="txtOutCommission" class="form-control" onKeyUp="getOutBrokageCommission();" autocomplete="off" value="0.00" /></td>
									<td width="20%"><input type="text" name="txtOutTotalCommission" readonly id="txtOutTotalCommission" class="form-control" autocomplete="off" value="0.00" /></td>
									<td width="20%"></td>
								</tr>
							</tbody>
							</table>
					</div>
				</div>
				  
				  
				  
				  
				  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers on Redelivery</h3><input type="hidden" name="txtOutOnRedelbid" id="txtOutOnRedelbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutOnRedelBunkerID_<?php echo $i;?>" name="txtOutOnRedelBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutOnRedelConspQtyMT_<?php echo $i;?>" id="txtOutOnRedelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutOnRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutOnRedelPrice_<?php echo $i;?>" id="txtOutOnRedelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutOnRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutOnRedelConspAmt_<?php echo $i;?>" id="txtOutOnRedelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutOnRedelConspPer_<?php echo $i;?>" id="txtOutOnRedelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutOnRedelBunkerGradeAmt();"/><input type="Hidden" name="txtOutOnRedelConspPerAmt_<?php echo $i;?>" id="txtOutOnRedelConspPerAmt_<?php echo $i;?>" value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutOnRedelCVE" id="txtOutOnRedelCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutOnRedelCVEAmt" id="txtOutOnRedelCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutOnRedelCVEPer" id="txtOutOnRedelCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutOnRedelCVEPerAmt" id="txtOutOnRedelCVEPerAmt" value="0.00"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div> 
				  
				  
				  
				  <div class="box">
					<div class="box-header">
						<h3 class="box-title">Other expenses</h3>
					</div>
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tbody id="tbodyOutBrokerage">
							  <tr id="OuttbrRow_1">
									<td width="15"><a href="#tb1'" onClick="removeOutBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
									<td width="22">Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
									
									<td width="20%"><input type="text" name="txtOutBrCommPercent_1" id="txtOutBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getOutBrokageCommission();"  /></td>
									<td width="20%"><input type="text" name="txtOutBrComm_1" id="txtOutBrComm_1" class="form-control" readonly value="0.00" /></td>
									<td width="20%"><select  name="selOutBrVendor_1" class="select form-control" id="selOutBrVendor_1"></select>
										<script>$("#selOutBrVendor_1").html($("#selVendor").html());$("#selOutBrVendor_1").val('');</script>
								</td>
							  </tr>
						
								</tbody>
								<tbody>
								<tr>
									<td><button type="button" class="btn btn-primary btn-flat" onClick="addOutBrokerageRow()">Add</button><input type="hidden" name="txtOutBRokageCount" id="txtOutBRokageCount" value="1"/></td>
									<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
									<td><input type="text" name="txtOutBrCommPercent" id="txtOutBrCommPercent" class="form-control" autocomplete="off" value="0.00" readonly/></td>
									<td><input type="text" name="txtOutBrComm" id="txtOutBrComm" class="form-control" readonly value="0.00" /></td>
									<td>
									</td>
								</tr>
								</tbody>
								
								
						</table>
					</div>
				</div>
				  
				  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Waiting Days</h3><input type="hidden" name="txtOutWaitbid" id="txtOutWaitbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
						    <thead>
								<tr>
									<td width="20%">Days</td>
									<td width="20%"></td>
									<td width="20%"></td>
									<td width="20%"></td>
									<td width="20%"><input type="text" name="txtWaitingDays" id="txtWaitingDays" class="form-control" autocomplete="off" value="0.00"  onKeyUp="getOutWaitBunkerGradeAmt();"/></td>
								</tr>
							</thead>
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutWaitBunkerID_<?php echo $i;?>" name="txtOutWaitBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutWaitConspQtyMT_<?php echo $i;?>" id="txtOutWaitConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutWaitBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutWaitPrice_<?php echo $i;?>" id="txtOutWaitPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutWaitBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutWaitConspAmt_<?php echo $i;?>" id="txtOutWaitConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutWaitConspPer_<?php echo $i;?>" id="txtOutWaitConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutWaitBunkerGradeAmt();"/><input type="Hidden" name="txtOutWaitConspPerAmt_<?php echo $i;?>" id="txtOutWaitConspPerAmt_<?php echo $i;?>" value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutWaitCVE" id="txtOutWaitCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutWaitCVEAmt" id="txtOutWaitCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutWaitCVEPer" id="txtOutWaitCVEPer" class="form-control"  placeholder="0.00" autocomplete="off"  value="0.00" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutWaitCVEPerAmt" id="txtOutWaitCVEPerAmt" value="0.00"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div> 
				  
				  
				  
				  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers Consp(Estimated) till redelivery</h3><input type="hidden" name="txtOutExpbid" id="txtOutExpbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutExpBunkerID_<?php echo $i;?>" name="txtOutExpBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutExpConspQtyMT_<?php echo $i;?>" id="txtOutExpConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutExpBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutExpPrice_<?php echo $i;?>" id="txtOutExpPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutExpBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutExpConspAmt_<?php echo $i;?>" id="txtOutExpConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutExpConspPer_<?php echo $i;?>" id="txtOutExpConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutExpBunkerGradeAmt();"/><input type="Hidden" name="txtOutExpConspPerAmt_<?php echo $i;?>" id="txtOutExpConspPerAmt_<?php echo $i;?>" value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutExpCVE" id="txtOutExpCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutExpCVEAmt" id="txtOutExpCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutExpCVEPer" id="txtOutExpCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutExpCVEPerAmt" id="txtOutExpCVEPerAmt" value="0.00"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
				  
				  
				  
				  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Ballast Days</h3><input type="hidden" name="txtOutBalbid" id="txtOutBalbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
						    <thead>
								<tr>
									<td width="20%">Days</td>
									<td width="20%"></td>
									<td width="20%"></td>
									<td width="20%"></td>
									<td width="20%"><input type="text" name="txtBalWaitingDays" id="txtBalWaitingDays" class="form-control" autocomplete="off" value="0.00" onKeyUp="getOutBalBunkerGradeAmt();" /></td>
									
								</tr>
							</thead>
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutBalBunkerID_<?php echo $i;?>" name="txtOutBalBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutBalConspQtyMT_<?php echo $i;?>" id="txtOutBalConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutBalBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutBalPrice_<?php echo $i;?>" id="txtOutBalPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutBalBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutBalConspAmt_<?php echo $i;?>" id="txtOutBalConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutBalConspPer_<?php echo $i;?>" id="txtOutBalConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOutBalBunkerGradeAmt();"/><input type="hidden" name="txtOutBalConspPerAmt_<?php echo $i;?>" id="txtOutBalConspPerAmt_<?php echo $i;?>" value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutBalCVE" id="txtOutBalCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutBalCVEAmt" id="txtOutBalCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
									<td><input type="text" name="txtOutBalCVEPer" id="txtOutBalCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/><input type="hidden" name="txtOutBalCVEPerAmt" id="txtOutBalCVEPerAmt" value="0.00"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div> 
				  
				  
				  
				  <div class="box">
                      <div class="box-body no-padding">
                      <h4>Hire Loss</h4>
                        <table class="table table-striped">
						    <tbody>
								<tr>
									<td width="30%">Days</td>
									<td width="20%"></td>
									<td width="25%"><input type="text" name="txtHireLossDays" id="txtHireLossDays" class="form-control" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
									<td width="25%"><input type="text" name="txtHireLossAmt" id="txtHireLossAmt" readonly class="form-control" autocomplete="off" value="0.00" /></td>
								</tr>
							</tbody>
                          </table>
						<h4>Hire Loss - AD Comm</h4>
                        <table class="table table-striped">
						    <tbody>
								<tr>
									<td width="30%">%</td>
									<td width="20%"></td>
									<td width="25%"><input type="text" name="txtAddComm" id="txtAddComm" class="form-control" autocomplete="off" value="0.00" onKeyUp="getCVETotalAmt();"/></td>
									<td width="25%"><input type="text" name="txtAddCommAmt" id="txtAddCommAmt" readonly class="form-control" autocomplete="off" value="0.00" /></td>
								</tr>
							</tbody>
                          </table>
						  <h4>Profit/Loss Details</h4>
                        <table class="table table-striped">
						    <tbody>
								<tr>
									<td width="30%">Total Profit/Loss</td>
									<td width="20%"></td>
									<td width="25%"></td>
									<td width="25%"><input type="text" name="txtProfitLoss" id="txtProfitLoss" readonly class="form-control" autocomplete="off" value="0.00" /></td>
								</tr>
							</tbody>
                          </table>
                    </div>
                  </div> 
				  
				  
				  <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Vessel Re-let Sheet Status
                                    <address>
										<select  name="selVType" class="select form-control" id="selVType" >
											<?php 
												$_REQUEST['selVType'] = $obj->getFun4();
												$obj->getVoyageType();
											?>
										</select>
                                    </address>
                                </div><!-- /.col -->
							</div>
					<?php 
					 	
					 	//if($rigts == 1)
						//{
					 ?>
					<div class="box-footer" align="right">
						<button type="submit" class="btn btn-primary btn-flat" >Submit</button>
						<input type="hidden" name="action" value="submit1" /><input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>
					<?php //} ?>
					
					
					
					
					
					
			  <?php }
					else
					{ ?>
					
					
					
					
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            VOYAGE FINANCIALS
                            <select  name="selPort" class="form-control" style="display:none;" id="selPort" >
                                <?php 
                                $obj->getPortList();
                                ?>
                               </select>
                               <select  name="selVendor" class="select form-control" style="display:none;" id="selVendor" >
									<?php $obj->getVendorListNewUpdate("");	?>
								</select> 
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Fixture Type
                            <address>
							   <input type="text" name="txtFixtureType" id="txtFixtureType" class="form-control" value="Vessel Re-let" readonly  placeholder="Vessel Name" autocomplete="off"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Main Particulars:
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                            Vessel Name
                            <address>
                               <input type="text" name="txtVesselName" id="txtVesselName" class="form-control" readonly  placeholder="Vessel Name" autocomplete="off" value="<?php echo $obj->getVesselIMOData($obj->insertTCContractDataBasedOnID($tc_conid,"VESSEL_IMO_ID"),"VESSEL_NAME");?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                           Nom ID
                            <address>
                               <input type="text" name="txtNomID" id="txtNomID" class="form-control" readonly  placeholder="Nom ID" autocomplete="off" value="<?php echo $obj->getFun2();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                           Vessel Type
                            <address>
                               <input type="text" name="txtVesselType" id="txtVesselType" class="form-control" readonly  placeholder="Vessel Type" autocomplete="off" value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->insertTCContractDataBasedOnID($tc_conid,"VESSEL_IMO_ID"),"VESSEL_TYPE"),"NAME");?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                           Date
                            <address>
                               <input type="text" name="txtSheetDate" id="txtSheetDate" class="form-control"  placeholder="dd-mm-YYYY" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($obj->getFun3()));?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                   
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            TC IN Details:
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
                            <h3 class="page-header">
                            Due to Owners
                            </h3>                            
                      
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            TC IN Owners
                            <address>
                               <select  name="selTCINOwner" class="form-control" id="selTCINOwner" >
					           </select>
							   <script>$("#selTCINOwner").html($("#selVendor").html());$("#selTCINOwner").val('<?php echo $obj->getFun4();?>');</script>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           CP Date
                            <address>
                               <input type="text" name="txtCPDate" id="txtCPDate" class="form-control" readonly  placeholder="dd-mm-YYYY" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($obj->insertTCContractDataBasedOnID($tc_conid,"CP_DATE")));?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Daily Hire(USD/Day)
                            <address>
                               <input type="text" name="txtDailyHire" id="txtDailyHire" class="form-control" placeholder="Daily Hire(USD/Day)" autocomplete="off" value="<?php echo $obj->getFun5();?>" onKeyUp="getCVETotalAmt();"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Date of Del
                            <address>
                               <input type="text" name="txtDateOfDel" id="txtDateOfDel" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($obj->getFun6()));?>"/>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                             Port of Del
                            <address>
                               <select  name="selDelPort" class="form-control" id="selDelPort" >
                               </select>
                               <script>$("#selDelPort").html($("#selPort").html());$("#selDelPort").val(<?php echo $obj->getFun8();?>);</script>
                            </address>
                        </div>
					</div>
					
					<div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                             Date of Re-Del
                            <address>
                                <input type="text" name="txtDateOfReDel" id="txtDateOfReDel" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($obj->getFun7()));?>"/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             Port of Re-Del
                            <address>
                               <select  name="selReDelPort" class="form-control" id="selReDelPort" >
                               </select>
                               <script>$("#selReDelPort").html($("#selPort").html());$("#selReDelPort").val(<?php echo $obj->getFun9();?>);</script>
                            </address>
                        </div>
					</div>
			
                    
                    <div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Off Hire Reason</th>
                                    <th>Off Hire From (Days)</th>
                                    <th>Off Hire To (Days)
									<?php $sql = "select * from tc_relet_sheet_slave1 where TC_SHEETID='".$obj->getFun1()."' and RECORD_IDENTITY='IN_OFF' ";
									      $result = mysql_query($sql);
										  $num = mysql_num_rows($result);?>
                                    <input type="hidden" name="txtOFFID" id="txtOFFID" value="<?php echo $num;?>"/></th>
									<th>Off Hire (%)</th>
                                </tr>
                            </thead>
                            <tbody id="tblOFF">
							<?php if($num ==0)
							        {?>
                                <tr id="off_Row_1">
                                   <td align="center"><a href="#pr1" onClick="removeOffRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtOffHireReason_1" id="txtOffHireReason_1" rows="2" placeholder="Off Hire Reason..."></textarea></td>
                                   <td><input type="text" name="txtFromOff_1" id="txtFromOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/></td>
                                   <td><input type="text" name="txtToOff_1" id="txtToOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/></td>
								   <td><input type="text" name="txtOffPer_1" id="txtOffPer_1" class="form-control"  placeholder="%" autocomplete="off" value="" onKeyUp="getCVETotalAmt();"/></td>
                                </tr>
						   <?php }else
							   	 {$i=0;
								while($rows = mysql_fetch_assoc($result)) 
								{$i = $i + 1;
								 ?>
								 <tr id="off_Row_<?php echo $i;?>">
                                   <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeOffRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtOffHireReason_<?php echo $i;?>" id="txtOffHireReason_<?php echo $i;?>" rows="2" placeholder="Off Hire Reason..."><?php echo $rows['OFFHIRE_REASON'];?></textarea></td>
                                   <td><input type="text" name="txtFromOff_<?php echo $i;?>" id="txtFromOff_<?php echo $i;?>" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($rows['FROM_OFF_DATE']));?>"/></td>
                                   <td><input type="text" name="txtToOff_<?php echo $i;?>" id="txtToOff_<?php echo $i;?>" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($rows['TO_OFF_DATE']));?>"/></td>
								   <td><input type="text" name="txtOffPer_<?php echo $i;?>" id="txtOffPer_<?php echo $i;?>" class="form-control"  placeholder="%" autocomplete="off" value="<?php echo $rows['OFFHIRE_PERCENT'];?>" onKeyUp="getCVETotalAmt();"/></td>
                                </tr>
							<?php }}?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="left" colspan="5"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewOffHire();">Add</button></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
                    
                   
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Nett Period(Days)
                            <address>
                               <input type="text" name="txtNettPeriod" id="txtNettPeriod" class="form-control" readonly  placeholder="Nett Period(Days)" autocomplete="off" value="<?php echo $obj->getFun10();?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                     <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                           
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Nett Hire
                            <address>
                               <input type="text" name="txtNettHire" id="txtNettHire" class="form-control" readonly  placeholder="Nett Period(Days)" autocomplete="off" value="<?php echo $obj->getFun11();?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers on Delivery</h3><input type="hidden" name="txtbid" id="txtbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
								 $sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='IN_DEL' and TC_SHEETID='".$obj->getFun1()."'";
								 $res2 = mysql_query($sql2);
								 $rows2 = mysql_fetch_assoc($res2);
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtBunkerID_<?php echo $i;?>" name="txtBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtConspQtyMT_<?php echo $i;?>" id="txtConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['QUANTITY'];?>" onKeyUp="getBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtPrice_<?php echo $i;?>" id="txtPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['RATE'];?>" onKeyUp="getBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtConspAmt_<?php echo $i;?>" id="txtConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows2['AMOUNT'];?>"/></td>
									<td><input type="text" name="txtConspPer_<?php echo $i;?>" id="txtConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['PERCENT'];?>" onKeyUp="getBunkerGradeAmt();"/><input type="Hidden" name="txtConspPerAmt_<?php echo $i;?>" id="txtConspPerAmt_<?php echo $i;?>" value="<?php echo $rows2['PERCENT_AMOUNT'];?>"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtCVE" id="txtCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun12();?>" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtCVEAmt" id="txtCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFun13();?>"/></td>
									<td><input type="text" name="txtCVEPer" id="txtCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun60();?>" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtCVEPerAmt" id="txtCVEPerAmt" value="<?php echo $obj->getFun61();?>"/></td>
                                </tr>
                                <tr>
                                    <td></td>
								    <td></td>
									<td></td>
                                    <td>ILOHC</td>
                                    <td><input type="text" name="txtILOHC" id="txtILOHC" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun14();?>" onKeyUp="getCVETotalAmt();"/></td>
                                </tr>
                                <tr>
                                    <td></td>
								    <td></td>
									<td></td>
                                    <td>Ballast Bonus</td>
                                    <td><input type="text" name="txtBallastBonus" id="txtBallastBonus" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun15();?>" onKeyUp="getCVETotalAmt();"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
                  
                  
                  
                  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers Consp(Estimated) till redelivery</h3><input type="hidden" name="txtRedelbid" id="txtRedelbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
								 $sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='IN_REDEL' and TC_SHEETID='".$obj->getFun1()."'";
								 $res2 = mysql_query($sql2);
								 $rows2 = mysql_fetch_assoc($res2);
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtRedelBunkerID_<?php echo $i;?>" name="txtRedelBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtRedelConspQtyMT_<?php echo $i;?>" id="txtRedelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['QUANTITY'];?>" onKeyUp="getRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtRedelPrice_<?php echo $i;?>" id="txtRedelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['RATE'];?>" onKeyUp="getRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtRedelConspAmt_<?php echo $i;?>" id="txtRedelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows2['AMOUNT'];?>"/></td>
									<td><input type="text" name="txtRedelConspPer_<?php echo $i;?>" id="txtRedelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['PERCENT'];?>" onKeyUp="getRedelBunkerGradeAmt();"/><input type="Hidden" name="txtRedelConspPerAmt_<?php echo $i;?>" id="txtRedelConspPerAmt_<?php echo $i;?>" value="<?php echo $rows2['PERCENT_AMOUNT'];?>"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtRedelCVE" id="txtRedelCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun16();?>" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtRedelCVEAmt" id="txtRedelCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFun17();?>"/></td>
									<td><input type="text" name="txtRedelCVEPer" id="txtRedelCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun62();?>" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtRedelCVEPerAmt" id="txtRedelCVEPerAmt" value="<?php echo $obj->getFun63();?>"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
                    
                    
                  <div class="box">
								<div class="box-header">
									<h3 class="box-title">Less</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<tbody>
											<tr>
												<td width="11%"></td>
												<td width="26%">Add. Commission</td>
												<td width="20%"><input type="text" name="txtCommission" id="txtCommission" class="form-control" onKeyUp="getBrokageCommission();" autocomplete="off" value="<?php echo $obj->getFun18();?>" /></td>
												<td width="20%"><input type="text" name="txtTotalCommission" id="txtTotalCommission" readonly class="form-control" autocomplete="off" value="<?php echo $obj->getFun19();?>" /></td>
												<td width="20%"></td>
											</tr>
										</tbody>
										<tbody id="tbodyBrokerage">
										<?php $sql = "select * from tc_relet_sheet_slave3 where TC_SHEETID='".$obj->getFun1()."' and RECORD_IDENTIFY='IN_BROKERAGE' ";
									      $result = mysql_query($sql);
										  $num = mysql_num_rows($result);
										  if($num==0)
										  {?>
										  <tr id="tbrRow_1">
                                                <td><a href="#tb1'" onClick="removeBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getBrokageCommission();"  /></td>
												<td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="form-control" readonly value="0.00" /></td>
												<td><select  name="selBrVendor_1" class="select form-control" id="selBrVendor_1"></select>
													<script>$("#selBrVendor_1").html($("#selVendor").html());$("#selBrVendor_1").val('');</script>
												</td>
											</tr>
									      <?php }else{
										  $i = 0;
										  while($rows = mysql_fetch_assoc($result))
										  {$i = $i + 1;?>
										  <tr id="tbrRow_<?php echo $i;?>">
                                                <td><a href="#tb<?php echo $i;?>'" onClick="removeBrokerage(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_<?php echo $i;?>" id="txtBrCommPercent_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows['BROKERAGE'];?>" onKeyUp="getBrokageCommission();"  /></td>
												<td><input type="text" name="txtBrComm_<?php echo $i;?>" id="txtBrComm_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows['BROKERAGE_AMOUNT'];?>" /></td>
												<td><select  name="selBrVendor_<?php echo $i;?>" class="select form-control" id="selBrVendor_<?php echo $i;?>"></select>
													<script>$("#selBrVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selBrVendor_<?php echo $i;?>").val('<?php echo $rows['VENDORID'];?>');</script>
												</td>
											</tr>
										  <?php }}?>
                                            </tbody>
                                            <tbody>
											<tr>
                                                <td><button type="button" class="btn btn-primary btn-flat" onClick="addBrokerageRow()">Add</button><input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="<?php echo $num;?>"/></td>
												<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												<td><input type="text" name="txtBrCommPercent" id="txtBrCommPercent" class="form-control" autocomplete="off" value="<?php echo $obj->getFun20();?>" readonly/></td>
												<td><input type="text" name="txtBrComm" id="txtBrComm" class="form-control" readonly value="<?php echo $obj->getFun21();?>" /></td>
												<td>
												</td>
											</tr>
											</tbody>
                                            
                                            
									</table>
								</div>
							</div>
                            
                            
                   <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers on Redelivery</h3><input type="hidden" name="txtOnRedelbid" id="txtOnRedelbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
								 $sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='IN_ONREDEL' and TC_SHEETID='".$obj->getFun1()."'";
								 $res2 = mysql_query($sql2);
								 $rows2 = mysql_fetch_assoc($res2);
								 
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOnRedelBunkerID_<?php echo $i;?>" name="txtOnRedelBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOnRedelConspQtyMT_<?php echo $i;?>" id="txtOnRedelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['QUANTITY'];?>" onKeyUp="getOnRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOnRedelPrice_<?php echo $i;?>" id="txtOnRedelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['RATE'];?>" onKeyUp="getOnRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOnRedelConspAmt_<?php echo $i;?>" id="txtOnRedelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows2['AMOUNT'];?>"/></td>
									<td><input type="text" name="txtOnRedelConspPer_<?php echo $i;?>" id="txtOnRedelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['PERCENT'];?>" onKeyUp="getOnRedelBunkerGradeAmt();"/><input type="Hidden" name="txtOnRedelConspPerAmt_<?php echo $i;?>" id="txtOnRedelConspPerAmt_<?php echo $i;?>" value="<?php echo $rows2['PERCENT_AMOUNT'];?>"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOnRedelCVE" id="txtOnRedelCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun22();?>" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOnRedelCVEAmt" id="txtOnRedelCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFun23();?>"/></td>
									<td><input type="text" name="txtOnRedelCVEPer" id="txtOnRedelCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun64();?>" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOnRedelCVEPerAmt" id="txtOnRedelCVEPerAmt" value="<?php echo $obj->getFun65();?>"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div> 
				  
				  <div class="box">
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tbody>
								<tr>
									<td width="26%">Balance to Owners</td>
									<td width="20%"><input type="text" name="txtTotalBalanceOwner" id="txtTotalBalanceOwner" readonly class="form-control" autocomplete="off" value="<?php echo $obj->getFun24();?>" /></td>
									<td width="20%"><select  name="selOwnerVendor" class="select form-control" id="selOwnerVendor"></select>
											<script>$("#selOwnerVendor").html($("#selVendor").html());$("#selOwnerVendor").val('<?php echo $obj->getFun25();?>');</script>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				  </div>   
						
						
				  <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              TC OUT(Relet) Details
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
                            <h3 class="page-header">
                             Due to Charterers
                            </h3> 
							    
                       <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            TC Out - Charterers
                            <address>
                               <select  name="selTCOutOwner" class="form-control" id="selTCOutOwner" >
                               </select>
							   <script>$("#selTCOutOwner").html($("#selVendor").html());$("#selTCOutOwner").val('<?php echo $obj->getFun26();?>');</script>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           CP Date
                            <address>
                               <input type="text" name="txtCPOutDate" id="txtCPOutDate" class="form-control" readonly  placeholder="dd-mm-YYYY" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($obj->insertTCContractDataBasedOnID($tc_conid,"CP_DATE")));?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Daily Hire(USD/Day)
                            <address>
                               <input type="text" name="txtOutDailyHire" id="txtOutDailyHire" class="form-control" placeholder="Daily Hire(USD/Day)" autocomplete="off" value="<?php echo $obj->getFun27();?>" onKeyUp="getCVETotalAmt();"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                         
                     
					 <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Date of Del
                            <address>
                               <input type="text" name="txtOutDateOfDel" id="txtOutDateOfDel" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($obj->getFun28()));?>"/>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                             Port of Del
                            <address>
                               <select  name="selOutDelPort" class="form-control" id="selOutDelPort" >
                               </select>
                               <script>$("#selOutDelPort").html($("#selPort").html());$("#selOutDelPort").val(<?php echo $obj->getFun30();?>);</script>
                            </address>
                        </div>
					</div>
					
					<div class="row invoice-info">
					  <div class="col-sm-4 invoice-col">
                             Date of Re-Del
                            <address>
                                <input type="text" name="txtOutDateOfReDel" id="txtOutDateOfReDel" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($obj->getFun29()));?>"/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             Port of Re-Del
                            <address>
                               <select  name="selOutReDelPort" class="form-control" id="selOutReDelPort" >
                               </select>
                               <script>$("#selOutReDelPort").html($("#selPort").html());$("#selOutReDelPort").val(<?php echo $obj->getFun31();?>);</script>
                            </address>
                        </div>
					</div>
					
					<div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Off Hire Reason</th>
                                    <th>Off Hire From (Days)</th>
                                    <th>Off Hire To (Days)
									<?php $sql = "select * from tc_relet_sheet_slave1 where TC_SHEETID='".$obj->getFun1()."' and RECORD_IDENTITY='OUT_OFF' ";
									      $result = mysql_query($sql);
										  $num = mysql_num_rows($result);?>
                                    <input type="hidden" name="txtOutOFFID" id="txtOutOFFID" value="<?php echo $num;?>"/></th>
									<th>Off Hire (%)</th>
                                </tr>
                            </thead>
                            <tbody id="tblOutOFF">
							<?php if($num==0)
							{?>
                                <tr id="Outoff_Row_1">
                                   <td align="center"><a href="#pr1" onClick="removeOutOffRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtOutOffHireReason_1" id="txtOutOffHireReason_1" rows="2" placeholder="Off Hire Reason..."></textarea></td>
                                   <td><input type="text" name="txtOutFromOff_1" id="txtOutFromOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/></td>
                                   <td><input type="text" name="txtOutToOff_1" id="txtOutToOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""/></td>
								   <td><input type="text" name="txtOutOffPer_1" id="txtOutOffPer_1" class="form-control"  placeholder="%" autocomplete="off" value="" onKeyUp="getCVETotalAmt();"/></td>
                                </tr>
								<?php }else
								{$i = 0;
								while($rows = mysql_fetch_assoc($result))
								{$i = $i + 1;
								?>
								<tr id="Outoff_Row_<?php echo $i;?>">
                                   <td align="center"><a href="#pr<?php echo $i;?>" onClick="removeOutOffRow(<?php echo $i;?>);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtOutOffHireReason_<?php echo $i;?>" id="txtOutOffHireReason_<?php echo $i;?>" rows="2" placeholder="Off Hire Reason..."><?php echo $rows['OFFHIRE_REASON'];?></textarea></td>
                                   <td><input type="text" name="txtOutFromOff_<?php echo $i;?>" id="txtOutFromOff_<?php echo $i;?>" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($rows['FROM_OFF_DATE']));?>"/></td>
                                   <td><input type="text" name="txtOutToOff_<?php echo $i;?>" id="txtOutToOff_<?php echo $i;?>" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value="<?php echo date('d-m-Y H:i',strtotime($rows['TO_OFF_DATE']));?>"/></td>
								   <td><input type="text" name="txtOutOffPer_<?php echo $i;?>" id="txtOutOffPer_<?php echo $i;?>" class="form-control"  placeholder="%" autocomplete="off" value="<?php echo $rows['OFFHIRE_PERCENT'];?>" onKeyUp="getCVETotalAmt();"/></td>
                                </tr>
								<?php }}?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td align="left" colspan="5"><button type="button" class="btn btn-primary btn-flat" onClick="AddOutNewOffHire();">Add</button></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                  </div>
				  
				  <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Nett Period(Days)
                            <address>
                               <input type="text" name="txtOutNettPeriod" id="txtOutNettPeriod" class="form-control" readonly  placeholder="Nett Period(Days)" autocomplete="off" value="<?php echo $obj->getFun32();?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
				  <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Nett Hire
                            <address>
                               <input type="text" name="txtOutNettHire" id="txtOutNettHire" class="form-control" readonly  placeholder="Nett Period(Days)" autocomplete="off" value="<?php echo $obj->getFun33();?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                     <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                           
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                              Nett Hire
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    
					
					<?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers on Delivery</h3><input type="hidden" name="txtOutbid" id="txtOutbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
								 $sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_DEL' and TC_SHEETID='".$obj->getFun1()."'";
								 $res2 = mysql_query($sql2);
								 $rows2 = mysql_fetch_assoc($res2);
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutBunkerID_<?php echo $i;?>" name="txtOutBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutConspQtyMT_<?php echo $i;?>" id="txtOutConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['QUANTITY'];?>" onKeyUp="getOutBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutPrice_<?php echo $i;?>" id="txtOutPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['RATE'];?>" onKeyUp="getOutBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutConspAmt_<?php echo $i;?>" id="txtOutConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows2['AMOUNT'];?>"/></td>
									<td><input type="text" name="txtOutConspPer_<?php echo $i;?>" id="txtOutConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['PERCENT'];?>" onKeyUp="getOutBunkerGradeAmt();"/><input type="Hidden" name="txtOutConspPerAmt_<?php echo $i;?>" id="txtOutConspPerAmt_<?php echo $i;?>" value="<?php echo $rows2['PERCENT_AMOUNT'];?>"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutCVE" id="txtOutCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun34();?>" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutCVEAmt" id="txtOutCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFun35();?>"/></td>
									<td><input type="text" name="txtOutCVEPer" id="txtOutCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun66();?>" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutCVEPerAmt" id="txtOutCVEPerAmt" value="<?php echo $obj->getFun67();?>"/></td>
                                </tr>
                                <tr>
                                    <td></td>
								    <td></td>
									<td></td>
                                    <td>ILOHC</td>
                                    <td><input type="text" name="txtOutILOHC" id="txtOutILOHC" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun36();?>"onKeyUp="getCVETotalAmt();"/></td>
                                </tr>
                                <tr>
                                    <td></td>
								    <td></td>
									<td></td>
                                    <td>Ballast Bonus</td>
                                    <td><input type="text" name="txtOutBallastBonus" id="txtOutBallastBonus" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun37();?>" onKeyUp="getCVETotalAmt();"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
				  
				      
				  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers Consp(Estimated) till redelivery</h3><input type="hidden" name="txtOutRedelbid" id="txtOutRedelbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
								 $sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_REDEL' and TC_SHEETID='".$obj->getFun1()."'";
								 $res2 = mysql_query($sql2);
								 $rows2 = mysql_fetch_assoc($res2);
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutRedelBunkerID_<?php echo $i;?>" name="txtOutRedelBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutRedelConspQtyMT_<?php echo $i;?>" id="txtOutRedelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['QUANTITY'];?>" onKeyUp="getOutRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutRedelPrice_<?php echo $i;?>" id="txtOutRedelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['RATE'];?>" onKeyUp="getOutRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutRedelConspAmt_<?php echo $i;?>" id="txtOutRedelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows2['AMOUNT'];?>"/></td>
									<td><input type="text" name="txtOutRedelConspPer_<?php echo $i;?>" id="txtOutRedelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['PERCENT'];?>" onKeyUp="getOutRedelBunkerGradeAmt();"/><input type="Hidden" name="txtOutRedelConspPerAmt_<?php echo $i;?>" id="txtOutRedelConspPerAmt_<?php echo $i;?>" value="<?php echo $rows2['PERCENT_AMOUNT'];?>"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutRedelCVE" id="txtOutRedelCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun38();?>" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutRedelCVEAmt" id="txtOutRedelCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFun39();?>"/></td>
									<td><input type="text" name="txtOutRedelCVEPer" id="txtOutRedelCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun68();?>" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutRedelCVEPerAmt" id="txtOutRedelCVEPerAmt" value="<?php echo $obj->getFun69();?>"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
				  
				  
				  <div class="box">
					<div class="box-header">
						<h3 class="box-title">Less</h3>
					</div>
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tbody>
								<tr>
									<td width="11%"></td>
									<td width="26%">Add. Commission (%)</td>
									<td width="20%"><input type="text" name="txtOutCommission" id="txtOutCommission" class="form-control" onKeyUp="getOutBrokageCommission();" autocomplete="off" value="<?php echo $obj->getFun40();?>" /></td>
									<td width="20%"><input type="text" name="txtOutTotalCommission" readonly id="txtOutTotalCommission" class="form-control" autocomplete="off" value="<?php echo $obj->getFun41();?>" /></td>
									<td width="20%"></td>
								</tr>
							</tbody>
							</table>
					</div>
				</div>
				  
				  
				  
				  
				  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers on Redelivery</h3><input type="hidden" name="txtOutOnRedelbid" id="txtOutOnRedelbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
								 $sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_ONREDEL' and TC_SHEETID='".$obj->getFun1()."'";////TC_SHEET_SLAVE2ID, TC_SHEETID, RECORD_IDENTIFY, QUANTITY, RATE, AMOUNT, BUNKER_GRADEID
								 $res2 = mysql_query($sql2);
								 $rows2 = mysql_fetch_assoc($res2);
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutOnRedelBunkerID_<?php echo $i;?>" name="txtOutOnRedelBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutOnRedelConspQtyMT_<?php echo $i;?>" id="txtOutOnRedelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['QUANTITY'];?>" onKeyUp="getOutOnRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutOnRedelPrice_<?php echo $i;?>" id="txtOutOnRedelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['RATE'];?>" onKeyUp="getOutOnRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutOnRedelConspAmt_<?php echo $i;?>" id="txtOutOnRedelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows2['AMOUNT'];?>"/></td>
									<td><input type="text" name="txtOutOnRedelConspPer_<?php echo $i;?>" id="txtOutOnRedelConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['PERCENT'];?>" onKeyUp="getOutOnRedelBunkerGradeAmt();"/><input type="Hidden" name="txtOutOnRedelConspPerAmt_<?php echo $i;?>" id="txtOutOnRedelConspPerAmt_<?php echo $i;?>" value="<?php echo $rows2['PERCENT_AMOUNT'];?>"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutOnRedelCVE" id="txtOutOnRedelCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun44();?>" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutOnRedelCVEAmt" id="txtOutOnRedelCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFun45();?>"/></td>
									<td><input type="text" name="txtOutOnRedelCVEPer" id="txtOutOnRedelCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun70();?>" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutOnRedelCVEPerAmt" id="txtOutOnRedelCVEPerAmt" value="<?php echo $obj->getFun71();?>"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div> 
				  
				  
				  
				  <div class="box">
					<div class="box-header">
						<h3 class="box-title">Other expenses</h3>
					</div>
					<div class="box-body no-padding">
						<table class="table table-striped">
							<tbody id="tbodyOutBrokerage">
							<?php $sql = "select * from tc_relet_sheet_slave3 where TC_SHEETID='".$obj->getFun1()."' and RECORD_IDENTIFY='OUT_BROKERAGE' ";
								  $result = mysql_query($sql);
								  $num = mysql_num_rows($result);
								  if($num==0)
								  {?>
							  <tr id="OuttbrRow_1">
									<td width="15"><a href="#tb1'" onClick="removeOutBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
									<td width="22">Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
									
									<td width="20%"><input type="text" name="txtOutBrCommPercent_1" id="txtOutBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getOutBrokageCommission();"  /></td>
									<td width="20%"><input type="text" name="txtOutBrComm_1" id="txtOutBrComm_1" class="form-control" readonly value="0.00" /></td>
									<td width="20%"><select  name="selOutBrVendor_1" class="select form-control" id="selOutBrVendor_1"></select>
										<script>$("#selOutBrVendor_1").html($("#selVendor").html());$("#selOutBrVendor_1").val('');</script>
								</td>
							  </tr>
						     <?php }else{
								  $i = 0;
								  while($rows = mysql_fetch_assoc($result))
								  {$i = $i + 1;?>
								  <tr id="OuttbrRow_<?php echo $i;?>">
									<td width="15"><a href="#tb<?php echo $i;?>'" onClick="removeOutBrokerage(<?php echo $i;?>);" ><i class="fa fa-times" style="color:red;"></i></a></td>
									<td width="22">Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
									
									<td width="20%"><input type="text" name="txtOutBrCommPercent_<?php echo $i;?>" id="txtOutBrCommPercent_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows['BROKERAGE'];?>" onKeyUp="getOutBrokageCommission();"  /></td>
									<td width="20%"><input type="text" name="txtOutBrComm_<?php echo $i;?>" id="txtOutBrComm_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows['BROKERAGE_AMOUNT'];?>" /></td>
									<td width="20%"><select  name="selOutBrVendor_<?php echo $i;?>" class="select form-control" id="selOutBrVendor_<?php echo $i;?>"></select>
										<script>$("#selOutBrVendor_<?php echo $i;?>").html($("#selVendor").html());$("#selOutBrVendor_<?php echo $i;?>").val('<?php echo $rows['VENDORID'];?>');</script>
								    </td>
							     </tr>
								  <?php }}?>
								</tbody>
								<tbody>
								<tr>
									<td><button type="button" class="btn btn-primary btn-flat" onClick="addOutBrokerageRow()">Add</button><input type="hidden" name="txtOutBRokageCount" id="txtOutBRokageCount" value="1"/></td>
									<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
									<td><input type="text" name="txtOutBrCommPercent" id="txtOutBrCommPercent" class="form-control" autocomplete="off" value="<?php echo $obj->getFun42();?>" readonly/></td>
									<td><input type="text" name="txtOutBrComm" id="txtOutBrComm" class="form-control" readonly value="<?php echo $obj->getFun43();?>" /></td>
									<td>
									</td>
								</tr>
								</tbody>
								
								
						</table>
					</div>
				</div>
				  
				  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Waiting Days</h3><input type="hidden" name="txtOutWaitbid" id="txtOutWaitbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
						    <thead>
								<tr>
									<td width="20%">Days</td>
									<td width="20%"></td>
									<td width="20%"></td>
									<td></td>
									<td width="20%"><input type="text" name="txtWaitingDays" id="txtWaitingDays" class="form-control" autocomplete="off" value="<?php echo $obj->getFun46();?>"  onKeyUp="getOutWaitBunkerGradeAmt();"/></td>
								</tr>
							</thead>
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
								 $sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_WAIT' and TC_SHEETID='".$obj->getFun1()."'";
								 $res2 = mysql_query($sql2);
								 $rows2 = mysql_fetch_assoc($res2);
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutWaitBunkerID_<?php echo $i;?>" name="txtOutWaitBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutWaitConspQtyMT_<?php echo $i;?>" id="txtOutWaitConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['QUANTITY'];?>" onKeyUp="getOutWaitBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutWaitPrice_<?php echo $i;?>" id="txtOutWaitPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['RATE'];?>" onKeyUp="getOutWaitBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutWaitConspAmt_<?php echo $i;?>" id="txtOutWaitConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows2['AMOUNT'];?>"/></td>
									<td><input type="text" name="txtOutWaitConspPer_<?php echo $i;?>" id="txtOutWaitConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['PERCENT'];?>" onKeyUp="getOutWaitBunkerGradeAmt();"/><input type="Hidden" name="txtOutWaitConspPerAmt_<?php echo $i;?>" id="txtOutWaitConspPerAmt_<?php echo $i;?>" value="<?php echo $rows2['PERCENT_AMOUNT'];?>"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutWaitCVE" id="txtOutWaitCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun47();?>" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutWaitCVEAmt" id="txtOutWaitCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFun48();?>"/></td>
									<td><input type="text" name="txtOutWaitCVEPer" id="txtOutWaitCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun72();?>" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutWaitCVEPerAmt" id="txtOutWaitCVEPerAmt" value="<?php echo $obj->getFun73();?>"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div> 
				  
				  
				  
				  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers Consp(Estimated) till redelivery</h3><input type="hidden" name="txtOutExpbid" id="txtOutExpbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
								 $sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_EXP' and TC_SHEETID='".$obj->getFun1()."'";
								 $res2 = mysql_query($sql2);
								 $rows2 = mysql_fetch_assoc($res2);
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutExpBunkerID_<?php echo $i;?>" name="txtOutExpBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutExpConspQtyMT_<?php echo $i;?>" id="txtOutExpConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['QUANTITY'];?>" onKeyUp="getOutExpBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutExpPrice_<?php echo $i;?>" id="txtOutExpPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['RATE'];?>" onKeyUp="getOutExpBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutExpConspAmt_<?php echo $i;?>" id="txtOutExpConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows2['AMOUNT'];?>"/></td>
									<td><input type="text" name="txtOutExpConspPer_<?php echo $i;?>" id="txtOutExpConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['PERCENT'];?>" onKeyUp="getOutExpBunkerGradeAmt();"/><input type="Hidden" name="txtOutExpConspPerAmt_<?php echo $i;?>" id="txtOutExpConspPerAmt_<?php echo $i;?>" value="<?php echo $rows2['PERCENT_AMOUNT'];?>"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutExpCVE" id="txtOutExpCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun49();?>" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutExpCVEAmt" id="txtOutExpCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFun50();?>"/></td>
									<td><input type="text" name="txtOutExpCVEPer" id="txtOutExpCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun74();?>" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutExpCVEPerAmt" id="txtOutExpCVEPerAmt" value="<?php echo $obj->getFun75();?>"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
				  
				  
				  
				  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Ballast Days</h3><input type="hidden" name="txtOutBalbid" id="txtOutBalbid" value="<?php echo $rec;?>" />
                        <table class="table table-striped">
						    <thead>
								<tr>
									<td width="20%">Days</td>
									<td width="20%"></td>
									<td width="20%"></td>
									<td></td>
									<td width="20%"><input type="text" name="txtBalWaitingDays" id="txtBalWaitingDays" class="form-control" autocomplete="off" value="<?php echo $obj->getFun51();?>" onKeyUp="getOutBalBunkerGradeAmt();" /></td>
								</tr>
							</thead>
                            <thead>
                                <tr>
                                    <th>Bunker Grade</th>
                                    <th>Quantity(MT)</th>
                                    <th>Price(USD/MT)</th>
                                    <th>Amount(USD)</th>
									<th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {$i = $i + 1;
								 $sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_BAL' and TC_SHEETID='".$obj->getFun1()."'";
								 $res2 = mysql_query($sql2);
								 $rows2 = mysql_fetch_assoc($res2);
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOutBalBunkerID_<?php echo $i;?>" name="txtOutBalBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOutBalConspQtyMT_<?php echo $i;?>" id="txtOutBalConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['QUANTITY'];?>" onKeyUp="getOutBalBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutBalPrice_<?php echo $i;?>" id="txtOutBalPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['RATE'];?>" onKeyUp="getOutBalBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOutBalConspAmt_<?php echo $i;?>" id="txtOutBalConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $rows2['AMOUNT'];?>"/></td>
									<td><input type="text" name="txtOutBalConspPer_<?php echo $i;?>" id="txtOutBalConspPer_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $rows2['PERCENT'];?>" onKeyUp="getOutBalBunkerGradeAmt();"/><input type="Hidden" name="txtOutBalConspPerAmt_<?php echo $i;?>" id="txtOutBalConspPerAmt_<?php echo $i;?>" value="<?php echo $rows2['PERCENT_AMOUNT'];?>"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td><input type="text" name="txtOutBalCVE" id="txtOutBalCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun52();?>" onKeyUp="getCVETotalAmt();"/></td>
                                    <td></td>
                                    <td><input type="text" name="txtOutBalCVEAmt" id="txtOutBalCVEAmt" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="<?php echo $obj->getFun53();?>"/></td>
									<td><input type="text" name="txtOutBalCVEPer" id="txtOutBalCVEPer" class="form-control"  placeholder="0.00" autocomplete="off" value="<?php echo $obj->getFun76();?>" onKeyUp="getCVETotalAmt();"/><input type="Hidden" name="txtOutBalCVEPerAmt" id="txtOutBalCVEPerAmt" value="<?php echo $obj->getFun77();?>"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div> 
				  
				  
				  
				  <div class="box">
                      <div class="box-body no-padding">
                      <h4>Hire Loss</h4>
                        <table class="table table-striped">
						    <tbody>
								<tr>
									<td width="30%">Days</td>
									<td width="20%"></td>
									<td width="25%"><input type="text" name="txtHireLossDays" id="txtHireLossDays" class="form-control" autocomplete="off" value="<?php echo $obj->getFun54();?>" onKeyUp="getCVETotalAmt();"/></td>
									<td width="25%"><input type="text" name="txtHireLossAmt" id="txtHireLossAmt" readonly class="form-control" autocomplete="off" value="<?php echo $obj->getFun55();?>" /></td>
								</tr>
							</tbody>
                          </table>
						<h4>Hire Loss - AD Comm</h4>
                        <table class="table table-striped">
						    <tbody>
								<tr>
									<td width="30%">%</td>
									<td width="20%"></td>
									<td width="25%"><input type="text" name="txtAddComm" id="txtAddComm" class="form-control" autocomplete="off" value="<?php echo $obj->getFun56();?>" onKeyUp="getCVETotalAmt();"/></td>
									<td width="25%"><input type="text" name="txtAddCommAmt" id="txtAddCommAmt" readonly class="form-control" autocomplete="off" value="<?php echo $obj->getFun57();?>" /></td>
								</tr>
							</tbody>
                          </table>
						  <h4>Profit/Loss Details</h4>
                        <table class="table table-striped">
						    <tbody>
								<tr>
									<td width="30%">Total Profit/Loss</td>
									<td width="20%"></td>
									<td width="25%"></td>
									<td width="25%"><input type="text" name="txtProfitLoss" id="txtProfitLoss" readonly class="form-control" autocomplete="off" value="<?php echo $obj->getFun58();?>" /></td>
								</tr>
							</tbody>
                          </table>
                    </div>
                  </div> 
				  
				  
				  <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Vessel Re-let Sheet Status
                                    <address>
									<?php if($sheetExists[0]!=0 && $sheetExists[1]!=0 && $obj->getFun59()==1)
									{?>
										<select  name="selVType" class="select form-control" id="selVType" >
											<?php 
												$_REQUEST['selVType'] = $obj->getFun4();
												$obj->getVoyageType();
											?>
										</select>
										<?php }
									else if($sheetExists[0]==0 && $sheetExists[1]!=0)
									{?>
										<select  name="selVType" class="select form-control" id="selVType" >
											<?php 
												$_REQUEST['selVType'] = $obj->getFun4();
												$obj->getVoyageType();
											?>
										</select>
										<?php }else{
										$statusArr = array(2=>'Closed');
										echo '<strong>&nbsp;&nbsp;&nbsp;'.$statusArr[$obj->getFun59()].'</strong>';
										?>
										
										<?php }?>
                                    </address>
                                </div><!-- /.col -->
							</div>
					<?php 
					 	
					 	//if($rigts == 1)
						//{
					 ?>
					<div class="box-footer" align="right">
					<?php if($sheetExists[0]!=0 && $sheetExists[1]!=0 && $obj->getFun59()==1)
						{?>
						<button type="submit" class="btn btn-primary btn-flat" >Submit</button>
						<input type="hidden" name="txtSheetId" id="txtSheetId" value="<?php echo $obj->getFun1();?>" />
						<?php }
					else if($sheetExists[0]==0 && $sheetExists[1]!=0)
							{?>
						<button type="submit" class="btn btn-primary btn-flat" >Submit</button>
						<input type="hidden" name="txtSheetId" id="txtSheetId" value="<?php echo $obj->getFun1();?>" />
						<?php }?>
				  <?php if($sheetExists[0]==0 && $sheetExists[1]!=0)
						   {?>
						     <input type="hidden" name="action" value="submit1" />
					 <?php }else{?>
					         <input type="hidden" name="action" value="submit2" />
						<?php }?>
						<input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>
					<?php //} ?>
					
					
					
					
					
			  <?php } ?>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>
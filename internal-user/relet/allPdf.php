<?php
session_start();
require_once("../../includes/functions_internal_user_relet.inc.php");
include_once("../../includes/fpdf.php");

@$i = $_REQUEST['id'];
$ret = "";
switch ($i)
{
  case 1:
        $ret = vessel_relet_pdf();
				break;
	default:
		    //$ret = funError();
				//echo $ret;
				break; 				
}

function vessel_relet_pdf(){
	
	$tc_conid     	= @$_REQUEST['tc_conid'];
	$vsl_reletid 	= @$_REQUEST['vsl_reletid'];
	$sheetid 		= @$_REQUEST['sheetid'];
	
	$obj = new data();
	
	class PDF extends FPDF
	{
		function Header()
		{
			$obj = new data();
			$obj->funConnect();
			
			$this->SetFillColor(255,255,255);
			$image_path1 = '../../img/logo1.jpg';
			$this->Image($image_path1,130,3,30,15);
			$this->SetFont('Arial','',10);
			$this->SetTextColor(220, 99, 30);
			$this->SetXY(170,13);
			$this->Cell(0,5,"Vessel Relet (".date('d-m-Y').")",0,1,'R',1);	
			$this->SetFont('Arial','',10);
			$this->SetFillColor(255,255,255);
			$this->SetTextColor(177,175,175);
			$this->Cell(278,1,"_____________________________________________________________________________________________________________________________________________",0,1,'C','1');
			$this->Ln(2);
		}
		
		function Footer()
				{
					$this->SetY(-21);
					//Select Arial italic 8
					$this->SetFillColor(255,255,255);
					$this->SetTextColor(177,175,175);
					$this->SetFont('Arial','',10);
					$this->Cell(278,1,"_____________________________________________________________________________________________________________________________________________",0,1,'L','1');
					$this->Ln(1);
					$this->SetFont('Arial','',6);
					
					if($_SESSION['company'] == 1)
					{
						$this->Cell(270,4,"Jaldhi Overseas Pte Ltd",0,1,'C');
						$this->Cell(270,4,"1 Coleman Street, #09-11 The Adelphi,Singapore 179803.",0,1,'C');
						$this->Cell(270,4,"Tel: +65-62238929 Fax: +65-62238920 email: ops@jaldhi.com",0,1,'C');
						$this->Cell(270,4,"",0,1,'C');
					}
					else if($_SESSION['company'] == 2)
					{
						$this->Cell(270,4,"Jaldhi Overseas Pte Ltd",0,1,'C');
						$this->Cell(270,4,"1 Coleman Street, #09-11 The Adelphi,Singapore 179803.",0,1,'C');
						$this->Cell(270,4,"Tel: +65-62238929 Fax: +65-62238920 email: ops@jaldhi.com",0,1,'C');
						$this->Cell(270,4,"",0,1,'C');
					}
				}
	}
	$pdf = new PDF();	
	$pdf->AddPage('L');
	$pdf->SetDisplayMode('real','single');
	$pdf->SetTitle('Vessel Relet Pdf');
	$pdf->SetAuthor('Seven Oceans Holdings');
	$pdf->SetSubject('Vessel Relet Pdf');
	$pdf->SetKeywords('Designed and Developed By Seven Oceans Holdings pvt. Ltd.');
	
	/**********************Main Particulars*************************/
	$sheetExists = $obj->getSheetExistsOrNot($tc_conid, $vsl_reletid, $sheetid);
	$sheetExists = explode('@@@',$sheetExists);
	
	$obj->getReletSheetData($tc_conid, $vsl_reletid, $sheetExists[2], $sheetExists[1]);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Main Particulars",0,1,'L');
					
	$arr1 = $arr2 = $arr3 = array();
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	$arr3=array("Vessel Name",":",$obj->getVesselIMOData($obj->insertTCContractDataBasedOnID($tc_conid,"VESSEL_IMO_ID"),"VESSEL_NAME"),"Vessel Type",":",$obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->insertTCContractDataBasedOnID($tc_conid,"VESSEL_IMO_ID"),"VESSEL_TYPE"),"NAME"),"Nom ID",":",$obj->getFun2());
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	$arr3=array("Date",":",date('d-m-Y',strtotime($obj->getFun3())));
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"TC In Details :- Due to Owners",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	$arr3=array("TC IN Owners",":",$obj->getVendorNameBasedOnID($obj->insertTCContractDataBasedOnID($tc_conid,"VENDORID")),"CP Date",":",date('d-m-Y',strtotime($obj->insertTCContractDataBasedOnID($tc_conid,"CP_DATE"))),"Daily Hire(USD/Day)",":",$obj->getFun5());
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	$arr3=array("Date of Del",":",date('d-m-Y H:i',strtotime($obj->getFun6())),"Port of Del",":",$obj->getPortNameBasedOnID($obj->getFun8()));
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	$arr3=array("Date of Re-Del",":",date('d-m-Y H:i',strtotime($obj->getFun7())),"Port of Re-Del",":",$obj->getPortNameBasedOnID($obj->getFun9()));
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$pdf->Ln(1);
	$pdf->Ln(2);
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L');
	$arr2=array(92,92,92);
	$arr3=array("Off Hire Reason","Off Hire From (Days)","Off Hire To (Days)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	$sql = "select * from tc_relet_sheet_slave1 where TC_SHEETID='".$obj->getFun1()."' and RECORD_IDENTITY='IN_OFF' ";
	$res1 = mysql_query($sql);
	$rec  = mysql_num_rows($res1);
	$i = 0;
	$j = 1;
	
	while($rows1 = mysql_fetch_assoc($res1))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L');
		$arr2=array(92,92,92);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		
		$arr3=array($rows1['OFFHIRE_REASON'],date('d-m-Y H:i',strtotime($rows1['FROM_OFF_DATE'])),date('d-m-Y H:i',strtotime($rows1['TO_OFF_DATE'])));
		
		$pdf->Row2($arr3);
		$i++;
		$j++;
	}
	
	$pdf->Ln(3);
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	$arr3=array("Nett Period(Days)",":",$obj->getFun10(),"Nett Hire",":",$obj->getFun11());
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$pdf->Ln(4);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Bunkers on Delivery",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$arr3=array("Bunker Grade","Quantity(MT)","Price(USD/MT)","Amount(USD)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	$sql2 = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
	$res2 = mysql_query($sql2);
	$i = 0;
	
	while($rows2 = mysql_fetch_assoc($res2))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		$sql3 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows2['BUNKERGRADEID']."' and RECORD_IDENTIFY='IN_DEL' and TC_SHEETID='".$obj->getFun1()."'";
		 $res3 = mysql_query($sql3);
		 $rows3 = mysql_fetch_assoc($res3);
		
		$arr3=array($rows2['NAME'],$rows3['QUANTITY'],$rows3['RATE'],$rows3['AMOUNT']);
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("CVE",$obj->getFun12(),"",$obj->getFun13());
	$pdf->Row2($arr3);
	
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("","","ILOHC",$obj->getFun14());
	$pdf->Row2($arr3);
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("","","Ballast Bonus",$obj->getFun15());
	$pdf->Row2($arr3);
	
	$pdf->Ln(5);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Bunkers Consp(Estimated) till redelivery",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$arr3=array("Bunker Grade","Quantity(MT)","Price(USD/MT)","Amount(USD)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	 $sql3 = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
	$res3 = mysql_query($sql3);
	$rec = mysql_num_rows($res);
	$i = 0;
	
	while($rows = mysql_fetch_assoc($res3))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		 $sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='IN_REDEL' and TC_SHEETID='".$obj->getFun1()."'";
		$res2 = mysql_query($sql2);
		$rows2 = mysql_fetch_assoc($res2);
		
		$arr3=array($rows['NAME'],$rows2['QUANTITY'],$rows2['RATE'],$rows2['AMOUNT']);
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("CVE",$obj->getFun16(),"",$obj->getFun17());
	$pdf->Row2($arr3);
	
	$pdf->Ln(6);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Less",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("Add. Commission",$obj->getFun18(),$obj->getFun19(),"");
	$pdf->Row2($arr3);
	
	$sql4 = "select * from tc_relet_sheet_slave3 where TC_SHEETID='".$obj->getFun1()."' and RECORD_IDENTIFY='IN_BROKERAGE' ";
	$res4 = mysql_query($sql4);
	$i = 0;
	while($rows4 = mysql_fetch_assoc($res4))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 != 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		$arr3=array("Brokerage Commission (%)",$rows4['BROKERAGE'],$rows4['BROKERAGE_AMOUNT'],$obj->getVendorNameBasedOnID($rows4['VENDORID']));
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("Total Brokerage Commission (%)",$obj->getFun20(),$obj->getFun21(),"");
	$pdf->Row2($arr3);
	
	$pdf->Ln(7);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Bunkers on Redelivery",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$arr3=array("Bunker Grade","Quantity(MT)","Price(USD/MT)","Amount(USD)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	 $sql5 = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
	$res5 = mysql_query($sql5);
	$i = 0;
	
	while($rows = mysql_fetch_assoc($res5))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		$sql2 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='IN_ONREDEL' and TC_SHEETID='".$obj->getFun1()."'";
		 $res2 = mysql_query($sql2);
		 $rows2 = mysql_fetch_assoc($res2);
		
		$arr3=array($rows['NAME'],$rows2['QUANTITY'],$rows2['RATE'],$rows2['AMOUNT']);
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("CVE",$obj->getFun22(),"",$obj->getFun23());
	$pdf->Row2($arr3);
	
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	if($obj->getFun25() == ""){ $vendor = ""; } else { $vendor = $obj->getVendorNameBasedOnID($obj->getFun25()); }
	$arr3=array("Balance to Owners","",$obj->getFun24(),$vendor);
	$pdf->Row2($arr3);	
	
	
	$pdf->Ln(8);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"TC OUT(Relet) Details :- Due to Charterers",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	if($obj->getFun26() == ""){ $vendor26 = ""; } else { $vendor26 = $obj->getVendorNameBasedOnID($obj->getFun26()); }
	$arr3=array("TC Out - Charterers",":",$vendor26,"CP Date",":",date('d-m-Y',strtotime($obj->insertTCContractDataBasedOnID($tc_conid,"CP_DATE"))),"Daily Hire(USD/Day)",":",$obj->getFun27());
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	$arr3=array("Date of Del",":",date('d-m-Y H:i',strtotime($obj->getFun28())),"Port of Del",":",$obj->getPortNameBasedOnID($obj->getFun30()));
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	$arr3=array("Date of Re-Del",":",date('d-m-Y H:i',strtotime($obj->getFun29())),"Port of Re-Del",":",$obj->getPortNameBasedOnID($obj->getFun31()));
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$pdf->Ln(9);
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L');
	$arr2=array(92,92,92);
	$arr3=array("Off Hire Reason","Off Hire From (Days)","Off Hire To (Days)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	$sql6 = "select * from tc_relet_sheet_slave1 where TC_SHEETID='".$obj->getFun1()."' and RECORD_IDENTITY='OUT_OFF' ";
	$res6 = mysql_query($sql6);
	$i = 0;
	
	while($rows1 = mysql_fetch_assoc($res6))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L');
		$arr2=array(92,92,92);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		
		$arr3=array($rows1['OFFHIRE_REASON'],date('d-m-Y H:i',strtotime($rows1['FROM_OFF_DATE'])),date('d-m-Y H:i',strtotime($rows1['TO_OFF_DATE'])));
		
		$pdf->Row2($arr3);
		$i++;
		$j++;
	}
	
	$pdf->Ln(10);
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	$arr3=array("Nett Period(Days)",":",$obj->getFun32(),"Nett Hire",":",$obj->getFun33());
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$pdf->Ln(11);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Nett Hire :- Bunkers on Delivery",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$arr3=array("Bunker Grade","Quantity(MT)","Price(USD/MT)","Amount(USD)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	$sql7 = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
	$res7 = mysql_query($sql7);
	$i = 0;
	
	while($rows2 = mysql_fetch_assoc($res7))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		 $sql3 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows2['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_DEL' and TC_SHEETID='".$obj->getFun1()."'";
		 $res3 = mysql_query($sql3);
		 $rows3 = mysql_fetch_assoc($res3);
		
		$arr3=array($rows2['NAME'],$rows3['QUANTITY'],$rows3['RATE'],$rows3['AMOUNT']);
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("CVE",$obj->getFun34(),"",$obj->getFun35());
	$pdf->Row2($arr3);
	
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("","","ILOHC",$obj->getFun36());
	$pdf->Row2($arr3);
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("","","Ballast Bonus",$obj->getFun37());
	$pdf->Row2($arr3);
	
	$pdf->Ln(12);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Bunkers Consp(Estimated) till redelivery",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$arr3=array("Bunker Grade","Quantity(MT)","Price(USD/MT)","Amount(USD)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	$sql8 = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
	$res8 = mysql_query($sql8);
	$i = 0;
	
	while($rows2 = mysql_fetch_assoc($res8))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		 $sql3 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows2['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_REDEL' and TC_SHEETID='".$obj->getFun1()."'";
		 $res3 = mysql_query($sql3);
		 $rows3 = mysql_fetch_assoc($res3);
		
		$arr3=array($rows2['NAME'],$rows3['QUANTITY'],$rows3['RATE'],$rows3['AMOUNT']);
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("CVE",$obj->getFun38(),"",$obj->getFun39());
	$pdf->Row2($arr3);
	
	$pdf->Ln(13);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Less",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("","Add. Commission (%)",$obj->getFun40(),$obj->getFun41());
	$pdf->Row2($arr3);
	
	$pdf->Ln(14);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Bunkers on Redelivery",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$arr3=array("Bunker Grade","Quantity(MT)","Price(USD/MT)","Amount(USD)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	$sql9 = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
	$res9 = mysql_query($sql9);
	$i = 0;
	
	while($rows2 = mysql_fetch_assoc($res9))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		 $sql3 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows2['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_ONREDEL' and TC_SHEETID='".$obj->getFun1()."'";
		 $res3 = mysql_query($sql3);
		 $rows3 = mysql_fetch_assoc($res3);
		
		$arr3=array($rows2['NAME'],$rows3['QUANTITY'],$rows3['RATE'],$rows3['AMOUNT']);
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("CVE",$obj->getFun44(),"",$obj->getFun45());
	$pdf->Row2($arr3);
	
	$pdf->Ln(15);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Other expenses",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	
	$sql10 = "select * from tc_relet_sheet_slave3 where TC_SHEETID='".$obj->getFun1()."' and RECORD_IDENTIFY='OUT_BROKERAGE' ";
	$res10 = mysql_query($sql10);
	$i = 0;
	while($rows = mysql_fetch_assoc($res10))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 != 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		$arr3=array("Brokerage Commission (%)",$rows['BROKERAGE'],$rows['BROKERAGE_AMOUNT'],$obj->getVendorNameBasedOnID($rows['VENDORID']));
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("Total Brokerage Commission (%)",$obj->getFun42(),$obj->getFun43(),"");
	$pdf->Row2($arr3);
	
	$pdf->Ln(16);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Waiting Days",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("","","Days",$obj->getFun46());
	$pdf->Row2($arr3);
	
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$arr3=array("Bunker Grade","Quantity(MT)","Price(USD/MT)","Amount(USD)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	$sql11 = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
	$res11 = mysql_query($sql11);
	$i = 0;
	
	while($rows = mysql_fetch_assoc($res11))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		 $sql3 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_WAIT' and TC_SHEETID='".$obj->getFun1()."'";
		 $res3 = mysql_query($sql3);
		 $rows3 = mysql_fetch_assoc($res3);
		
		$arr3=array($rows['NAME'],$rows3['QUANTITY'],$rows3['RATE'],$rows3['AMOUNT']);
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("CVE",$obj->getFun47(),"",$obj->getFun48());
	$pdf->Row2($arr3);
	
	$pdf->Ln(17);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Bunkers Consp(Estimated) till redelivery",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$arr3=array("Bunker Grade","Quantity(MT)","Price(USD/MT)","Amount(USD)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	$sql12 = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
	$res12 = mysql_query($sql12);
	$i = 0;
	
	while($rows = mysql_fetch_assoc($res12))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		 $sql3 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_EXP' and TC_SHEETID='".$obj->getFun1()."'";
		 $res3 = mysql_query($sql3);
		 $rows3 = mysql_fetch_assoc($res3);
		
		$arr3=array($rows['NAME'],$rows3['QUANTITY'],$rows3['RATE'],$rows3['AMOUNT']);
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("CVE",$obj->getFun49(),"",$obj->getFun50());
	$pdf->Row2($arr3);
	
	$pdf->Ln(18);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Ballast Days",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("","","Days",$obj->getFun51());
	$pdf->Row2($arr3);
	
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$arr3=array("Bunker Grade","Quantity(MT)","Price(USD/MT)","Amount(USD)");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	$sql13 = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
	$res13 = mysql_query($sql13);
	$i = 0;
	
	while($rows = mysql_fetch_assoc($res13))
	{
		$arr1 = $arr2 = $arr3 = $arr5 = array();
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(255,255,255);
		}
		else
		{
			$pdf->SetFillColor(244,244,244);
		}
		$pdf->SetTextColor(0,0,0);
		$arr1=array('L','L','L','L');
		$arr2=array(69,69,69,69);
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		
		 $sql3 = "select * from tc_relet_sheet_slave2 where BUNKER_GRADEID='".$rows['BUNKERGRADEID']."' and RECORD_IDENTIFY='OUT_BAL' and TC_SHEETID='".$obj->getFun1()."'";
		 $res3 = mysql_query($sql3);
		 $rows3 = mysql_fetch_assoc($res3);
		
		$arr3=array($rows['NAME'],$rows3['QUANTITY'],$rows3['RATE'],$rows3['AMOUNT']);
		
		$pdf->Row2($arr3);
		$i++;
	}
	
	$pdf->SetFillColor(244,244,244);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("CVE",$obj->getFun52(),"",$obj->getFun53());
	$pdf->Row2($arr3);
	
	$pdf->Ln(19);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Hire Loss",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("Days","",$obj->getFun54(),$obj->getFun55());
	$pdf->Row2($arr3);
	
	$pdf->Ln(20);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Hire Loss - AD Comm",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("%","",$obj->getFun56(),$obj->getFun57());
	$pdf->Row2($arr3);
	
	$pdf->Ln(21);
	
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(27,119,166);
	$pdf->Cell(190,6,"Profit/Loss Details",0,1,'L');
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','L','L','L');
	$arr2=array(69,69,69,69);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$arr3=array("Total Profit/Loss","","",$obj->getFun58());
	$pdf->Row2($arr3);
	
	$pdf->Ln(22);
	
	$pdf->SetFont('Arial','',7);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	$arr1=array('L','C','L','L','C','L','L','C','L');
	$arr2=array(50,5,50,40,5,50,40,5,40);
	
	if($obj->getFun59()==1)
	{
		$arr3=array("Vessel Re-let Sheet Status",":","Open");
	 }
	else{
		$arr3=array("Vessel Re-let Sheet Status",":","Closed");
	}
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1($arr3);
	
	$filename = "Vessel Relet".date("d-M-Y",time()).".pdf";
	$pdf->Output($filename,'D');
}


?>
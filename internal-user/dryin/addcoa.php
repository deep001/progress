<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertCOADetails();
	header('Location : ./coa_list.php?msg='.$msg);
}
$pagename = basename($_SERVER['PHP_SELF']);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(4); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-truck"></i>&nbsp;Contract of Affreightment&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Contract of Affreightment</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="coa_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
					<div class="row invoice-info" style="display:none">
                        <div class="col-sm-4 invoice-col">
                        	<select  name="selPort" class="form-control" style="display:none;" id="selPort" >
                                <?php $obj->getPortList(); ?>
                               </select>
                    		<select  name="selVendor" class="select form-control" style="display:none;" id="selVendor" >
								<?php $obj->getVendorListNewUpdate("");	?>
                            </select>
                         </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             CREATE A NEW COA    
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Cargo/Material Type
                            <address>
                               <select  name="selCargoType" class="form-control" id="selCargoType" >
								<?php 
                                $obj->getCargoTypeList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Vessel Category
                            <address>
                                <select  name="selVCType" class="form-control" id="selVCType" >
									<?php 
                                    $obj->getVesselCategoryList();
                                    ?>
                                    </select>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            COA Number
                            <address>
                               <input type="text" name="txtCOANo" id="txtCOANo" class="form-control"  placeholder="COA Number" autocomplete="off"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            COA CP Date
                            <address>
                                <input type="text" name="txtCOADate" id="txtCOADate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off"/>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Cargo Qty(MT)
                            <address>
                               <input type="text" name="txtCQty" id="txtCQty" class="form-control" autocomplete="off" placeholder="Cargo Qty(MT)"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Owner
                            <address>
                                <select  name="selOwner" class="form-control" id="selOwner">
								<?php 
                                $obj->getVendorListNewForCOA(11);
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Broker
                            <address>
                                <select  name="selBroker" class="form-control" id="selBroker">
								<?php 
								$obj->getVendorListNewForCOA(12);
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Charterer
                            <address>
                                <select  name="selCharterer" class="form-control" id="selCharterer">
								<?php 
								$obj->getVendorListNewForCOA(7);
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
					</div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            No. of Shipments
                            <address>
                               <input type="text" name="txtNoofShipment" id="txtNoofShipment" class="form-control"  placeholder="No. of Shipments" autocomplete="off"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Shipment Qty(MT)
                            <address>
                                <input type="text" name="txtSQty" id="txtSQty" class="form-control"  placeholder="Shipment Qty(MT)" autocomplete="off"/>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Tolerance ( % )
                            <address>
                               <input type="text" name="txtTolerance" id="txtTolerance" class="form-control" autocomplete="off" placeholder="Tolerance ( % )"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                  	<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Shipment ( s )
                            <address>
                               <input type="text" name="txtShipment" id="txtShipment" class="form-control"  placeholder="Shipment ( s )" autocomplete="off"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Per/Month
                            <address>
                                <input type="text" name="txtper" id="txtper" class="form-control"  placeholder="Per/Month" autocomplete="off"/>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Starting Date
                            <address>
                               <input type="text" name="txtStartDate" id="txtStartDate" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Notice ( Days )
                            <address>
                               <input type="text" name="txtNotice" id="txtNotice" class="form-control"  placeholder="Notice ( Days )" autocomplete="off"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Freight Rate (USD/MT)
                            <address>
                                <input type="text" name="txtFRate" id="txtFRate" class="form-control"  placeholder="Freight Rate (USD/MT)" autocomplete="off"/>
                             </address>
                        </div><!-- /.col -->
                        
                         <div class="col-sm-4 invoice-col">
						   Freight Matrix
                            <address>
								<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Inspector's Report">
                                    <i class="fa fa-paperclip"></i> Freight Matrix
                                    <input type="file" class="form-control" multiple name="fre_matrix" id="fre_matrix" title="" data-widget="Freight Matrix" data-toggle="tooltip" data-original-title="Freight Matrix"/>
                                </div>
							</address>
						</div>
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Demurrage Rate
                            <address>
                               <input type="text" name="txtDemuRate" id="txtDemuRate" class="form-control"  placeholder="Demurrage Rate" autocomplete="off"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Dispatch Rate
                            <address>
                                <input type="text" name="txtDisRate" id="txtDisRate" class="form-control"  placeholder="Dispatch Rate" autocomplete="off"/>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                            Add Comm ( % )
                            <address>
                               <input type="text" name="txtAddComm" id="txtAddComm" class="form-control" autocomplete="off" placeholder="Add Comm ( % )"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           BAF
                            <address>
                            	<input type="checkbox" name="checkBAFbox" id="checkBAFbox" onclick="getShow();" value="1"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info" style="display:none;" id="rowBAF">
                        <div class="col-sm-4 invoice-col">
                           Bunker Price IFO  ( USD/MT )
                            <address>
                               <input type="text" name="txtBPrice" id="txtBPrice" class="form-control"  placeholder="Bunker Price IFO ( USD/MT )" autocomplete="off"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            BAF Terms ( Days )
                            <address>
                                <input type="text" name="txtBAFTerms" id="txtBAFTerms" class="form-control"  placeholder="BAF Terms ( Days )" autocomplete="off"/>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                            BAF ( USD/MT )
                            <address>
                               <input type="text" name="txtIncDec" id="txtIncDec" class="form-control" autocomplete="off" placeholder="BAF ( USD/MT )"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                     <div class="row invoice-info">
                        <div class="col-sm-8 invoice-col">
                            Remarks
                            <address>
                               <textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" rows="3" placeholder="Remarks ..." ></textarea>
                            </address>
                        </div><!-- /.col -->
                       <div class="col-sm-4 invoice-col">
                           &nbsp;
                            <address>
								<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Inspector's Report">
                                    <i class="fa fa-paperclip"></i> Attachment
                                    <input type="file" class="form-control" multiple name="attach_file" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                </div>
							</address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            TC IN Details:
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    <h3 class="page-header">
                    	Due to Owners
                    </h3>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            TC IN Owners
                            <address>
                               <select  name="selTCINOwner" class="form-control" id="selTCINOwner" >
					           </select>
							   <script>$("#selTCINOwner").html($("#selVendor").html());$("#selTCINOwner").val();</script>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Daily Hire(USD/Day)
                            <address>
                               <input type="text" name="txtDailyHire" id="txtDailyHire" class="form-control" placeholder="Daily Hire(USD/Day)" autocomplete="off" onKeyUp="getCVETotalAmt();"/>
                            </address>
                        </div><!-- /.col -->
                     </div>  
                     <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            Date of Del
                            <address>
                               <input type="text" name="txtDateOfDel" id="txtDateOfDel" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off"/>
                            </address>
                        </div>
                        <div class="col-sm-4 invoice-col">
                             Port of Del
                            <address>
                               <select  name="selDelPort" class="form-control" id="selDelPort" >
                               </select>
                               <script>$("#selDelPort").html($("#selPort").html());$("#selDelPort").val("");</script>
                            </address>
                        </div>
					</div>
                    
                    <div class="row invoice-info">
						<div class="col-sm-4 invoice-col">
                             Date of Re-Del
                            <address>
                                <input type="text" name="txtDateOfReDel" id="txtDateOfReDel" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off"/>
                            </address>
                        </div>
						<div class="col-sm-4 invoice-col">
                             Port of Re-Del
                            <address>
                               <select  name="selReDelPort" class="form-control" id="selReDelPort" >
                               </select>
                               <script>$("#selReDelPort").html($("#selPort").html());$("#selReDelPort").val("");</script>
                            </address>
                        </div>
					</div>
                    
                    <div class="box">
                      <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Off Hire Reason</th>
                                    <th>Off Hire From (Days)</th>
                                    <th>Off Hire To (Days)</th>
                                    <input type="hidden" name="txtOFFID" id="txtOFFID" value="1"/>
                                </tr>
                            </thead>
                            <tbody id="tblOFF">
                                <tr id="off_Row_1">
                                   <td align="center"><a href="#pr1" onClick="removeOffRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                   <td><textarea class="form-control areasize" name="txtOffHireReason_1" id="txtOffHireReason_1" rows="2" placeholder="Off Hire Reason..."></textarea></td>
                                   <td><input type="text" name="txtFromOff_1" id="txtFromOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off"/></td>
                                   <td><input type="text" name="txtToOff_1" id="txtToOff_1" class="form-control"  placeholder="dd-mm-yyyy HH:MM" autocomplete="off"/></td>
                                </tr>
                             </tbody>
                             <tfoot>
                                <tr>
                                    <td align="left" colspan="5"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewOffHire();">Add</button></td>
                                </tr>
                            </tfoot>
                          </table>
                     </div>
                   </div>
                   
                   <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers on Delivery</h3>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="25%">Bunker Grade</th>
                                    <th width="25%">Quantity(MT)</th>
                                    <th width="25%">Price(USD/MT)</th>
                                    <th width="25%">Amount(USD)<input type="hidden" name="txtbid" id="txtbid" value="<?php echo $rec;?>" /></th>
                                </tr>
                            </thead>
                            <tbody>
                     		<?php 
					 		$i=0; 
					 		while($rows = mysql_fetch_assoc($res))
					        {
									 $i = $i + 1;
			               	?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtBunkerID_<?php echo $i;?>" name="txtBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtConspQtyMT_<?php echo $i;?>" id="txtConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getBunkerOnDeliveryAmt();"/></td>
                                    <td><input type="text" name="txtPrice_<?php echo $i;?>" id="txtPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getBunkerOnDeliveryAmt();"/></td>
                                    <td><input type="text" name="txtConspAmt_<?php echo $i;?>" id="txtConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td></td>
                                    <td></td>
                                    <td><input type="text" name="txtBODCVE" id="txtBODCVE" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00"/></td>
                                </tr>
                                <tr style="background:#f3f4f5">
                                    <td>ILOHC</td>
								    <td></td>
                                    <td></td>
                                    <td><input type="text" name="txtBODILOHC" id="txtBODILOHC" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
                  
                  
                  
                  <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers Consp(Estimated) till redelivery</h3>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="25%">Bunker Grade</th>
                                    <th width="25%">Quantity(MT)</th>
                                    <th width="25%">Price(USD/MT)</th>
                                    <th width="25%">Amount(USD)</th>
                                    <input type="hidden" name="txtRedelbid" id="txtRedelbid" value="<?php echo $rec;?>" />
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; 
					 		while($rows = mysql_fetch_assoc($res))
					        {
								$i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtRedelBunkerID_<?php echo $i;?>" name="txtRedelBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtRedelConspQtyMT_<?php echo $i;?>" id="txtRedelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtRedelPrice_<?php echo $i;?>" id="txtRedelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtRedelConspAmt_<?php echo $i;?>" id="txtRedelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td></td>
                                    <td></td>
                                    <td><input type="text" name="txtBCTRVE" id="txtBCTRVE" class="form-control" placeholder="0.00" autocomplete="off" value="0.00"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>
                            
                   <?php
					 $sql = "SELECT * FROM bunker_grade_master WHERE STATUS=1 and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' order by NAME";
					 $res = mysql_query($sql);
					 $rec = mysql_num_rows($res);
					?>
                    <div class="box">
                      <div class="box-body no-padding">
                      <h3>Bunkers on Redelivery</h3>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="25%">Bunker Grade</th>
                                    <th width="25%">Quantity(MT)</th>
                                    <th width="25%">Price(USD/MT)</th>
                                    <th width="25%">Amount(USD)</th>
                                    <input type="hidden" name="txtOnRedelbid" id="txtOnRedelbid" value="<?php echo $rec;?>" />
                                </tr>
                            </thead>
                            <tbody>
                     <?php $i=0; while($rows = mysql_fetch_assoc($res))
					             {
									 $i = $i + 1;
			               ?>
                                  <tr>
                                    <td><?php echo $rows['NAME'];?><input type="hidden" id="txtOnRedelBunkerID_<?php echo $i;?>" name="txtOnRedelBunkerID_<?php echo $i;?>" value="<?php echo $rows['BUNKERGRADEID'];?>" /></td>
								    <td><input type="text" name="txtOnRedelConspQtyMT_<?php echo $i;?>" id="txtOnRedelConspQtyMT_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOnRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOnRedelPrice_<?php echo $i;?>" id="txtOnRedelPrice_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" value="0.00" onKeyUp="getOnRedelBunkerGradeAmt();"/></td>
                                    <td><input type="text" name="txtOnRedelConspAmt_<?php echo $i;?>" id="txtOnRedelConspAmt_<?php echo $i;?>" class="form-control"  placeholder="0.00" autocomplete="off" readonly value="0.00"/></td>
                                </tr>
                                <?php }?>
                            </tbody>
                            <tfoot>
                               <tr>
                                    <td>CVE</td>
								    <td></td>
                                    <td></td>
                                    <td><input type="text" name="txtBORCVE" id="txtBORCVE" class="form-control" placeholder="0.00" autocomplete="off" value="0.00"/></td>
                                </tr>
                            </tfoot>
                          </table>
                    </div>
                  </div>          
                    <div class="box-footer" align="right">
                        <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
                        <input type="hidden" name="action" value="submit" />
                        <input type="hidden" name="upstatus" id="upstatus" />
                    </div>
                </form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>

<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<script type="text/javascript">
$(document).ready(function(){ 
	$("#txtCQty,#txtNoofShipment,#txtSQty,#txtShipment,#txtper,#txtNotice,#txtFRate,#txtDisRate,#txtAddComm,#txtBPrice,#txtBAFTerms,#txtIncDec,#txtTolerance,#txtDemuRate,[id^=txtOffPer_],#txtDailyHire").numeric();
	$('#txtCOADate,#txtStartDate').datepicker({
		format: 'dd-mm-yyyy',
		autoclose:true
	});

	$('#txtDateOfDel,#txtDateOfReDel,[id^=txtFromOff_],[id^=txtToOff_]').datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
		autoclose:true
	});

	$(".areasize").autosize({append: "\n"});

	$("#frm1").validate({
		rules: {
				selCargoType:{required:true},
				selVCType:{required:true},
				txtCOANo:{required:true},
				txtCOADate:{required:true},
				txtCQty:{required:true, number:true},
				selOwner:{required:true},
				selBroker:{required:true},
				selCharterer:{required:true},
				txtNoofShipment:{required:true, number:true},
				txtSQty:{required:true, number:true},
				txtTolerance:{required:true},
				txtShipment:{required:true, number:true},
				txtper:{required:true, number:true},
				txtStartDate:{required:true},
				txtNotice:{required:true,number:true},
				txtFRate:{required:true,number:true},
				txtDemuRate:{number:true},
				txtDisRate:{number:true},
				txtAddComm:{number:true},
				txtBPrice:{required:true,number:true},
				txtBAFTerms:{required:true,number:true},
				txtIncDec:{required:true, number:true}
			},
			messages: {
				selCargoType:{required:"*"},
				selVCType:{required:"*"},
				txtCOANo:{required:"*"},
				txtCOADate:{required:"*"},
				txtCQty:{required:"*"},
				selOwner:{required:"*"},
				selBroker:{required:"*"},
				selCharterer:{required:"*"},
				txtNoofShipment:{required:"*"},
				txtSQty:{required:"*"},
				txtTolerance:{required:"*"},
				txtShipment:{required:"*"},
				txtper:{required:"*"},
				txtStartDate:{required:"*"},
				txtNotice:{required:"*"},
				txtFRate:{required:"*"},
				txtDemuRate:{required:"*"},
				txtDisRate:{required:"*"},
				txtAddComm:{required:"*"},
				txtBPrice:{required : "*" },
				txtBAFTerms:{required : "*" },
				txtIncDec:{required : "*" }
			},
			submitHandler: function(form)  {
				jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
				$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
				$("#popup_content").css({"background":"none","text-align":"center"});
				$("#popup_ok,#popup_title").hide();  
				frm1.form.submit();
			}
	});

});


function getShow()
{
	if ($("#checkBAFbox").iCheck('check')) 
	{
		$("#rowBAF").show();
	}
	else
	{
		$("#rowBAF").hide();
		$("#txtBPrice,#txtBAFTerms,#txtIncDec").val("");
	}
}

function AddNewOffHire()
{
	var id = $("#txtOFFID").val();
	if($("#txtOffHireReason_"+id).val() != "" && $("#txtFromOff_"+id).val() != "" && $("#txtToOff_"+id).val() != "" && $("#txtOffPer_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="off_Row_'+id+'"><td align="center"><a href="#pr'+id+'" onclick="removeOffRow('+id+');"><i class="fa fa-times" style="color:red;"></i></a></td><td><textarea class="form-control areasize" name="txtOffHireReason_'+id+'" id="txtOffHireReason_'+id+'" rows="2" placeholder="Off Hire Reason..."></textarea></td><td><input type="text" name="txtFromOff_'+id+'" id="txtFromOff_'+id+'" class="form-control" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""></td><td><input type="text" name="txtToOff_'+id+'" id="txtToOff_'+id+'" class="form-control" placeholder="dd-mm-yyyy HH:MM" autocomplete="off" value=""></td></tr>').appendTo("#tblOFF");
		$("#txtOFFID").val(id);
		$("[id^=txtOffPer_]").numeric();	
		$('#txtFromOff_'+id+',#txtToOff_'+id).datetimepicker({
			format: 'dd-mm-yyyy hh:ii',
			autoclose: true,
		}).on('changeDate', function(){ });
	}else{
		jAlert('Please fill all data', 'Alert');
		return false;	
	}
}

function removeOffRow(var1)
{
	jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
		if(r){
				$("#off_Row_"+var1).remove();	
			 }
		else{return false;}
		});
}

function getBunkerOnDeliveryAmt(){
	
	var total = $("#txtbid").val();
	for(var i =1; i <=total; i++)
	{
		var amt = parseFloat($("#txtConspQtyMT_"+i).val())*parseFloat($("#txtPrice_"+i).val());
		
		if(isNaN(amt)){amt = '0.00';}
		$('#txtConspAmt_'+i).val(amt);
	}
}

function getRedelBunkerGradeAmt(){
	
	var total = $("#txtRedelbid").val();
	for(var i =1; i <=total; i++)
	{
		var amt = parseFloat($("#txtRedelConspQtyMT_"+i).val())*parseFloat($("#txtRedelPrice_"+i).val());
		if(isNaN(amt)){amt = '0.00';}
		$('#txtRedelConspAmt_'+i).val(amt);
	}	
}

function getOnRedelBunkerGradeAmt(){
	
	var total = $("#txtOnRedelbid").val();
	for(var i =1; i <=total; i++)
	{
		var amt = parseFloat($("#txtOnRedelConspQtyMT_"+i).val())*parseFloat($("#txtOnRedelPrice_"+i).val());
		if(isNaN(amt)){amt = '0.00';}
		$('#txtOnRedelConspAmt_'+i).val(amt);
	}
	
}

</script>
    </body>
</html>
<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$mappingid 	= $_REQUEST['mappingid'];
$page  		= $_REQUEST['page'];

if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}

if (@$_REQUEST['action'] == 'submit')
 { 
 	$msg = $obj->insertchecklistDetails();
	header('Location: ./'.$page_link.'?msg='.$msg);
 }

$cost_sheet_id 	= $obj->getLatestCostSheetID($mappingid);
$arr  			= $obj->getLoadPortAndDischargePortArrBasedOnMappingidAndProcessWithoutTBN($mappingid,$cost_sheet_id);
$port_arr  		= $obj->getLoadPortAndDischargePortArrBasedOnIDMappingidAndProcessWithoutTBN($mappingid,$cost_sheet_id);

$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&page=".$page;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Check List</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right">
                    <a href="allPdf.php?id=44&mappingid=<?php echo $mappingid; ?>" title="CHeck List Pdf">
                        <button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                    </a>
                    <a href="in_ops_at_glance.php"><button class="btn btn-info btn-flat">Back</button></a>
                </div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
				
				<div class="row">
					<div class="col-xs-12">
					  <h3 style=" text-align:center;">CHECKLIST</h3>		                       
					</div><!-- /.col -->
				</div>
				<div style="height:10px;"></div>	
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
						 FIXTURE DETAILS
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				<?php
				$sql = "select * from check_list where MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";
				$res = mysql_query($sql);
				$rows = mysql_fetch_assoc($res);
				$chartPni = $rows['CHARTERER_PNI'];
				?>	
				
				<div class="row invoice-info">
					<div class="col-sm-3 invoice-col">
					   REF ID 
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getMappingData($mappingid,"NOM_NAME");?></strong>
						</address>
					</div>
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					   VESSEL NAME
						<address>
						  <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					   CP DATE
						<address>
						<?php 
						$nomination_data = $obj->getMappingData($mappingid,"OPEN_VESSEL_ID");
						$cp_date = $obj->getVesselOpenEntryDeta($nomination_data,'CP_DATE');
						echo date("d-M-Y",strtotime($cp_date));
						
						?>	
						</address>
					</div>
					<div class="col-sm-4 invoice-col">&nbsp;</div>
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					  CHARTERER
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getNomDetailsData($mappingid,"CHARTERER");?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  OWNER
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVendorListNewData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"OWNER"),"NAME");?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  BROKER
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVendorListNewData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"BROKER"),"NAME");?></strong>
						</address>
					</div>
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					  DEMMURAGE
						<address>
						<input type="text" name="dem_rate" id="dem_rate" class="form-control" placeholder="DEMMURAGE" autocomplete="off" value="<?php echo $rows['DEMM_RATE'];?>" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  OPERATOR
						<address>
						<input type="text" name="operator" id="operator" class="form-control" placeholder="OPERATOR" autocomplete="off" value="<?php echo $rows['OPERATOR'];?>" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col"></div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
						 VSL DETAILS
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					  BUILT
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"YEARBUILT");?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  DEADWEIGHT
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"DWT");?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  DRAFT
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"DRAFTM");?></strong>
						</address>
					</div>
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					  GRT
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselParticularData('GT_INATERNATIONAL','vessel_master_1',$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"));?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  TPC
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselParticularData('SUMMER_3','vessel_master_1',$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"));?></strong>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  VESSEL PNI
						<address>
						<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getVesselParticularData('P_I','vessel_master_6',$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"));?></strong>
						</address>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
							VESSEL CHECKS
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					  REG
						<address>
						<?php if($rows['REG']=="1"){$style_1 = 'checked';} else {$style_1 = '';}?>
							<input type="checkbox" name="regcheckbox" <?php echo $style_1; ?> id="regcheckbox" class="form-control" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  CLASS
						<address>
						<?php if($rows['CLASS']=="1"){$style_2 = 'checked';} else {$style_2 = '';}?>
							<input type="checkbox" name="classcheckbox" <?php echo $style_2; ?> id="classcheckbox" class="form-control" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  PNI
						<address>
						<?php if($rows['PNI']=="1"){$style_3 = 'checked';} else {$style_3 = '';}?>
							<input type="checkbox" name="pnicheckbox" <?php echo $style_3; ?> id="pnicheckbox" class="form-control" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  ISM
						<address>
						<?php if($rows['ISM']=="1"){$style_4 = 'checked';} else {$style_4 = '';}?>
							<input type="checkbox" name="ismcheckbox" <?php echo $style_4; ?> id="ismcheckbox" class="form-control" value="1" />
						</address>
					</div>
					
					<div class="col-sm-4 invoice-col">
					  DOC
						<address>
						<?php if($rows['DOC']=="1"){$style_5 = 'checked';} else {$style_5 = '';}?>
							<input type="checkbox" name="doccheckbox" <?php echo $style_5; ?> id="doccheckbox" class="form-control" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  ITC
						<address>
						<?php if($rows['ITC']=="1"){$style_6 = 'checked';} else {$style_6 = '';}?>
							<input type="checkbox" name="itccheckbox" <?php echo $style_6; ?> id="itccheckbox" class="form-control" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  ISPS
						<address>
						<?php if($rows['ISPS']=="1"){$style_7 = 'checked';} else {$style_7 = '';}?>
							<input type="checkbox" name="ispscheckbox" <?php echo $style_7; ?> id="ispscheckbox" class="form-control" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  LL
						<address>
						<?php if($rows['LL']=="1"){$style_8 = 'checked';} else {$style_8 = '';}?>
							<input type="checkbox" name="llcheckbox" <?php echo $style_8; ?> id="llcheckbox" class="form-control" value="1" />
						</address>
					</div>
					
					<div class="col-sm-4 invoice-col">
					  BQ
						<address>
						<?php if($rows['BQ']=="1"){$style_9 = 'checked';} else {$style_9 = '';}?>
							<input type="checkbox" name="bqcheckbox" <?php echo $style_9; ?> id="bqcheckbox" class="form-control" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  H&M
						<address>
						<?php if($rows['H_M']=="1"){$style_10 = 'checked';} else {$style_10 = '';}?>
							<input type="checkbox" name="hmcheckbox" <?php echo $style_10; ?> id="hmcheckbox" class="form-control" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  SEA-WEB
						<address>
						<?php if($rows['SEA_WEB']=="1"){$style_11 = 'checked';} else {$style_11 = '';}?>
							<input type="checkbox" name="seawebcheckbox" <?php echo $style_11; ?> id="seawebcheckbox" class="form-control" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  CARGO DECL. SIGN MASTER
						<address>
						<?php if($rows['CARGODECL_MASTER']=="1"){$style_12 = 'checked';} else {$style_12 = '';}?>
							<input type="checkbox" name="cargodmcheckbox" <?php echo $style_12; ?> id="cargodmcheckbox" class="form-control"  value="1" />
						</address>
					</div>
				
					<div class="col-sm-4 invoice-col">
					  CERTIS AND BQ SENT TO INSU. DESK
						<address>
						<?php if($rows['CERTBQ_DESK']=="1"){$style_13 = 'checked';} else {$style_13 = '';}?>
						<input type="checkbox" name="cbiseakcheckbox" <?php echo $style_13; ?> id="cbiseakcheckbox" class="form-control"  value="1" />
						</address>
					</div>

					<div class="col-sm-4 invoice-col">
					  CHARTERERS PNI
						<address>
						<select  name="chartPni" class="form-control" id="chartPni" >
							<?php 
								$obj->updatecheckListChartererPniByVendorListNewForCOA(17,$chartPni);
							?>
						</select>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  LAST PORT AGENT
						<address>
						<input type="text" name="LPAdetail" id="LPAdetail" class="form-control" placeholder="LAST PORT AGENT" autocomplete="off" value="<?php echo $rows['LPA_DETAIL'];?>" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					  WORKING CP
						<address>
						<input type="text" name="workingCp" id="workingCp" class="form-control" placeholder="WORKING CP" autocomplete="off" value="<?php echo $rows['WORKING_CP'];?>" />
						</address>
					</div>
				</div>
				
				<!------------------------ For LOADPORT -------------------------->
				
				<?php if(count($arr)!=0){ ?>
				
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
							LOADPORT
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				
				<div class="box-body table-responsive">
				<table aria-describedby="example1_info" id="example1" class="table table-bordered dataTable">
					<thead>
						<tr role="row">
						<th style="width: 12%; color:#fff; background-color:#3c8dbc;">&nbsp;</th>
						<?php 
						for($j=0;$j<count($arr);$j++){
						if(substr($arr[$j],0,2) == "LP"){
						?>
						<th style="width: 12%; color:#fff;  background-color:#3c8dbc;"><?php echo $arr[$j]; ?></th>
						<?php }} ?>
						</tr>
					</thead>
						
					<tr>
					<?php 
					$arr_1 = array("LAYCAN (OWNER)","LAYCAN (SHIPPER)","SHIPPER","DRAFT RESTRICTION","PARCEL SIZE","LOAD RATE","AGENT","REMARKS 1","","STOW PLAN QTY","SP DEP DRAFT","SP ARR DRAFT","","ETA 30 DAYS","ETA 25 DAYS","ETA 20 DAYS","ETA 15 DAYS","ETA 10 DAYS","ETA 7 DAYS","ETA 5 DAYS","ETA 3 DAYS","ETA 2 DAYS","ETA 1 DAY","ACTUAL ARRIVAL","NOR TENDERED","","COMMENCED","ETC/D","BALANCE","TOTAL","24HRS","COMPLETED","VESSEL SAILED","DOCUMENT(S)","REMARKS 2","","DRAFT BL","FINAL B/L QTY1");
						
					for($i=0;$i<count($arr_1);$i++)
					{
					if($i==8){
					?>
					<td width="12%" align="center" valign="top" style="color:#000000; background-color:#f3f4f5">STOWAGE PLAN</td>
					<?php } elseif($i==12){ ?>
					<td width="12%" align="center" valign="top" style="color:#000000; background-color:#f3f4f5">ETA NOTICES</td>
					<?php } elseif($i==25){ ?>
					<td width="12%" align="center" valign="top" style="color:#000000; background-color:#f3f4f5">LOAD PORT VESSEL OPERATIONS</td>
					<?php } elseif($i==35){ ?>
					<td width="12%" align="center" valign="top" style="color:#000000; background-color:#f3f4f5">BILL OF LADING</td>
					<?php } else {?>
					<tr>
					<td width="12%" align="left" valign="top"><?php echo $arr_1[$i]; ?><input type="hidden" name="txtlpentity_<?php echo $i; ?>" id="txtlpentity_<?php echo $i; ?>" class="form-control" autocomplete="off" value="<?php echo $arr_1[$i];?>" /></td>
					<?php } for($j=0;$j<count($arr);$j++){
					if(substr($arr[$j],0,2) == "LP"){
						
						//if($i>11 && $i<23 || $i==26){$style = 'readonly="true"';}else{$style = '';}
						
					$sql_query = "select max(SOFSLAVEID) as max from sof_slave_5 where LOGIN='AGENT' and PORT='LP' and PORTID='".$port_arr[$j]."' and MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' ";
					$result = mysql_query($sql_query);
					$testres = mysql_fetch_assoc($result);
					
					$lpvessaloper = "select * from sof_slave_5 where SOFSLAVEID='".$testres['max']."'"; 
					$result1 = mysql_query($lpvessaloper);
					$testrec = mysql_fetch_assoc($result1);	
					$etc_date = date("d-M-Y",strtotime($testrec['ETCD']));
					$completed_date = date("d-M-Y",strtotime($testrec['PRE_DATE']));
					
					$comp_sql = "select MIN(SOFSLAVEID) as min from sof_slave_5 where LOGIN='AGENT' and PORT='LP' and PORTID='".$port_arr[$j]."' and MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";
					$rock_rs = mysql_query($comp_sql);
					$min_val = mysql_fetch_assoc($rock_rs);
					
					$comp_sql1 = "select * from sof_slave_5 where SOFSLAVEID='".$min_val['min']."'"; 
					$rock_rs1 = mysql_query($comp_sql1);
					$min_val = mysql_fetch_assoc($rock_rs1);	
					$commenced = date("d-M-Y",strtotime($min_val['PRE_DATE']));
					
					$sqlVss = "select * from sof_master where LOGIN='AGENT' and PORT='LP' and PORTID='".$port_arr[$j]."' and MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' ";
					$resVss = mysql_query($sqlVss);
					$sofVesselsailesLP = mysql_fetch_assoc($resVss);
					
					$pre_sql = "select * from sof_slave_4 where LOGIN='AGENT' and PORT='LP' and PORTID='".$port_arr[$j]."' and MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";
					$pre_res = mysql_query($pre_sql);
					$preArivalLp = mysql_fetch_assoc($pre_res);
						
					$sql1 = mysql_query("select * from checklist_loadport where PORT='LP' and PORTID='".$port_arr[$j]."' and CHECKLIST_ID='".$rows['CHECKLIST_ID']."' and ENTITY_NAME='".$arr_1[$i]."'");
					$rows1 = mysql_fetch_assoc($sql1);
					if($i==8)
					{
					?>
					<td width="12%" align="left" valign="top" style="color:#000000; background-color:#f3f4f5"></td>
					<?php } elseif($i==12){ ?>
					
					<td width="12%" align="left" valign="top" style="color:#000000; background-color:#f3f4f5"></td>
					
					<?php } elseif($i==25){ ?>
					
					<td width="12%" align="left" valign="top" style="color:#000000; background-color:#f3f4f5"></td>
					
					<?php } elseif($i==35){ ?>
					
					<td width="12%" align="left" valign="top" style="color:#000000; background-color:#f3f4f5"></td>
						
					<?php } else {?>
					
					<?php if($i==7 || $i==34){ ?>
					
					<td width="12%" align="left" valign="top"><textarea name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>"  id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control areasize"  /><?php echo $rows1['ENTITY_VALUE'];?></textarea></td>
					<?php } elseif($i==35){ ?>
					
					<?php if($rows1['ENTITY_VALUE']=="1"){$style_14 = 'checked';} else {$style_14 = '';}?>
					
						<td width="12%" align="left" valign="top"><input type="checkbox" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" <?php echo $style_14;?> id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control"  value="1" /><label for="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>"></label></td>
						
						<!------------------- for COMMENCED ------------------>
						<?php } 
						elseif($i==21 && $sofVesselsailesLP['LC'] == "" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==21 && $sofVesselsailesLP['LC']!="" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesLP['LC'];?>" /></td>
						
						<?php } elseif($i==21 && $sofVesselsailesLP['LC']=="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==21 && $sofVesselsailesLP['LC']!="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesLP['LC'];?>" /></td>
						
						<!------------------- for ETC/D ------------------>
						<?php } 
						elseif($i==22 && $etc_date == "01-Jan-1970" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==22 && $etc_date!="01-Jan-1970" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $etc_date;?>" /></td>
						
						<?php } elseif($i==22 && $etc_date=="01-Jan-1970" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==22 && $etc_date!="01-Jan-1970" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $etc_date;?>" /></td>
							
						<!------------------- for BALANCE ------------------>
						
						<?php } elseif($i==23 && $testrec['BALANCE']=="0" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==23 && $testrec['BALANCE']!="0" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['BALANCE'];?>" /></td>
						
						<?php } elseif($i==23 && $testrec['BALANCE']=="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==23 && $testrec['BALANCE']!="0" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['BALANCE'];?>" /></td>
						
						<!------------------- for TOTAL ------------------>
						
						<?php } elseif($i==24 && $testrec['TTL_LOAD']=="0" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="" /> </td>
							
						<?php } elseif($i==24 && $testrec['TTL_LOAD']!="0" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['TTL_LOAD'];?>" /></td>
						
						<?php } elseif($i==24 && $testrec['TTL_LOAD']=="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==24 && $testrec['TTL_LOAD']!="0" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['TTL_LOAD'];?>" /></td>
						
						<!------------------- for 24HRS ------------------>
						
						<?php } elseif($i==25 && $testrec['LOAD_LAST']=="0" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==25 && $testrec['LOAD_LAST']!="0" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['LOAD_LAST'];?>" /></td>
						
						<?php } elseif($i==25 && $testrec['LOAD_LAST']=="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==25 && $testrec['LOAD_LAST']!="0" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['LOAD_LAST'];?>" /></td>
						
						<!------------------- for COMPLETED ------------------>
						
						<?php } elseif($i==26 && $sofVesselsailesLP['LC1'] == "" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==26 && $sofVesselsailesLP['LC1']!="" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesLP['LC1'];?>" /></td>
						
						<?php } elseif($i==26 && $sofVesselsailesLP['LC1']=="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==26 && $sofVesselsailesLP['LC1']!="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesLP['LC1'];?>" /></td>
						
						<!------------------- for VESSEL SAILED ------------------>
						
						<?php } elseif($i==27 && $sofVesselsailesLP['VS'] == "" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==27 && $sofVesselsailesLP['VS']!="" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesLP['VS'];?>" /></td>
						
						<?php } elseif($i==27 && $sofVesselsailesLP['VS']=="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==27 && $sofVesselsailesLP['VS']!="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesLP['VS'];?>" /></td>
						
						
						<!------------------- for STOW PLAN QTY ------------------>
						
						<?php } elseif($i==9 && $preArivalLp['STOW_PLAN_QTY'] == "" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==9 && $preArivalLp['STOW_PLAN_QTY']!="" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $preArivalLp['STOW_PLAN_QTY'];?>" /></td>
						
						<?php } elseif($i==9 && $preArivalLp['STOW_PLAN_QTY']=="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==9 && $preArivalLp['STOW_PLAN_QTY']!="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $preArivalLp['STOW_PLAN_QTY'];?>" /></td>
						
						<!------------------- for SP DEP DRAFT ------------------>
						
						<?php } elseif($i==10 && $preArivalLp['SP_DEPT_DRAFT'] == "" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==10 && $preArivalLp['SP_DEPT_DRAFT']!="" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $preArivalLp['SP_DEPT_DRAFT'];?>" /></td>
						
						<?php } elseif($i==10 && $preArivalLp['SP_DEPT_DRAFT']=="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==10 && $preArivalLp['SP_DEPT_DRAFT']!="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $preArivalLp['SP_DEPT_DRAFT'];?>" /></td>
						
						<!------------------- for SP ARR DRAFT ------------------>
						
						<?php } elseif($i==11 && $preArivalLp['SP_ARR_DRAFT'] == "" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==11 && $preArivalLp['SP_ARR_DRAFT']!="" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $preArivalLp['SP_ARR_DRAFT'];?>" /></td>
						
						<?php } elseif($i==11 && $preArivalLp['SP_ARR_DRAFT']=="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==11 && $preArivalLp['SP_ARR_DRAFT']!="" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $preArivalLp['SP_ARR_DRAFT'];?>" /></td>
			
						<!------------------- for ETA 30 DAYS ------------------>
						
						<?php } elseif($i==13 && date("Y-m-d",strtotime($preArivalLp['ETA_30_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==13 && date("Y-m-d",strtotime($preArivalLp['ETA_30_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_30_DAYS']));?>" /></td>
						
						<?php } elseif($i==13 && date("Y-m-d",strtotime($preArivalLp['ETA_30_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==13 && date("Y-m-d",strtotime($preArivalLp['ETA_30_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_30_DAYS']));?>" /></td>
						
						<!------------------- for ETA 25 DAYS ------------------>
						
						<?php } elseif($i==14 && date("Y-m-d",strtotime($preArivalLp['ETA_25_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==14 && date("Y-m-d",strtotime($preArivalLp['ETA_25_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_25_DAYS']));?>" /></td>
						
						<?php } elseif($i==14 && date("Y-m-d",strtotime($preArivalLp['ETA_25_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==14 && date("Y-m-d",strtotime($preArivalLp['ETA_25_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_25_DAYS']));?>" /></td>
						
						<!------------------- for ETA 20 DAYS ------------------>
						
						<?php } elseif($i==15 && date("Y-m-d",strtotime($preArivalLp['ETA_20_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==15 && date("Y-m-d",strtotime($preArivalLp['ETA_20_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_20_DAYS']));?>" /></td>
						
						<?php } elseif($i==15 && date("Y-m-d",strtotime($preArivalLp['ETA_20_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==15 && date("Y-m-d",strtotime($preArivalLp['ETA_20_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_20_DAYS']));?>" /></td>
						
						<!------------------- for ETA 15 DAYS ------------------>
						
						<?php } elseif($i==16 && date("Y-m-d",strtotime($preArivalLp['ETA_15_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==16 && date("Y-m-d",strtotime($preArivalLp['ETA_15_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_15_DAYS']));?>" /></td>
						
						<?php } elseif($i==16 && date("Y-m-d",strtotime($preArivalLp['ETA_15_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==16 && date("Y-m-d",strtotime($preArivalLp['ETA_15_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_15_DAYS']));?>" /></td>
						
						<!------------------- for ETA 10 DAYS ------------------>
						
						<?php } elseif($i==17 && date("Y-m-d",strtotime($preArivalLp['ETA_10_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==17 && date("Y-m-d",strtotime($preArivalLp['ETA_10_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_10_DAYS']));?>" /></td>
						
						<?php } elseif($i==17 && date("Y-m-d",strtotime($preArivalLp['ETA_10_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==17 && date("Y-m-d",strtotime($preArivalLp['ETA_10_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_10_DAYS']));?>" /></td>
						
						<!------------------- for ETA 7 DAYS ------------------>
						
						<?php } elseif($i==18 && date("Y-m-d",strtotime($preArivalLp['ETA_7_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==18 && date("Y-m-d",strtotime($preArivalLp['ETA_7_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_7_DAYS']));?>" /></td>
						
						<?php } elseif($i==18 && date("Y-m-d",strtotime($preArivalLp['ETA_7_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==18 && date("Y-m-d",strtotime($preArivalLp['ETA_7_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_7_DAYS']));?>" /></td>
						
						<!------------------- for ETA 5 DAYS ------------------>
						
						<?php } elseif($i==19 && date("Y-m-d",strtotime($preArivalLp['ETA_5_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==19 && date("Y-m-d",strtotime($preArivalLp['ETA_5_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_5_DAYS']));?>" /></td>
						
						<?php } elseif($i==19 && date("Y-m-d",strtotime($preArivalLp['ETA_5_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==19 && date("Y-m-d",strtotime($preArivalLp['ETA_5_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_5_DAYS']));?>" /></td>
						
						<!------------------- for ETA 3 DAYS ------------------>
						
						<?php } elseif($i==20 && date("Y-m-d",strtotime($preArivalLp['ETA_3_DAYS'])) == "1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left"  valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==20 && date("Y-m-d",strtotime($preArivalLp['ETA_3_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_3_DAYS']));?>" /></td>
						
						<?php } elseif($i==20 && date("Y-m-d",strtotime($preArivalLp['ETA_3_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==20 && date("Y-m-d",strtotime($preArivalLp['ETA_3_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_3_DAYS']));?>" /></td>
						
						<!------------------- for ETA 2 DAYS ------------------>
						
						<?php } elseif($i==21 && date("Y-m-d",strtotime($preArivalLp['ETA_2_DAYS'])) == "1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==21 && date("Y-m-d",strtotime($preArivalLp['ETA_2_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_2_DAYS']));?>" /></td>
						
						<?php } elseif($i==21 && date("Y-m-d",strtotime($preArivalLp['ETA_2_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==21 && date("Y-m-d",strtotime($preArivalLp['ETA_2_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_2_DAYS']));?>" /></td>
						
						<!------------------- for ETA 1 DAYS ------------------>
						
						<?php } elseif($i==22 && date("Y-m-d",strtotime($preArivalLp['ETA_1_DAYS'])) == "1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==22 && date("Y-m-d",strtotime($preArivalLp['ETA_1_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_1_DAYS']));?>" /></td>
						
						<?php } elseif($i==22 && date("Y-m-d",strtotime($preArivalLp['ETA_1_DAYS']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==22 && date("Y-m-d",strtotime($preArivalLp['ETA_1_DAYS']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ETA_1_DAYS']));?>" /></td>
						
						<!------------------- for ACTUAL ARRIVAL ------------------>
						
						<?php } elseif($i==23 && date("Y-m-d",strtotime($preArivalLp['ACTUAL_ARRIVAL'])) == "1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==23 && date("Y-m-d",strtotime($preArivalLp['ACTUAL_ARRIVAL']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ACTUAL_ARRIVAL']));?>" /></td>
						
						<?php } elseif($i==23 && date("Y-m-d",strtotime($preArivalLp['ACTUAL_ARRIVAL']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==23 && date("Y-m-d",strtotime($preArivalLp['ACTUAL_ARRIVAL']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['ACTUAL_ARRIVAL']));?>" /></td>
						
						<!------------------- for NOR TENDERED ------------------>
						
						<?php } elseif($i==24 && date("Y-m-d",strtotime($preArivalLp['NOR_TENDERED'])) == "1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
							
						<?php } elseif($i==24 && date("Y-m-d",strtotime($preArivalLp['NOR_TENDERED']))!="1970-01-01" && $rows1['ENTITY_VALUE']==""){ ?>
						
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['NOR_TENDERED']));?>" /></td>
						
						<?php } elseif($i==24 && date("Y-m-d",strtotime($preArivalLp['NOR_TENDERED']))=="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
						
						<?php } elseif($i==24 && date("Y-m-d",strtotime($preArivalLp['NOR_TENDERED']))!="1970-01-01" && $rows1['ENTITY_VALUE']!=""){ ?>
						<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtlpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalLp['NOR_TENDERED']));?>" /></td>
						
						<!------------------- end ------------------>
						
						<?php } else{ ?>
					<td width="12%" align="left" valign="top"><input type="text" name="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtlpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows1['ENTITY_VALUE'];?>" /></td>
					
					<?php }}}} ?>	
					</tr>
					<?php }?>
					<tr height="10" style="display:none;"><td><input type="hidden" name="txtLPSize" id="txtLPSize" class="form-control" autocomplete="off" value="<?php echo count($arr_1);?>" /></td></tr>
					
					<tbody id="sort1">
					<?php 
					$j=0;
					$k=0;
					$sql_fty = "select * from checklist_finalqty where CHECKLIST_ID='".$rows['CHECKLIST_ID']."'";
					$fty_res = mysql_query($sql_fty);
					$fty_rec = mysql_num_rows($fty_res);
					
					while($fty_rows = mysql_fetch_assoc($fty_res))
					{
						$barr[$k] = $fty_rows['QTY_ID'];
						$name_arr[$k] = $fty_rows['ENTITY_NAME'];
						$k++;
					}
					
					$unique_arr = array_values(array_unique($name_arr));
					$count = sizeof($unique_arr);
					//echo'<pre>';print_r($unique_arr);
					if($fty_rec > 0)
					{
						for($i=0;$i<$count;$i++)
						{ $j = $j+1; 
						
					?>
					<tr>
					<td width="12%" align="left" valign="top"><input type="text" name="txtfty_Name_<?php echo $j;?>" id="txtfty_Name_<?php echo $j;?>"class="form-control" autocomplete="off" value="<?php echo $unique_arr[$i];?>" /></td>
					<?php 
					unset($barr_1);
					$sql_fty_1 = "select * from checklist_finalqty where CHECKLIST_ID='".$rows['CHECKLIST_ID']."' and ENTITY_NAME='".$unique_arr[$i]."'";
					//echo $i.'<br />';
					$fty_res_1 = mysql_query($sql_fty_1);
					$k=0;
					while($fty_rows_1 = mysql_fetch_assoc($fty_res_1))
					{
						$barr_1[$k] = $fty_rows_1['ENTITY_VALUE'];
						$k++;
					}
					
					for($x=0;$x<count($arr);$x++)
					{
						if(substr($arr[$x],0,2) == "LP")
						{
					?>
					<td width="18%" align="left" valign="top"><input type="text" name="txtfty_Value_<?php echo $j; ?>_<?php echo $x;?>" id="txtfty_Value_<?php echo $j; ?>_<?php echo $x;?>" class="form-control" autocomplete="off" value="<?php echo $barr_1[$x];?>" /></td>
					<?php }} ?>
					</tr>
					<?php }} ?>
					</tbody>
				
					</tr>
					<tr>
					<td colspan="6" align="left" width="23%" valign="middle" class="input-text" style="font-size:11px;"><submit class="btn btn-info btn-flat" onClick="addUI_Row(<?php echo $p+1;?>);">Add</submit><input type="hidden" class="input" name="txtBOL_<?php echo $p+1;?>" id="txtBOL_<?php echo $p+1;?>" value="<?php echo $j;?>" size="5" /><input type="hidden" name="txttabid1[]" id="txttabid1_<?php echo $i+1;?>" class="form-control" value="" /></td>
					</tr>
				</table>
				</div>
                
				<?php } ?>
				
				<!------------------------ For LOAD PORT DAILY QTY  -------------------------->
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
						  LOAD PORT DAILY QTY
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				<div class="row">
				   <div class="col-xs-12">
					  <div class="box box-primary">
						 <div class="box-body no-padding" style="overflow:auto;">
						  <table class="table table-striped" >
						   <thead>
							<tr>
							<th width="12%" align="left" valign="top">DATE</th>
							<th width="15%" align="left" valign="top">ENGAGEMENT QTY</th>
							<th width="20%" align="left" valign="top">LOAD LAST 24 HRS </th>
							<th width="15%" align="left" valign="top">TOTAL LOADED</th>
							<th width="15%" align="left" valign="top">BALANCE</th>
							<th width="18%" align="left" valign="top">ETCD</th>
							</tr>
						   </thead>
						   <tbody id="pre_arrival_<?php echo $i+1;?>" class="todo-list">
						   <?php
						   for($i=0;$i<count($arr);$i++){ 
						   if(substr($arr[$i],0,2) == "LP"){
							 $sql16 = "select * from sof_slave_5 where MAPPINGID='".$mappingid."' and LOGIN='AGENT' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and PORT='LP' and PORTID='".$port_arr[$i]."'"; 
						   	$lpdailyqty = mysql_query($sql16);
							}}
							$rec16 = mysql_num_rows($lpdailyqty);
							if($rec16 == 0)
							{ 
							?>
							<tr>
								<td align="center" colspan="6" valign="middle">sorry currently there are zero(0) record</td>
							</tr>
							<?php }else{
							 for($i=0;$i<count($arr);$i++){ 
						   		if(substr($arr[$i],0,2) == "LP"){
							while($rows16 = mysql_fetch_assoc($lpdailyqty)){ 
							if(date("d-M-Y",strtotime($rows16['PRE_DATE'])) == '01-Jan-1970') {$pre_date = "";} else {$pre_date = date("d-M-Y",strtotime($rows16['PRE_DATE']));}
							if(date("d-M-Y",strtotime($rows16['ETCD'])) == '01-Jan-1970') {$etcd = "";} else {$etcd = date("d-M-Y H:i",strtotime($rows16['ETCD']));}
							?>
							<tr>
								<td align="left" valign="middle"><?php echo $pre_date;?></td>
								<td align="left" valign="middle"><?php echo $rows16['ENGAGEMENT_QTY'];?></td>
								<td align="left" valign="middle"><?php echo $rows16['LOAD_LAST'];?></td>
								<td align="left" valign="middle"><?php echo $rows16['TTL_LOAD'];?></td>
								<td align="left" valign="middle"><?php echo $rows16['BALANCE'];?></td>
								<td align="left" valign="middle"><?php echo $etcd;?></td>
							</tr>
							<?php }}}}?>
							</tbody>
							
							</table>
							</div><!-- /.box-body -->
						</div>                          
					</div><!-- /.col -->
				</div>
				
				<!------------------------ For Redelivery Notices  -------------------------->
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
							REDELIVERY NOTICES	
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				<?php 
				$etadate = array("ETA 30 DAYS","ETA 25 DAYS","ETA 20 DAYS","ETA 15 DAYS","ETA 10 DAYS","ETA 7 DAYS","ETA 5 DAYS","ETA 3 DAYS","ETA 2 DAYS","ETA 1 DAY");
				
				?>
				
				<div class="box-body table-responsive">
				<table aria-describedby="example1_info" id="example1" class="table table-bordered dataTable">
				<thead>
				<tr role="row">
				<th style="width: 12%; color:#fff; background-color:#3c8dbc;">&nbsp;</th>
				<?php 
				for($j=0;$j<count($arr);$j++){
				if(substr($arr[$j],0,2) == "LP"){
				?>
				<th style="width: 12%; color:#fff;  background-color:#3c8dbc;"><?php echo $arr[$j]; ?></th>
				<?php }} ?>
				</tr>
				</thead>
				<tr height="10" style="display:none;"><td><input type="hidden" name="txtETAdateSize" id="txtETAdateSize" class="form-control" autocomplete="off" value="<?php echo count($etadate);?>" /></td></tr>
				
				
				<?php 
				for($i=0;$i<count($etadate);$i++){ 
				?>
				<tr>
					<td width="12%" align="left" valign="top"><?php echo $etadate[$i]; ?><input type="hidden" name="txtredelivery_date_<?php echo $i; ?>" id="txtredelivery_date_<?php echo $i; ?>" class="form-control" autocomplete="off" value="<?php echo $etadate[$i];?>" /></td>
				
				<?php 
				 for($j=0;$j<count($arr);$j++){ 
				 if(substr($arr[$j],0,2) == "LP"){ 
				
				if($obj->getRedeliveryNotice($port_arr[$j],$rows['CHECKLIST_ID'],$etadate[$i])==""){ $redeliverydate = ""; } else { $redeliverydate = $obj->getRedeliveryNotice($port_arr[$j],$rows['CHECKLIST_ID'],$etadate[$i]); }
				?>
				<td width="17%" align="left" valign="top"><input type="text" name="txredelivery_<?php echo $i; ?>_<?php echo $j; ?>" id="txtentity_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $redeliverydate;?>" /></td>
				
				<?php }}} ?>
				</tr>
				</table>
				</div>
				
				<!------------------------ For PAYMENTS  -------------------------->
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
							PAYMENTS
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					   	95% FREIGHT PAYMENT
						<address>
						<?php echo $rows['FREIGHT_PAYMENT']; if($rows['FREIGHT_PAYMENT']=="1"){$style_15 = 'checked';} else {$style_15 = '';}?>
		<input type="radio" name="frepaycheckbox" <?php echo $style_15; ?> id="frepaycheckbox" class="form-control" value="1" /><label for="frepaycheckbox"></label>
						</address>
                        90% FREIGHT PAYMENT
						<address>
						<?php if($rows['FREIGHT_PAYMENT']=="2"){$style_15 = 'checked';} else {$style_15 = '';}?>
		<input type="radio" name="frepaycheckbox" <?php echo $style_15; ?> id="frepaycheckbox" class="form-control" value="2" /><label for="frepaycheckbox"></label>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					   	5% FREIGHT PAYMENT
						<address>
						<?php if($rows['FREIGHT_PAYMENT']=="1"){$style_15 = 'checked';} else {$style_15 = '';}?>
		<input type="radio" name="fivefrepaycheckbox" <?php echo $style_15; ?> id="fivefrepaycheckbox" class="form-control" value="1" /><label for="fivefrepaycheckbox"></label>
						</address>
                        10% FREIGHT PAYMENT
						<address>
						<?php if($rows['FREIGHT_PAYMENT']=="2"){$style_15 = 'checked';} else {$style_15 = '';}?>
		<input type="radio" name="fivefrepaycheckbox" <?php echo $style_15; ?> id="fivefrepaycheckbox" class="form-control" value="2" /><label for="frepaycheckbox"></label>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					   	DA ADVANCE  AGENT
						<address>
						<?php if($rows['DAADV_AGENT']=="1"){$style_16 = 'checked';} else {$style_16 = '';}?>
		<input type="checkbox" name="daadvagecheckbox" <?php echo $style_16; ?> id="daadvagecheckbox" class="form-control" value="1" /><label for="daadvagecheckbox"></label>
						</address>
					</div>
					
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					   	DA final
						<address>
						<?php //if($rows['FREIGHT_PAYMENT']=="1"){$style_15 = 'checked';} else {$style_15 = '';}?>
		<input type="checkbox" name="DAfinalcheckbox" <?php echo $style_15; ?> id="DAfinalcheckbox" class="form-control" value="1" /><label for="DAfinalcheckbox"></label>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">
					   	Hire
						<address>
						<?php //if($rows['FREIGHT_PAYMENT']=="1"){$style_15 = 'checked';} else {$style_15 = '';}?>
		<input type="checkbox" name="Hirecheckbox" <?php echo $style_15; ?> id="Hirecheckbox" class="form-control" value="1" /><label for="Hirecheckbox"></label>
						</address>
					</div>
					<div class="col-sm-4 invoice-col">&nbsp;</div>
				</div>
				
				<!------------------------ For DISCHARGE PORT  -------------------------->		
				<?php if(count($arr)!=0){ ?>
				<div class="row">
				<div class="col-xs-12">
					<h2 class="page-header">
						DISCHARGE PORT
					</h2>                            
				</div><!-- /.col -->
				</div>
				<div class="box-body table-responsive">
				<table aria-describedby="example1_info" id="example1" class="table table-bordered dataTable">	
				<thead>
					<tr role="row">
					<th style="width: 12%; color:#fff; background-color:#3c8dbc;">&nbsp;</th>
					<?php 
					for($j=0;$j<count($arr);$j++){
					if(substr($arr[$j],0,2) == "DP"){
					?>
					<th style="width: 12%; color:#fff;  background-color:#3c8dbc;"><?php echo $arr[$j]; ?></th>
					<?php }} ?>
					</tr>
				</thead>
				<?php 
				$arr_2 = array("PORT (S)","AGENT","DISCHARGE RATE","RECEIVER","REMARKS 3","","LOI FROM RECIVER","LOI TO OWNER","BL MANIFEST","DISCHARGE INSTRUCTION","REMARKS","","ETA 30 DAYS","ETA 25 DAYS","ETA 20 DAYS","ETA 15 DAYS","ETA 10 DAYS","ETA 7 DAYS","ETA 5 DAYS","ETA 3 DAYS","ETA 2 DAYS","ETA 1 DAY","NOR TENDERED","","COMMENCED","ETC/D","BALANCE","TOTAL","24HRS ","COMPLETED","VESSEL SAILED","DOCUMENT(S)","REMARKS 4");
				
				for($i=0;$i<count($arr_2);$i++)
				{
				if($i==5){
				?>
				<td width="12%" align="center" class="input-text"  valign="top" style="color:#000000; background-color:#f3f4f5">LOI</td>
				
				<?php } elseif($i==11){ ?>
				<td width="12%" align="center" class="input-text"  valign="top" style="color:#000000; background-color:#f3f4f5">DISCHARGE PORT ETA NOTICES</td>
				
				<?php } elseif($i==23){ ?>
				
				<td width="12%" align="center" class="input-text"  valign="top" style="color:#000000; background-color:#f3f4f5">DISCHARGE PORT VESSEL OPERATIONS</td>
				
				<?php } else{ ?>
				<tr>
				<td width="12%" align="left" class="input-text"  valign="top"><?php echo $arr_2[$i]; ?><input type="hidden" name="txtdpentity_<?php echo $i; ?>" id="txtdpentity_<?php echo $i; ?>" class="form-control" autocomplete="off" value="<?php echo $arr_2[$i];?>" /></td>
				<?php }
				for($j=0;$j<count($arr);$j++){
				if(substr($arr[$j],0,2) == "DP"){
				
				//if($i>11 && $i<21 || $i==24){$style = 'readonly="true"';}else{$style = '';}
				
				$sql_query = "select max(SOFSLAVEID) as max from sof_slave_5 where LOGIN='AGENT' and PORT='DP' and PORTID='".$port_arr[$j]."' and MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' ";
				$result = mysql_query($sql_query);
				$testres = mysql_fetch_assoc($result);
				
				$lpvessaloper = "select * from sof_slave_5 where SOFSLAVEID='".$testres['max']."'"; 
				$result1 = mysql_query($lpvessaloper);
				$testrec = mysql_fetch_assoc($result1);	
				$etc_date = date("d-M-Y",strtotime($testrec['ETCD']));
				$completed_date = date("d-M-Y",strtotime($testrec['PRE_DATE']));
				
				$comp_sql = "select MIN(SOFSLAVEID) as min from sof_slave_5 where LOGIN='".AGENT."' and PORT='".DP."' and PORTID='".$port_arr[$j]."' and MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";
				$rock_rs = mysql_query($comp_sql);
				$min_val = mysql_fetch_assoc($rock_rs);
				
				$comp_sql1 = "select * from sof_slave_5 where SOFSLAVEID='".$min_val['min']."'"; 
				$rock_rs1 = mysql_query($comp_sql1);
				$min_val = mysql_fetch_assoc($rock_rs1);	
				$commenced = date("d-M-Y",strtotime($min_val['PRE_DATE']));
				
				$sqlDp = "select * from sof_master where LOGIN='AGENT' and PORT='DP' and PORTID='".$port_arr[$j]."' and MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' ";
				$resultDp = mysql_query($sqlDp);
				$sofVesselsailesDp = mysql_fetch_assoc($resultDp);
				
				$pre_sqlDP = "select * from sof_slave_4 where LOGIN='AGENT' and PORT='DP' and PORTID='".$port_arr[$j]."' and MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";
				$pre_resDP = mysql_query($pre_sqlDP);
				$preArivalDp = mysql_fetch_assoc($pre_resDP);
				
				$sql2 = "select * from checklist_loadport where PORT='DP' and PORTID='".$port_arr[$j]."' and CHECKLIST_ID='".$rows['CHECKLIST_ID']."' and ENTITY_NAME='".$arr_2[$i]."'";
				$res2 = mysql_query($sql2);
				$rows2 = mysql_fetch_assoc($res2);
				//echo'<pre>';print_r($rows2);
				if($i==5){
				?>
				<td width="12%" align="center" class="input-text"  valign="top" style="color:#000000; background-color:#f3f4f5"></td>
				
				<?php } elseif($i==11){ ?>
				<td width="12%" align="center" class="input-text"  valign="top" style="color:#000000; background-color:#f3f4f5"></td>
				
				<?php } elseif($i==23){ ?>
				<td width="12%" align="center" class="input-text"  valign="top" style="color:#000000; background-color:#f3f4f5"></td>
				
				<?php } elseif($i==4 || $i==10 || $i==32){ ?>
				<td width="12%" align="left" class="input-text"  valign="top"><textarea name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control areasize" autocomplete="off" <?php echo $style;?>/><?php echo $rows2['ENTITY_VALUE'];?></textarea></td>
				
				<?php } elseif($i>5 && $i<11){ ?>
				<?php if($rows2['ENTITY_VALUE']=="1"){$style_17 = 'checked';} else {$style_17 = '';}?>
					<td width="12%" align="left"  class="input-text"  valign="top"><input type="checkbox" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" <?php echo $style_17; ?>  class="form-control"  value="1" /><label for="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>"></label></td>
					
				<!------------------- for COMMENCED ------------------>
					<?php } elseif($i==24 && $sofVesselsailesDp['LC'] == "" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==24 && $sofVesselsailesDp['LC']!="" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesDp['LC'];?>" /></td>
					
					<?php } elseif($i==24 && $sofVesselsailesDp['LC']=="" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==24 && $sofVesselsailesDp['LC']!="" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesDp['LC'];?>" /></td>
				
				<!------------------- for ETC/D ------------------>
					<?php } 
					elseif($i==25 && $etc_date == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==25 && $etc_date!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $etc_date;?>" /></td>
					
					<?php } elseif($i==25 && $etc_date=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==25 && $etc_date!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $etc_date;?>" /></td>
						
					<!------------------- for BALANCE ------------------>
					
					<?php } elseif($i==26 && $testrec['BALANCE']=="0" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==26 && $testrec['BALANCE']!="0" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['BALANCE'];?>" /></td>
					
					<?php } elseif($i==26 && $testrec['BALANCE']=="" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==26 && $testrec['BALANCE']!="0" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['BALANCE'];?>" /></td>
					
					<!------------------- for TOTAL ------------------>
					
					<?php } elseif($i==27 && $testrec['TTL_LOAD']=="0" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==27 && $testrec['TTL_LOAD']!="0" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['TTL_LOAD'];?>"/></td>
					
					<?php } elseif($i==27 && $testrec['TTL_LOAD']=="" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==27 && $testrec['TTL_LOAD']!="0" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['TTL_LOAD'];?>" /></td>
					
					<!------------------- for 24HRS ------------------>
					
					<?php } elseif($i==28 && $testrec['LOAD_LAST']=="0" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==28 && $testrec['LOAD_LAST']!="0" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['LOAD_LAST'];?>" /></td>
					
					<?php } elseif($i==28 && $testrec['LOAD_LAST']=="" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text"  name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==28 && $testrec['LOAD_LAST']!="0" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $testrec['LOAD_LAST'];?>" /></td>
					
					<!------------------- for COMPLETED ------------------>
					
					<?php } elseif($i==29 && $sofVesselsailesDp['LC1'] == "" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==29 && $sofVesselsailesDp['LC1']!="" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesDp['LC1'];?>" /></td>
					
					<?php } elseif($i==29 && $sofVesselsailesDp['LC1']=="" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==29 && $sofVesselsailesDp['LC1']!="" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesDp['LC1'];?>" /></td>
					
					<!------------------- for VESSEL SAILED ------------------>
					
					<?php } elseif($i==30 && $sofVesselsailesDp['VS'] == "" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==30 && $sofVesselsailesDp['VS']!="" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesDp['VS'];?>" /></td>
					
					<?php } elseif($i==30 && $sofVesselsailesDp['VS']=="" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==30 && $sofVesselsailesDp['VS']!="" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $sofVesselsailesDp['VS'];?>" /></td>
					
					<!------------------- for PRE ARR ETA 30 DAYS ------------------>
					
					<?php } elseif($i==12 && date("d-M-Y",strtotime($preArivalDp['ETA_30_DAYS'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==12 && date("d-M-Y",strtotime($preArivalDp['ETA_30_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_30_DAYS']));?>" <?php echo $style;?>/></td>
					
					<?php } elseif($i==12 && date("d-M-Y",strtotime($preArivalDp['ETA_30_DAYS']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==12 && date("d-M-Y",strtotime($preArivalDp['ETA_30_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_30_DAYS']));?>" /></td>
					
					<!------------------- for PRE ARR ETA 25 DAYS ------------------>
					
					<?php } elseif($i==13 && date("d-M-Y",strtotime($preArivalDp['ETA_25_DAYS'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==13 && date("d-M-Y",strtotime($preArivalDp['ETA_25_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_25_DAYS']));?>" <?php echo $style;?>/></td>
					
					<?php } elseif($i==13 && date("d-M-Y",strtotime($preArivalDp['ETA_25_DAYS']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==13 && date("d-M-Y",strtotime($preArivalDp['ETA_25_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_25_DAYS']));?>" /></td>
					
					<!------------------- for PRE ARR ETA 20 DAYS ------------------>
					
					<?php } elseif($i==14 && date("d-M-Y",strtotime($preArivalDp['ETA_20_DAYS'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==14 && date("d-M-Y",strtotime($preArivalDp['ETA_20_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_20_DAYS']));?>" <?php echo $style;?>/></td>
					
					<?php } elseif($i==14 && date("d-M-Y",strtotime($preArivalDp['ETA_20_DAYS']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==14 && date("d-M-Y",strtotime($preArivalDp['ETA_20_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_20_DAYS']));?>" /></td>
					
					<!------------------- for PRE ARR ETA 7 DAYS ------------------>
					
					<?php } elseif($i==15 && date("d-M-Y",strtotime($preArivalDp['ETA_15_DAYS'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==15 && date("d-M-Y",strtotime($preArivalDp['ETA_15_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_15_DAYS']));?>" <?php echo $style;?>/></td>
					
					<?php } elseif($i==15 && date("d-M-Y",strtotime($preArivalDp['ETA_15_DAYS']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==15 && date("d-M-Y",strtotime($preArivalDp['ETA_15_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_15_DAYS']));?>" /></td>
					
					<!------------------- for PRE ARR ETA 10 DAYS ------------------>
					
					<?php } elseif($i==16 && date("d-M-Y",strtotime($preArivalDp['ETA_10_DAYS'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==16 && date("d-M-Y",strtotime($preArivalDp['ETA_10_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_10_DAYS']));?>" <?php echo $style;?>/></td>
					
					<?php } elseif($i==16 && date("d-M-Y",strtotime($preArivalDp['ETA_10_DAYS']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==16 && date("d-M-Y",strtotime($preArivalDp['ETA_10_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_10_DAYS']));?>" /></td>
					
					<!------------------- for PRE ARR ETA 7 DAYS ------------------>
					
					<?php } elseif($i==17 && date("d-M-Y",strtotime($preArivalDp['ETA_7_DAYS'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==17 && date("d-M-Y",strtotime($preArivalDp['ETA_7_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_7_DAYS']));?>" <?php echo $style;?>/></td>
					
					<?php } elseif($i==17 && date("d-M-Y",strtotime($preArivalDp['ETA_7_DAYS']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==17 && date("d-M-Y",strtotime($preArivalDp['ETA_7_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_7_DAYS']));?>" /></td>
					
					<!------------------- for PRE ARR ETA 5 DAYS ------------------>
					
					<?php } elseif($i==18 && date("d-M-Y",strtotime($preArivalDp['ETA_5_DAYS'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==18 && date("d-M-Y",strtotime($preArivalDp['ETA_5_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_5_DAYS']));?>" /></td>
					
					<?php } elseif($i==18 && date("d-M-Y",strtotime($preArivalDp['ETA_5_DAYS']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==18 && date("d-M-Y",strtotime($preArivalDp['ETA_5_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_5_DAYS']));?>" /></td>
					
					<!------------------- for PRE ARR ETA 3 DAYS ------------------>
					
					<?php } elseif($i==19 && date("d-M-Y",strtotime($preArivalDp['ETA_3_DAYS'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==19 && date("d-M-Y",strtotime($preArivalDp['ETA_3_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_3_DAYS']));?>" /></td>
					
					<?php } elseif($i==19 && date("d-M-Y",strtotime($preArivalDp['ETA_3_DAYS']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==19 && date("d-M-Y",strtotime($preArivalDp['ETA_3_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_3_DAYS']));?>" /></td>
					
					<!------------------- for PRE ARR ETA 2 DAYS ------------------>
					
					<?php } elseif($i==20 && date("d-M-Y",strtotime($preArivalDp['ETA_2_DAYS'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==20 && date("d-M-Y",strtotime($preArivalDp['ETA_2_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_2_DAYS']));?>" /></td>
					
					<?php } elseif($i==20 && date("d-M-Y",strtotime($preArivalDp['ETA_2_DAYS']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==20 && date("d-M-Y",strtotime($preArivalDp['ETA_2_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_2_DAYS']));?>" /></td>
					
					<!------------------- for PRE ARR ETA 1 DAYS ------------------>
					
					<?php } elseif($i==21 && date("d-M-Y",strtotime($preArivalDp['ETA_1_DAYS'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==21 && date("d-M-Y",strtotime($preArivalDp['ETA_1_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_1_DAYS']));?>" /></td>
					
					<?php } elseif($i==21 && date("d-M-Y",strtotime($preArivalDp['ETA_1_DAYS']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==21 && date("d-M-Y",strtotime($preArivalDp['ETA_1_DAYS']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['ETA_1_DAYS']));?>" /></td>
					
					<!------------------- for PRE ARR NOR TENDERED ------------------>
					
					<?php } elseif($i==22 && date("d-M-Y",strtotime($preArivalDp['NOR_TENDERED'])) == "01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="" /></td>
						
					<?php } elseif($i==22 && date("d-M-Y",strtotime($preArivalDp['NOR_TENDERED']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']==""){ ?>
					
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['NOR_TENDERED']));?>" /></td>
					
					<?php } elseif($i==22 && date("d-M-Y",strtotime($preArivalDp['NOR_TENDERED']))=="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
					
					<?php } elseif($i==22 && date("d-M-Y",strtotime($preArivalDp['NOR_TENDERED']))!="01-Jan-1970" && $rows2['ENTITY_VALUE']!=""){ ?>
					<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo date("d-M-Y H:i",strtotime($preArivalDp['NOR_TENDERED']));?>" /></td>
					
					<!------------------- end ------------------>
				<?php } elseif($i>11 && $i<23) { ?>
				<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" onClick="Remove_Date('txtdpid_<?php echo $i; ?>_<?php echo $j; ?>')" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" >/></td>	
				
				<?php } else {?>
				<td width="12%" align="left" class="input-text"  valign="top"><input type="text" name="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" id="txtdpid_<?php echo $i; ?>_<?php echo $j; ?>" class="form-control" autocomplete="off" value="<?php echo $rows2['ENTITY_VALUE'];?>" /></td>
				<?php }}} ?>
				</tr>
				<?php }?>
				<tr height="10" style="display:none;"><td><input type="hidden" name="txtDPSize" id="txtDPSize" class="form-control" autocomplete="off" value="<?php echo count($arr_2);?>" /></td></tr>
				</table>
				</div>
				
				<?php } ?>
				
				<!------------------------ For LOAD PORT DAILY QTY  -------------------------->
				<div class="row">
					<div class="col-xs-12">
						<h2 class="page-header">
						  DISCHARGE PORT DAILY QTY
						</h2>                            
					</div><!-- /.col -->
				</div>
				
				<div class="row">
				   <div class="col-xs-12">
					  <div class="box box-primary">
						 <div class="box-body no-padding" style="overflow:auto;">
						  <table class="table table-striped" >
						   <thead>
							<tr>
							<th width="12%" align="left" valign="top">DATE</th>
							<th width="15%" align="left" valign="top">ENGAGEMENT QTY</th>
							<th width="20%" align="left" valign="top">LOAD LAST 24 HRS </th>
							<th width="15%" align="left" valign="top">TOTAL LOADED</th>
							<th width="15%" align="left" valign="top">BALANCE</th>
							<th width="18%" align="left" valign="top">ETCD</th>
							</tr>
						   </thead>
						   <tbody id="pre_arrival_<?php echo $i+1;?>" class="todo-list">
						   <?php
						   for($i=0;$i<count($arr);$i++){ 
						   if(substr($arr[$i],0,2) == "DP"){
							 $sql16 = "select * from sof_slave_5 where MAPPINGID='".$mappingid."' and LOGIN='AGENT' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and PORT='DP' and PORTID='".$port_arr[$i]."'"; 
						   	$lpdailyqty = mysql_query($sql16);
							}}
							$rec16 = mysql_num_rows($lpdailyqty);
							if($rec16 == 0)
							{ 
							?>
							<tr>
								<td align="center" colspan="6" valign="middle">sorry currently there are zero(0) record</td>
							</tr>
							<?php }else{
							 for($i=0;$i<count($arr);$i++){ 
						   		if(substr($arr[$i],0,2) == "LP"){
							while($rows16 = mysql_fetch_assoc($lpdailyqty)){ 
							if(date("d-M-Y",strtotime($rows16['PRE_DATE'])) == '01-Jan-1970') {$pre_date = "";} else {$pre_date = date("d-M-Y",strtotime($rows16['PRE_DATE']));}
							if(date("d-M-Y",strtotime($rows16['ETCD'])) == '01-Jan-1970') {$etcd = "";} else {$etcd = date("d-M-Y H:i",strtotime($rows16['ETCD']));}
							?>
							<tr>
								<td align="left" valign="middle"><?php echo $pre_date;?></td>
								<td align="left" valign="middle"><?php echo $rows16['ENGAGEMENT_QTY'];?></td>
								<td align="left" valign="middle"><?php echo $rows16['LOAD_LAST'];?></td>
								<td align="left" valign="middle"><?php echo $rows16['TTL_LOAD'];?></td>
								<td align="left" valign="middle"><?php echo $rows16['BALANCE'];?></td>
								<td align="left" valign="middle"><?php echo $etcd;?></td>
							</tr>
							<?php }}}}?>
							</tbody>
							
							</table>
							</div><!-- /.box-body -->
						</div>                          
					</div><!-- /.col -->
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
					  FILED TO FOLDER
						<address>
						<?php if($rows['LP_FTF']=="1"){$style_18 = 'checked';} else {$style_18 = '';}?>
							<input type="checkbox" name="ftfcheckbox" <?php echo $style_18; ?> id="ftfcheckbox" class="form-control" value="1" />
						</address>
					</div>
					<div class="col-sm-4 invoice-col"></div>
					<div class="col-sm-4 invoice-col"></div>
				</div>
				
				<div class="row invoice-info">
					<div class="col-sm-6 invoice-col">
					 REMARKS
						<address>
						<textarea class="form-control areasize" name="remark" id="remark"><?php echo $rows['REMARKS'];?></textarea>
						</address>
					</div>
					<div class="col-sm-4 invoice-col"></div>
					<div class="col-sm-2 invoice-col">
					SIGNATURE
						<address>
						&nbsp;&nbsp;
						</address>
					</div>
				</div>
				<?php if($rights == 1){ ?>
				<div class="box-footer" align="right">
					<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
					<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
				</div>
				<?php } ?>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#dem_rate").numeric();
$("[id^=txtlpid_28_],[id^=txtlpid_29_],[id^=txtlpid_30_],[id^=txtdpid_26_],[id^=txtdpid_27_],[id^=txtdpid_28_]").numeric(); 

/*$("#date, [id^=txtlpid_12_],[id^=txtlpid_13_],[id^=txtlpid_14_],[id^=txtlpid_15_],[id^=txtlpid_16_], [id^=txtlpid_17_],[id^=txtlpid_18_],[id^=txtlpid_19_],[id^=txtlpid_20_],[id^=txtlpid_21_],[id^=txtlpid_22_],[id^=txtlpid_23_],[id^=txtlpid_24_],[id^=txtlpid_26_],[id^=txtlpid_27_],[id^=txtlpid_31_],[id^=txtlpid_32_],[id^=txtdpid_12_], [id^=txtdpid_13_],[id^=txtdpid_14_],[id^=txtdpid_15_],[id^=txtdpid_16_],[id^=txtdpid_17_],[id^=txtdpid_18_],[id^=txtdpid_19_],[id^=txtdpid_20_],[id^=txtdpid_21_],[id^=txtdpid_22_],[id^=txtdpid_24_],[id^=txtdpid_25_],[id^=txtdpid_29_],[id^=txtdpid_30_], [id^=txtentity_]").datetimepicker({
	format: 'dd-mm-yyyy hh:ii', 
	autoclose: true,
	todayBtn: true,
	minuteStep: 1	
});*/

$("#date, [id^=txtlpid_12_],[id^=txtlpid_13_],[id^=txtlpid_14_],[id^=txtlpid_15_],[id^=txtlpid_16_], [id^=txtlpid_17_],[id^=txtlpid_18_],[id^=txtlpid_19_],[id^=txtlpid_20_],[id^=txtlpid_21_],[id^=txtlpid_22_],[id^=txtlpid_23_],[id^=txtlpid_24_],[id^=txtlpid_26_],[id^=txtlpid_27_],[id^=txtlpid_31_],[id^=txtlpid_32_],[id^=txtdpid_12_], [id^=txtdpid_13_],[id^=txtdpid_14_],[id^=txtdpid_15_],[id^=txtdpid_16_],[id^=txtdpid_17_],[id^=txtdpid_18_],[id^=txtdpid_19_],[id^=txtdpid_20_],[id^=txtdpid_21_],[id^=txtdpid_22_],[id^=txtdpid_24_],[id^=txtdpid_25_],[id^=txtdpid_29_],[id^=txtdpid_30_], [id^=txtentity_]").datetimepicker({
		format: 'dd-mm-yyyy hh:ii',
	    autoclose: true,
        todayBtn: true,
        minuteStep: 1
		});

});

function getPdf(var1)
{
	location.href='allPdf.php?id=26&mappingid='+var1;
}

function Remove_Date(var1)
{
//alert(var1);
$('#'+var1).val(''); 
}

function addUI_Row(var1)
{
		var id = $("#txtBOL_"+var1).val();
		//alert(id);
		if(id==0){
		id  = (id - 1 )+ 2;
		
		$('<tr><td width="12%" align="left" valign="top"><input type="text" name="txtfty_Name_'+id+'" id="txtfty_Name_'+id+'" class="form-control" autocomplete="off" value="" /></td><?php for($x=0;$x<count($arr);$x++){ if(substr($arr[$x],0,2) == "LP"){ ?><td align="left" width="12%" valign="top" ><input type="text" name="txtfty_Value_'+id+'_<?php echo $x; ?>" id="txtfty_Value_'+id+'_<?php echo $x; ?>" class="form-control" autocomplete="off" value="" /></td><?php }}?></tr>').appendTo("#sort1");
		$("#txtBOL_"+var1).val(id);	
		}
		else
		{
			if($("#txtfty_Name_"+id).val() != "" && $("#txtfty_Name_"+id).val != "" )
			{
				id  = (id - 1 )+ 2;
		
				$('<tr><td width="12%" align="left" valign="top"><input type="text" name="txtfty_Name_'+id+'" id="txtfty_Name_'+id+'" class="form-control" autocomplete="off" value="" /></td><?php for($x=0;$x<count($arr);$x++){ if(substr($arr[$x],0,2) == "LP"){ ?><td align="left" width="12%" valign="top" ><input type="text" name="txtfty_Value_'+id+'_<?php echo $x; ?>" id="txtfty_Value_'+id+'_<?php echo $x; ?>" class="form-control" autocomplete="off" value="" /></td><?php }}?></tr>').appendTo("#sort1");
				$("#txtBOL_"+var1).val(id);	
			}
			else
			{
				jAlert('Please fill the above entries.', 'Alert');
			}
		}
}
	
</script>
    </body>
</html>
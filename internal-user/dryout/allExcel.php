<?php
session_start();
ini_set("include_path", "../../PEAR");  
include_once 'Spreadsheet/Excel/Writer.php';
require_once("../../includes/functions_internal_user_dryout.inc.php");
@$i  = $_REQUEST['id'];
$ret = "";
switch ($i)
{
  case 1:
        $ret = Total_vendor_excel();
				break;
  case 2:
        $ret = shipment_register_excel();
				break;
  case 3:
        $ret = sof_excel();
				break;
  case 4:
  		$ret = daily_log_report_excel();
				break;
  case 5:
  		$ret = vessels_in_port_excel();
				break;			
	 case 6:
  		$ret = laytime_excel();
				break;
	 case 7:
  		$ret = cargo_open_position_excel();
				break;
	 case 8:
  		$ret = getCOAReportDataPdf_excel();
				break;
	case 9:
  		$ret = agent_list_excel();
				break;
	case 10:
  		$ret = spot_fixture_pdf();
				break;
	case 11:
  		$ret = demmurage_summary_pdf();
				break;
	case 12:
  		$ret = owner_broker_fixture_excel();
				break;
	case 13:
  		$ret = pni_club_declaration_excel();
				break;
	case 14:
  		$ret = dead_freight_summery_excel();
				break;
	case 15:
  		$ret = profitability_excel();
				break;
	case 16:
  		$ret = coa_excel();
				break;
	case 17:
  		$ret = aging_payable_excel();
				break;
	case 18:
  		$ret = aging_receivable_excel();
				break;
	case 20:
  		$ret = material_excel();
				break;
	case 21:
  		$ret = vendor_excel();
				break;
	case 22:
  		$ret = getProfitabilityHeadWiseReport();
				break;
	case 23:
  		$ret = getCompareSheetReport();
				break;
	case 24:
  		$ret = getCharteringRegisterReport();
				break;
	case 25:
  		$ret = getCharteringRegisterDetailedReport();
				break;
	case 27:
  		$ret = getCompareSheetReportTC();
				break;
  	default:
		//$ret = funError();
			//echo $ret;
				break; 				
}

function getCompareSheetReportTC()
{
	$comid          =  $_REQUEST['comid'];
	$obj = new data();
	$obj->funConnect();
	$sql11 = "select chartering_estimate_tc_compare.COMID as COMID, TC_DATE, TC_NO, CP_DATE1 as CP_DATE, SUMMER_DWT, VESSEL_IMO_ID, VESSEL_TYPE from chartering_estimate_tc_compare inner join chartering_estimate_tc_master on chartering_estimate_tc_master.COMID=chartering_estimate_tc_compare.COMID where chartering_estimate_tc_master.COMID='".$comid."' order by chartering_estimate_tc_master.TCOUTID desc";
	$result11 = mysql_query($sql11) or die($sql11);
	$rows11 = mysql_fetch_assoc($result11);
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	
	$worksheet =& $workbook->addWorksheet('Compare Sheets');
	// creating different different formats for displaying data 
		
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	
	$body_text = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_left = & $workbook->addFormat(array('Size' => 8,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_left1 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_right = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'Right', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$body_text_right1 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'Center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 25.00);
	$worksheet->setColumn(1, 1, 25.00);
	$worksheet->setColumn(2, 2, 25.00);
	$worksheet->setColumn(3, 3, 25.00);
	$worksheet->setColumn(4, 4, 25.00);
	$worksheet->setColumn(5, 5, 25.00);
	$worksheet->setColumn(6, 6, 25.00);
	$worksheet->setColumn(7, 7, 25.00);
	$worksheet->setColumn(8, 8, 25.00);
	$worksheet->setColumn(9, 9, 25.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,6,"Compare Sheets",$main_header);
	
	$worksheet->setRow(2,15.75);$worksheet->setRow(3,15.75);$worksheet->setRow(4,15.75);$worksheet->setRow(5,15.75);
	$worksheet->writeString(2,0,"Vessel Name :",$body_text_left);
	$worksheet->writeString(2,1,$obj->getVesselIMOData($rows11['VESSEL_IMO_ID'],"VESSEL_NAME"),$body_text_left1);
	$worksheet->writeString(2,2,"Vessel Type :",$body_text_left);
	$worksheet->writeString(2,3,$rows11['VESSEL_TYPE'],$body_text_left1);
	$worksheet->writeString(2,4,"DWT Summer :",$body_text_left);
	$worksheet->writeString(2,5,$rows11['SUMMER_DWT'],$body_text_left1);
	
	$worksheet->writeString(3,0,"TC Date :",$body_text_left);
	$worksheet->writeString(3,1,date('d-m-Y',strtotime($rows11['TC_DATE'])),$body_text_left1);
	$worksheet->writeString(3,2,"CP Date:",$body_text_left);
	$worksheet->writeString(3,3,date('d-m-Y',strtotime($rows11['CP_DATE'])),$body_text_left1);
	
	$worksheet->writeString(3,4,"TC No.:",$body_text_left);
	$worksheet->writeString(3,5,$rows11['TC_NO'],$body_text_left1);
	
	$worksheet->writeString(5,0,"Sheet Name/Parameters",$body_text_left);
	$sql = "SELECT * FROM chartering_estimate_tc_master where COMID='".$comid."' order by TCOUTID asc";
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	$width = 90/$num;
	$result = mysql_query($sql);
	$i = 1;
	while($rows= mysql_fetch_assoc($result))
	{
		$sql1 = "select SHEET_NAME from cost_sheettc_name_master where COST_SHEETID='".$rows['SHEET_NO']."' order by COST_SHEETID ";
		$res1 = mysql_query($sql1);
		$rows1 = mysql_fetch_assoc($res1);
		if(isset($rows1['SHEET_NAME']))
		{
		    $worksheet->writeString(6,$i,$rows1['SHEET_NAME'],$body_text_left);
		}
		else
		{
			$worksheet->writeString(6,$i,"Fixture TC",$body_text_left);
		}
		$i++;
	}
	
	
	$worksheet->writeString(7,0,"REVENUE",$body_text);
	
	
	$worksheet->writeString(8,0,"Daily Gross Hire(USD/Day)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select DAILY_GROSS_HIRE_EST from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['DAILY_GROSS_HIRE_EST'];
		}
		$worksheet->writeNumber(8,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	
	
	$worksheet->writeString(9,0,"Add Comm(USD)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select ADD_COMM_CAL_EST from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['ADD_COMM_CAL_EST'];
		}
		$worksheet->writeNumber(9,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(10,0,"Broker's Comm(USD)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select BROKER_COMM_CAL_EST from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['BROKER_COMM_CAL_EST'];
		}
		$worksheet->writeNumber(10,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(11,0,"Nett Hire(USD/day)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select NETT_HIRE_EST from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['NETT_HIRE_EST'];
		}
		$worksheet->writeNumber(11,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(12,0,"Nett Rev(USD)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select TOTAL_REV_EST from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['TOTAL_REV_EST'];
		}
		$worksheet->writeNumber(12,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(13,0,"Less Off Hire(USD)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select LESS_OFF_HIRE_EST from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['LESS_OFF_HIRE_EST'];
		}
		$worksheet->writeNumber(13,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(14,0,"CVE(USD)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select CVE_EST from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['CVE_EST'];
		}
		$worksheet->writeNumber(14,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(15,0,"Nett Hire to invoice(USD)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select NET_HIRE_AMT from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['NET_HIRE_AMT'];
		}
		//$array_revenue[$rows['TCOUTID']][] = array_sum($arr_t);
		$worksheet->writeNumber(15,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	
	
	$worksheet->writeString(16,0,"Other Income(USD)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select NET_HIRE_AMT from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_tt = array();
			$sql3 = "select OTHER_AMT from chartering_estimate_tc_slave2 where STATUS=1 and TC_SLAVE1ID='".$rows2['TC_SLAVE1ID']."'";
			$result3 = mysql_query($sql3);
			while($rows3 = mysql_fetch_assoc($result3))
			{
			    $arr_tt[] = $rows3['OTHER_AMT'];
			}
			$arr_t[] = array_sum($arr_tt);
		}
		$worksheet->writeNumber(16,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	
	
	$worksheet->writeString(17,0,"Total Revenue(USD)",$body_text);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select NET_HIRE_AMT from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['NET_HIRE_AMT'];
		}
		$worksheet->writeNumber(17,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	

	$worksheet->writeString(18,0,"EXPENSES",$body_text);
	
	$worksheet->writeString(19,0,"Other expenses(USD)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select TC_SLAVE1ID from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_tt = array();
			$sql3 = "select OTHER_AMT from chartering_estimate_tc_slave2 where STATUS=2 and TC_SLAVE1ID='".$rows2['TC_SLAVE1ID']."'";
			$result3 = mysql_query($sql3);
			while($rows3 = mysql_fetch_assoc($result3))
			{
			    $arr_tt[] = $rows3['OTHER_AMT'];
			}
			$arr_t[] = array_sum($arr_tt);
		}
		$worksheet->writeNumber(19,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(20,0,"Total Expenses(USD)",$body_text);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select TC_SLAVE1ID from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_tt = array();
			$sql3 = "select OTHER_AMT from chartering_estimate_tc_slave2 where STATUS=2 and TC_SLAVE1ID='".$rows2['TC_SLAVE1ID']."'";
			$result3 = mysql_query($sql3);
			while($rows3 = mysql_fetch_assoc($result3))
			{
			    $arr_tt[] = $rows3['OTHER_AMT'];
			}
			$arr_t[] = array_sum($arr_tt);
		}
		$worksheet->writeNumber(20,$i,array_sum($arr_t),$body_text_right);
		$i++;
	}
	
	$j = 20;
	$j++;
	
	$worksheet->writeString($j,0,"RESULTS",$body_text_left);
	$j++;
	$worksheet->writeString($j,0,"TC Earnings(USD)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select VOYAGE_EARN_EST from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['VOYAGE_EARN_EST'];
		}
		$worksheet->writeNumber($j,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}$j++;
	
	
	$worksheet->writeString($j,0,"Profit/Day(USD)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_t = array();
		$sql2 = "select PROFIT_PER_DAY_EST from chartering_tc_estimate_slave1 where TCOUTID='".$rows['TCOUTID']."'";
		$result2 = mysql_query($sql2);
		while($rows2= mysql_fetch_assoc($result2))
		{
			$arr_t[] = $rows2['PROFIT_PER_DAY_EST'];
		}
		$worksheet->writeNumber($j,$i,implode('/',$arr_t),$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"P/L",$body_text);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['PROFIT_LOSS'],$body_text_right);
		$i++;
	}$j++;
	
	$workbook->send('Compare Sheets ('.$obj->getVesselIMOData($rows11['VESSEL_IMO_ID'],"VESSEL_NAME").') '." ".date('d-m-Y',strtotime($rows11['CP_DATE'])).".xls"); 
	$workbook->close();
	
}




function getCharteringRegisterDetailedReport()
{
	$txtFrom_Date   = strtotime($_REQUEST['txtFrom_Date']); 
	$txtTo_Date     = strtotime($_REQUEST['txtTo_Date']);
	$selBType       =  $_REQUEST['selBType'];
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Chartering Register');
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center3 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'Right', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center1 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center2 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'Right', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	$worksheet->freezePanes(array(4, 0));
	
	$worksheet->setColumn(0, 0, 10.00);
	$worksheet->setColumn(1, 1, 10.00);
	$worksheet->setColumn(2, 4, 15.00);
	$worksheet->setColumn(5, 5, 10.00);
	$worksheet->setColumn(6, 6, 15.00);
	$worksheet->setColumn(7, 7, 12.00);
	$worksheet->setColumn(8, 8, 25.00);
	$worksheet->setColumn(9, 9, 25.00);
	$worksheet->setColumn(10, 10, 12.00);
	$worksheet->setColumn(11, 11, 21.00);
	$worksheet->setColumn(12, 29, 12.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,0);
	$worksheet->formatmergeCellsMultiple(0,0,0,27,"Chartering Register - Detailed [ From(".date("d-M-Y",strtotime($_REQUEST['txtFrom_Date'])).") - To(".date("d-M-Y",strtotime($_REQUEST['txtTo_Date'])).") ]",$main_header);
	
	$worksheet->setRow(2,20);
	
	$worksheet->formatmergeCellsMultiple(2,0,3,0,"Sr. No.",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,1,3,1,"Nom Id",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,2,3,2,"Vessel",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,3,3,3,"CPIC",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,4,3,4,"OPS PIC",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,5,3,5,"DWT",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,6,3,6,"Cargo Name",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,7,3,7,"CP Date",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,8,3,8,"Owner Name",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,9,3,9,"Charterer",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,10,3,10,"Fixture Date",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,11,3,11,"Voyage No./ Voyage Name",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,12,2,19,"Initial P & L",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,12,3,12,"P & L",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,13,3,13,"Revenue",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,14,3,14,"Demurrage /Dispatch",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,15,3,15,"Operational Expenses",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,16,3,16,"Port Expenses",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,17,3,17,"Bunker Expenses",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,18,3,18,"Hire Due",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,19,3,19,"Hire Paid",$main_header_1_center);
	
	$worksheet->formatmergeCellsMultiple(2,20,2,27,"Final P & L",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,20,3,20,"P & L",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,21,3,21,"Revenue",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,22,3,22,"Demurrage /Dispatch",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,23,3,23,"Operational Expenses",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,24,3,24,"Port Expenses",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,25,3,25,"Bunker Expenses",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,26,3,26,"Hire Due",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(3,27,3,27,"Hire Paid",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,28,3,28,"Remarks",$main_header_1_center);
	
	
	$txtFrom_Date = date('Y-m-d',strtotime($_REQUEST['txtFrom_Date']));
	$txtTo_Date   =  date('Y-m-d',strtotime($_REQUEST['txtTo_Date']));
	
	$sql = "select freight_cost_estimate_compare.COMID as COMID, freight_cost_estimete_master.VESSEL_IMO_ID as VESSEL_IMO_ID, freight_cost_estimate_compare.MESSAGE as MESSAGE, freight_cost_estimete_master.CP_DATE as CP_DATE, freight_cost_estimete_master.DWT_SUMMER as DWT_SUMMER, freight_cost_estimete_master.DWT_TOPICAL as DWT_TOPICAL, freight_cost_estimete_master.QTY_TYPE_RADIO as QTY_TYPE_RADIO, freight_cost_estimete_master.TRANS_DATE as TRANS_DATE, freight_cost_estimete_master.FCAID as FCAID, freight_cost_estimete_master.VOYAGE_NO as VOYAGE_NO, freight_cost_estimete_master.VOYAGE_NAME as VOYAGE_NAME from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS in (1,2) and freight_cost_estimete_master.TRANS_DATE>='".$txtFrom_Date."' and freight_cost_estimete_master.TRANS_DATE<='".$txtTo_Date."'  and freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' group by freight_cost_estimate_compare.COMID"; 
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$k = 4;
	if($rec == 0)
	{
		$worksheet->formatmergeCellsMultiple($k,0,$k,14,"SORRY CURRENTLY THERE ARE ZERO(0) RECORDS FOUND",$body_text_center);
	}
	else
	{   
		$l = 1;
	    $arr_sumin = $arr_sumout = array();
		while($rows = mysql_fetch_assoc($res))
		{
		    $l_cost_sheet_id = $obj->getLatestCostSheetID($rows['COMID']);
			$cpdate = $obj->getCompareEstimateData($rows['COMID'],"CP_DATE");
			if($cpdate!="0000-00-00" && $cpdate!=""){$cpdate = date('d-m-Y',strtotime($cpdate));}else{$cpdate = "";}
			$fixturedate = date('d-m-Y',strtotime($obj->getCompareEstimateData($rows['COMID'],"TRANS_DATE")));
			
			$arr_sumin1 = $arr_sumout1 = array();
			$worksheet->setRow($k,20);
			$worksheet->writeString($k,0,$l,$body_text_center);
			$worksheet->writeString($k,1,$rows['MESSAGE'],$body_text_center);
			$worksheet->writeString($k,2,$obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME"),$body_text_center1);
			$worksheet->writeString($k,3,'',$body_text_center1);
			$worksheet->writeString($k,4,'',$body_text_center1);
			$worksheet->writeNumber($k,5,$obj->getCompareEstimateData($rows['COMID'],"DWT_SUMMER"),$body_text_center);
			$worksheet->writeString($k,6,$obj->getMaterialCodeDesBasedOnId($obj->getCompareEstimateData($rows['COMID'],"CARGO_ID")),$body_text_center);
			$worksheet->writeString($k,7,$cpdate,$body_text_center1);
			$worksheet->writeString($k,8,$obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($rows['COMID'],"DTCVENDORID")),$body_text_center1);
			$qty_type = $obj->getCompareEstimateData($rows['COMID'],"QTY_TYPE_RADIO");
			if($qty_type==1)
			{
				$worksheet->writeString($k,9,$obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($rows['COMID'],"FGFF_VENDORID")),$body_text_center1);
			}
			else
			{
				$vendor = array();
				$res11 = mysql_query("select (select NAME from vendor_master where vendor_master.CODE=freight_cost_estimete_slave7.QTY_VENDORID) as QTY_VENDORID from freight_cost_estimete_slave7 where FCAID='".$l_cost_sheet_id."' and QTY_VENDORID!='' ");
				while($row11 = mysql_fetch_assoc($res11))
				{
					$vendor[] = $row11['QTY_VENDORID'];
				}
				$worksheet->writeString($k,9,implode(', ',$vendor),$body_text_center1);
			}
			$worksheet->writeString($k,10,$fixturedate,$body_text_center1);
			$worksheet->writeString($k,11,$obj->getCompareEstimateData($rows['COMID'],"VOYAGE_NO").'/'.$obj->getCompareEstimateData($rows['COMID'],"VOYAGE_NAME"),$body_text_center1);
			$res14 = mysql_query("select FCAID, PROFIT_LOSS, REVENUES_FREIGHT, TOTAL_ORC_EXP_AMT, PORT_EXPENSES, BUNKER_EXPENSES, HIDDEN_HIRAGE from freight_cost_estimete_master where COMID='".$rows['COMID']."' and SHEET_NO!='' order by FCAID asc");
			$i =0;
			$first_pl = $last_pl = $first_revenue = $last_revenue = $first_demudes = $last_demudes = $first_operation = $last_operation = $first_port = $last_port = $first_bunker = $last_bunker = $first_hiredue = $last_hiredue = $first_hirepaid = $last_hirepaid = '';
			while($row14 = mysql_fetch_assoc($res14))
			{
				if($i==0)
				{
					$first_pl = $row14['PROFIT_LOSS'];
					$first_revenue = $row14['REVENUES_FREIGHT'];
					$sql15 = "select sum(DDCLP_NETCOST + DDCDP_NETCOST) as sum from freight_cost_estimete_slave1 where FCAID='".$row14['FCAID']."'";
					$result15 = mysql_query($sql15);
					$row15 = mysql_fetch_assoc($result15);
					$first_demudes = $row15['sum'];
					$first_operation = $row14['TOTAL_ORC_EXP_AMT'];
					$first_port = $row14['PORT_EXPENSES'];
					$first_bunker = $row14['BUNKER_EXPENSES'];
					$sqlsavedinvoic = "select * from freight_cost_estimete_slave11 where FCAID='".$row14['FCAID']."'";
					$ressavedinvoice = mysql_query($sqlsavedinvoic);
					$sum1 = $valueinv = 0;
					$hirage = $row14['HIDDEN_HIRAGE'];
					while($rowssavedinvoice = mysql_fetch_assoc($ressavedinvoice))
					{
						$sqlinvoice = "select BALANCE_TO_OWNER from invoice_hire_master WHERE INVOICEID='".$rowssavedinvoice['INVOICEID']."'";
						$resinvoice = mysql_query($sqlinvoice);
						$numinvoice = mysql_num_rows($resinvoice);
						
						while($rowsinvoice = mysql_fetch_assoc($resinvoice))
						{
							$sum1 = $sum1 + $rowsinvoice['BALANCE_TO_OWNER'];
							$hirage = $hirage - $rowsinvoice['BALANCE_TO_OWNER'];
						    if($rowsinvoice['INVOICE_TYPE']=="Final"){$valueinv = 1;}
						}
					}
					if($valueinv == 1){$hirage=0;}
					$first_hiredue = $hirage;
					$first_hirepaid = $sum1;
					
				}
				else
				{
					$last_pl = $row14['PROFIT_LOSS'];
					$last_revenue = $row14['REVENUES_FREIGHT'];
					
					$sql15 = "select sum(TTL_DEMURRAGE) as sum1, sum(TTL_DESPATCH) as sum2 from laytime_master where COMID='".$rows['COMID']."' ";
					$res15 = mysql_query($sql15);
					$rec15 = mysql_num_rows($res15);
					$row15 = mysql_fetch_assoc($res15);
					$last_demudes = number_format($row15['sum1'],2,'.','') - number_format($row15['sum2'],2,'.','');
					$last_operation = $row14['TOTAL_ORC_EXP_AMT'];
					$last_port = $row14['PORT_EXPENSES'];
					$last_bunker = $row14['BUNKER_EXPENSES'];
					$sqlsavedinvoic = "select * from freight_cost_estimete_slave11 where FCAID='".$row14['FCAID']."'";
					$ressavedinvoice = mysql_query($sqlsavedinvoic);
					$sum1 = $valueinv = 0;
					$hirage = $row14['HIDDEN_HIRAGE'];
					while($rowssavedinvoice = mysql_fetch_assoc($ressavedinvoice))
					{
						$sqlinvoice = "select BALANCE_TO_OWNER from invoice_hire_master WHERE INVOICEID='".$rowssavedinvoice['INVOICEID']."'";
						$resinvoice = mysql_query($sqlinvoice);
						$numinvoice = mysql_num_rows($resinvoice);
						
						while($rowsinvoice = mysql_fetch_assoc($resinvoice))
						{
							$sum1 = $sum1 + $rowsinvoice['BALANCE_TO_OWNER'];
							$hirage = $hirage - $rowsinvoice['BALANCE_TO_OWNER'];
						    if($rowsinvoice['INVOICE_TYPE']=="Final"){$valueinv = 1;}
						}
					}
					if($valueinv == 1){$hirage=0;}
					$last_hiredue = $hirage;
					$last_hirepaid = $sum1;
				}
				$i = $i + 1;
			}
			$worksheet->writeNumber($k,12,$first_pl,$body_text_center3);
			$worksheet->writeNumber($k,13,$first_revenue,$body_text_center3);
			$worksheet->writeNumber($k,14,$first_demudes,$body_text_center3);
			$worksheet->writeNumber($k,15,$first_operation,$body_text_center3);
			$worksheet->writeNumber($k,16,$first_port,$body_text_center3);
			$worksheet->writeNumber($k,17,$first_bunker,$body_text_center3);
			$worksheet->writeNumber($k,18,$first_hiredue,$body_text_center3);
			$worksheet->writeNumber($k,19,$first_hirepaid,$body_text_center3);
			
			$worksheet->writeNumber($k,20,$last_pl,$body_text_center3);
			$worksheet->writeNumber($k,21,$last_revenue,$body_text_center3);
			$worksheet->writeNumber($k,22,$last_demudes,$body_text_center3);
			$worksheet->writeNumber($k,23,$last_operation,$body_text_center3);
			$worksheet->writeNumber($k,24,$last_port,$body_text_center3);
			$worksheet->writeNumber($k,25,$last_bunker,$body_text_center3);
			$worksheet->writeNumber($k,26,$last_hiredue,$body_text_center3);
			$worksheet->writeNumber($k,27,$last_hirepaid,$body_text_center3);

			$worksheet->writeString($k,28,'',$body_text_center3);
			$k++;
			$l++;
		}
	}
	
	$workbook->send('Chartering Register '." ".date("d-M-Y",time()).'.xls'); 
	$workbook->close();
}


function getCharteringRegisterReport()
{
	$txtFrom_Date = strtotime($_REQUEST['txtFrom_Date']); 
	$txtTo_Date   = strtotime($_REQUEST['txtTo_Date']);
	$selBType     = $_REQUEST['selBType'];
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Chartering Register');
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center3 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'Right', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center1 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center2 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'Right', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 10.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 20.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 20.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 20.00);
	$worksheet->setColumn(8, 8, 20.00);
	$worksheet->setColumn(9, 9, 20.00);
	$worksheet->setColumn(10, 10, 20.00);
	$worksheet->setColumn(11, 11, 20.00);
	$worksheet->setColumn(12, 12, 20.00);
	$worksheet->setColumn(13, 13, 20.00);
	$worksheet->setColumn(14, 14, 20.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,8,"Chartering Register [ From(".date("d-M-Y",strtotime($_REQUEST['txtFrom_Date'])).") - To(".date("d-M-Y",strtotime($_REQUEST['txtTo_Date'])).") ]",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->freezePanes(array(3, 0));
	$worksheet->writeString(2,0,"Sr. No.",$main_header_1);
	$worksheet->writeString(2,1,"Nom Id",$main_header_1_center);
	$worksheet->writeString(2,2,"Vessel",$main_header_1_center);
	$worksheet->writeString(2,3,"CPIC",$main_header_1_center);
	$worksheet->writeString(2,4,"OPS PIC",$main_header_1_center);
	$worksheet->writeString(2,5,"DWT",$main_header_1_center);
	$worksheet->writeString(2,6,"Cargo Name",$main_header_1_center);
	$worksheet->writeString(2,7,"CP Date",$main_header_1_center);
	$worksheet->writeString(2,8,"Owner Name",$main_header_1_center);
	$worksheet->writeString(2,9,"Charterer",$main_header_1_center);
	$worksheet->writeString(2,10,"Fixture Date",$main_header_1_center);
	$worksheet->writeString(2,11,"Voyage No./ Voyage Name",$main_header_1_center);
	$worksheet->writeString(2,12,"Revenue",$main_header_1_center);
	$worksheet->writeString(2,13,"Initial P & L",$main_header_1_center);
	$worksheet->writeString(2,14,"Final P & L",$main_header_1_center);
	$worksheet->writeString(2,15,"Remarks",$main_header_1_center);
	
	
	$txtFrom_Date = date('Y-m-d',strtotime($_REQUEST['txtFrom_Date']));
	$txtTo_Date   =  date('Y-m-d',strtotime($_REQUEST['txtTo_Date']));
	$selBType     =  $_REQUEST['selBType'];
	
	$sql = "select freight_cost_estimate_compare.COMID as COMID, freight_cost_estimete_master.VESSEL_IMO_ID as VESSEL_IMO_ID, freight_cost_estimete_master.ESTIMATE_TYPE as ESTIMATE_TYPE, freight_cost_estimate_compare.MESSAGE as MESSAGE, freight_cost_estimete_master.CP_DATE as CP_DATE, freight_cost_estimete_master.DWT_SUMMER as DWT_SUMMER, freight_cost_estimete_master.DWT_TOPICAL as DWT_TOPICAL, freight_cost_estimete_master.QTY_TYPE_RADIO as QTY_TYPE_RADIO, freight_cost_estimete_master.TRANS_DATE as TRANS_DATE, freight_cost_estimete_master.FCAID as FCAID, freight_cost_estimete_master.VOYAGE_NO as VOYAGE_NO, freight_cost_estimete_master.VOYAGE_NAME as VOYAGE_NAME from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS in (1,2) and freight_cost_estimete_master.TRANS_DATE>='".$txtFrom_Date."' and freight_cost_estimete_master.TRANS_DATE<='".$txtTo_Date."' and freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' group by freight_cost_estimate_compare.COMID"; 
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$k = 3;
	if($rec == 0)
	{
		$worksheet->formatmergeCellsMultiple($k,0,$k,14,"SORRY CURRENTLY THERE ARE ZERO(0) RECORDS FOUND",$body_text_center);
	}
	else
	{   
		$i = 1;
	    $arr_sumin = $arr_sumout = array();
		while($rows = mysql_fetch_assoc($res))
		{
		    $l_cost_sheet_id = $obj->getLatestCostSheetID($rows['COMID']);
			$cpdate = $obj->getCompareEstimateData($rows['COMID'],"CP_DATE");
			if($cpdate!="0000-00-00" && $cpdate!=""){$cpdate = date('d-m-Y',strtotime($cpdate));}else{$cpdate = "";}
			$fixturedate = date('d-m-Y',strtotime($obj->getCompareEstimateData($rows['COMID'],"TRANS_DATE")));
			
			$arr_sumin1 = $arr_sumout1 = array();
			$worksheet->setRow($k,20);
			$worksheet->writeString($k,0,$i,$body_text_center);
			$worksheet->writeString($k,1,$rows['MESSAGE'],$body_text_center);
			$worksheet->writeString($k,2,$obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME"),$body_text_center1);
			$worksheet->writeString($k,3,'',$body_text_center1);
			$worksheet->writeString($k,4,'',$body_text_center1);
			$worksheet->writeNumber($k,5,$obj->getCompareEstimateData($rows['COMID'],"DWT_SUMMER"),$body_text_center);
			$worksheet->writeString($k,6,$obj->getMaterialCodeDesBasedOnId($obj->getCompareEstimateData($rows['COMID'],"CARGO_ID")),$body_text_center);
			$worksheet->writeString($k,7,$cpdate,$body_text_center1);
			$worksheet->writeString($k,8,$obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($rows['COMID'],"DTCVENDORID")),$body_text_center1);
			$qty_type = $obj->getCompareEstimateData($rows['COMID'],"QTY_TYPE_RADIO");
			if($qty_type==1)
			{
				$worksheet->writeString($k,9,$obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($rows['COMID'],"FGFF_VENDORID")),$body_text_center1);
			}
			else
			{
				$vendor = array();
				$res11 = mysql_query("select (select NAME from vendor_master where vendor_master.CODE=freight_cost_estimete_slave7.QTY_VENDORID) as QTY_VENDORID from freight_cost_estimete_slave7 where FCAID='".$l_cost_sheet_id."' and QTY_VENDORID!='' ");
				while($row11 = mysql_fetch_assoc($res11))
				{
					$vendor[] = $row11['QTY_VENDORID'];
				}
				$worksheet->writeString($k,9,implode(', ',$vendor),$body_text_center1);
			}
			$worksheet->writeString($k,10,$fixturedate,$body_text_center1);
			$worksheet->writeString($k,11,$obj->getCompareEstimateData($rows['COMID'],"VOYAGE_NO").'/'.$obj->getCompareEstimateData($rows['COMID'],"VOYAGE_NAME"),$body_text_center1);
			$worksheet->writeString($k,12,$obj->getCompareEstimateData($rows['COMID'],"REVENUES_FREIGHT"),$body_text_center1);
			$res14 = mysql_query("select PROFIT_LOSS from freight_cost_estimete_master where COMID='".$rows['COMID']."' and SHEET_NO!='' order by SHEET_NO asc");
			$i =0;
			$first = $last = '';
			while($row14 = mysql_fetch_assoc($res14))
			{
				if($i==0)
				{
					$first = $row14['PROFIT_LOSS'];
				}
				else
				{
					$last = $row14['PROFIT_LOSS'];
				}
				$i = $i + 1;
			}
			$worksheet->writeNumber($k,13,$first,$body_text_center3);
			$worksheet->writeNumber($k,14,$last,$body_text_center3);
			$worksheet->writeString($k,15,'',$body_text_center3);
			$k++;
		}
	}
	
	$workbook->send('Chartering Register '." ".date("d-M-Y",time()).'.xls'); 
	$workbook->close();
}



function getCompareSheetReport()
{
	$comid          =  $_REQUEST['comid'];
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Compare Sheets');
		
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	
	$body_text = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_left = & $workbook->addFormat(array('Size' => 8,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_left1 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_right = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'Right', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$body_text_right1 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'Center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 25.00);
	$worksheet->setColumn(1, 1, 25.00);
	$worksheet->setColumn(2, 2, 25.00);
	$worksheet->setColumn(3, 3, 25.00);
	$worksheet->setColumn(4, 4, 25.00);
	$worksheet->setColumn(5, 5, 25.00);
	$worksheet->setColumn(6, 6, 25.00);
	$worksheet->setColumn(7, 7, 25.00);
	$worksheet->setColumn(8, 8, 25.00);
	$worksheet->setColumn(9, 9, 25.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,6,"Compare Sheets",$main_header);
	
	$worksheet->setRow(2,15.75);$worksheet->setRow(3,15.75);$worksheet->setRow(4,15.75);$worksheet->setRow(5,15.75);
	$worksheet->writeString(2,0,"Vessel Name :",$body_text_left);
	$worksheet->writeString(2,1,$obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME"),$body_text_left1);
	$worksheet->writeString(2,2,"Vessel Type :",$body_text_left);
	$worksheet->writeString(2,3,$obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_TYPE")),$body_text_left1);
	$worksheet->writeString(2,4,"Flag :",$body_text_left);
	$worksheet->writeString(2,5,$obj->getCompareEstimateData($comid,"FLAG"),$body_text_left1);
	
	$worksheet->writeString(3,0,"Fixture Date :",$body_text_left);
	$worksheet->writeString(3,1,date('d-m-Y',strtotime($obj->getCompareEstimateData($comid,"TRANS_DATE"))),$body_text_left1);
	$worksheet->writeString(3,2,"Voyage No. / Coa / Spot :",$body_text_left);
	$worksheet->writeString(3,3,$obj->getCompareEstimateData($comid,"VOYAGE_NO"),$body_text_left1);
	$worksheet->writeString(3,4,"Voyage Financials Name :",$body_text_left);
	$worksheet->writeString(3,5,$obj->getCompareEstimateData($comid,"VOYAGE_NAME"),$body_text_left1);
	
	$worksheet->writeString(4,0,"DWT (Summer) :",$body_text_left);
	$worksheet->writeNumber(4,1,$obj->getCompareEstimateData($comid,"DWT_SUMMER"),$body_text_right);
	$worksheet->writeString(4,2,"DWT (Tropical)  :",$body_text_left);
	$worksheet->writeNumber(4,3,$obj->getCompareEstimateData($comid,"DWT_TOPICAL"),$body_text_right);
	
	$worksheet->writeString(7,0,"Sheet Name/Parameters",$body_text_left);
	$sql = "SELECT * FROM freight_cost_estimete_master where COMID='".$comid."'";
	$result = mysql_query($sql);
	$num = mysql_num_rows($result);
	$width = 90/$num;
	$result = mysql_query($sql);
	$i = 1;
	while($rows= mysql_fetch_assoc($result))
	{
		$sql1 = "select SHEET_NAME from cost_sheet_name_master where COST_SHEETID='".$rows['SHEET_NO']."'";
		$res1 = mysql_query($sql1);
		$rows1 = mysql_fetch_assoc($res1);
		if($rows1['SHEET_NAME']!='')
		{
			$worksheet->writeString(7,$i,$rows1['SHEET_NAME'],$body_text_left);
		}
		else
		{
			$worksheet->writeString(7,$i,'FVF',$body_text_left);
		}
		$i++;
	}
	
	$worksheet->writeString(8,0,"Cargo Type",$body_text);
	
	
	$worksheet->writeString(9,0,"",$body_text_left);
	$result = mysql_query($sql);
	$i = 1;
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeString(9,$i,$obj->getEstimateList($rows['ESTIMATE_TYPE']),$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(10,0,"Revenue",$body_text);
	
	
	$worksheet->writeString(11,0,"Demurrage",$body_text_left);
	$result = mysql_query($sql);
	$i = 1;
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_sum = array();
		$sql1 = "select DDCLP_NETCOST from freight_cost_estimete_slave1 where FCAID='".$rows['FCAID']."'";
		$res1 = mysql_query($sql1);
		while($rows1 = mysql_fetch_assoc($res1))
		{
			if($rows1['DDCLP_NETCOST']>0)
			{
				$arr_sum[] = $rows1['DDCLP_NETCOST'];
			}
		}
		
		$sql2 = "select DDCDP_NETCOST from freight_cost_estimete_slave1 where FCAID='".$rows['FCAID']."'";
		$res2 = mysql_query($sql2);
		while($rows2 = mysql_fetch_assoc($res2))
		{
			if($rows2['DDCDP_NETCOST']>0)
			{
				$arr_sum[] = $rows2['DDCDP_NETCOST'];
			}
		}
		$array_revenue[$rows['FCAID']][] = array_sum($arr_sum);
		$worksheet->writeNumber(11,$i,array_sum($arr_sum),$body_text_right);
		$i++;
	}
	
	
	$worksheet->writeString(12,0,"Dispatch",$body_text_left);
	$result = mysql_query($sql);
	$i = 1;
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_sum = array();
		$sql1 = "select DDCLP_NETCOST from freight_cost_estimete_slave1 where FCAID='".$rows['FCAID']."'";
		$res1 = mysql_query($sql1);
		while($rows1 = mysql_fetch_assoc($res1))
		{
			if($rows1['DDCLP_NETCOST']<0)
			{
				$arr_sum[] = $rows1['DDCLP_NETCOST'];
			}
		}
		
		$sql2 = "select DDCDP_NETCOST from freight_cost_estimete_slave1 where FCAID='".$rows['FCAID']."'";
		$res2 = mysql_query($sql2);
		while($rows2 = mysql_fetch_assoc($res2))
		{
			if($rows2['DDCDP_NETCOST']< 0)
			{
				$arr_sum[] = $rows2['DDCDP_NETCOST'];
			}
		}
		$array_revenue[$rows['FCAID']][] = array_sum($arr_sum);
		$worksheet->writeNumber(12,$i,array_sum($arr_sum),$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(13,0,"Final Nett Freight",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$array_revenue[$rows['FCAID']][] = $rows['REVENUES_FREIGHT'];
		$worksheet->writeNumber(13,$i,$rows['REVENUES_FREIGHT'],$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(14,0,"Total Revenue",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber(14,$i,array_sum($array_revenue[$rows['FCAID']]),$body_text_right);
		$i++;
	}
	
	
	
	$worksheet->writeString(16,0,"Expenses- Cargo",$body_text);
	
	$array_expense = array();
	$worksheet->writeString(17,0,"Loading Port",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_sum = array();
		$sql1 = "select sum(LOAD_PORT_COST) as sum from freight_cost_estimete_slave1 where FCAID='".$rows['FCAID']."'";
		$res1 = mysql_query($sql1);
		$rows1 = mysql_fetch_assoc($res1);
		$array_expense[$rows['FCAID']][] = $rows1['sum'];
		$worksheet->writeNumber(17,$i,$rows1['sum'],$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(18,0,"Discharge Port",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_sum = array();
		$sql1 = "select sum(DISC_PORT_COST) as sum from freight_cost_estimete_slave1 where FCAID='".$rows['FCAID']."'";
		$res1 = mysql_query($sql1);
		$rows1 = mysql_fetch_assoc($res1);
		$array_expense[$rows['FCAID']][] = $rows1['sum'];
		$worksheet->writeNumber(18,$i,$rows1['sum'],$body_text_right);
		$i++;
	}
	
	$worksheet->writeString(19,0,"Bunker Port",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$arr_sum = array();
		$sql1 = "select sum(TRANSIT_PORT_COST) as sum from freight_cost_estimete_slave1 where FCAID='".$rows['FCAID']."'";
		$res1 = mysql_query($sql1);
		$rows1 = mysql_fetch_assoc($res1);
		$array_expense[$rows['FCAID']][] = $rows1['sum'];
		$worksheet->writeNumber(19,$i,$rows1['sum'],$body_text_right);
		$i++;
	}
	
	
	$sqlop = "SELECT owner_related_cost_master.OWNER_RCOSTID, owner_related_cost_master.NAME FROM owner_related_cost_master inner join freight_cost_estimete_slave3 on freight_cost_estimete_slave3.IDENTY_ID=owner_related_cost_master.OWNER_RCOSTID inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID =freight_cost_estimete_slave3.FCAID  where owner_related_cost_master.STATUS=1 and freight_cost_estimete_master.COMID='".$comid."' and freight_cost_estimete_slave3.RAW_AMOUNT > 0 group by freight_cost_estimete_slave3.IDENTY_ID";
    $resop = mysql_query($sqlop);
	$i = 1;
	$j = 20;
    $numop = mysql_num_rows($resop);
    while($rowsop = mysql_fetch_assoc($resop))
    {
		$worksheet->writeString($j,0,$rowsop['NAME'],$body_text_left);
		$result = mysql_query($sql);
		while($rows= mysql_fetch_assoc($result))
		{
			$sql1 = "select sum(RAW_AMOUNT) as sum from freight_cost_estimete_slave3 where FCAID='".$rows['FCAID']."' and IDENTIFY='ORC' and IDENTY_ID='".$rowsop['OWNER_RCOSTID']."'";
			$res1 = mysql_query($sql1);
			$rows1 = mysql_fetch_assoc($res1);
			$array_expense[$rows['FCAID']][] = $rows1['sum'];
			$worksheet->writeNumber($j,$i,$rows1['sum'],$body_text_right);
			$i++;
		}
		$i = 1;
		$j++;
	}
	
	
	$worksheet->writeString($j,0,"CVE",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$array_expense[$rows['FCAID']][] = $rows['CVE_AMT'];
		$worksheet->writeNumber($j,$i,$rows['CVE_AMT'],$body_text_right);
		$i++;
	}$j++;
	
	
	
	$worksheet->writeString($j,0,"ILOHC",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$array_expense[$rows['FCAID']][] = $rows['ILOHC_AMT'];
		$worksheet->writeNumber($j,$i,$rows['ILOHC_AMT'],$body_text_right);
		$i++;
	}$j++;
	
	
	$worksheet->writeString($j,0,"Address Commission",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$array_expense[$rows['FCAID']][] = $rows['ILOHC_AMT'];
		$worksheet->writeNumber($j,$i,$rows['ADDRESS_COMMISSION_AMT'],$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Brokerage Commission",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$sql1 = "select sum(BROKAGE_AMT) as sum from freight_cost_estimete_slave4 where FCAID='".$rows['FCAID']."' ";
		$res1 = mysql_query($sql1);
		$rows1 = mysql_fetch_assoc($res1);
		$array_expense[$rows['FCAID']][] = $rows1['sum'];
		$worksheet->writeNumber($j,$i,$rows1['sum'],$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Total Expense-Cargo",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,array_sum($array_expense[$rows['FCAID']]),$body_text_right);
		$i++;
	}$j++;

	
	$j++;
	$worksheet->writeString($j++,0,"Totals",$body_text);
	
	$worksheet->writeString($j,0,"Laden Dist",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['LADEN_DIST'],$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Ballast Dist",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['BALLAST_DIST'],$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Total Dist",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['TOTAL_DISTANCE'],$body_text_right);
		$i++;
	}$j++;
	
	
	$worksheet->writeString($j,0,"Laden Days",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['LADEN_DAYS'],$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Ballast Days",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['BALLAST_DAYS'],$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Total Sea Days",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['TOTAL_PASSAGE_DAYS'],$body_text_right);
		$i++;
	}$j++;
	
	
	$worksheet->writeString($j,0,"Ttl Port Idle Days",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['IDEAL_DAYS'],$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Ttl Port Work Days",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['WORKING_DAYS'],$body_text_right);
		$i++;
	}$j++;
	
	
	$worksheet->writeString($j,0,"Total Days",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['TOTAL_DAYS'],$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Ttl FO Consp MT",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['FO_CONSP'],$body_text_right);
		$i++;
	}$j++;
	
	
	$worksheet->writeString($j,0,"Ttl DO Consp MT",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['DO_CONSP'],$body_text_right);
		$i++;
	}$j++;
	
	$j++;
	$worksheet->writeString($j,0,"Bunker Expenses (Qty / Price / Amount)",$body_text);$j++;
	
	$sql2 = "SELECT bunker_grade_master.BUNKERGRADEID, bunker_grade_master.NAME FROM bunker_grade_master inner join freight_cost_estimete_slave2 on freight_cost_estimete_slave2.BUNKERGRADEID=bunker_grade_master.BUNKERGRADEID inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID =freight_cost_estimete_slave2.FCAID  where bunker_grade_master.MODULEID='".$_SESSION['moduleid']."' AND bunker_grade_master.MCOMPANYID='".$_SESSION['company']."' and bunker_grade_master.STATUS=1 and freight_cost_estimete_master.COMID='".$comid."' and (freight_cost_estimete_slave2.EST_COST > 0 or freight_cost_estimete_slave2.ACTUAL_COST>0) group by freight_cost_estimete_slave2.BUNKERGRADEID";
	$res2 = mysql_query($sql2);
	$rec2 = mysql_num_rows($res2);
	$m=0; 
	$i = 1;
	while($rows2 = mysql_fetch_assoc($res2))
	{
		$worksheet->writeString($j,0,$rows2['NAME'],$body_text_left);
		$result = mysql_query($sql);
		while($rows= mysql_fetch_assoc($result))
		{   
		    $sql1 = "select * from freight_cost_estimete_slave2 where FCAID='".$rows['FCAID']."' and BUNKERGRADEID='".$rows2['BUNKERGRADEID']."'";
			$res1 = mysql_query($sql1);
			$rows1 = mysql_fetch_assoc($res1);
			$qty = $price = $amount = 0;
			if($rows1['ACTUAL_COST']!='')
			{
				$qty    = $rows1['EST_MT'];
				$price  = $rows1['EST_PRICE'];
				$amount = $rows1['EST_COST'];
			}
			else
			{
				$qty    = $rows1['ACTUAL_MT'];
				$price  = $rows1['ACTUAL_PRICE'];
				$amount = $rows1['ACTUAL_COST'];
			}
			$bunker_expense[$rows['FCAID']][] =  $amount;
			$val = $qty.'/'.$price.'/'.$amount;
			$worksheet->writeString($j,$i,$val,$body_text_right);
			$i++;
		}
		$j++;
		$i = 1;
	}
	
	
	$j++;
	$worksheet->writeString($j,0,"Total Bunker Expense",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,array_sum($bunker_expense[$rows['FCAID']]),$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Navigation Expenses",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,(array_sum($array_expense[$rows['FCAID']]) + array_sum($bunker_expense[$rows['FCAID']])),$body_text_right);
		$i++;
	}$j++;
	
	$j++;
	$worksheet->writeString($j,0,"Expense - Ship",$body_text);$j++;
	
	$worksheet->writeString($j,0,"Hire",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['DAILY_VESSEL_OPERATION_EXP'],$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Hireage",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$hirage = $rows['DAILY_VESSEL_OPERATION_EXP']*$rows['TOTAL_DAYS'];
		$sqlinvoice = "select INVOICE_TYPE, INVOICE_NO, BALANCE_TO_OWNER from invoice_hire_master WHERE MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$comid."' and STATUS=2 order by INVOICEID";
		$resinvoice = mysql_query($sqlinvoice);
		$numinvoice = mysql_num_rows($resinvoice);
		$hierage_sign = 0;
		while($rowsinvoice = mysql_fetch_assoc($resinvoice))
		{
		   $hirage = $hirage - $rowsinvoice['BALANCE_TO_OWNER'];
		   if($rowsinvoice['INVOICE_TYPE']=="Final"){$hierage_sign = 1;}
		}
		if($hierage_sign == 1 ){$hirage = $rows['DAILY_VESSEL_OPERATION_EXP']*$rows['TOTAL_DAYS'];}
		$worksheet->writeNumber($j,$i,$hirage,$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Ballast Bonus",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['BALLAST_BONUS'],$body_text_right);
		$i++;
	}$j++;
	
	$worksheet->writeString($j,0,"Add Comm (%/Amt)",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$hirage = $rows['DAILY_VESSEL_OPERATION_EXP']*$rows['TOTAL_DAYS'];
		$sqlinvoice = "select INVOICE_TYPE, INVOICE_NO, BALANCE_TO_OWNER from invoice_hire_master WHERE MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$comid."' and STATUS=2 order by INVOICEID";
		$resinvoice = mysql_query($sqlinvoice);
		$numinvoice = mysql_num_rows($resinvoice);
		$hierage_sign = 0;
		while($rowsinvoice = mysql_fetch_assoc($resinvoice))
		{
		   $hirage = $hirage - $rowsinvoice['BALANCE_TO_OWNER'];
		   if($rowsinvoice['INVOICE_TYPE']=="Final"){$hierage_sign = 1;}
		}
		if($hierage_sign == 1 ){$hirage = $rows['DAILY_VESSEL_OPERATION_EXP']*$rows['TOTAL_DAYS'];}
		$worksheet->writeString($j,$i,$rows['HIREAGE_PERCENT'].' / '.(($hirage*$rows['HIREAGE_PERCENT'])/100),$body_text_right);
		$i++;
	}$j++;
	
	
	$worksheet->writeString($j,0,"Nett Hireage",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$hirage = $rows['DAILY_VESSEL_OPERATION_EXP']*$rows['TOTAL_DAYS'];
		$sqlinvoice = "select INVOICE_TYPE, INVOICE_NO, BALANCE_TO_OWNER from invoice_hire_master WHERE MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$comid."' and STATUS=2 order by INVOICEID";
		$resinvoice = mysql_query($sqlinvoice);
		$numinvoice = mysql_num_rows($resinvoice);
		$hierage_sign = 0;
		while($rowsinvoice = mysql_fetch_assoc($resinvoice))
		{
		   $hirage = $hirage - $rowsinvoice['BALANCE_TO_OWNER'];
		   if($rowsinvoice['INVOICE_TYPE']=="Final"){$hierage_sign = 1;}
		}
		if($hierage_sign == 1 ){$hirage = $rows['DAILY_VESSEL_OPERATION_EXP']*$rows['TOTAL_DAYS'];}
		$worksheet->writeNumber($j,$i,($hirage - (($hirage*$rows['HIREAGE_PERCENT'])/100) + $rows['BALLAST_BONUS']),$body_text_right);
		$i++;
	}$j++;
	
	
	$worksheet->writeString($j,0,"Nett Daily Profit",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['NET_DAILY_PROFIT'],$body_text_right);
		$i++;
	}$j++;
	
	
	$worksheet->writeString($j,0,"P/L",$body_text_left);
	$i = 1;
	$result = mysql_query($sql);
	while($rows= mysql_fetch_assoc($result))
	{
		$worksheet->writeNumber($j,$i,$rows['PROFIT_LOSS'],$body_text_right);
		$i++;
	}$j++;
	
	$workbook->send('Compare Sheets ('.$obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME").') '." ".date('d-m-Y',strtotime($obj->getCompareEstimateData($comid,"CP_DATE"))).'.xls'); 
	$workbook->close();
	
}


function getProfitabilityHeadWiseReport()
{
	$txtFrom_Date = strtotime($_REQUEST['txtFrom_Date']); 
	$txtTo_Date = strtotime($_REQUEST['txtTo_Date']);
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Profitability Report');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center3 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'Right', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center2 = & $workbook->addFormat(array('Size' => 9,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'Right', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 25.00);
	$worksheet->setColumn(1, 1, 25.00);
	$worksheet->setColumn(2, 2, 25.00);
	$worksheet->setColumn(3, 3, 25.00);
	$worksheet->setColumn(4, 4, 25.00);
	$worksheet->setColumn(5, 5, 25.00);
	$worksheet->setColumn(6, 6, 25.00);
	$worksheet->setColumn(7, 7, 25.00);
	$worksheet->setColumn(8, 8, 25.00);
	$worksheet->setColumn(9, 9, 25.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,8,"Profitability Head Wise Report (".$name.") [ From(".date("d-M-Y",strtotime($_REQUEST['txtFrom_Date'])).") - To(".date("d-M-Y",strtotime($_REQUEST['txtTo_Date'])).") ]",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"Sr. No.",$main_header_1);
	$worksheet->writeString(2,1,"Nom Id",$main_header_1_center);
	$worksheet->writeString(2,2,"Vessel",$main_header_1_center);
	$worksheet->writeString(2,3,"CP Date",$main_header_1_center);
	$worksheet->writeString(2,4,"Total Qty. (MT)",$main_header_1_center);
	$worksheet->writeString(2,5,"Vendor",$main_header_1_center);
	$worksheet->writeString(2,6,"Cost In (Revenue)",$main_header_1_center);
	$worksheet->writeString(2,7,"Cost Out (Expense)",$main_header_1_center);
	$worksheet->writeString(2,8,"Difference",$main_header_1_center);
	
	
	$sql = "select * from freight_cost_analysis_master inner join mapping_master on mapping_master.MAPPINGID = freight_cost_analysis_master.MAPPINGID where TRANS_DATE >='".date('Y-m-d',strtotime($_REQUEST['txtFrom_Date']))."' and TRANS_DATE <='".date('Y-m-d',strtotime($_REQUEST['txtTo_Date']))."' and freight_cost_analysis_master.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_analysis_master.MCOMPANYID='".$_SESSION['company']."' group by freight_cost_analysis_master.MAPPINGID ";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$k = 3;
	if($rec == 0)
	{
		$worksheet->formatmergeCellsMultiple($k,0,$k,8,"SORRY CURRENTLY THERE ARE ZERO(0) RECORDS FOUND",$body_text_center);
	}
	else
	{   
		$i = 1;
	    $arr_sumin = $arr_sumout = array();
		while($rows = mysql_fetch_assoc($res))
		{
		    $sheetNo = $obj->getLatestCostSheetID($rows['MAPPINGID']);
			$cpdate = date('Y-m-d',strtotime($rows['TRANS_DATE']));
			$sel2 = "select sum(COSTIN) as sum1, sum(COSTOUT) as sum2 from profit_head_master where MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and MAPPINGID='".$rows['MAPPINGID']."' and SHEET_NO='".$sheetNo."' and (COSTIN >0 or COSTOUT>0) and VENDORID!=''";
			$result2 = mysql_query($sel2) or die($sel2);
			$rows2  = mysql_fetch_assoc($result2);
			
			$sel = "select * from profit_head_master where MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and MAPPINGID='".$rows['MAPPINGID']."' and SHEET_NO='".$sheetNo."' and (COSTIN >0 or COSTOUT>0) and VENDORID!=''";
			$result = mysql_query($sel) or die($sel);
			$num1   = mysql_num_rows($result);
			if($num1>0){$colspan = $num1+ 2;}else{$colspan = 1;}
			
			if($num1>0)
			{
			    $arr_sumin1 = $arr_sumout1 = array();
				$worksheet->setRow($k,20);
				$worksheet->writeNumber($k,0,$i,$body_text_center);
				$worksheet->writeString($k,1,$obj->getMappingData($rows['MAPPINGID'],"NOM_NAME"),$body_text_center);
				$worksheet->writeString($k,2,$obj->getVesselIMOData($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'VESSEL_IMO_ID'),"VESSEL_NAME"),$body_text_center);
				$worksheet->writeString($k,3,date('d-m-Y',strtotime($cpdate)),$body_text_center);
				$worksheet->writeString($k,4,$rows['CARGO_QTY'],$body_text_center);
				$worksheet->writeString($k,5,"",$body_text_center);
				$worksheet->writeNumber($k,6,$rows2['sum1'],$body_text_center3);
				$worksheet->writeNumber($k,7,$rows2['sum2'],$body_text_center3);
				$worksheet->writeNumber($k,8,$rows2['sum1'] - $rows2['sum2'],$body_text_center3);
				$k++;
				while($rows1  = mysql_fetch_assoc($result))
				{
				    $text = "";
					$arr_sumin1[] = $rows1['COSTIN']; $arr_sumout1[]=$rows1['COSTOUT'];
				    if($rows1['IDENTIFY']=="FREIGHT")
					{$text = "Final Nett Freight";}
					else if($rows1['IDENTIFY']=="BROKERAGE")
					{$text = "Total Brokerage Commission (%)";}
					else if($rows1['IDENTIFY']=="BROKERAGE")
					{$text = "Total Brokerage Commission (%)";}
					else if($rows1['IDENTIFY']=="BUNKERNETT")
					{$text = "Total Brokerage Commission (%)";}
					else if($rows1['IDENTIFY']=="OWNERRELATED")
					{$text = "Owner Related Costs (".$obj->getOwnerRelatedCostNameBasedOnID($rows1['IDENTIFY_ID']).")";}
					else if($rows1['IDENTIFY']=="CHARTERER")
					{$text = "Charterers' Costs (".$obj->getChartererCostNameBasedOnID($rows1['IDENTIFY_ID']).")";}
					else if($rows1['IDENTIFY']=="LPCOST")
					{$text = "Port Cost (Load Port  ".$obj->getPortNameBasedOnID($rows1['IDENTIFY_ID']).")";}
					
					else if($rows1['IDENTIFY']=="DPCOST")
					{$text = "Port Cost (Discharge Port ".$obj->getPortNameBasedOnID($rows1['IDENTIFY_ID']).")";}
					else if($rows1['IDENTIFY']=="TPCOST")
					{$text = "Port Cost (Transit Port ".$obj->getPortNameBasedOnID($rows1['IDENTIFY_ID']).")";}
					else if($rows1['IDENTIFY']=="DLPCOST")
					{$text = "Demurrage (Load Port  ".$obj->getPortNameBasedOnID($rows1['IDENTIFY_ID']).")";}
					else if($rows1['IDENTIFY']=="DDPCOST")
					{$text = "Demurrage (Discharge Port  ".$obj->getPortNameBasedOnID($rows1['IDENTIFY_ID']).")";}
					else if($rows1['IDENTIFY']=="OTHERMISC")
					{$text = "Other Misc. Income (".$obj->getOtherMiscCostNameBasedOnID($rows1['IDENTIFY_ID']).")";}
					$worksheet->writeString($k,4,$text,$body_text_center1);
					$worksheet->writeString($k,5,$obj->getVendorListNewBasedOnID($rows1['VENDORID']),$body_text_center1);
					$worksheet->writeNumber($k,6,$rows1['COSTIN'],$body_text_center2);
					$worksheet->writeNumber($k,7,$rows1['COSTOUT'],$body_text_center2);
					$worksheet->writeNumber($k,8,$rows1['COSTIN'] - $rows1['COSTOUT'],$body_text_center2);
					$k++;$i++;
				}
				$worksheet->formatmergeCellsMultiple($k,4,$k,5,"Total",$body_text_center);
				$worksheet->writeNumber($k,6,array_sum($arr_sumin1),$body_text_center3);
				$worksheet->writeNumber($k,7,array_sum($arr_sumout1),$body_text_center3);
				$worksheet->writeNumber($k,8,array_sum($arr_sumin1) - array_sum($arr_sumout1),$body_text_center3);
				$k++;
			}
		}
	}
	
	$workbook->send('Profitability Head Wise Report'." ".date("d-M-Y",time()).'.xls'); 
	$workbook->close();
	
}

function vendor_excel()
{
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Vendor List');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(100);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 5.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 35.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 35.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,4,"Vendor List ( ".date("d-M-Y",time())." )",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"S.No.",$main_header_1);
	$worksheet->writeString(2,1,"Vendor Type",$main_header_1);
	$worksheet->writeString(2,2,"Vendor Name",$main_header_1);
	$worksheet->writeString(2,3,"Short Name",$main_header_1);
	$worksheet->writeString(2,4,"Code",$main_header_1);
	
	$worksheet->repeatRows(0,2);
	$worksheet->freezePanes(array(3, 1));
	
	$sql = "select * from vendor_master WHERE 1=1 order by VENDORID desc";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$i=1;$k=3;
	
	$arr = array();
	if($rec == 0)
	{
		$worksheet->formatmergeCellsMultiple($k,0,$k,10,"sorry currently there are zero(0) records",$body_text);
	}
	else
	{
		while($rows = mysql_fetch_assoc($res))
		{
			$worksheet->setRow($k,20);
			$worksheet->writeString($k,0,$i.".",$body_text_center);
			$worksheet->writeString($k,1,$obj->getVendorTypeNameBasedOnID($rows['VENDOR_TYPEID']),$body_text_center);	
			$worksheet->writeString($k,2,$rows['NAME'],$body_text_center);
			$worksheet->writeString($k,3,$rows['SHORT_NAME'],$body_text_center);
			$worksheet->writeString($k,4,$rows['CODE'],$body_text_center);
				
			$i++;$k++;	
		}	
	}
	
	$workbook->send('Vendor Report'." ".date("d-M-Y",time()).'.xls');
	$workbook->close();
}


function material_excel()
{
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Material List');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(100);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 5.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 35.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 35.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,4,"Material List ( ".date("d-M-Y",time())." )",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"S.No.",$main_header_1);
	$worksheet->writeString(2,1,"Material Type",$main_header_1);
	$worksheet->writeString(2,2,"Material Type Description",$main_header_1);
	$worksheet->writeString(2,3,"Material Code",$main_header_1);
	$worksheet->writeString(2,4,"Material Code Description",$main_header_1);
	
	$worksheet->repeatRows(0,2);
	$worksheet->freezePanes(array(3, 1));
	
	$sql = "select * from cargo_master order by MATERIALID desc";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$i=1;$k=3;
	
	$arr = array();
	if($rec == 0)
	{
		$worksheet->formatmergeCellsMultiple($k,0,$k,10,"sorry currently there are zero(0) records",$body_text);
	}
	else
	{
		while($rows = mysql_fetch_assoc($res))
		{
			$worksheet->setRow($k,20);
			$worksheet->writeString($k,0,$i.".",$body_text_center);
			$worksheet->writeString($k,1,$rows['MATERIAL_TYPE'],$body_text_center);	
			$worksheet->writeString($k,2,$rows['MATERIAL_TYPE_DESC'],$body_text_center);
			$worksheet->writeString($k,3,$rows['MATERIAL_CODE'],$body_text_center);
			$worksheet->writeString($k,4,$rows['MATERIAL_CODE_DESC'],$body_text_center);
				
			$i++;$k++;	
		}	
	}
	
	$workbook->send('Material Report'." ".date("d-M-Y",time()).'.xls');
	$workbook->close();
}

function aging_receivable_excel()
{
	
	$workbook 	= new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj 		= new data();
				$obj->funConnect();
	$worksheet 	=& $workbook->addWorksheet('Aging Report (Receivables)');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(100);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 5.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 15.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 20.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 20.00);
	$worksheet->setColumn(8, 8, 20.00);
	$worksheet->setColumn(9, 9, 20.00);
	$worksheet->setColumn(10, 10, 20.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,10,"Aging Report (Receivables) ( ".date("d-M-Y",time())." )",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"S.No.",$main_header_1);
	$worksheet->writeString(2,1,"Client",$main_header_1);
	$worksheet->writeString(2,2,"Nom Id",$main_header_1);
	$worksheet->writeString(2,3,"Vessel",$main_header_1);
	$worksheet->writeString(2,4,"CP Date",$main_header_1);
	$worksheet->writeString(2,5,"Payment Type",$main_header_1);
	$worksheet->writeString(2,6,"Bill Date",$main_header_1);
	$worksheet->writeString(2,7,"Amount Billed",$main_header_1);
	$worksheet->writeString(2,8,"Amount Received",$main_header_1);
	$worksheet->writeString(2,9,"Difference",$main_header_1);
	$worksheet->writeString(2,10,"Delay (Days)",$main_header_1);
	
	$worksheet->repeatRows(0,2);
	$worksheet->freezePanes(array(3, 1));
	
	$selVendor      = $_REQUEST['selVendor'];
	$selBType    	= $_REQUEST['selBType'];
	$selDate_Type 	= $_REQUEST['selDate_Type'];
	
	$var = '';
	if($selDate_Type == 1)
	{
	    $var = 1;
	}
	else if($selDate_Type == 2)
	{
	    $from_date = date('Y-m-d',strtotime('-30 days'));
	    $to_date = date('Y-m-d',time());
		$var = 2;
	}
	else if($selDate_Type == 3)
	{
        $from_date = date('Y-m-d',strtotime('-60 days'));
	    $to_date = date('Y-m-d',strtotime('-30 days'));	
		$var = 2;
	}
	else if($selDate_Type == 4)
	{
	    $from_date = date('Y-m-d',strtotime('-90 days'));
	    $to_date = date('Y-m-d',strtotime('-60 days'));	
		$var = 2;
	}
	else if($selDate_Type == 5)
	{
	    $from_date = date('Y-m-d',strtotime('-90 days'));
		$var = 3;
	}
	
	$sql = "select * from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS in (1,2) and freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' order by freight_cost_estimate_compare.COMID desc";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$i = 1;$k=3; $ttl_net_amt = 0; $ttl_amt_received = 0; $ttl_net_diff = 0;
	if($rec == 0)
	{
		
	}
	else
	{	
		while($rows = mysql_fetch_assoc($res))
		{		
		    $fcaid = $obj->getLatestCostSheetID($rows['COMID']);
			$obj->viewFreightCostEstimationTempleteRecordsNew($fcaid);
			if($var == 1)
			{
				$sql1 = "select * from dumerage_invoice_master where ".(($_REQUEST['selVendor']!='')?"VENDOR='".$selVendor."' and ":"")." COMID='".$rows['COMID']."'";
			}
			else if($var == 2)
			{
				$sql1 = "select * from dumerage_invoice_master where ".(($_REQUEST['selVendor']!='')?"VENDOR='".$selVendor."' and ":"")." (DATE BETWEEN '".date("Y-m-d",strtotime($from_date))."' and '".date("Y-m-d",strtotime($to_date))."')  and COMID='".$rows['COMID']."' ";
			}
			else if($var == 3)
			{
				$sql1 = "select * from dumerage_invoice_master where ".(($_REQUEST['selVendor']!='')?"VENDOR='".$selVendor."' and ":"")." DATE<'".date("Y-m-d",strtotime($from_date))."'  and COMID='".$rows['COMID']."' ";
			}
			$res1 = mysql_query($sql1) or die($sql1);
			while ($rows1 = mysql_fetch_assoc($res1))
			{
				$invoice_amt= $rows1['NET_AMT'];
				$amt_paid = $rows1['P_AMT'];
				$diff = $invoice_amt - $amt_paid;
				$cost_type = 'Demurrage Dispatch Ship Owner - '.$obj->getPortNameBasedOnID($rows1['PORTID']).' ('.$rows1['PORT_TYPE'].')';
				$start = strtotime($rows1['DATE']);
				$end = strtotime(date('Y-m-d',time()));
				$delay_days = ceil(abs($end - $start) / 86400);
				if($rows1['VENDOR']!="")
				{
					$worksheet->writeString($k,0,$i.".",$body_text_center);
					$worksheet->writeString($k,1,$obj->getVendorListNewBasedOnID($rows1['VENDOR']),$body_text);
					$worksheet->writeString($k,2,$obj->getCompareTableData($rows['COMID'],"MESSAGE"),$body_text);
					$worksheet->writeString($k,3,$obj->getVesselIMOData($obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID"),"VESSEL_NAME"),$body_text);
					$worksheet->writeString($k,4,(($obj->getFun112()=='' || $obj->getFun112()=='0000-00-00' || $obj->getFun112()=='1970-01-01')?'':date("d-m-Y",strtotime($obj->getFun112()))),$body_text);
					$worksheet->writeString($k,5,$cost_type,$body_text);
					$worksheet->writeString($k,6,date("d-m-Y",strtotime($rows1['DATE'])),$body_text);
					$worksheet->writeString($k,7,$obj->getNumberFormat($invoice_amt),$body_text);
					$worksheet->writeString($k,8,$obj->getNumberFormat($amt_paid),$body_text);
					$worksheet->writeString($k,9,$obj->getNumberFormat($diff),$body_text);
					$worksheet->writeString($k,10,$delay_days,$body_text);
					$i++;$k++;
				}
				
				
			}
			if($var == 1)
			{
				$sql1 = "select * from freight_invoice_master where COMID='".$rows['COMID']."' ";
			}
			else if($var == 2)
			{
				$sql1 = "select * from freight_invoice_master where (DATE BETWEEN '".date("Y-m-d",strtotime($from_date))."' and '".date("Y-m-d",strtotime($to_date))."')  and COMID='".$rows['COMID']."'";
			}
			else if($var == 3)
			{
				$sql1 = "select * from freight_invoice_master where DATE<'".date("Y-m-d",strtotime($from_date))."'  and COMID='".$rows['COMID']."' ";
			}
			$res1 = mysql_query($sql1) or die($sql1);
			while($rows1 = mysql_fetch_assoc($res1))
			{
				if($rows1['VENDOR']!="")
				{
					$cost_type = 'Final Nett Freight';
					$invoice_amt= $rows1['NET_PAYABLE'];
					$amt_paid = $rows1['P_AMT'];
					$diff = $invoice_amt - $amt_paid;
					$start = strtotime($rows1['DATE']);
					$end = strtotime(date('Y-m-d',time()));
					$delay_days = ceil(abs($end - $start) / 86400);
					
					
			        $worksheet->writeString($k,0,$i.".",$body_text_center);
					$worksheet->writeString($k,1,$obj->getVendorListNewBasedOnID($rows1['VENDOR']),$body_text);
					$worksheet->writeString($k,2,$obj->getCompareTableData($rows['COMID'],"MESSAGE"),$body_text);
					$worksheet->writeString($k,3,$obj->getVesselIMOData($obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID"),"VESSEL_NAME"),$body_text);
					$worksheet->writeString($k,4,(($obj->getFun112()=='' || $obj->getFun112()=='0000-00-00' || $obj->getFun112()=='1970-01-01')?'':date("d-m-Y",strtotime($obj->getFun112()))),$body_text);
					$worksheet->writeString($k,5,$cost_type,$body_text);
					$worksheet->writeString($k,6,date("d-m-Y",strtotime($rows1['DATE'])),$body_text);
					$worksheet->writeString($k,7,$obj->getNumberFormat($invoice_amt),$body_text);
					$worksheet->writeString($k,8,$obj->getNumberFormat($amt_paid),$body_text);
					$worksheet->writeString($k,9,$obj->getNumberFormat($diff),$body_text);
					$worksheet->writeString($k,10,$delay_days,$body_text);
					$i++;$k++;
				}
			}
		}
	}
	
	$workbook->send('Aging Report (Receivables) '.date("d-m-Y",time()).'.xls'); 
	$workbook->close();
}

function aging_payable_excel()
{
	
	$workbook 	= new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj 		= new data();
				  $obj->funConnect();
	$worksheet 	=& $workbook->addWorksheet('Aging Report (Payable)');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(100);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 5.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 15.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 20.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 20.00);
	$worksheet->setColumn(8, 8, 20.00);
	$worksheet->setColumn(9, 9, 20.00);
	$worksheet->setColumn(10, 10, 20.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,10,"Aging Report (Payable) ( ".date("d-M-Y",time())." )",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"S.No.",$main_header_1);
	$worksheet->writeString(2,1,"Vendor",$main_header_1);
	$worksheet->writeString(2,2,"Nom Id",$main_header_1);
	$worksheet->writeString(2,3,"Vessel",$main_header_1);
	$worksheet->writeString(2,4,"CP Date",$main_header_1);
	$worksheet->writeString(2,5,"Cost Type",$main_header_1);
	$worksheet->writeString(2,6,"Bill Date",$main_header_1);
	$worksheet->writeString(2,7,"Amount Billed",$main_header_1);
	$worksheet->writeString(2,8,"Amount Paid",$main_header_1);
	$worksheet->writeString(2,9,"Difference",$main_header_1);
	$worksheet->writeString(2,10,"Delay (Days)",$main_header_1);
	
	$worksheet->repeatRows(0,2);
	$worksheet->freezePanes(array(3, 1));
	$k = 3;
	$selVendor      = $_REQUEST['selVendor'];
	$selDate_Type 	= $_REQUEST['selDate_Type'];
	$selBType       = $_REQUEST['selBType'];
	
	$sql = "select * from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS in (1,2) and freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' group by freight_cost_estimate_compare.COMID";
	$res = mysql_query($sql) or die($sql);
	$rec = mysql_num_rows($res);
	$i = 1;
	if($rec == 0)
	{
		
	}
	else
	{	
		while($rows = mysql_fetch_assoc($res))
		{		
		    $fcaid = $obj->getLatestCostSheetID($rows['COMID']);
			$obj->viewFreightCostEstimationTempleteRecordsNew($fcaid);
			if($var = 1)
			{
				$sql1 = "select * from request_master where ".(($_REQUEST['selVendor']!='')?"VENDOR='".$selVendor."' and ":"")." COMID='".$rows['COMID']."'";
			}
			else if($var = 2)
			{
				$sql1 = "select * from request_master where ".(($_REQUEST['selVendor']!='')?"VENDOR='".$selVendor."' and ":"")."  (INVOICE_DATE BETWEEN '".date("Y-m-d",strtotime($from_date))."' and '".date("Y-m-d",strtotime($to_date))."')  and COMID='".$rows['COMID']."' ";
			}
			else if($var = 3)
			{
				$sql1 = "select * from request_master where ".(($_REQUEST['selVendor']!='')?"VENDOR='".$selVendor."' and ":"")."  INVOICE_DATE<'".date("Y-m-d",strtotime($from_date))."'  and COMID='".$rows['COMID']."' ";
			}
			$res1 = mysql_query($sql1);
			while ($rows1 = mysql_fetch_assoc($res1))
			{
				if($rows1['NAME_ID'] == 2)
				{
					$cost_type = 'Bunkers Nett Supply - '.$obj->getBunkerGradeBasedOnID($rows1['GRADEID']);
				}
				else if($rows1['NAME_ID'] == 3)
				{
					if($rows1['GRADEID']==0)
					{
						$cost_type = 'Operational Costs (Others) - Brokerage Commission (%)';
					}
					else
					{
						$cost_type = 'Operational Costs (Others) - '.$obj->getOwnerRelatedCostNameBasedOnID($rows1['GRADEID']);
					}
				}
				else if($rows1['NAME_ID'] == 4)
				{
					$cost_type = 'Charterers Costs';
				}
				else if($rows1['NAME_ID'] == 5)
				{
					if($rows1['PORT'] == 'Load')
					{
						$cost_type = 'Port Costs - '.$obj->getPortNameBasedOnID($rows1['GRADEID']).' (LP)';
					}
					else if($rows1['PORT'] == 'Discharge')
					{
						$cost_type = 'Port Costs - '.$obj->getPortNameBasedOnID($rows1['GRADEID']).' (DP)';
					}
					else if($rows1['PORT'] == 'Transit')
					{
						$cost_type = 'Port Costs - '.$obj->getPortNameBasedOnID($rows1['GRADEID']).' (TP)';
					}
				}
				else if($rows1['NAME_ID'] == 7)
				{
					$cost_type[$j] = 'Other Miscellaneous Costs';
				}
				else if($rows1['NAME_ID'] == 8)
				{
				    $cost_type[$j] = $rows1['NAME'];	
				}
				$invoice_amt= $rows1['NET_AMT'];
				$amt_paid = $rows1['P_AMT'];
				$diff = $invoice_amt - $amt_paid;
				
				$start = strtotime($rows1['INVOICE_DATE']);
				$end = strtotime(date('Y-m-d',time()));
				$delay_days = ceil(abs($end - $start) / 86400);
					
				if($rows1['VENDOR']!="")
				{
					$worksheet->writeString($k,0,$i.".",$body_text_center);
					$worksheet->writeString($k,1,$obj->getVendorListNewBasedOnID($rows1['VENDOR']),$body_text);
					$worksheet->writeString($k,2,$obj->getCompareTableData($rows['COMID'],"MESSAGE"),$body_text);
					$worksheet->writeString($k,3,$obj->getVesselIMOData($obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID"),"VESSEL_NAME"),$body_text);
					$worksheet->writeString($k,4,(($obj->getFun112()=='' || $obj->getFun112()=='0000-00-00' || $obj->getFun112()=='1970-01-01')?'':date("d-m-Y",strtotime($obj->getFun112()))),$body_text);
					$worksheet->writeString($k,5,$cost_type,$body_text);
					$worksheet->writeString($k,6,date("d-m-Y",strtotime($rows1['INVOICE_DATE'])),$body_text);
					$worksheet->writeString($k,7,$obj->getNumberFormat($invoice_amt),$body_text);
					$worksheet->writeString($k,8,$obj->getNumberFormat($amt_paid),$body_text);
					$worksheet->writeString($k,9,$obj->getNumberFormat($diff),$body_text);
					$worksheet->writeString($k,10,$delay_days,$body_text);
				   
				    $i++;$k++;
			    }	
			}
			if($var = 1)
			{
				$sql1 = "select * from invoice_hire_master where COMID='".$rows['COMID']."' ";
			}
			else if($var = 2)
			{
				$sql1 = "select * from invoice_hire_master where (INVOICE_DATE BETWEEN '".date("Y-m-d",strtotime($from_date))."' and '".date("Y-m-d",strtotime($to_date))."')  and COMID='".$rows['COMID']."'";
			}
			else if($var = 3)
			{
				$sql1 = "select * from invoice_hire_master where INVOICE_DATE<'".date("Y-m-d",strtotime($from_date))."'  and COMID='".$rows['COMID']."' ";
			}
			$res1 = mysql_query($sql1);
			while($rows1 = mysql_fetch_assoc($res1))
			{
				if($rows1['VENDORID']!="")
				{
					$cost_type = 'Daily Time Charter';
					$invoice_amt= $rows1['BALANCE_TO_OWNER'];
					$amt_paid = $rows1['P_AMT'];
					$diff = $invoice_amt - $amt_paid;
					$start = strtotime($rows1['INVOICE_DATE']);
					$end = strtotime(date('Y-m-d',time()));
					$delay_days = ceil(abs($end - $start) / 86400);
					
					$worksheet->writeString($k,0,$i.".",$body_text_center);
					$worksheet->writeString($k,1,$obj->getVendorListNewBasedOnID($rows1['VENDORID']),$body_text);
					$worksheet->writeString($k,2,$obj->getCompareTableData($rows['COMID'],"MESSAGE"),$body_text);
					$worksheet->writeString($k,3,$obj->getVesselIMOData($obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID"),"VESSEL_NAME"),$body_text);
					$worksheet->writeString($k,4,(($obj->getFun112()=='' || $obj->getFun112()=='0000-00-00' || $obj->getFun112()=='1970-01-01')?'':date("d-m-Y",strtotime($obj->getFun112()))),$body_text);
					$worksheet->writeString($k,5,$cost_type,$body_text);
					$worksheet->writeString($k,6,date("d-m-Y",strtotime($rows1['INVOICE_DATE'])),$body_text);
					$worksheet->writeString($k,7,$obj->getNumberFormat($invoice_amt),$body_text);
					$worksheet->writeString($k,8,$obj->getNumberFormat($amt_paid),$body_text);
					$worksheet->writeString($k,9,$obj->getNumberFormat($diff),$body_text);
					$worksheet->writeString($k,10,$delay_days,$body_text);
				    $i++;$k++;
			   }
			}
		}
	}
	$workbook->send('Aging Report (Payable) '.date("d-m-Y",time()).'.xls'); 
	$workbook->close();
}

function coa_excel()
{
	$selActive1 = $_REQUEST['selActive'];
	$txtCOADate = $_REQUEST['txtCOADate'];
	$selOwner = $_REQUEST['selOwner'];
	
	if($_REQUEST['selActive'] == ""){$selActive = 1;}
	else{if($_REQUEST['selActive'] == 3){$selActive = 1;}else{$selActive = $_REQUEST['selActive'];}}
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('COA Report');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(23);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 10.00);
	$worksheet->setColumn(1, 1, 25.00);
	$worksheet->setColumn(2, 2, 25.00);
	$worksheet->setColumn(3, 3, 30.00);
	$worksheet->setColumn(4, 4, 25.00);
	$worksheet->setColumn(5, 5, 25.00);
	$worksheet->setColumn(6, 6, 30.00);
	$worksheet->setColumn(7, 7, 30.00);
	$worksheet->setColumn(8, 8, 30.00);
	$worksheet->setColumn(9, 9, 25.00);
	$worksheet->setColumn(9, 10, 25.00);
	$worksheet->setColumn(9, 11, 25.00);
	$worksheet->setColumn(9, 12, 25.00);
	$worksheet->setColumn(9, 13, 25.00);
	$worksheet->setColumn(9, 14, 25.00);
	$worksheet->setColumn(9, 15, 25.00);
	$worksheet->setColumn(9, 16, 25.00);
	$worksheet->setColumn(9, 17, 25.00);
	$worksheet->setColumn(9, 18, 25.00);
	$worksheet->setColumn(9, 19, 25.00);
	$worksheet->setColumn(9, 20, 30.00);
	$worksheet->setColumn(9, 21, 25.00);
	$worksheet->setColumn(9, 22, 25.00);
	$worksheet->setColumn(9, 23, 35.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,23,"COA Report",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"Sr. No.",$main_header_1);
	$worksheet->writeString(2,1,"Cargo/Material Type",$main_header_1_center);
	$worksheet->writeString(2,2,"Vessel Category",$main_header_1_center);
	$worksheet->writeString(2,3,"COA Number",$main_header_1_center);
	$worksheet->writeString(2,4,"COA CP Date",$main_header_1_center);
	$worksheet->writeString(2,5,"Cargo Qty(MT)",$main_header_1_center);
	$worksheet->writeString(2,6,"Owner",$main_header_1_center);
	$worksheet->writeString(2,7,"Broker",$main_header_1_center);
	$worksheet->writeString(2,8,"Charterer",$main_header_1_center);
	$worksheet->writeString(2,9,"No. of Shipments",$main_header_1_center);
	$worksheet->writeString(2,10,"Shipment Qty(MT)",$main_header_1_center);
	$worksheet->writeString(2,11,"Tolerance ( % )",$main_header_1_center);
	$worksheet->writeString(2,12,"Shipment ( s )",$main_header_1_center);
	$worksheet->writeString(2,13,"per (month)",$main_header_1_center);
	$worksheet->writeString(2,14,"Starting Date",$main_header_1_center);
	$worksheet->writeString(2,15,"Notice ( Days )",$main_header_1_center);
	$worksheet->writeString(2,16,"Freight Rate (USD/MT)",$main_header_1_center);
	$worksheet->writeString(2,17,"Demurrage Rate",$main_header_1_center);
	$worksheet->writeString(2,18,"Dispatch Rate",$main_header_1_center);
	$worksheet->writeString(2,19,"Add Comm ( % )",$main_header_1_center);
	$worksheet->writeString(2,20,"Bunker Price IFO ( USD/MT )",$main_header_1_center);
	$worksheet->writeString(2,21,"BAF Terms ( Days )",$main_header_1_center);
	$worksheet->writeString(2,22,"BAF ( USD/MT )",$main_header_1_center);
	$worksheet->writeString(2,23,"Remarks",$main_header_1_center);
	
	$sql = "SELECT * from coa_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS='".$selActive."' ";
	if($_REQUEST['txtCOADate'] != "")
	{
	$sql.= " and COA_DATE = '".date("Y-m-d",strtotime($_REQUEST['txtCOADate']))."'  ";
	}
	if($_REQUEST['selOwner'] != "")
	{
	$sql.= " and OWNER = '".$_REQUEST['selOwner']."'  ";
	}
	$sql.="order by COA_DATE desc";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$j = 1; $k = 3;
	if($rec == 0)
	{	
		$worksheet->formatmergeCellsMultiple($k,0,$k,23,"SORRY CURRENTLY THERE ARE ZERO(0) RECORDS FOUND",$body_text_center);
	}
	else
	{
		while($rows = mysql_fetch_assoc($res))
		{ 
			if($selActive1 == 3)
			{
				if((int)$rows['NO_OF_SHIPMENT'] == (int)$obj->getPerformedVesselRec($rows['COAID']))
				{
					if($rows['BAF'] == 1)
					{$var1 = $obj->getNumberFormat($rows['BUNKER_PRICE']); $var2 = $rows['BAF_TERMS']; $var3 = $rows['INCREASE_DECREASE'];}
					else{$var1 = ''; $var2 = ''; $var3 = '';}
					
					$worksheet->setRow($k,20);
					$worksheet->writeString($k,0,$j.".",$body_text_center);
					$worksheet->writeString($k,1,$obj->getCargoNameBasedOnId($rows['CARGOTYPEID']),$body_text_center);
					$worksheet->writeString($k,2,$obj->getVesselCategoryTypeBasedOnId($rows['VESSEL_CATEGORY_ID']),$body_text_center);
					$worksheet->writeString($k,3,$rows['COA_NO'],$body_text);
					$worksheet->writeString($k,4,date("d-M-Y",strtotime($rows['COA_DATE'])),$body_text_center);
					$worksheet->writeString($k,5,$obj->getNumberFormat($rows['CARGO_QTY']),$body_text_center);
					$worksheet->writeString($k,6,$obj->getVendorListNewBasedOnID($rows['OWNER']),$body_text);
					$worksheet->writeString($k,7,$obj->getVendorListNewBasedOnID($rows['BROKER']),$body_text);
					$worksheet->writeString($k,8,$obj->getVendorListNewBasedOnID($rows['CHARTERER']),$body_text);
					$worksheet->writeString($k,9,$rows['NO_OF_SHIPMENT'],$body_text_center);
					$worksheet->writeString($k,10,$obj->getNumberFormat($rows['SIZE_OF_QTY']),$body_text_center);
					$worksheet->writeString($k,11,$rows['TOLERANCE'],$body_text_center);
					$worksheet->writeString($k,12,$rows['SHIPMENT'],$body_text_center);
					$worksheet->writeString($k,13,$rows['PER_MONTH'],$body_text_center);
					$worksheet->writeString($k,14,date("d-M-Y",strtotime($rows['START_DATE'])),$body_text_center);
					$worksheet->writeString($k,15,$rows['NOTICE_DAYS'],$body_text_center);
					$worksheet->writeString($k,16,$rows['FREIGHT_DAYS'],$body_text_center);
					$worksheet->writeString($k,17,$rows['DEMURRAGE_RATE'],$body_text_center);
					$worksheet->writeString($k,18,$rows['DISPATCH_RATE'],$body_text_center);
					$worksheet->writeString($k,19,$rows['ADD_COMM'],$body_text_center);
					$worksheet->writeString($k,20,$var1,$body_text_center);
					$worksheet->writeString($k,21,$var2,$body_text_center);
					$worksheet->writeString($k,22,$var3,$body_text_center);
					$worksheet->writeString($k,23,$rows['REMARKS'],$body_text);
					
					$k++; $j++;
				}
			}
			else
			{
				if((int)$rows['NO_OF_SHIPMENT'] != (int)$obj->getPerformedVesselRec($rows['COAID']))
				{
					if($rows['BAF'] == 1)
					{$var1 = $obj->getNumberFormat($rows['BUNKER_PRICE']); $var2 = $rows['BAF_TERMS']; $var3 = $rows['INCREASE_DECREASE'];}
					else{$var1 = ''; $var2 = ''; $var3 = '';}
					
					$worksheet->setRow($k,20);
					$worksheet->writeString($k,0,$j.".",$body_text_center);
					$worksheet->writeString($k,1,$obj->getCargoNameBasedOnId($rows['CARGOTYPEID']),$body_text_center);
					$worksheet->writeString($k,2,$obj->getVesselCategoryTypeBasedOnId($rows['VESSEL_CATEGORY_ID']),$body_text_center);
					$worksheet->writeString($k,3,$rows['COA_NO'],$body_text);
					$worksheet->writeString($k,4,date("d-M-Y",strtotime($rows['COA_DATE'])),$body_text_center);
					$worksheet->writeString($k,5,$obj->getNumberFormat($rows['CARGO_QTY']),$body_text_center);
					$worksheet->writeString($k,6,$obj->getVendorListNewBasedOnID($rows['OWNER']),$body_text);
					$worksheet->writeString($k,7,$obj->getVendorListNewBasedOnID($rows['BROKER']),$body_text);
					$worksheet->writeString($k,8,$obj->getVendorListNewBasedOnID($rows['CHARTERER']),$body_text);
					$worksheet->writeString($k,9,$rows['NO_OF_SHIPMENT'],$body_text_center);
					$worksheet->writeString($k,10,$obj->getNumberFormat($rows['SIZE_OF_QTY']),$body_text_center);
					$worksheet->writeString($k,11,$rows['TOLERANCE'],$body_text_center);
					$worksheet->writeString($k,12,$rows['SHIPMENT'],$body_text_center);
					$worksheet->writeString($k,13,$rows['PER_MONTH'],$body_text_center);
					$worksheet->writeString($k,14,date("d-M-Y",strtotime($rows['START_DATE'])),$body_text_center);
					$worksheet->writeString($k,15,$rows['NOTICE_DAYS'],$body_text_center);
					$worksheet->writeString($k,16,$rows['FREIGHT_DAYS'],$body_text_center);
					$worksheet->writeString($k,17,$rows['DEMURRAGE_RATE'],$body_text_center);
					$worksheet->writeString($k,18,$rows['DISPATCH_RATE'],$body_text_center);
					$worksheet->writeString($k,19,$rows['ADD_COMM'],$body_text_center);
					$worksheet->writeString($k,20,$var1,$body_text_center);
					$worksheet->writeString($k,21,$var2,$body_text_center);
					$worksheet->writeString($k,22,$var3,$body_text_center);
					$worksheet->writeString($k,23,$rows['REMARKS'],$body_text);
					
					$k++; $j++;
				}
			}
		}
	}
	
	$workbook->send('COA Report '.date("d-M-Y",time())); 
	$workbook->close();
}

function dead_freight_summery_excel()
{
	$txtFrom_Date = strtotime($_REQUEST['txtFrom_Date']); 
	$txtTo_Date   = strtotime($_REQUEST['txtTo_Date']);
	$chk          = $_REQUEST['chk'];
	$selBType     = $_REQUEST['selBType'];
	
	$workbook 	  = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj 	      = new data();
					$obj->funConnect();
	
	$worksheet =& $workbook->addWorksheet('Dead Freight Summary Report');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 10.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 15.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 20.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 35.00);
	$worksheet->setColumn(8, 8, 35.00);
	$worksheet->setColumn(9, 9, 35.00);
	$worksheet->setColumn(10, 10, 35.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,10,"Dead Freight Summary Report [ From(".date("d-m-Y",strtotime($_REQUEST['txtFrom_Date'])).") - To(".date("d-m-Y",strtotime($_REQUEST['txtTo_Date'])).") ]",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"Sr. No.",$main_header_1);
	$worksheet->writeString(2,1,"Nom Id",$main_header_1);
	$worksheet->writeString(2,2,"Vessel Name",$main_header_1);
	$worksheet->writeString(2,3,"CP Date",$main_header_1);
	$worksheet->writeString(2,4,"Shipper",$main_header_1);
	$worksheet->writeString(2,5,"Load Port",$main_header_1);
	$worksheet->writeString(2,6,"Discharge Port",$main_header_1);
	$worksheet->writeString(2,7,"Contract Qty (MT)",$main_header_1);
	$worksheet->writeString(2,8,"Dead freight Qty",$main_header_1);
	$worksheet->writeString(2,9,"Dead freight Amount",$main_header_1);
	
	$worksheet->freezePanes(array(3, 0));
	
	$sql = "select * from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS in (1,2) AND freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' order by freight_cost_estimate_compare.COMID desc";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$i=1; $k = 3;
	$cost_sheet_id = "";
	$arr = array();
	
	if($rec == 0)
	{	
		$worksheet->formatmergeCellsMultiple($k,0,$k,9,"sorry currently there are zero(0) records",$body_text);
	}
	else
	{
		while($rows = mysql_fetch_assoc($res))
		{
			$fcaid = $obj->getLatestCostSheetID($rows['COMID']);
            $obj->viewFreightCostEstimationTempleteRecordsNew($fcaid);
			
			$cpdate = strtotime($obj->getFun112());
			
			if($txtFrom_Date <= $cpdate && $cpdate <= $txtTo_Date)
			{
				if($obj->getFun2() == 2)
				{
					if($chk == 1)
					{
						if($obj->getFun67()!= 0)
						{
							$quantity = 0;
							if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==1)
							{
								$quantity = $obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY");
							}
							else if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==2)
							{
								$quantity = $obj->getCompareEstimateData($rows['COMID'],"TANK_QUANTITY");
							}
							else
							{
								if($obj->getCompareEstimateData($rows['COMID'],"QTY_TYPE_RADIO")== 1)
								{
									$quantity = $obj->getCompareEstimateData($rows['COMID'],"QUANTITY");
								}
								else
								{
									$sql12 = "select sum(QUANTITY) as sum from freight_cost_estimete_slave7 where FCAID='".$fcaid."'";
									$res12 = mysql_query($sql12);
									$row12 = mysql_fetch_assoc($res12);
									$quantity = $row12['sum'];
								}
							}
							
							if(date("d-M-Y",strtotime($obj->getFun112())) == "01-Jan-1970")
							{
								$cp_date = '';
							}
							else
							{
								$cp_date = date("d-m-Y",strtotime($obj->getFun112()));
							}
								
							$worksheet->setRow($k,20);
							$worksheet->writeString($k,0,$i.".",$body_text);
							$worksheet->writeString($k,1,$obj->getCompareTableData($rows['COMID'],"MESSAGE"),$body_text);
							$worksheet->writeString($k,2,$obj->getVesselIMOData($obj->getFun5(),"VESSEL_NAME"),$body_text);
							$worksheet->writeString($k,3,$cp_date,$body_text);
							$worksheet->writeString($k,4,$obj->getVendorListNewBasedOnID($obj->getFun117()),$body_text);
							$worksheet->writeString($k,5,implode(" , ",$obj->getLoadPortNameArrBasedOnIDComidAndProcessWithoutTBN($rows['COMID'])),$body_text);
							$worksheet->writeString($k,6,implode(" , ",$obj->getDisPortNameArrBasedOnIDComidAndProcessWithoutTBN($rows['COMID'])),$body_text);
							$worksheet->writeString($k,7,$quantity,$body_text_center);
							$worksheet->writeString($k,8,$obj->getFun53(),$body_text_center);
							$worksheet->writeString($k,9,$obj->getFun67(),$body_text_center);
							$i++;$k++;
						}
					}
					else
					{
						$quantity = 0;
						if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==1)
						{
							$quantity = $obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY");
						}
						else if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==2)
						{
							$quantity = $obj->getCompareEstimateData($rows['COMID'],"TANK_QUANTITY");
						}
						else
						{
							if($obj->getCompareEstimateData($rows['COMID'],"QTY_TYPE_RADIO")== 1)
							{
								$quantity = $obj->getCompareEstimateData($rows['COMID'],"QUANTITY");
							}
							else
							{
								$sql12 = "select sum(QUANTITY) as sum from freight_cost_estimete_slave7 where FCAID='".$fcaid."'";
								$res12 = mysql_query($sql12);
								$row12 = mysql_fetch_assoc($res12);
								$quantity = $row12['sum'];
							}
						}
						
						if(date("d-M-Y",strtotime($obj->getFun112())) == "01-Jan-1970")
						{
							$cp_date = '';
						}
						else
						{
							$cp_date = date("d-m-Y",strtotime($obj->getFun112()));
						}
						
						$worksheet->setRow($k,20);
						$worksheet->writeString($k,0,$i.".",$body_text);
						$worksheet->writeString($k,1,$obj->getCompareTableData($rows['COMID'],"MESSAGE"),$body_text);
						$worksheet->writeString($k,2,$obj->getVesselIMOData($obj->getFun5(),"VESSEL_NAME"),$body_text);
						$worksheet->writeString($k,3,$cp_date,$body_text);
						$worksheet->writeString($k,4,$obj->getVendorListNewBasedOnID($obj->getFun117()),$body_text);
						$worksheet->writeString($k,5,implode(" , ",$obj->getLoadPortNameArrBasedOnIDComidAndProcessWithoutTBN($rows['COMID'])),$body_text);
						$worksheet->writeString($k,6,implode(" , ",$obj->getDisPortNameArrBasedOnIDComidAndProcessWithoutTBN($rows['COMID'])),$body_text);
						$worksheet->writeString($k,7,$quantity,$body_text_center);
						$worksheet->writeString($k,8,$obj->getFun53(),$body_text_center);
						$worksheet->writeString($k,9,$obj->getFun67(),$body_text_center);
						$i++;$k++;
					}
				}
			}
		}
   }
	
	$workbook->send('Dead Freight Summary Report '.date("d-m-Y",time()).'.xls'); 
	$workbook->close();
}

function profitability_excel()
{
	$txtFrom_Date   = strtotime($_REQUEST['txtFrom_Date']); 
	$txtTo_Date     = strtotime($_REQUEST['txtTo_Date']);
	$selBType 	    = $_REQUEST['selBType'];

	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Profitability Report');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 10.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 15.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 20.00);
	$worksheet->setColumn(6, 6, 30.00);
	$worksheet->setColumn(7, 7, 30.00);
	$worksheet->setColumn(8, 8, 30.00);
	$worksheet->setColumn(9, 9, 30.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,8,"Profitability Report (".$name.") [ From(".date("d-M-Y",strtotime($_REQUEST['txtFrom_Date'])).") - To(".date("d-M-Y",strtotime($_REQUEST['txtTo_Date'])).") ]",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"Sr. No.",$main_header_1);
	$worksheet->writeString(2,1,"Nom Id",$main_header_1_center);
	$worksheet->writeString(2,2,"Vessel Name",$main_header_1_center);
	$worksheet->writeString(2,3,"CP Date",$main_header_1_center);
	$worksheet->writeString(2,4,"Status",$main_header_1_center);
	$worksheet->writeString(2,5,"Total Qty.",$main_header_1_center);
	$worksheet->writeString(2,6,"Earnings (Including Demurrage)",$main_header_1_center);
	$worksheet->writeString(2,7,"Expense",$main_header_1_center);
	$worksheet->writeString(2,8,"Earnings",$main_header_1_center);
	
	$sql = "select * from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS in (1,2) and freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' order by freight_cost_estimate_compare.COMID desc";
	$res = mysql_query($sql) or die($sql);
	$rec = mysql_num_rows($res);
	$j = 1;$k = 3;
	if($rec == 0)
	{
		
	}
	else
	{
		while($rows = mysql_fetch_assoc($res))
		{
			$var  = 0; 
			$fcaid = $obj->getLatestCostSheetID($rows['COMID']);
			$obj->viewFreightCostEstimationTempleteRecordsNew($fcaid);
			
			
			$quantity = 0;
			if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==1)
			{
				$quantity = $obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY");
			}
			else if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==2)
			{
				$quantity = $obj->getCompareEstimateData($rows['COMID'],"TANK_QUANTITY");
			}
			else
			{
				if($obj->getCompareEstimateData($rows['COMID'],"QTY_TYPE_RADIO")== 1)
				{
					$quantity = $obj->getCompareEstimateData($rows['COMID'],"QUANTITY");
				}
				else
				{
					$sql12 = "select sum(QUANTITY) as sum from freight_cost_estimete_slave7 where FCAID='".$fcaid."'";
					$res12 = mysql_query($sql12);
					$row12 = mysql_fetch_assoc($res12);
					$quantity = $row12['sum'];
				}
			}
			
			$cpdate = strtotime($obj->getFun112());
			
			if($rows['STATUS'] == '1'){$status = 'In Ops';}else{$status = 'Post Ops';}
			
			$mysql2 = "select sum(DDCDP_NETCOST) as sum2, sum(DDCLP_NETCOST) as sum3 from freight_cost_estimete_slave1 where FCAID='".$fcaid."'";
			$myres2 = mysql_query($mysql2) or die($mysql2);
			$myrows2 = mysql_fetch_assoc($myres2);
			
			$profit = $obj->getFun86() + $myrows2['sum2'] + $myrows2['sum3'];
			
			$expense = $obj->getFun84() + $obj->getFun153() + $obj->getFun152() + $obj->getFun131();
			
			$diff = $profit - $expense;
			
			if($txtFrom_Date <= $cpdate && $cpdate <= $txtTo_Date)
			{
				$worksheet->setRow($k,20);
				$worksheet->writeString($k,0,$j.".",$body_text_center);
				$worksheet->writeString($k,1,$obj->getCompareTableData($rows['COMID'],"MESSAGE"),$body_text_center);
				$worksheet->writeString($k,2,$obj->getVesselIMOData($obj->getFun5(),"VESSEL_NAME"),$body_text_center);
				$worksheet->writeString($k,3,(($obj->getFun112()=='' || $obj->getFun112()=='0000-00-00' || $obj->getFun112()=='1970-01-01')?'':date("d-m-Y",strtotime($obj->getFun112()))),$body_text_center);
				$worksheet->writeString($k,4,$status,$body_text_center);
				$worksheet->writeString($k,5,$quantity,$body_text_center);
				$worksheet->writeString($k,6,$obj->getNumberFormat($profit),$body_text_center);
				$worksheet->writeString($k,7,$expense,$body_text_center);
				$worksheet->writeString($k,8,$diff,$body_text_center);
				$k++; $j++;
			}
		}
	}
	
	$workbook->send('Profitability Report '.date("d-m-Y",time()).'.xls'); 
	$workbook->close();
	
}

function pni_club_declaration_excel()
{
	$txtFrom_Date = strtotime($_REQUEST['txtFrom_Date']); 
	$txtTo_Date = strtotime($_REQUEST['txtTo_Date']);
	$chk = $_REQUEST['chk'];
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('PNI Club Declaration Report');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 10.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 15.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 20.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 35.00);
	$worksheet->setColumn(8, 8, 35.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,8,"PNI Club Declaration Report [ From(".date("d-M-Y",strtotime($_REQUEST['txtFrom_Date'])).") - To(".date("d-M-Y",strtotime($_REQUEST['txtTo_Date'])).") ]",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"Sr. No.",$main_header_1);
	$worksheet->writeString(2,1,"Nom Id",$main_header_1);
	$worksheet->writeString(2,2,"Vessel Name",$main_header_1);
	$worksheet->writeString(2,3,"CP Date",$main_header_1);
	$worksheet->writeString(2,4,"Material Code",$main_header_1_center);
	$worksheet->writeString(2,5,"PNI Club Vendor",$main_header_1);
	$worksheet->writeString(2,6,"Amount",$main_header_1);
	$worksheet->writeString(2,7,"Paid",$main_header_1);
	$worksheet->writeString(2,8,"Balance",$main_header_1);
	
	$sql = "select * from mapping_master where MODULEID='".$_SESSION['moduleid']."'  AND MCOMPANYID='".$_SESSION['company']."' and STATUS in (2,5) order by MAPPINGID";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$i=1; $k = 3;
	$cost_sheet_id = "";
	$arr = array();
	
	if($rec == 0)
	{	
		$worksheet->formatmergeCellsMultiple(0,0,0,9,"sorry currently there are zero(0) records",$body_text);
	}
	else
	{
		while($rows = mysql_fetch_assoc($res))
		{
			$l_cost_sheet_id = $obj->getLatestCostSheetID($rows['MAPPINGID']);
			$obj->viewFreightEstimationRecords($rows['MAPPINGID'],$l_cost_sheet_id);
			//$cpdate = strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($rows['MAPPINGID'],"OPEN_VESSEL_ID"),"CP_DATE"));
			$cpdate = strtotime($obj->getFun3());
			
			if($txtFrom_Date <= $cpdate && $cpdate <= $txtTo_Date)
			{
					if($chk == 1)
					{
						if($obj->getOtherMiscDetailsBasedOnMappingid($rows['MAPPINGID'],$obj->getFun1(),'3',"AMOUNT") != 0)
						{
							if(date("d-M-Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($rows['MAPPINGID'],"OPEN_VESSEL_ID"),"CP_DATE"))) == "01-Jan-1970")
							{
								$cp_date == "";
							}
							else
							{
								$cp_date = date("d M Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($rows['MAPPINGID'],"OPEN_VESSEL_ID"),"CP_DATE")));
							}
							
							if($obj->getOtherMiscDetailsBasedOnMappingid($rows['MAPPINGID'],$obj->getFun1(),'3',"VENDORID") != "")
							{
								$paid = $obj->getPaidRequestAmount($rows['MAPPINGID'],'6','3');
								$final_paid = number_format($paid,2,'.',',');
							}
							else
							{
								$paid = 0.00;
								$final_paid = $paid;
							}
							
							$worksheet->setRow($k,20);
							$worksheet->writeString($k,0,$i.".",$body_text);
							$worksheet->writeString($k,1,$obj->getMappingData($rows['MAPPINGID'],"NOM_NAME"),$body_text);
							$worksheet->writeString($k,2,$obj->getVesselIMOData($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'VESSEL_IMO_ID'),"VESSEL_NAME"),$body_text);
							$worksheet->writeString($k,3,$cp_date,$body_text);
							$worksheet->writeString($k,4,$obj->getCargoContarctForMapping($obj->getMappingData($rows['MAPPINGID'],"CARGO_IDS"),$obj->getMappingData($rows['MAPPINGID'],"CARGO_POSITION")),$body_text_center);
							$worksheet->writeString($k,5,$obj->getVendorListNewBasedOnID($obj->getOtherMiscDetailsBasedOnMappingid($rows['MAPPINGID'],$obj->getFun1(),'3',"VENDORID")),$body_text);
							$worksheet->writeString($k,6,number_format($obj->getOtherMiscDetailsBasedOnMappingid($rows['MAPPINGID'],$obj->getFun1(),'3',"AMOUNT"),2,'.',','),$body_text);
							$worksheet->writeString($k,7,$final_paid,$body_text);
							$worksheet->writeString($k,8,number_format(($obj->getOtherMiscDetailsBasedOnMappingid($rows['MAPPINGID'],$obj->getFun1(),'3',"AMOUNT") - $paid),2,'.',','),$body_text_center);
							$i++;$k++;
						}
					}
					else
					{
							if(date("d-M-Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($rows['MAPPINGID'],"OPEN_VESSEL_ID"),"CP_DATE"))) == "01-Jan-1970")
							{
								$cp_date == "";
							}
							else
							{
								$cp_date = date("d M Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($rows['MAPPINGID'],"OPEN_VESSEL_ID"),"CP_DATE")));
							}
							
							if($obj->getOtherMiscDetailsBasedOnMappingid($rows['MAPPINGID'],$obj->getFun1(),'3',"VENDORID") != "")
							{
								$paid = $obj->getPaidRequestAmount($rows['MAPPINGID'],'6','3');
								$final_paid = number_format($paid,2,'.',',');
							}
							else
							{
								$paid = 0.00;
								$final_paid = $paid;
							}
							
							$worksheet->setRow($k,20);
							$worksheet->writeString($k,0,$i.".",$body_text);
							$worksheet->writeString($k,1,$obj->getMappingData($rows['MAPPINGID'],"NOM_NAME"),$body_text);
							$worksheet->writeString($k,2,$obj->getVesselIMOData($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'VESSEL_IMO_ID'),"VESSEL_NAME"),$body_text);
							$worksheet->writeString($k,3,$cp_date,$body_text);
							$worksheet->writeString($k,4,$obj->getCargoContarctForMapping($obj->getMappingData($rows['MAPPINGID'],"CARGO_IDS"),$obj->getMappingData($rows['MAPPINGID'],"CARGO_POSITION")),$body_text_center);
							$worksheet->writeString($k,5,$obj->getVendorListNewBasedOnID($obj->getOtherMiscDetailsBasedOnMappingid($rows['MAPPINGID'],$obj->getFun1(),'3',"VENDORID")),$body_text);
							$worksheet->writeString($k,6,number_format($obj->getOtherMiscDetailsBasedOnMappingid($rows['MAPPINGID'],$obj->getFun1(),'3',"AMOUNT"),2,'.',','),$body_text);
							$worksheet->writeString($k,7,$final_paid,$body_text);
							$worksheet->writeString($k,8,number_format(($obj->getOtherMiscDetailsBasedOnMappingid($rows['MAPPINGID'],$obj->getFun1(),'3',"AMOUNT") - $paid),2,'.',','),$body_text_center);
							$i++;$k++;
					}
			}
		 }	
	}
	
	$workbook->send('PNI Club Declaration Report '.date("d-M-Y",time())); 
	$workbook->close();
	
}

function owner_broker_fixture_excel()
{
	$selVendor = $_REQUEST['selVendor'];
	$selBOwner = $_REQUEST['selBOwner'];
	$selBType  = $_REQUEST['selBType'];
	if($selBOwner == 11){$label = "Owner";$label1 = "Broker";}else if($selBOwner == 12){$label = "Broker";$label1 = "Owner";}
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj 	  = new data();
	$obj->funConnect();
	
	$worksheet =& $workbook->addWorksheet('Owner Broker Report');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 10.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 20.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 20.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 20.00);
	$worksheet->setColumn(8, 8, 20.00);
	$worksheet->setColumn(9, 9, 20.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,9,"Owner/Broker Wise Fixture Report ( ".date("d-m-Y",time())." )",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"S.No.",$main_header_1);
	$worksheet->writeString(2,1,$label,$main_header_1);
	$worksheet->writeString(2,2,"Vessel Type",$main_header_1);
	$worksheet->writeString(2,3,"Nom ID",$main_header_1);
	$worksheet->writeString(2,4,"Vessel Name",$main_header_1);
	$worksheet->writeString(2,5,"CP Date",$main_header_1);
	$worksheet->writeString(2,6,"Contract Qty",$main_header_1);
	$worksheet->writeString(2,7,"COA/Spot",$main_header_1);
	$worksheet->writeString(2,8,"Fixture Type",$main_header_1);
	$worksheet->writeString(2,9,$label1,$main_header_1);
	
	$worksheet->freezePanes(array(3, 0));
	
	$sql1 = "select * from vessel_type_master where STATUS=1 and BusinessType='".$selBType."' order by VesselType";
	$res1 = mysql_query($sql1);
	$i=1;$j = 3;
	while($rows1 = mysql_fetch_assoc($res1))
	{
		$sql = "select freight_cost_estimate_compare.COMID, freight_cost_estimete_master.FCAID from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS=1 and freight_cost_estimete_master.VESSEL_TYPE='".$rows1['VesselType']."' and freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."'  order by freight_cost_estimete_master.FCAID desc";
		$res = mysql_query($sql);
		$rec = mysql_num_rows($res);
		
		$cost_sheet_id = "";
		$arr = array();
		while($rows = mysql_fetch_assoc($res))
		{  
			$fcaid = $obj->getCompareEstimateData($rows['COMID'],"FCAID");
			$obj->viewFreightCostEstimationTempleteRecordsNew($fcaid);
			if($selBOwner == 11)
			{  
				if($selVendor == $obj->getFun140())
				{
					$cpdate = $obj->getCompareEstimateData($rows['COMID'],"CP_DATE");
					if(date("d-M-Y",strtotime($cpdate)) == "01-Jan-1970")
					{
						$cpdate = '';
					}
					else
					{
						$cpdate = date("d-m-Y",strtotime($cpdate));
					}
					$sql2 = "select * from freight_cost_estimete_slave4 where FCAID='".$obj->getFun1()."'";
					$res2 = mysql_query($sql2);
					$rec2 = mysql_num_rows($res2);
					$vendorid = array();
					while($rows2 = mysql_fetch_assoc($res2))
					{
						$vendorid[] = $obj->getVendorListNewBasedOnID($rows2['VENDORID']);
					}
					$worksheet->writeString($j,0,$i.".",$body_text_center);
					$worksheet->writeString($j,1,$obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($rows['COMID'],"DTCVENDORID")),$body_text);
					$worksheet->writeString($j,2,$obj->getCompareEstimateData($rows['COMID'],"VESSEL_TYPE"),$body_text);
					$worksheet->writeString($j,3,$obj->getCompareEstimateData($rows['COMID'],"VOYAGE_NAME"),$body_text);
					$worksheet->writeString($j,4,$obj->getVesselIMOData($obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID"),"VESSEL_NAME"),$body_text);
					$worksheet->writeString($j,5,$cp_date,$body_text);
					
					$quantity = 0;
					if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==1)
					{
						$quantity = $obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY");
					}
					else if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==2)
					{
						$quantity = $obj->getCompareEstimateData($rows['COMID'],"TANK_QUANTITY");
					}
					else
					{
						if($obj->getCompareEstimateData($rows['COMID'],"QTY_TYPE_RADIO")== 1)
						{
							$quantity = $obj->getCompareEstimateData($rows['COMID'],"QUANTITY");
						}
						else
						{
							$sql12 = "select sum(QUANTITY) as sum from freight_cost_estimete_slave7 where FCAID='".$fcaid."'";
							$res12 = mysql_query($sql12);
							$row12 = mysql_fetch_assoc($res12);
							$quantity = $row12['sum'];
						}
					}
					$worksheet->writeString($j,6,$quantity,$body_text);
					$worksheet->writeString($j,7,$obj->getCompareEstimateData($rows['COMID'],"COA_SPOT"),$body_text);
					$worksheet->writeString($j,8,$obj->getFreightFixtureBasedOnID($obj->getCompareEstimateData($rows['COMID'],"FIXTURETYPEID")),$body_text);
					$worksheet->writeString($j,9,implode(', ',$vendorid),$body_text);
					$i++;$j++;	
				}
			}
		
			if($selBOwner == 12)
			{
				$sql2 = "select * from freight_cost_estimete_slave4 where FCAID='".$obj->getFun1()."'";
				$res2 = mysql_query($sql2);
				$rec2 = mysql_num_rows($res2);
				$vendorid = array();
				while($rows2 = mysql_fetch_assoc($res2))
				{
					if($selVendor == $rows2['VENDORID'])
					{
						$cpdate = $obj->getCompareEstimateData($rows['COMID'],"CP_DATE");
						if(date("d-M-Y",strtotime($cpdate)) == "01-Jan-1970")
						{
							$cp_date == "";
						}
						else
						{
							$cp_date = date("d-M-Y",strtotime($cpdate));
						}
						$worksheet->writeString($j,0,$i.".",$body_text_center);
						$worksheet->writeString($j,1,$obj->getVendorListNewBasedOnID($rows2['VENDORID']),$body_text);
						$worksheet->writeString($j,2,$obj->getCompareEstimateData($rows['COMID'],"VESSEL_TYPE"),$body_text);
						$worksheet->writeString($j,3,$obj->getCompareEstimateData($rows['COMID'],"VOYAGE_NAME"),$body_text);
						$worksheet->writeString($j,4,$obj->getVesselIMOData($obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID"),"VESSEL_NAME"),$body_text);
						$worksheet->writeString($j,5,$cp_date,$body_text);
						$quantity = 0;
						if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==1)
						{
							$quantity = $obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY");
						}
						else if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==2)
						{
							$quantity = $obj->getCompareEstimateData($rows['COMID'],"TANK_QUANTITY");
						}
						else
						{
							if($obj->getCompareEstimateData($rows['COMID'],"QTY_TYPE_RADIO")== 1)
							{
								$quantity = $obj->getCompareEstimateData($rows['COMID'],"QUANTITY");
							}
							else
							{
								$sql12 = "select sum(QUANTITY) as sum from freight_cost_estimete_slave7 where FCAID='".$fcaid."'";
								$res12 = mysql_query($sql12);
								$row12 = mysql_fetch_assoc($res12);
								$quantity = $row12['sum'];
							}
						}
						$worksheet->writeString($j,6,$quantity,$body_text);
						$worksheet->writeString($j,7,$obj->getCompareEstimateData($rows['COMID'],"COA_SPOT"),$body_text);
						$worksheet->writeString($j,8,$obj->getFreightFixtureBasedOnID($obj->getCompareEstimateData($rows['COMID'],"FIXTURETYPEID")),$body_text);
						$worksheet->writeString($j,9,$obj->getVendorListNewBasedOnID($obj->getFun140()),$body_text);
						$i++;$j++;
					}
				}
			}
		}
		if($i > 1)
			{
				
				$worksheet->writeString($j,0,"Total ".$obj->getVesselCategoryTypeBasedOnId($rows1['VESSEL_CATEGORY_ID']),$body_text);
				$worksheet->writeString($j,1,($i-1),$body_text);
				
			}
	}
	
	$workbook->send('Owner/Broker Wise Fixture Report '.date("d-m-Y",time()).'.xls'); 
	$workbook->close();
}

function demmurage_summary_pdf()
{
	$txtFrom_Date 	= strtotime($_REQUEST['txtFrom_Date']); 
	$txtTo_Date	  	= strtotime($_REQUEST['txtTo_Date']);
	$chk 		  	= $_REQUEST['chk'];
	$selBType       = $_REQUEST['selBType'];
	
	$workbook 		= new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj 	  		= new data();
					$obj->funConnect();
	$worksheet 		=& $workbook->addWorksheet('Demmurage Summary Report');
	
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 10.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 15.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 20.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 35.00);
	$worksheet->setColumn(8, 8, 35.00);
	$worksheet->setColumn(9, 15, 35.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->setRow(2,20.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,9,"Demmurage Summary Report [ From(".date("d-M-Y",strtotime($_REQUEST['txtFrom_Date'])).") - To(".date("d-M-Y",strtotime($_REQUEST['txtTo_Date'])).") ]",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"Sr. No.",$main_header_1);
	$worksheet->writeString(2,1,"Nom Id",$main_header_1);
	$worksheet->writeString(2,2,"Vessel Name",$main_header_1);
	$worksheet->writeString(2,3,"CP Date",$main_header_1);
	$worksheet->writeString(2,4,"Total Qty.",$main_header_1);
	$worksheet->writeString(2,5,"Port",$main_header_1);
	$worksheet->writeString(2,6,"Turn Time (Hrs)",$main_header_1);
	$worksheet->writeString(2,7,"TT terms",$main_header_1);
	$worksheet->writeString(2,8,"Load/Dispatch Rate (MT/Day)",$main_header_1);
	$worksheet->writeString(2,9,"Laytime Allowed(hrs)",$main_header_1);
	$worksheet->writeString(2,10,"Actual Laytime(hrs)",$main_header_1);
	$worksheet->writeString(2,11,"Time to demurrage/despatch(hrs)",$main_header_1);
	$worksheet->writeString(2,12,"Demurrage / Dispatch (USD)",$main_header_1);
	$worksheet->writeString(2,13,"Total Demurrage /Dispatch (USD)",$main_header_1);
	$worksheet->writeString(2,14,"Net Freight Effect (USD/MT)",$main_header_1);
	
	$k = 3;
	
	$sql = "select * from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS in (1,2) AND freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' order by freight_cost_estimate_compare.COMID desc";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$j = 3;
	if($rec == 0)
	{
		
	}
	else
	{	
		while($rows = mysql_fetch_assoc($res))
		{
			$fcaid = $obj->getLatestCostSheetID($rows['COMID']);
            $obj->viewFreightCostEstimationTempleteRecordsNew($fcaid);
			$cpdate = strtotime($obj->getFun112());
			if($txtFrom_Date <= $cpdate && $cpdate <= $txtTo_Date)
			{
				$arr       = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$fcaid);
				$port_arr  = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$fcaid);
				
				$lp_name = array(); $dp_name =array(); $lp_name_1 = array(); $dp_name_1 = array(); $totals = 0;
				$lp_turnTime = array();$dp_turnTime = array();$lp_ttterms = array();$dp_ttterms = array();$lp_loadRate = array();$dp_loadRate = array();
				$lp_laytime = array();$dp_laytime = array();$lp_Alaytime = array();$dp_Alaytime = array();$lp_timetodum = array();$dp_timetodum = array();
				for($i=0;$i<count($arr);$i++)
				{
					$arr_portval = explode('@@',$arr[$i]);
					if($arr_portval[0] == "LP")
					{
						$sql1 = "select * from laytime_master where COMID='".$rows['COMID']."' and LOGIN='INTERNAL_USER' and PORT='LP' and PORTID='".$arr_portval[1]."' and RANDOMID='".$arr_portval[2]."'";
						$res1 = mysql_query($sql1);
						$rec1 = mysql_num_rows($res1);
						$rows1 = mysql_fetch_assoc($res1);
						
						if($rows1['TTL_DEMURRAGE'] == 0 && $rows1['TTL_DESPATCH'] == 0){$value = 0;}
						if($rows1['TTL_DEMURRAGE'] == 0){$value = "-".$rows1['TTL_DESPATCH'];}
						if($rows1['TTL_DESPATCH'] == 0){$value = $rows1['TTL_DEMURRAGE'];}
						
						if($rec1 > 0){$lp_name_1[]= 'LP- '.$obj->getPortNameBasedOnID($arr_portval[1]).'- '.$obj->getNumberFormat($value); $totals = $totals+$value;}
						$lp_name[]= 'LP-'.$obj->getPortNameBasedOnID($arr_portval[1]);
						$lp_turnTime[]= 'LP- '.$obj->getPortNameBasedOnID($arr_portval[1]).'- '.$obj->getNumberFormat($rows1['TURN_TIME']);
						$lp_ttterms[]= 'LP- '.$obj->getPortNameBasedOnID($arr_portval[1]).'- '.$rows1['TT_TERMS'];
						$lp_loadRate[]= 'LP- '.$obj->getPortNameBasedOnID($arr_portval[1]).'- '.$rows1['LOADED_RATE'];
						$lp_laytime[]= 'LP- '.$obj->getPortNameBasedOnID($arr_portval[1]).'- '.$rows1['LAYTIME_ALLOWED'];
						$lp_Alaytime[]= 'LP- '.$obj->getPortNameBasedOnID($arr_portval[1]).'- '.$rows1['ACTUAL_LAYTIME'];
						if($rows1['TIME_TO_DEMURRAGE'] >0)
						{$lp_timetodum[]= 'LP- '.$obj->getPortNameBasedOnID($arr_portval[1]).'- DEM- '.$rows1['TIME_TO_DEMURRAGE'];}
						else
						{$lp_timetodum[]= 'LP- '.$obj->getPortNameBasedOnID($arr_portval[1]).'- DES- '.$rows1['TIME_TO_DESPATCH'];}
					}
					else
					{
						$sql1 = "select * from laytime_master where COMID='".$rows['COMID']."' and LOGIN='INTERNAL_USER' and PORT='DP' and PORTID='".$arr_portval[1]."' and RANDOMID='".$arr_portval[2]."'";
						$res1 = mysql_query($sql1);
						$rec1 = mysql_num_rows($res1);
						$rows1 = mysql_fetch_assoc($res1);
						
						if($rows1['TTL_DEMURRAGE'] == 0 && $rows1['TTL_DESPATCH'] == 0){$value = 0;}
						if($rows1['TTL_DEMURRAGE'] == 0){$value = "-".$rows1['TTL_DESPATCH'];}
						if($rows1['TTL_DESPATCH'] == 0){$value = $rows1['TTL_DEMURRAGE'];}
						
						if($rec1 > 0){$dp_name_1[]= 'DP-'.$obj->getPortNameBasedOnID($arr_portval[1]).'-'.$obj->getNumberFormat($value); $totals = $totals+$value;}
						$dp_name[]= 'DP-'.$obj->getPortNameBasedOnID($arr_portval[1]);
						$dp_turnTime[]= 'DP-'.$obj->getPortNameBasedOnID($arr_portval[1]).'-'.$obj->getNumberFormat($rows1['TURN_TIME']);
						$dp_ttterms[]= 'DP-'.$obj->getPortNameBasedOnID($arr_portval[1]).'- '.$rows1['TT_TERMS'];
						$dp_loadRate[]= 'DP-'.$obj->getPortNameBasedOnID($arr_portval[1]).'- '.$rows1['LOADED_RATE'];
						$dp_laytime[]= 'DP-'.$obj->getPortNameBasedOnID($arr_portval[1]).'- '.$rows1['LAYTIME_ALLOWED'];
						$dp_Alaytime[]= 'DP-'.$obj->getPortNameBasedOnID($arr_portval[1]).'- '.$rows1['ACTUAL_LAYTIME'];
						if($rows1['TIME_TO_DEMURRAGE'] >0)
						{$dp_timetodum[]= 'DP- '.$obj->getPortNameBasedOnID($arr_portval[1]).'- DEM- '.$rows1['TIME_TO_DEMURRAGE'];}
						else
						{$dp_timetodum[]= 'DP- '.$obj->getPortNameBasedOnID($arr_portval[1]).'- DES- '.$rows1['TIME_TO_DESPATCH'];}
					}
				}
				
				if(count($lp_name) >0 && count($dp_name) >0)
				{$portsdata = implode(', ',$lp_name).'  '.implode(', ',$dp_name);}
				if(count($lp_name)==0)
				{$portsdata = implode(', ',$dp_name);}
				if(count($dp_name)==0)
				{$portsdata = implode(', ',$lp_name);}
				
				if(count($lp_name_1) >0 && count($dp_name_1) >0)
				{$portsdata_1 = implode(', ',$lp_name_1).'  '.implode(', ',$dp_name_1);}
				if(count($lp_name_1)==0)
				{$portsdata_1 = implode(', ',$dp_name_1);}
				if(count($dp_name_1)==0)
				{$portsdata_1 = implode(', ',$lp_name_1);}
				
				if(count($lp_turnTime) >0 && count($dp_turnTime) >0)
				{$portsdata_2 = implode(', ',$lp_turnTime).'  '.implode(', ',$dp_turnTime);}
				if(count($lp_turnTime)==0)
				{$portsdata_2 = implode(', >',$dp_turnTime);}
				if(count($dp_turnTime)==0)
				{$portsdata_2 = implode(', ',$lp_turnTime);}
				
				if(count($lp_ttterms) >0 && count($dp_ttterms) >0)
				{$portsdata_3 = implode(', ',$lp_ttterms).'  '.implode(', ',$dp_ttterms);}
				if(count($lp_ttterms)==0)
				{$portsdata_3 = implode(', ',$dp_ttterms);}
				if(count($dp_ttterms)==0)
				{$portsdata_3 = implode(', ',$lp_ttterms);}
				
				if(count($lp_loadRate) >0 && count($dp_loadRate) >0)
				{$portsdata_4 = implode(', ',$lp_loadRate).'  '.implode(',  ',$dp_loadRate);}
				if(count($lp_loadRate)==0)
				{$portsdata_4 = implode(', ',$dp_loadRate);}
				if(count($dp_loadRate)==0)
				{$portsdata_4 = implode(', ',$lp_loadRate);}
				
				if(count($lp_laytime) >0 && count($dp_laytime) >0)
				{$portsdata_5 = implode(', ',$lp_laytime).'  '.implode(', ',$dp_laytime);}
				if(count($lp_laytime)==0)
				{$portsdata_5 = implode(', ',$dp_laytime);}
				if(count($dp_laytime)==0)
				{$portsdata_5 = implode(', ',$lp_laytime);}
				
				if(count($lp_Alaytime) >0 && count($dp_Alaytime) >0)
				{$portsdata_6 = implode(', ',$lp_Alaytime).'  '.implode(', ',$dp_Alaytime);}
				if(count($lp_Alaytime)==0)
				{$portsdata_6 = implode(', ',$dp_Alaytime);}
				if(count($dp_Alaytime)==0)
				{$portsdata_6 = implode(', ',$lp_Alaytime);}
				
				if(count($lp_timetodum) >0 && count($dp_timetodum) >0)
				{$portsdata_7 = implode(', ',$lp_timetodum).'  '.implode(', ',$dp_timetodum);}
				if(count($lp_timetodum)==0)
				{$portsdata_7 = implode(', ',$dp_timetodum);}
				if(count($dp_timetodum)==0)
				{$portsdata_7 = implode(', ',$lp_timetodum);}
				
				
				if($chk == 1){if($totals == 0){$var = 0;}else{$var = 1;}}
				else{$var = 1;}
				$quantity = 0;
				if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==1)
				{
					$quantity = $obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY");
				}
				else if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==2)
				{
					$quantity = $obj->getCompareEstimateData($rows['COMID'],"TANK_QUANTITY");
				}
				else
				{
					if($obj->getCompareEstimateData($rows['COMID'],"QTY_TYPE_RADIO")== 1)
					{
						$quantity = $obj->getCompareEstimateData($rows['COMID'],"QUANTITY");
					}
					else
					{
						$sql12 = "select sum(QUANTITY) as sum from freight_cost_estimete_slave7 where FCAID='".$fcaid."'";
						$res12 = mysql_query($sql12);
						$row12 = mysql_fetch_assoc($res12);
						$quantity = $row12['sum'];
					}
				}
				if($var == 1)
				{
					$worksheet->setRow($k,50);
					$worksheet->writeString($k,0,$k-2,$body_text);
					$worksheet->writeString($k,1,$obj->getCompareTableData($rows['COMID'],"MESSAGE"),$body_text);
					$worksheet->writeString($k,2,$obj->getVesselIMOData($obj->getFun5(),"VESSEL_NAME"),$body_text);
					$worksheet->writeString($k,3,date("d-m-Y",strtotime($obj->getFun112())),$body_text);
					$worksheet->writeString($k,4,$quantity,$body_text);
					$worksheet->writeString($k,5,$portsdata,$body_text);
					$worksheet->writeString($k,6,$portsdata_2,$body_text);
					$worksheet->writeString($k,7,$portsdata_3,$body_text);
					$worksheet->writeString($k,8,$portsdata_4,$body_text);
					$worksheet->writeString($k,9,$portsdata_5,$body_text);
					$worksheet->writeString($k,10,$portsdata_6,$body_text);
					$worksheet->writeString($k,11,$portsdata_7,$body_text);
					$worksheet->writeString($k,12,$portsdata_1,$body_text);
					$worksheet->writeString($k,13,$obj->getNumberFormat($totals),$body_text_center);
					$net_effect = $totals/$obj->getFun50();
					$worksheet->writeString($k,14,$obj->getNumberFormat($net_effect),$body_text_center);
					
					$j++;$k++;
				}
			}
		}
	}
	
	$workbook->send('Demmurage Summary Report '.date("d-m-Y",time()).'.xls'); 
	$workbook->close();
}

function spot_fixture_pdf()
{
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Fixture Report');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(72);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 5.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 15.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 20.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 20.00);
	$worksheet->setColumn(8, 8, 20.00);
	$worksheet->setColumn(9, 9, 20.00);
	$worksheet->setColumn(10, 10, 20.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,10,"Fixture Report ( ".date("d-M-Y",time())." )",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"S.No.",$main_header_1);
	$worksheet->writeString(2,1,"Voyage Financials Name",$main_header_1);
	$worksheet->writeString(2,2,"Vessel Name",$main_header_1);
	$worksheet->writeString(2,3,"CP Date",$main_header_1);
	$worksheet->writeString(2,4,"Owner",$main_header_1);
	$worksheet->writeString(2,5,"Broker",$main_header_1);
	$worksheet->writeString(2,6,"Vessel Type",$main_header_1);
	$worksheet->writeString(2,7,"Contract Qty (MT)",$main_header_1);
	$worksheet->writeString(2,8,"Load Port",$main_header_1);
	$worksheet->writeString(2,9,"Discharge Port",$main_header_1);
	$worksheet->writeString(2,10,"Freight (USD)",$main_header_1);
	
	$worksheet->repeatRows(0,2);
	$worksheet->freezePanes(array(3, 2));
	$txtFrom_Date = strtotime($_REQUEST['txtFrom_Date']);
	$txtTo_Date = strtotime($_REQUEST['txtTo_Date']);
	$selBType   = $_REQUEST['selBType'];
	
	$sql = "select freight_cost_estimate_compare.COMID as COMID, freight_cost_estimete_master.VESSEL_IMO_ID as VESSEL_IMO_ID, freight_cost_estimate_compare.MESSAGE as MESSAGE, freight_cost_estimete_master.COA_SPOT as COA_SPOT, freight_cost_estimete_master.FIXTURETYPEID as FIXTURETYPEID from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS=1 and (freight_cost_estimete_master.CP_DATE!='0000-00-00' or freight_cost_estimete_master.CP_DATE is not NULL) and freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' order by freight_cost_estimete_master.FCAID desc"; 
	
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$i=1;$j=3;
	$cost_sheet_id = "";
	$arr = array();
	while($rows = mysql_fetch_assoc($res))
	{
		$cp_date = $obj->getCompareEstimateData($rows['COMID'],"CP_DATE");
		$cpdate = strtotime($cp_date);
		if($txtFrom_Date <= $cpdate && $cpdate <= $txtTo_Date)
		{
			if($rows['COA_SPOT'] == 1)
			{
				$arr_load = $arr_discharge = array();
				$sql1 = "select FROM_PORT, TO_PORT, LOAD_PORT_COST, LOAD_PORT_QTY, DISC_PORT_COST, DISC_PORT_QTY from freight_cost_estimete_slave1 where FCAID='".$obj->getCompareEstimateData($rows['COMID'],"FCAID")."'";
				$res1 = mysql_query($sql1);
				while($row1 = mysql_fetch_assoc($res1))
				{
					if($row1['LOAD_PORT_QTY']>0)
					{
						$arr_load[] = $obj->getPortNameBasedOnID($row1['FROM_PORT']);
					}
					if($row1['DISC_PORT_QTY']>0)
					{
						$arr_discharge[] = $obj->getPortNameBasedOnID($row1['TO_PORT']);
					}
				}
				
				if(date("d-M-Y",strtotime($cp_date)) == "01-Jan-1970")
				{
					$cp_date == "";
				}
				else
				{
					$cp_date = date("d-M-Y",strtotime($cp_date));
				}
				
				
				$sql2 = "select * from freight_cost_estimete_slave4 where FCAID='".$obj->getCompareEstimateData($rows['COMID'],"FCAID")."'";
				$res2 = mysql_query($sql2);
				$rec2 = mysql_num_rows($res2);
				$vendorid = array();
				while($rows2 = mysql_fetch_assoc($res2))
				{
					$vendorid[] = $obj->getVendorListNewBasedOnID($rows2['VENDORID']);
				}
				
				$sql12 = "select * from freight_cost_estimete_slave7 where FCAID='".$obj->getCompareEstimateData($rows['COMID'],"FCAID")."'";
				$res12 = mysql_query($sql12);
				$rec12 = mysql_num_rows($res12);
				$arr2 = array();
				while($rows2 = mysql_fetch_assoc($res12))
				{
					$arr2[] = $rows2['GROSS_FREIGHT'];
				}
				
				$quantity = $freight = 0;
					if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==1)
					{
						$quantity = $obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY");
						if($obj->getCompareEstimateData($rows['COMID'],"GAS_MARKET"))
						{
							$freight = $obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY");
						}
						else
						{
							$freight = number_format(($obj->getCompareEstimateData($rows['COMID'],"GAS_BALTIC") + $obj->getCompareEstimateData($rows['COMID'],"ADDNL_PRENIUM"))*$obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY"), 2, '.', '');
						}
					}
					else if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==2)
					{
						$quantity = $obj->getCompareEstimateData($rows['COMID'],"TANK_QUANTITY");
						$sql12 = "select sum(TOTAL_AMOUNT) as sum1 from freight_cost_estimete_slave12 where FCAID='".$obj->getCompareEstimateData($rows['COMID'],"FCAID")."'";
						$res12 = mysql_query($sql12);
						$row12 = mysql_fetch_assoc($res12);
						$freight  = $row12['sum1'];
					}
					else
					{
						if($obj->getCompareEstimateData($rows['COMID'],"QTY_TYPE_RADIO")== 1)
						{
							$quantity = $obj->getCompareEstimateData($rows['COMID'],"QUANTITY");
							$freight  = $obj->getCompareEstimateData($rows['COMID'],"NET_PAYABLE");
						}
						else
						{
							$sql12 = "select sum(QUANTITY) as sum, sum(NET_FREIGHT) as sum2 from freight_cost_estimete_slave7 where FCAID='".$obj->getCompareEstimateData($rows['COMID'],"FCAID")."'";
							$res12 = mysql_query($sql12);
							$row12 = mysql_fetch_assoc($res12);
							$quantity = $row12['sum'];
							$freight  = number_format($row12['sum2'], 2, '.', '');
						}
					}
				
				$worksheet->writeString($j,0,$i,$body_text_center);
				$worksheet->writeString($j,1,$obj->getCompareEstimateData($rows['COMID'],"VOYAGE_NAME"),$body_text);
				$worksheet->writeString($j,2,$obj->getVesselIMOData($obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID"),"VESSEL_NAME"),$body_text);
				$worksheet->writeString($j,3,$cp_date,$body_text);
				$worksheet->writeString($j,4,$obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($rows['COMID'],"DTCVENDORID")),$body_text);
				$worksheet->writeString($j,5,implode(', ',$vendorid),$body_text);
				$worksheet->writeString($j,6,$obj->getCompareEstimateData($rows['COMID'],"VESSEL_TYPE"),$body_text);
				$worksheet->writeString($j,7,$quantity,$body_text);
				$worksheet->writeString($j,8,implode(', ',$arr_load),$body_text);
				$worksheet->writeString($j,9,implode(', ',$arr_discharge),$body_text);
				$worksheet->writeNumber($j,10,$freight,$body_text);
				$i++;$j++;
			}
		}
	}
		
		
		
		
		
		
		
	
	$workbook->send('Spot Fixture Report '.date("d-m-Y",time()).'.xls'); 
	$workbook->close();
}


function agent_list_excel()
{
	
	$workbook = new Spreadsheet_Excel_Writer(); 
	// creating an object for excel sheets
	$obj 	  = new data();
				$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Agent List');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(100);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 5.00);
	$worksheet->setColumn(1, 1, 20.00);
	$worksheet->setColumn(2, 2, 15.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 20.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 20.00);
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,7,"Agent List ( ".date("d-m-Y",time())." )",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->writeString(2,0,"S.No.",$main_header_1);
	$worksheet->writeString(2,1,"Voyage Financials Name",$main_header_1);
	$worksheet->writeString(2,2,"CP Date",$main_header_1);
	$worksheet->writeString(2,3,"Vessel Name",$main_header_1);
	$worksheet->writeString(2,4,"Port",$main_header_1);
	$worksheet->writeString(2,5,"Material Code",$main_header_1);
	$worksheet->writeString(2,6,"Agent",$main_header_1);
	$worksheet->writeString(2,7,"Shipper",$main_header_1);
	
	$worksheet->repeatRows(0,2);
	$worksheet->freezePanes(array(3, 2));
	$selPort = $_REQUEST['selPort'];
	
	$sql = "select * from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimate_compare.STATUS in (1,2) order by freight_cost_estimate_compare.COMID desc";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	$i=1;$k=3;
	$cost_sheet_id = "";
	$arr = array();
	while($rows = mysql_fetch_assoc($res))
	{
		$l_cost_sheet_id = $obj->getLatestCostSheetID($rows['COMID']);
		$arr = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($rows['COMID'],$l_cost_sheet_id);
		$port_arr = $obj->getPortNameBasedOnID($rows['LPID']);
		for($j=0;$j<count($arr);$j++)
		{
			$arr_portval = explode('@@',$arr[$j]);
			if($selPort == $arr_portval[1])
			{
				$worksheet->writeString($k,0,$i.".",$body_text_center);
				$worksheet->writeString($k,1,$obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME"),$body_text);
				if(date("d-M-Y",strtotime($rows['CP_DATE'])) == "01-Jan-1970")
				{
					$cp_date = '';
				}
				else
				{
					$cp_date = date("d-m-Y",strtotime($rows['CP_DATE']));
				}
				$worksheet->writeString($k,2,$cp_date,$body_text);
				$worksheet->writeString($k,3,$rows['VOYAGE_NAME'],$body_text);
				$worksheet->writeString($k,4,$obj->getPortName($arr_portval[1]),$body_text);
				$worksheet->writeString($k,5,$obj->getCargoNameListForMultipleName($rows['CARGO_ID']),$body_text);
								
				$mysql = "select * from generate_agency_letter where COMID='".$rows['COMID']."' and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_portval[0]."' and PORTID='".$arr_portval[1]."' and RANDOMID='".$arr_portval[2]."' and STATUS=2";
				$myres = mysql_query($mysql);
				$myrec = mysql_num_rows($myres);
				if($myrec == 1)
				{
					$myrows  = mysql_fetch_assoc($myres);
					$agent = $obj->getVendorListNewBasedOnID($myrows['VENDORID']);
				}
				else
				{
					$agent = "";
				}
				$worksheet->writeString($k,6,$agent,$body_text);
				$worksheet->writeString($k,7,$obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($rows['COMID'],"SHIPPER")),$body_text);
				$i++;$k++;
			}
			else if($selPort == "")
			{
				$worksheet->writeString($k,0,$i.".",$body_text_center);
				$worksheet->writeString($k,1,$obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME"),$body_text);
				if(date("d-M-Y",strtotime($rows['CP_DATE'])) == "01-Jan-1970")
				{
					$cp_date = '';
				}
				else
				{
					$cp_date = date("d-m-Y",strtotime($rows['CP_DATE']));
				}
				$worksheet->writeString($k,2,$cp_date,$body_text);
				$worksheet->writeString($k,3,$rows['VOYAGE_NAME'],$body_text);
				$worksheet->writeString($k,4,$obj->getPortName($arr_portval[1]),$body_text);
				$worksheet->writeString($k,5,$obj->getCargoNameListForMultipleName($rows['CARGO_ID']),$body_text);
								
				$mysql = "select * from generate_agency_letter where COMID='".$rows['COMID']."' and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and PORT='".$arr_portval[0]."' and PORTID='".$port_arr[$j]."' and RANDOMID='".$arr_portval[2]."' and STATUS=2";
				$myres = mysql_query($mysql);
				$myrec = mysql_num_rows($myres);
				if($myrec == 1)
				{
					$myrows  = mysql_fetch_assoc($myres);
					$agent = $obj->getVendorListNewBasedOnID($myrows['VENDORID']);
				}
				else
				{
					$agent = "";
				}
				$worksheet->writeString($k,6,$agent,$body_text);
				$worksheet->writeString($k,7,$obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($rows['COMID'],"SHIPPER")),$body_text);
				$i++;$k++;
			}
		}
	}	
	
	$workbook->send('Agent Report '.date("d-m-Y",time()).'.xls'); 
	$workbook->close();
}

function getCOAReportDataPdf_excel()
{
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	if(isset($_REQUEST['selFYear'])){$curr_fyear = $_REQUEST['selFYear']; }
	$worksheet =& $workbook->addWorksheet($curr_fyear.' COA');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 12,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1,'color'=>'black'));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'lightgrey' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text_bold = & $workbook->addFormat(array('Size' => 8,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'color'=>'blue'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(52);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 2, 13.00);
	$worksheet->setColumn(3, 21, 12.00);
	
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,21,$curr_fyear."    COA STATUS SHEET",$main_header);
	$curr_fyear_arr = explode("-",$curr_fyear);
	
	$worksheet->setRow(2,20.75);
	$worksheet->formatmergeCellsMultiple(2,0,2,0,"Owner",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,1,2,1,"COA CP Date",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,2,2,3,"Total",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,4,2,5,"Performed / Nominated",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,6,2,7,"Balance",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,8,2,8,'Apr-'.$curr_fyear_arr[0],$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,9,2,9,'May-'.$curr_fyear_arr[0],$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,10,2,10,'Jun-'.$curr_fyear_arr[0],$main_header_1_center);
	
	$worksheet->formatmergeCellsMultiple(2,11,2,11,'July-'.$curr_fyear_arr[0],$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,12,2,12,'Aug-'.$curr_fyear_arr[0],$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,13,2,13,'Sep-'.$curr_fyear_arr[0],$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,14,2,14,'Oct-'.$curr_fyear_arr[0],$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,15,2,15,'Nov-'.$curr_fyear_arr[0],$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,16,2,16,'Dec-'.$curr_fyear_arr[0],$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,17,2,17,'Jan-'.($curr_fyear_arr[0]+1),$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,18,2,18,'Feb-'.($curr_fyear_arr[0]+1),$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,19,2,19,'Mar-'.($curr_fyear_arr[0]+1),$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,20,2,20,'Notice(d)',$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,21,2,21,'Dem',$main_header_1_center);
	
	$worksheet->setRow(3,20.75);
	$worksheet->formatmergeCellsMultiple(3,0,3,0,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,1,3,1,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,2,3,2,"Vol. MT",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,3,3,3,"Shpmt No",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,4,3,4,"Vol. MT",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,5,3,5,"Shpmt No",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,6,3,6,"Vol. MT",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,7,3,7,"Shpmt No",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,8,3,8,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,9,3,9,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,10,3,10,"",$main_header_1);
	
	$worksheet->formatmergeCellsMultiple(3,11,3,11,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,12,3,12,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,13,3,13,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,14,3,14,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,15,3,15,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,16,3,16,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,17,3,17,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,18,3,18,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,19,3,19,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,20,3,20,"",$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,21,3,21,"",$main_header_1);
		
	$worksheet->repeatRows(0,3);
	$worksheet->freezePanes(array(4, 2));
	
	$ttl_vol_mt = $ttl_shpmt = $ttl_perf = $ttl_perf_shpmt = $blnce_vol_mt = $blnce_shpmt = 0;
	
	$i=4;
	$sql = "select * from vessel_category_master where STATUS=1 order by NAME";
	$res  = mysql_query($sql);
	while($rows = mysql_fetch_assoc($res))
	{
		$worksheet->setRow($i,20.75);
		$worksheet->formatmergeCellsMultiple($i,0,$i,21,strtoupper($rows['NAME']),$body_text_bold);
		$j=$i+1;
		$sql1 = "select * from coa_master where MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and VESSEL_CATEGORY_ID='".$rows['VESSEL_CATEGORY_ID']."' and START_DATE between '".$curr_fyear_arr[0]."-04-01' and '".$curr_fyear_arr[1]."-03-31'";
		$res1 = mysql_query($sql1);
		$rec1 = mysql_num_rows($res1);
		if($rec1 == 0)
		{
			$worksheet->formatmergeCellsMultiple($j,0,$j,21,"",$body_text_bold);
		}
		else
		{
			$ttl_vol_mt_l = $ttl_shpmt_l = $ttl_perf_l = $ttl_perf_shpmt_l = $blnce_vol_mt_l = $blnce_shpmt_l = 0;
			while($rows1 = mysql_fetch_assoc($res1))
			{
				$worksheet->setRow($j,40.75);
				if($obj->getPerformedVesselRec($rows1['COAID']) == 0){$perf_qty = "0.00";}else{$perf_qty = ($obj->getPerformedVesselRec($rows1['COAID']) * $rows['SIZE']) / 1000000;}
				if($rows1['STATUS'] == 2)
				{
					
					$worksheet->formatmergeCellsMultiple($j,0,$j,0,$obj->getVendorListNewBasedOnID($rows1['OWNER']),$body_text);
					$worksheet->formatmergeCellsMultiple($j,1,$j,1,date("d M Y",strtotime($rows1['COA_DATE'])),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,2,$j,2,$obj->getNumberFormat(($rows1['NO_OF_SHIPMENT'] * $rows['SIZE']) / 1000000),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,3,$j,3,$rows1['NO_OF_SHIPMENT'],$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,4,$j,4,$obj->getNumberFormat($perf_qty),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,5,$j,5,$obj->getPerformedVesselRec($rows1['COAID']),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,6,$j,6,$obj->getNumberFormat((($rows1['NO_OF_SHIPMENT'] * $rows['SIZE']) / 1000000) - $perf_qty),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,7,$j,7,($rows1['NO_OF_SHIPMENT'] - $obj->getPerformedVesselRec($rows1['COAID'])),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,8,$j,19,$rows1['CANCEL_REMARKS'],$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,20,$j,20,$rows1['NOTICE_DAYS'].' (d)',$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,21,$j,21,$obj->getNumberFormat($rows1['DEMURRAGE_RATE']),$body_text_center);
				}
				else
				{
				
					$worksheet->formatmergeCellsMultiple($j,0,$j,0,$obj->getVendorListNewBasedOnID($rows1['OWNER']),$body_text);
					$worksheet->formatmergeCellsMultiple($j,1,$j,1,date("d M Y",strtotime($rows1['COA_DATE'])),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,2,$j,2,$obj->getNumberFormat(($rows1['NO_OF_SHIPMENT'] * $rows['SIZE']) / 1000000),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,3,$j,3,$rows1['NO_OF_SHIPMENT'],$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,4,$j,4,$obj->getNumberFormat($perf_qty),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,5,$j,5,$obj->getPerformedVesselRec($rows1['COAID']),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,6,$j,6,$obj->getNumberFormat((($rows1['NO_OF_SHIPMENT'] * $rows['SIZE']) / 1000000) - $perf_qty),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,7,$j,7,($rows1['NO_OF_SHIPMENT'] - $obj->getPerformedVesselRec($rows1['COAID'])),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,8,$j,8,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"04"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,9,$j,9,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"05"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,10,$j,10,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"06"),$body_text_center);
					
					$worksheet->formatmergeCellsMultiple($j,11,$j,11,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"07"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,12,$j,12,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"08"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,13,$j,13,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"09"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,14,$j,14,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"10"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,15,$j,15,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"11"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,16,$j,16,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"12"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,17,$j,17,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"01"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,18,$j,18,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"02"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,19,$j,19,$obj->getAttachedVesselWithCOABasedOnFinancialYearForPDF($rows1['COAID'],$curr_fyear_arr[0],"03"),$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,20,$j,20,$rows1['NOTICE_DAYS'].' (d)',$body_text_center);
					$worksheet->formatmergeCellsMultiple($j,21,$j,21,$obj->getNumberFormat($rows1['DEMURRAGE_RATE']),$body_text_center);
				}
				$ttl_vol_mt += $obj->getNumberFormat(($rows1['NO_OF_SHIPMENT'] * $rows['SIZE']) / 1000000);
				$ttl_shpmt += $rows1['NO_OF_SHIPMENT'];
				$ttl_perf += $perf_qty;
				$ttl_perf_shpmt += $obj->getPerformedVesselRec($rows1['COAID']);
				$blnce_vol_mt += $obj->getNumberFormat((($rows1['NO_OF_SHIPMENT'] * $rows['SIZE']) / 1000000) - $perf_qty);
				$blnce_shpmt += ($rows1['NO_OF_SHIPMENT'] - $obj->getPerformedVesselRec($rows1['COAID']));
				
				$ttl_vol_mt_l += $obj->getNumberFormat(($rows1['NO_OF_SHIPMENT'] * $rows['SIZE']) / 1000000);
				$ttl_shpmt_l += $rows1['NO_OF_SHIPMENT'];
				$ttl_perf_l += $perf_qty;
				$ttl_perf_shpmt_l += $obj->getPerformedVesselRec($rows1['COAID']);
				$blnce_vol_mt_l += $obj->getNumberFormat((($rows1['NO_OF_SHIPMENT'] * $rows['SIZE']) / 1000000) - $perf_qty);
				$blnce_shpmt_l += ($rows1['NO_OF_SHIPMENT'] - $obj->getPerformedVesselRec($rows1['COAID']));
				$j++;
			}
				$worksheet->setRow($j,20.75);
				$worksheet->formatmergeCellsMultiple($j,0,$j,1,"Totals ".strtoupper($rows['NAME']),$body_text);
				$worksheet->formatmergeCellsMultiple($j,2,$j,2,$ttl_vol_mt_l,$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,3,$j,3,$ttl_shpmt_l,$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,4,$j,4,$obj->getNumberFormat($ttl_perf_l),$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,5,$j,5,$ttl_perf_shpmt_l,$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,6,$j,6,$blnce_vol_mt_l,$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,7,$j,7,$blnce_shpmt_l,$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,8,$j,8,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,9,$j,9,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,10,$j,10,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,11,$j,11,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,12,$j,12,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,13,$j,13,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,14,$j,14,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,15,$j,15,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,16,$j,16,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,17,$j,17,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,18,$j,18,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,19,$j,19,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,20,$j,20,"",$body_text_center);
				$worksheet->formatmergeCellsMultiple($j,21,$j,21,"",$body_text_center);
			
		}		
		$i = $j+1;
	}
	$worksheet->setRow($i,20.75);
	$worksheet->formatmergeCellsMultiple($i,0,$i,1,"GRAND TOTALS ",$body_text);
	$worksheet->formatmergeCellsMultiple($i,2,$i,2,$ttl_vol_mt,$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,3,$i,3,$ttl_shpmt,$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,4,$i,4,$obj->getNumberFormat($ttl_perf),$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,5,$i,5,$ttl_perf_shpmt,$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,6,$i,6,$blnce_vol_mt,$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,7,$i,7,$blnce_shpmt,$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,8,$i,8,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,9,$i,9,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,10,$i,10,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,11,$i,11,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,12,$i,12,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,13,$i,13,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,14,$i,14,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,15,$i,15,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,16,$i,16,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,17,$i,17,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,18,$i,18,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,19,$i,19,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,20,$i,20,"",$body_text_center);
	$worksheet->formatmergeCellsMultiple($i,21,$i,21,"",$body_text_center);
	
	$workbook->send($curr_fyear." COA Status Sheet -  ".date("d-m-Y",time()).".xls"); 
	$workbook->close();
}

function cargo_open_position_excel()
{

	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj 	  = new data();
	$selBType = $_REQUEST['selBType'];
	$obj->funConnect();
	$worksheet = & $workbook->addWorksheet('Pending Cargo List');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(64);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 15.00);
	$worksheet->setColumn(1, 1, 25.00);
	$worksheet->setColumn(2, 2, 30.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 25.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 20.00);
	
	
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,9,"List of Cargos Pending For Vessel Nomination ( ".date("d-m-Y",time())." )",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->formatmergeCellsMultiple(2,0,2,0,"Cargo ID",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,1,2,1,"Shipper",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,2,2,2,"Material Desc",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,3,2,3,"Material Qty",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,4,2,4,"Load Port",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,5,2,5,"Discharge Port",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,6,2,6,"Date Range",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,7,2,7,"Open Vessel",$main_header_1_center);
	
	$worksheet->repeatRows(0,2);
	$worksheet->freezePanes(array(3, 2));
	//$sort = $_REQUEST['txtSort'];
	
	$sql = "select * from freight_cost_estimete_master where SHEET_NO is NULL and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'  and CARGOID!='' and ESTIMATE_TYPE='".$selBType."'";
	$res = mysql_query($sql);
	$i=3;
	while($rows = mysql_fetch_assoc($res))
	{
		$quantity = 0;
		if($rows['ESTIMATE_TYPE']==1)
		{
			$quantity = $rows['GAS_QUANTITY'];
		}
		else if($rows['ESTIMATE_TYPE']==2)
		{
			$quantity = $rows['TANK_QUANTITY'];
		}
		else
		{
			if($rows['QTY_TYPE_RADIO'] == 1)
			{
				$quantity = $rows['QUANTITY'];
			}
			else
			{
				$sql12 = "select sum(QUANTITY) as sum from freight_cost_estimete_slave7 where FCAID='".$rows['FCAID']."'";
				$res12 = mysql_query($sql12);
				$row12 = mysql_fetch_assoc($res12);
				$quantity = $row12['sum'];
			}
		}
		if($rows['LAYCAN_START_DATE']=="" || $rows['LAYCAN_START_DATE']=="0000-00-00"){$txtFDate = "";}else{$txtFDate = date('d-m-Y',strtotime($rows['LAYCAN_START_DATE']));}
		if($rows['LAYCAN_FINISH_DATE']=="" || $rows['LAYCAN_FINISH_DATE']=="0000-00-00"){$txtTDate = "";}else{$txtTDate = date('d-m-Y',strtotime($rows['LAYCAN_FINISH_DATE']));}
		$arr_load = $arr_discharge = array();
		$sql1 = "select FROM_PORT, TO_PORT, LOAD_PORT_COST, LOAD_PORT_QTY, DISC_PORT_COST, DISC_PORT_QTY from freight_cost_estimete_slave1 where FCAID='".$rows['FCAID']."'";
		$res1 = mysql_query($sql1);
		while($row1 = mysql_fetch_assoc($res1))
		{
			if($row1['LOAD_PORT_QTY']>0)
			{
				$arr_load[] = $obj->getPortNameBasedOnID($row1['FROM_PORT']);
			}
			if($row1['DISC_PORT_QTY']>0)
			{
				$arr_discharge[] = $obj->getPortNameBasedOnID($row1['TO_PORT']);
			}
		}
	
		$worksheet->setRow($i,28.75);
		$worksheet->formatmergeCellsMultiple($i,0,$i,0,$rows['CARGOID'],$body_text);
		$worksheet->formatmergeCellsMultiple($i,1,$i,1,$obj->getVendorListNewBasedOnID($rows['SHIPPER']),$body_text);
		$worksheet->formatmergeCellsMultiple($i,2,$i,2,$obj->getCargoNameListForMultipleNamemultiple($rows['CARGO_ID']),$body_text_center);
		$worksheet->formatmergeCellsMultiple($i,3,$i,3,$quantity,$body_text);
		$worksheet->formatmergeCellsMultiple($i,4,$i,4,implode(', ',$arr_load),$body_text);
		$worksheet->formatmergeCellsMultiple($i,5,$i,5,implode(', ',$arr_discharge),$body_text);
		$worksheet->formatmergeCellsMultiple($i,6,$i,6,$txtFDate.' To '.$txtTDate,$body_text);
		$worksheet->formatmergeCellsMultiple($i,7,$i,7,$obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME"),$body_text);
		
		$i++;
	}
	
	$workbook->send('Cargo Open Position Report '.date("d-m-Y",time()).'.xls'); 
	$workbook->close();
}

function cargo_open_position_excel_old()
{
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Pending Cargo List');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'middle','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$main_header_1_center = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1 ,'bgcolor'=>'black' ,'color'=>'white'));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text_center = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(9);
	$worksheet->setPrintScale(70);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 0, 15.00);
	$worksheet->setColumn(1, 1, 15.00);
	$worksheet->setColumn(2, 2, 30.00);
	$worksheet->setColumn(3, 3, 20.00);
	$worksheet->setColumn(4, 4, 20.00);
	$worksheet->setColumn(5, 5, 25.00);
	$worksheet->setColumn(6, 6, 20.00);
	$worksheet->setColumn(7, 7, 17.00);
	$worksheet->setColumn(8, 8, 45.00);
	$worksheet->setRow(0,20.75);
	$worksheet->setRow(1,10.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,8,"List of Cargos Pending For Vessel Nomination ( ".date("d-M-Y",time())." )",$main_header);
	
	$worksheet->setRow(2,15.75);
	$worksheet->formatmergeCellsMultiple(2,0,2,0,"Contract ID",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,1,2,1,"Material Type",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,2,2,2,"Material Desc",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,3,2,3,"Load Port",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,4,2,4,"Discharge Port",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,5,2,5,"Date Range",$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,6,2,6,"Material Qty",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,7,2,7,"Tolerance (+/-)",$main_header_1_center);
	$worksheet->formatmergeCellsMultiple(2,8,2,8,"Remarks",$main_header_1);
	
	$worksheet->repeatRows(0,2);
	$worksheet->freezePanes(array(3, 2));
	//$sort = $_REQUEST['txtSort'];
	
	$sql = "select CONTRACTID,SHIPPING_STAGE from open_cargo_master where CONTRACT_TYPE in ('ZBP1') AND MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' AND STATUS=1 AND CARGO_STATUS=0 and MAP_STATUS=1 and SHIIPING_REQ='Y' GROUP BY CONTRACTID,SHIPPING_STAGE ORDER BY CONTRACTID asc";
	$res = mysql_query($sql);
	$i = 3;
	$k=0;
	while($rows = mysql_fetch_assoc($res))
	{
				
		if($obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"PURCHASE_OVER_TOL") != "0" && $obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"PURCHASE_UNDER_TOL") != "0")
		{
			$tolerance = "+/-".$obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"PURCHASE_OVER_TOL");
		}
		else if($obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"PURCHASE_OVER_TOL") == "0" && $obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"PURCHASE_UNDER_TOL") != "0")
		{
			$tolerance = "-".$obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"PURCHASE_UNDER_TOL");
		}
		else if($obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"PURCHASE_OVER_TOL") != "0" && $obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"PURCHASE_UNDER_TOL") == "0")
		{
			$tolerance = "+".$obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"PURCHASE_OVER_TOL");
		}
		else
		{
			$tolerance = "";
		}
		
		$arr4[$k]=array($rows['CONTRACTID'], $obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"MATERIAL_TYPE"),$obj->getMaterialCodeDesBasedOnCode($obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"MATERIAL_CODE")),$obj->getPortNameBasedOnCode($obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"LOADPORT")),$obj->getPortNameBasedOnCode($obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"DISPORT")),date("d-M-Y",strtotime($obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"LAYCAN_START"))).' To '.date("d-M-Y",strtotime($obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"LAYCAN_FINISH"))),$obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"MATERIAL_QUANTITY").' ('.$obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"UOM").')',$tolerance,$obj->getOpenCargoMasterDataBasedOnContractID($rows['CONTRACTID'],$rows['SHIPPING_STAGE'],"REMARKS"));
		
		$k++;
	}
	
	$arr4 = $obj->msort_ascending($arr4, array('1','0'));
	
	for($k=0;$k<count($arr4);$k++)
	{
		$worksheet->setRow($i,28.75);
		$worksheet->formatmergeCellsMultiple($i,0,$i,0,$arr4[$k][0],$body_text);
		$worksheet->formatmergeCellsMultiple($i,1,$i,1,$arr4[$k][1],$body_text_center);
		$worksheet->formatmergeCellsMultiple($i,2,$i,2,$arr4[$k][2],$body_text);
		$worksheet->formatmergeCellsMultiple($i,3,$i,3,$arr4[$k][3],$body_text);
		$worksheet->formatmergeCellsMultiple($i,4,$i,4,$arr4[$k][4],$body_text);
		$worksheet->formatmergeCellsMultiple($i,5,$i,5,$arr4[$k][5],$body_text);
		$worksheet->formatmergeCellsMultiple($i,6,$i,6,$arr4[$k][6],$body_text_center);
		$worksheet->formatmergeCellsMultiple($i,7,$i,7,$arr4[$k][7],$body_text_center);
		$worksheet->formatmergeCellsMultiple($i,8,$i,8,$arr4[$k][8],$body_text);
		$i++;
	}
	
	$workbook->send('Cargo Open Position Report '.date("d-M-Y",time()).'.xls'); 
	$workbook->close();
}


function vessels_in_port_excel()
{
    //echo $_SESSION['company'];    die();
    if(isset($_REQUEST['txtDate'])){$date = $_REQUEST['txtDate']; }
	if(isset($_REQUEST['selMType'])){$selMType = $_REQUEST['selMType']; }
	$_SESSION['date'] = $date; 
	$_SESSION['selMType'] = $selMType;
	
	$selMType_1 = explode("@@@",$_REQUEST['selMType']);
	$_SESSION['selMType_1'] = $selMType_1[1];
	$_SESSION['date_1'] = date('Y-m-d', strtotime($date. '- 7 day'));


	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	
	$worksheet =& $workbook->addWorksheet("Weekly Report");
	
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));	
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(5);
	$worksheet->setPrintScale(120);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();	
	
	
   //............................................	
	$worksheet->setColumn(0, 3, 30.00);
	$worksheet->setRow(0,20.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,3,"Weekly ".$obj->getMaterialCodeDesBasedOnCode($_SESSION['selMType_1'])." Report ",$main_header);
	$worksheet->setRow(1,10.75);
	$worksheet->setRow(2,20.75);
	
	$worksheet->formatmergeCellsMultiple(2,0,2,0,"Date",$main_header);
	$worksheet->formatmergeCellsMultiple(2,1,2,1,date("d-M-Y",strtotime($_SESSION['date'])),$main_header);
	
	
	$worksheet->formatmergeCellsMultiple(2,2,2,2,"Material",$main_header);
	$worksheet->formatmergeCellsMultiple(2,3,2,3,$obj->getMaterialCodeDesBasedOnCode($_SESSION['selMType_1']),$main_header);
	
	
	
	$worksheet->setRow(3,10.75);
	$worksheet->setRow(4,20.75);
	$worksheet->formatmergeCellsMultiple(4,0,4,0,"PORT",$header);
	$worksheet->formatmergeCellsMultiple(4,1,4,1,"UNDER LOADING",$header);
	$worksheet->formatmergeCellsMultiple(4,2,4,2,"EXPECTED",$header);
	$worksheet->formatmergeCellsMultiple(4,3,4,3,"LAST LOADED",$header);
	
	
	$sql = "select PORTCODE from global_port_position where MATERIAL_CODE='".$_SESSION['selMType']."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' group by PORTCODE";
	
$res = mysql_query($sql);
$rec = mysql_num_rows($res);
//echo $sql; die();
$ro = 5;
if($rec == 0)
{   
	$worksheet->setRow($ro,20.75);
	$worksheet->formatmergeCellsMultiple($ro,0,$ro,3,"SORRY CURRENTLY THERE ARE ZERO(0) RECORDS FOUND",$main_header);
	
}else{

	$j = 5;
	$worksheet->freezePanes(array(5, 0));
	$v = 11;
	while($rows = mysql_fetch_assoc($res))
	{   //print_r($rows);
		if($rows['PORTCODE'] == "AE06"){$i=0;}
		else if($rows['PORTCODE'] == "KW01"){$i=1;}
		else if($rows['PORTCODE'] == "QA02"){$i=2;}
		else if($rows['PORTCODE'] == "QA01"){$i=3;}
		else if($rows['PORTCODE'] == "SA01"){$i=4;}
		else if($rows['PORTCODE'] == "IR04"){$i=5;}
		else if($rows['PORTCODE'] == "IR03"){$i=6;}
		else if($rows['PORTCODE'] == "IR08"){$i=7;}
		else if($rows['PORTCODE'] == "IR01"){$i=8;}
		else if($rows['PORTCODE'] == "IR07"){$i=9;}
		else if($rows['PORTCODE'] == "BH05"){$i=10;}
		else {$i = $v++;}
		$arr4[]= array( 'id' => $i,'PORTCODE' => $rows['PORTCODE']);	
	}
	$arr4 = msort($arr4, array('id'));
	for($k=0;$k<count($arr4);$k++){
	
	$worksheet->setRow($j,200.75);
	$worksheet->formatmergeCellsMultiple($j,0,$j,0,$obj->getPortNameBasedOnCode($arr4[$k]['PORTCODE']),$body_text);
	$worksheet->formatmergeCellsMultiple($j,1,$j,1,$obj->getPortPositionDataUnder($arr4[$k]['PORTCODE'],$_SESSION['date'],$_SESSION['selMType']),$body_text);
	$worksheet->formatmergeCellsMultiple($j,2,$j,2,$obj->getPortPositionDataExpected($arr4[$k]['PORTCODE'],$_SESSION['date'],$_SESSION['selMType']),$body_text);
	$worksheet->formatmergeCellsMultiple($j,3,$j,3,$obj->getPortPositionDataLast($arr4[$k]['PORTCODE'],$_SESSION['date'],$_SESSION['selMType']),$body_text);
	 $j++;
	}
}
	$workbook->send("Weekly Report ".$obj->getMaterialCodeDesBasedOnCode($_SESSION['selMType_1'])." ".date("d-M-Y",time()).'.xls'); 
	$workbook->close();
	
}

function msort($array, $key, $sort_flags = SORT_REGULAR) {
    if (is_array($array) && count($array) > 0) {
        if (!empty($key)) {
            $mapping = array();
            foreach ($array as $k => $v) {
                $sort_key = '';
                if (!is_array($key)) {
                    $sort_key = $v[$key];
                } else {
                    foreach ($key as $key_key) {
                        $sort_key .= $v[$key_key];
                    }
                    $sort_flags = SORT_REGULAR;
                }
                $mapping[$k] = $sort_key;
            }
            asort($mapping, $sort_flags);
            $sorted = array();
            foreach ($mapping as $k => $v) {
                $sorted[] = $array[$k];
            }
            return $sorted;
        }
    }
    return $array;
}

function laytime_excel()
{
	$laytimeid = $_REQUEST['laytimeid'];
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Laytime List');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(8);
	$worksheet->setPrintScale(70);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$sql 	= "select * from laytime_master where LAYTIME_ID='".$laytimeid."'";
	$res 	= mysql_query($sql);
	$rows 	= mysql_fetch_assoc($res);
	
	if($rows['APPLICABLE_STATUS'] == 1){$app_status = 'Yes';}else{$app_status = 'No';}
	if($rows['PORT'] == 'LP'){$name = 'Load'; $name_1 = 'BL_QTY_LOADED';}else{$name = 'Discharge'; $name_1 = 'BL_QTY_LOADED';}
	if($rows['METHOD'] == 2)
	{
		$ttl_demmurrage = ($rows['TIME_TO_DEMURRAGE']/24)*$rows['DEMURRAGE_RATE'];
		$ttl_despatch = ($rows['TIME_TO_DESPATCH']/24)*$rows['DESPATCH_RATE'];
	}
	else
	{
		$ttl_demmurrage = $rows['TTL_DEMURRAGE'];
		$ttl_despatch = $rows['TTL_DESPATCH'];
	}
	
	$worksheet->setColumn(0, 8, 30.00);
	$worksheet->setRow(0,20.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,8,"Laytime List",$main_header);
	$worksheet->setRow(1,10.75);
	$worksheet->setRow(2,20.75);
	$worksheet->formatmergeCellsMultiple(2,0,2,0,"Port",$main_header);
	$worksheet->formatmergeCellsMultiple(2,1,2,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(2,2,2,2,$rows['PORT']." - ".$obj->getPortNameBasedOnID($rows['PORTID']),$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,3,2,3,"Laytime Applicable",$main_header);
	$worksheet->formatmergeCellsMultiple(2,4,2,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(2,5,2,5,$app_status,$main_header_1);
	$worksheet->formatmergeCellsMultiple(2,6,2,6,"Entity",$main_header);
	$worksheet->formatmergeCellsMultiple(2,7,2,7,":",$main_header);
	$worksheet->formatmergeCellsMultiple(2,8,2,8,"Ship Owner",$main_header_1);
	
	$worksheet->setRow(3,20.75);
	$worksheet->formatmergeCellsMultiple(3,0,3,0,"Vendor",$main_header);
	$worksheet->formatmergeCellsMultiple(3,1,3,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(3,2,3,2,$obj->getVendorListNewBasedOnID($rows['VENDOR']),$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,3,3,3,"Nomination ID",$main_header);
	$worksheet->formatmergeCellsMultiple(3,4,3,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(3,5,3,5,$obj->getMappingData($rows['MAPPINGID'],"NOM_NAME"),$main_header_1);
	$worksheet->formatmergeCellsMultiple(3,6,3,6,"Vessel",$main_header);
	$worksheet->formatmergeCellsMultiple(3,7,3,7,":",$main_header);
	$worksheet->formatmergeCellsMultiple(3,8,3,8,$obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($rows['MAPPINGID'],"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME"),$main_header_1);
	
	$worksheet->setRow(4,20.75);
	$worksheet->formatmergeCellsMultiple(4,0,4,0,"Terminal/Berth",$main_header);
	$worksheet->formatmergeCellsMultiple(4,1,4,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(4,2,4,2,$obj->getSOFdata($rows['MAPPINGID'],$rows['PORT'],$rows['PORTID'],"TERMINAL"),$main_header_1);
	$worksheet->formatmergeCellsMultiple(4,3,4,3,$name." Qty (MT)",$main_header);
	$worksheet->formatmergeCellsMultiple(4,4,4,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(4,5,4,5,$obj->getNumberFormat($rows['LOADED_QTY']),$main_header_1);
	$worksheet->formatmergeCellsMultiple(4,6,4,6,$name." Rate (MT/Day)",$main_header);
	$worksheet->formatmergeCellsMultiple(4,7,4,7,":",$main_header);
	$worksheet->formatmergeCellsMultiple(4,8,4,8,$obj->getNumberFormat($rows['LOADED_RATE']),$main_header_1);
	
	$worksheet->setRow(5,20.75);
	$worksheet->formatmergeCellsMultiple(5,0,5,0,$name." Terms",$main_header);
	$worksheet->formatmergeCellsMultiple(5,1,5,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(5,2,5,2,$rows["LOADED_TERMS"],$main_header_1);
	$worksheet->formatmergeCellsMultiple(5,3,5,3,"NOR Terms",$main_header);
	$worksheet->formatmergeCellsMultiple(5,4,5,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(5,5,5,5,$rows['NOR_TERMS'],$main_header_1);
	
	$worksheet->setRow(6,20.75);
	$worksheet->formatmergeCellsMultiple(6,0,6,0,"Turn Time (Hrs)",$main_header);
	$worksheet->formatmergeCellsMultiple(6,1,6,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(6,2,6,2,$rows["TURN_TIME"],$main_header_1);
	$worksheet->formatmergeCellsMultiple(6,3,6,3,"TT terms",$main_header);
	$worksheet->formatmergeCellsMultiple(6,4,6,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(6,5,6,5,$rows['TT_TERMS'],$main_header_1);
	$worksheet->formatmergeCellsMultiple(6,6,6,6,"LAYCAN",$main_header);
	$worksheet->formatmergeCellsMultiple(6,7,6,7,":",$main_header);
	$worksheet->formatmergeCellsMultiple(6,8,6,8,$rows['LAYCAN'],$main_header_1);
	
	$worksheet->setRow(7,20.75);
	$worksheet->formatmergeCellsMultiple(7,0,7,0,"Laytime Allowed(hrs)",$main_header);
	$worksheet->formatmergeCellsMultiple(7,1,7,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(7,2,7,2,$rows['LAYTIME_ALLOWED'],$main_header_1);
	$worksheet->formatmergeCellsMultiple(7,3,7,3,"Actual Laytime",$main_header);
	$worksheet->formatmergeCellsMultiple(7,4,7,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(7,5,7,5,$rows['ACTUAL_LAYTIME'],$main_header_1);
	
	$worksheet->setRow(8,20.75);
	$worksheet->formatmergeCellsMultiple(8,0,8,0,"Time to demurrage(hrs)",$main_header);
	$worksheet->formatmergeCellsMultiple(8,1,8,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(8,2,8,2,$rows['TIME_TO_DEMURRAGE'],$main_header_1);
	$worksheet->formatmergeCellsMultiple(8,3,8,3,"Demurrage Rate(USD/Day)",$main_header);
	$worksheet->formatmergeCellsMultiple(8,4,8,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(8,5,8,5,$obj->getNumberFormat($rows['DEMURRAGE_RATE']),$main_header_1);
	$worksheet->formatmergeCellsMultiple(8,6,8,6,"Demurrage(USD)",$main_header);
	$worksheet->formatmergeCellsMultiple(8,7,8,7,":",$main_header);
	$worksheet->formatmergeCellsMultiple(8,8,8,8,$obj->getNumberFormat($rows['TTL_DEMURRAGE']),$main_header_1);
	
	$worksheet->setRow(9,20.75);
	$worksheet->formatmergeCellsMultiple(9,0,9,0,"Time to despatch(hrs)",$main_header);
	$worksheet->formatmergeCellsMultiple(9,1,9,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(9,2,9,2,$rows['TIME_TO_DESPATCH'],$main_header_1);
	$worksheet->formatmergeCellsMultiple(9,3,9,3,"Despatch Rate(USD/Day)",$main_header);
	$worksheet->formatmergeCellsMultiple(9,4,9,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(9,5,9,5,$obj->getNumberFormat($rows['DESPATCH_RATE']),$main_header_1);
	$worksheet->formatmergeCellsMultiple(9,6,9,6,"Despatch(USD)",$main_header);
	$worksheet->formatmergeCellsMultiple(9,7,9,7,":",$main_header);
	$worksheet->formatmergeCellsMultiple(9,8,9,8,$obj->getNumberFormat($rows['TTL_DESPATCH']),$main_header_1);
	
	if(date("d-M-Y",strtotime($rows['NS_1'])) == '01-Jan-1970') {$ns_1 = "";} else {$ns_1 = date("d-M-Y,D,H:i",strtotime($rows['NS_1']));}
	if(date("d-M-Y",strtotime($rows['NS_2'])) == '01-Jan-1970') {$ns_2 = "";} else {$ns_2 = date("d-M-Y,D,H:i",strtotime($rows['NS_2']));}
	if(date("d-M-Y",strtotime($rows['NS_3'])) == '01-Jan-1970') {$ns_3 = "";} else {$ns_3 = date("d-M-Y,D,H:i",strtotime($rows['NS_3']));}
	if(date("d-M-Y",strtotime($rows['NS_4'])) == '01-Jan-1970') {$ns_4 = "";} else {$ns_4 = date("d-M-Y,D,H:i",strtotime($rows['NS_4']));}
	if(date("d-M-Y",strtotime($rows['NS_5'])) == '01-Jan-1970') {$ns_5 = "";} else {$ns_5 = date("d-M-Y,D,H:i",strtotime($rows['NS_5']));}
	if(date("d-M-Y",strtotime($rows['NS_6'])) == '01-Jan-1970') {$ns_6 = "";} else {$ns_6 = date("d-M-Y,D,H:i",strtotime($rows['NS_6']));}
	if(date("d-M-Y",strtotime($rows['NS_7'])) == '01-Jan-1970') {$ns_7 = "";} else {$ns_7 = date("d-M-Y,D,H:i",strtotime($rows['NS_7']));}
	if(date("d-M-Y",strtotime($rows['NS_8'])) == '01-Jan-1970') {$ns_8 = "";} else {$ns_8 = date("d-M-Y,D,H:i",strtotime($rows['NS_8']));}
	
	$worksheet->setRow(10,20.75);
	$worksheet->formatmergeCellsMultiple(10,0,10,0,"Vessel Arrived",$main_header);
	$worksheet->formatmergeCellsMultiple(10,1,10,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(10,2,10,2,$ns_1,$main_header_1);
	$worksheet->formatmergeCellsMultiple(10,3,10,3,"Nor Tendered",$main_header);
	$worksheet->formatmergeCellsMultiple(10,4,10,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(10,5,10,5,$ns_2,$main_header_1);
	$worksheet->formatmergeCellsMultiple(10,6,10,6,"Nor Deemed Tendered",$main_header);
	$worksheet->formatmergeCellsMultiple(10,7,10,7,":",$main_header);
	$worksheet->formatmergeCellsMultiple(10,8,10,8,$ns_3,$main_header_1);
	
	$worksheet->setRow(11,20.75);
	$worksheet->formatmergeCellsMultiple(11,0,11,0,"Commenced Cargo",$main_header);
	$worksheet->formatmergeCellsMultiple(11,1,11,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(11,2,11,2,$ns_4,$main_header_1);
	$worksheet->formatmergeCellsMultiple(11,3,11,3,"Completed Cargo",$main_header);
	$worksheet->formatmergeCellsMultiple(11,4,11,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(11,5,11,5,$ns_5,$main_header_1);
	$worksheet->formatmergeCellsMultiple(11,6,11,6,"Laytime Commences At",$main_header);
	$worksheet->formatmergeCellsMultiple(11,7,11,7,":",$main_header);
	$worksheet->formatmergeCellsMultiple(11,8,11,8,$ns_6,$main_header_1);
	
	$worksheet->setRow(12,20.75);
	$worksheet->formatmergeCellsMultiple(12,0,12,0,"Laytime To Start Counting",$main_header);
	$worksheet->formatmergeCellsMultiple(12,1,12,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(12,2,12,2,$ns_7,$main_header_1);
	$worksheet->formatmergeCellsMultiple(12,3,12,3,"Laytime Completed",$main_header);
	$worksheet->formatmergeCellsMultiple(12,4,12,4,":",$main_header);
	$worksheet->formatmergeCellsMultiple(12,5,12,5,$ns_8,$main_header_1);
	
	$worksheet->setRow(13,20.75);
	$worksheet->formatmergeCellsMultiple(13,0,13,0,"Remarks",$main_header);
	$worksheet->formatmergeCellsMultiple(13,1,13,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(13,2,13,2,$rows['REMARKS'],$main_header_1);
	
	$worksheet->setRow(14,10.75);
	$worksheet->setRow(15,20.75);
	$worksheet->formatmergeCellsMultiple(15,0,15,0,"Activity in Port",$header);
	$worksheet->formatmergeCellsMultiple(15,1,15,1,"Date / Time - Start",$header);
	$worksheet->formatmergeCellsMultiple(15,2,15,2,"Date / Time - Finish",$header);
	$worksheet->formatmergeCellsMultiple(15,3,15,3,"Duration(Hrs)",$header);
	$worksheet->formatmergeCellsMultiple(15,4,15,4,"LT(Partial %)",$header);
	$worksheet->formatmergeCellsMultiple(15,5,15,5,"Cummulative Time",$header);
	$worksheet->formatmergeCellsMultiple(15,6,15,6,"Notes",$header);
	
	$query = "select * from laytime_slave where LAYTIME_ID='".$laytimeid."'";
	$qres = mysql_query($query);
	$i = 16;
	while($qrows = mysql_fetch_assoc($qres))
	{
		if(date("d-M-Y",strtotime($qrows['START_DATETIME'])) == '01-Jan-1970'){$start_date = '';}else{$start_date = date("d-M-Y,D,H:i",strtotime($qrows['START_DATETIME']));}
		if(date("d-M-Y",strtotime($qrows['FINISH_DATETIME'])) == '01-Jan-1970'){$finish_date = '';}else{$finish_date = date("d-M-Y,D,H:i",strtotime($qrows['FINISH_DATETIME']));}
	
		if($qrows['LT_C'] == 1){$style = 'checked';}else{$style = '';}
		if($qrows['LT_NC'] == 1){$style1 = 'checked';}else{$style1 = '';}
	
		$worksheet->setRow($i,20.75);
		$worksheet->formatmergeCellsMultiple($i,0,$i,0,$qrows['ACTIVITYID'],$body_text);
		$worksheet->formatmergeCellsMultiple($i,1,$i,1,$start_date,$body_text);
		$worksheet->formatmergeCellsMultiple($i,2,$i,2,$finish_date,$body_text);
		$worksheet->formatmergeCellsMultiple($i,3,$i,3,$qrows['DURATION'],$body_text);
		$worksheet->formatmergeCellsMultiple($i,4,$i,4,$qrows['LT_PARTIAL'],$body_text);
		$worksheet->formatmergeCellsMultiple($i,5,$i,5,$qrows['CUMMULATIVE_TIME'],$body_text);
		$worksheet->formatmergeCellsMultiple($i,6,$i,6,$qrows['NOTES'],$body_text);
		$i++;
	}
	
	$workbook->send('Laytime List ('.$rows['PORT'].' - '.$obj->getPortNameBasedOnID($rows['PORTID']).')_'.date("d-m-Y",time())); 
	$workbook->close();
}


function daily_log_report_excel(){
	$txtDate = $_REQUEST['txtDate'];
    //echo $txtDate; die();
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Daily Morning Report');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));		

	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(5);
	$worksheet->setPrintScale(65);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
//............................................	
	$worksheet->setColumn(0, 8, 30.00);
	$worksheet->setRow(0,20.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,8,"Daily Morning Report",$main_header);
	$worksheet->setRow(1,10.75);
	$worksheet->setRow(2,20.75);
	$worksheet->formatmergeCellsMultiple(2,0,2,0,"Date",$main_header);
	$worksheet->formatmergeCellsMultiple(2,1,2,1,$txtDate,$main_header);
	
	
	$worksheet->setRow(3,10.75);
	$worksheet->setRow(4,20.75);
	$worksheet->formatmergeCellsMultiple(4,0,4,0,"Vessel",$header);
	$worksheet->formatmergeCellsMultiple(4,1,4,1,"Material Description",$header);
	$worksheet->formatmergeCellsMultiple(4,2,4,2,"Fixture Type",$header);
	$worksheet->formatmergeCellsMultiple(4,3,4,3,"Load Port(s)",$header);
	$worksheet->formatmergeCellsMultiple(4,4,4,4,"Discharge Port(s)",$header);
	$worksheet->formatmergeCellsMultiple(4,5,4,5,"Load / Disch (In Last 24 Hrs)",$header);
	$worksheet->formatmergeCellsMultiple(4,6,4,6,"Total Load / Disch",$header);
	$worksheet->formatmergeCellsMultiple(4,7,4,7,"Balance To Load / Disch",$header);
	$worksheet->formatmergeCellsMultiple(4,8,4,8,"Port Status",$header);
	
	
		
	$sql = "select * from mapping_master where MODULEID='".$_SESSION['moduleid']."'  AND MCOMPANYID='".$_SESSION['company']."' and STATUS=2 order by MAPPINGID";
	$res = mysql_query($sql);
	//echo $sql;die();		
	
	$j = 5;
	$pop = $worksheet->freezePanes(array(5, 0));
	
		while($rows = mysql_fetch_assoc($res))
		{
			$cost_sheet_id 	= $obj->getLatestCostSheetID($rows['MAPPINGID']);
			$arr  			= $obj->getLoadPortAndDischargePortArrBasedOnMappingidAndProcessWithoutTBN($rows['MAPPINGID'],$cost_sheet_id);
			$port_arr  		= $obj->getLoadPortAndDischargePortArrBasedOnIDMappingidAndProcessWithoutTBN($rows['MAPPINGID'],$cost_sheet_id);
			
			//echo"<pre>";print_r($arr); 
			//echo $arr; die();
			
			$lp_name = ''; $dp_name = '';
			$obj->viewFreightEstimationRecords($rows['MAPPINGID'],$cost_sheet_id);
			for($i=0;$i<count($arr);$i++)
			{
				$sql3 	= "select COUNTRY_NAME from PORT_MASTER where PortId='".$port_arr[$i]."'";
				$res3 	= mysql_query($sql3);
				$rows3 	= mysql_fetch_assoc($res3);
				//echo"<pre>";print_r($rows3); 
				if(substr($arr[$i],0,2) == "LP"){$lp_name .= substr($arr[$i],5).' ('.$obj->getCountryNameOnId($rows3['COUNTRY_NAME'])."),\n";}
				else{$dp_name .= substr($arr[$i],5).",\n";}
			}
			
			$lp_name = substr($lp_name,0,-2);
			$dp_name = substr($dp_name,0,-2);
			
			
			$sql1 = "select * from daily_log where MAPPINGID='".$rows['MAPPINGID']."' and TRANS_DATE='".date("Y-m-d",strtotime($txtDate))."'";
			$res1 = mysql_query($sql1);
			$rec1 = mysql_num_rows($res1);
			$rows1 = mysql_fetch_assoc($res1);
			
			
		                $worksheet->setRow($j,100.75);
						$worksheet->formatmergeCellsMultiple($j,0,$j,0,$obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($rows['MAPPINGID'],"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME"),$body_text);
						$worksheet->formatmergeCellsMultiple($j,1,$j,1,$obj->getCargoContarctForMapping($obj->getMappingData($rows['MAPPINGID'],"CARGO_IDS"),$obj->getMappingData($rows['MAPPINGID'],"CARGO_POSITION"),$obj->getMappingData($rows['MAPPINGID'],"SHIPPING_STAGE")),$body_text);
						$worksheet->formatmergeCellsMultiple($j,2,$j,2,$obj->getFreightFixtureBasedOnID($obj->getFun2()),$body_text);
						$worksheet->formatmergeCellsMultiple($j,3,$j,3,$lp_name,$body_text);
						$worksheet->formatmergeCellsMultiple($j,4,$j,4,$dp_name,$body_text);
						$worksheet->formatmergeCellsMultiple($j,5,$j,5,$rows1['DAILY_CARGO_QTY'],$body_text);
						$worksheet->formatmergeCellsMultiple($j,6,$j,6,$rows1['TTL_CARGO_QTY'],$body_text);
						$worksheet->formatmergeCellsMultiple($j,7,$j,7,$rows1['BAL_QTY'],$body_text);
						$worksheet->formatmergeCellsMultiple($j,8,$j,8,$rows1['PORT_STATUS'],$body_text);	
			$j++;
	}
	$workbook->send('Daily Morning Report'." ".date("d-M-Y",time()).'.xls'); 
	$workbook->close();
	
}


function sof_excel()
{
	$mappingid = $_REQUEST['mappingid'];
	$port = $_REQUEST['port'];
	$portid = $_REQUEST['portid'];
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('SOF List');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$main_header_1 = & $workbook->addFormat(array('Size' => 9,'Bold' => 0,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$main_header_2 = & $workbook->addFormat(array('Size' => 8,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$main_header_3 = & $workbook->addFormat(array('Size' => 8,'Bold' => 0,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));
	$main_header_4 = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));	
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(8);
	$worksheet->setPrintScale(70);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.10);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.10);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
	$worksheet->setColumn(0, 4, 35.00);
	$worksheet->setRow(0,20.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,4,"SOF List",$main_header);
	$worksheet->setRow(1,10.75);
	$worksheet->setRow(2,20.75);
	$worksheet->formatmergeCellsMultiple(2,0,2,0,"Port",$main_header_2);
	$worksheet->formatmergeCellsMultiple(2,1,2,1,":",$main_header);
	$worksheet->formatmergeCellsMultiple(2,2,2,2,$port." - ".$obj->getPortNameBasedOnID($portid),$main_header_1);
	
	if($port == 'LP')
	{$name = 'SHIPPER';}else{$name = 'RECEIVER';}
	
	$worksheet->setRow(3,10.75);
	
	$arr = array("NAME OF VESSEL","BUILT","GRT/NRT","FLAG","DWT(s)","LOA/BEAM","GEAR/GRABS","HATCH/HOLD","NAME OF MASTER","NAME OF OWNERS","NAME OF CHARTERERS","NAME OF ".$name,"NAME OF STEVEDORES","LOAD PORT","DISCHARGE PORT","CARGO DESCRIPTION","CARGO QUANTITY","TERMINAL/BERTH/ANCHORAGE","VESSEL ARRIVED PILOT STATION","VESSEL ANCHORED","NOR TENDERED","NOR RETENDERED (IF ANY)","NOR ACCEPTED","ANCHOR AWEIGH","PILOT BOARDED FOR BERTHING","VESSEL ALL FAST","FREE PRATIQUE GRANTED","CUSTOM CLEARED","INTIAL DRAFT SURVEY COMMENCED","INITIAL DRAFT SURVEY COMPLETED","HOLD INSPECTION COMMENCED","HOLD INSPECTION COMPLTED","HOLDS PASSED","ON HIRE SURVEY COMMENCED","ON HIRE SURVEY COMPLETED","LOADING COMMENCED","LOADING COMPLETED","FINAL DRAFT SURVEY STARTED","FINAL DRAFT SURVEY COMPLETED","CARGO LOADED AS PER DRAFT SURVEY","DOCUMENTATION COMPLETED","VESSEL SAILED");
	
	$arr_1 = array("VESSEL_NAME","VAPS","VA","NT","NR","NA","AA","PBFB","NAME_OF_MASTER","OWNER","CHARTERER","NAME_OF_SHIPPER","NAME_OF_STEVEDORES","LOAD_PORT","DISCHARGE_PORT","CARGO_DESC","CARGO_QTY","TERMINAL","VAPS_1","VA_1","NT_1","NR_1","NA_1","AA_1","PBFB_1","VAF","FPG","CC","IDSC","IDSC_1","HIC","HIC_1","HP","OHSC","OHSC_1","LC","LC1","FDSS","FDSC","CLAPDS","DC","VS");
	
	$k = 3;
	for($i=0;$i<sizeof($arr);$i++)
	{
		$j = $i+1;
		$k = $k+1;
		$worksheet->setRow($k,20.75);
		$worksheet->formatmergeCellsMultiple($k,0,$k,0,$arr[$i],$main_header_3);
		
		if($i == 0)
		{
			$worksheet->formatmergeCellsMultiple($k,1,$k,1,$obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME"),$main_header_3);}
		else if($i == 9)
		{ $worksheet->formatmergeCellsMultiple($k,1,$k,1,$obj->getVendorListNewBasedOnID($obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'OWNER')),$main_header_3);}
		else if($i == 10)
		{ $worksheet->formatmergeCellsMultiple($k,1,$k,1,$obj->getVendorListNewBasedOnID($obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'CHARTERER')),$main_header_3);}
		else
		{ $worksheet->formatmergeCellsMultiple($k,1,$k,1,$obj->getSOFdata($mappingid,$port,$portid,$arr_1[$i]),$main_header_3);}
	}
	
	$sql = "select * from sof_master where MAPPINGID='".$mappingid."' and LOGIN='INTERNAL_USER' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and PORT='".$port."' and PORTID='".$portid."'";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	if($rec == 0)
	{
		$sql = "select * from sof_master where MAPPINGID='".$mappingid."' and LOGIN='AGENT' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and PORT='".$port."' and PORTID='".$portid."' and SUBMITID='2'";
		$res = mysql_query($sql);
		$rows = mysql_fetch_assoc($res);
	}
	else
	{
		$rows = mysql_fetch_assoc($res);
	}
	
	$sql1 = "select * from sof_slave_3 where SOFID='".$rows['SOFID']."'";
	$res1 = mysql_query($sql1);
	$rec1 = mysql_num_rows($res1);
	if($rec1 > 0)
	{ 
		while($rows1 = mysql_fetch_assoc($res1))
		{ 
			$k = $k+1;
			$worksheet->setRow($k,20.75);
			$worksheet->formatmergeCellsMultiple($k,0,$k,0,$rows1['ENTITY_NAME'],$main_header_3);
			$worksheet->formatmergeCellsMultiple($k,1,$k,1,$rows1['ENTITY_VALUE'],$main_header_3);
		}
	}
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	$worksheet->formatmergeCellsMultiple($k,0,$k,0,"BL Date",$header);
	$worksheet->formatmergeCellsMultiple($k,1,$k,1,"BL Qty(MT)",$header);
	
	$sql2 = "select * from sof_slave_1 where SOFID='".$rows['SOFID']."'";
	$res2 = mysql_query($sql2);
	while($rows2 = mysql_fetch_assoc($res2))
	{
		$k = $k+1;
		$worksheet->setRow($k,20.75);
		$worksheet->formatmergeCellsMultiple($k,0,$k,0,date("d-M-Y",strtotime($rows2['BL_DATE'])),$main_header_3);
		$worksheet->formatmergeCellsMultiple($k,1,$k,1,$rows2['BL_QTY'],$main_header_3);
	}
	
	$arr_2 = array("ALL ACTIVITIES IN PORT TO BE MENTIONED IN BELOW SECTION START FROM VESSEL ARRIVAL, WORKING, STOPPAGES TILL VESSEL SAILING AND CLEARING PORT","HATCH OPENING AND CLOSING","GRAB FIXING","HATCH WISE COMMENCEMENT AND COMPLETION OF CARGO WORK","PORT WORKING / RECESS HOURS","CRANE DELAYS - DELAYS/DETENTION/STOPPAGE OF WORK DUE TO CRANE /GRAB BREAKDOWN","DELAYS/DETENTION/STOPPAGE OF WORK ON SHIPS ACCOUNT (OTHER THAN GEAR)","DELAYS/DETENTION/STOPPAGE OF WORK ON SHORE ACCOUNT","BARGE TIMINGS - CARGO BARGE ARRIVAL / DEPARTURE TIMINGS","RAIN AND BAD WEATHER DELAYS (PRE BERTHING)","RAIN AND BAD WEATHER DELAYS (AT BERTH)");
	
	$arr_3 = array("ACTIVITY IN PORT","HATCH NO.","CRANE NO.","HATCH NO.","SHIFT NO.","SR. NO.","SR. NO.","SR. NO.","SR. NO.","SR. NO.","SR. NO.");
	
	$arr_4 = array("0","REMARKS_1","REMARKS_2","REMARKS_3","REMARKS_4","REMARKS_5","REMARKS_6","REMARKS_7","REMARKS_8","REMARKS_9","REMARKS_10");
	
	$arr_5 = array("FROM","OPENED","COMMENCED","COMMENCED","FROM","FROM","FROM","FROM","FROM","FROM","FROM");
	$arr_6 = array("TO","CLOSED","COMPLETED","COMPLETED","TO","TO","TO","TO","TO","TO","TO");
	
	for($i=0;$i<sizeof($arr_2);$i++)
	{
		$k = $k+1;
		$worksheet->setRow($k,20.75);
		
		$k = $k+1;
		$worksheet->setRow($k,20.75);
		$worksheet->formatmergeCellsMultiple($k,0,$k,4,$arr_2[$i],$main_header_4);
		
		$k = $k+1;
		$worksheet->setRow($k,20.75);
		$worksheet->formatmergeCellsMultiple($k,0,$k,0,$arr_3[$i],$header);
		$worksheet->formatmergeCellsMultiple($k,1,$k,1,$arr_5[$i],$header);
		$worksheet->formatmergeCellsMultiple($k,2,$k,2,$arr_6[$i],$header);
		$worksheet->formatmergeCellsMultiple($k,3,$k,3,"DURATION",$header);
		$worksheet->formatmergeCellsMultiple($k,4,$k,4,"REMARKS",$header);
		
		if($arr_4[$i] == "0")
		{
			$sql3 = "select * from sof_slave where SOFID='".$rows['SOFID']."' and GROUP_NAME='1'";
		}
		else
		{
			$sql3 = "select * from sof_slave where SOFID='".$rows['SOFID']."' and GROUP_NAME='".$arr_2[$i]."'";
		}
		$res3 = mysql_query($sql3);
		while($rows3 = mysql_fetch_assoc($res3))
		{
			$k = $k+1;
			
			if(date("d-M-Y",strtotime($rows3['START_DATETIME'])) == '01-Jan-1970') {$s_date = "";} else {$s_date = date("d-M-Y H:i",strtotime($rows3['START_DATETIME']));}
			if(date("d-M-Y",strtotime($rows3['FINISH_DATETIME'])) == '01-Jan-1970') {$f_date = "";} else {$f_date = date("d-M-Y H:i",strtotime($rows3['FINISH_DATETIME']));}
			
			if($arr_4[$i] == "0")
			{
				if(date("d-M-Y",strtotime($rows3['START_DATETIME'])) == '01-Jan-1970' || date("d-M-Y",strtotime($rows3['FINISH_DATETIME'])) == '01-Jan-1970')
				{$duration = '';}else{$duration = $rows3['DURATION'];}
			}
			else
			{$duration = $rows3['DURATION'];}
			
			$worksheet->setRow($k,20.75);
			$worksheet->formatmergeCellsMultiple($k,0,$k,0,$rows3['ACTIVITYID'],$main_header_3);
			$worksheet->formatmergeCellsMultiple($k,1,$k,1,$s_date,$main_header_3);
			$worksheet->formatmergeCellsMultiple($k,2,$k,2,$f_date,$main_header_3);
			$worksheet->formatmergeCellsMultiple($k,3,$k,3,$duration,$main_header_3);
			$worksheet->formatmergeCellsMultiple($k,4,$k,4,$rows3['NOTES'],$main_header_3);
		}
		
		if($arr_4[$i] != "0")
		{
			$k = $k+1;
			$worksheet->setRow($k,20.75);
			
			$k = $k+1;
			$worksheet->setRow($k,20.75);
			$worksheet->formatmergeCellsMultiple($k,0,$k,0,"REMARKS",$main_header_2);
			$worksheet->formatmergeCellsMultiple($k,1,$k,1,":",$main_header);
			$worksheet->formatmergeCellsMultiple($k,2,$k,2,$arr_4[$i],$main_header_1);
		}
	}
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	$worksheet->formatmergeCellsMultiple($k,0,$k,0,"SATURDAYS",$header);
	$worksheet->formatmergeCellsMultiple($k,1,$k,1,"SUNDAYS",$header);
	$worksheet->formatmergeCellsMultiple($k,2,$k,2,"HOLIDAYS",$header);
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	$worksheet->formatmergeCellsMultiple($k,0,$k,0,$obj->getSOFdata($mappingid,$port,$portid,"SATURDAYS"),$main_header_3);
	$worksheet->formatmergeCellsMultiple($k,1,$k,1,$obj->getSOFdata($mappingid,$port,$portid,"SUNDAYS"),$main_header_3);
	$worksheet->formatmergeCellsMultiple($k,2,$k,2,$obj->getSOFdata($mappingid,$port,$portid,"HOLIDAYS"),$main_header_3);
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	$worksheet->formatmergeCellsMultiple($k,0,$k,0,$name."'S / AGENT'S REMARKS",$header);
	$worksheet->formatmergeCellsMultiple($k,1,$k,1,"CHARTERER'S / AGENT'S REMARKS",$header);
	$worksheet->formatmergeCellsMultiple($k,2,$k,2,"MASTER'S / OWNER'S REMARKS",$header);
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	$worksheet->formatmergeCellsMultiple($k,0,$k,0,$obj->getSOFdata($mappingid,$port,$portid,"SHIPPER_REMARKS"),$main_header_3);
	$worksheet->formatmergeCellsMultiple($k,1,$k,1,$obj->getSOFdata($mappingid,$port,$portid,"CHARTERER_REMARKS"),$main_header_3);
	$worksheet->formatmergeCellsMultiple($k,2,$k,2,$obj->getSOFdata($mappingid,$port,$portid,"MASTER_REMARKS"),$main_header_3);
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	$worksheet->formatmergeCellsMultiple($k,0,$k,0,"",$header);
	$worksheet->formatmergeCellsMultiple($k,1,$k,1,"ARRIVAL CONDITIONS",$header);
	$worksheet->formatmergeCellsMultiple($k,2,$k,2,"DEPARTURE CONDITIONS",$header);
	
	$sql14 = "select * from sof_slave_2 where SOFID=".$rows['SOFID'];
	$res14 = mysql_query($sql14);
	$rec14 = mysql_num_rows($res14);
	if($rec14 == 0)
	{
		$arr_7 = array("Fore Draft(Mtrs)","Aft Draft(Mtrs)","Mid Draft(Mtrs)","FO(MT)","DO(MT)","FW(MT)","FW(MT)","LO(MT)","DENSITY(KG/CUM)");
		for($m=0;$m<sizeof($arr1);$m++)
		{
			$k = $k+1;
			$worksheet->setRow($k,20.75);
			$worksheet->formatmergeCellsMultiple($k,0,$k,0,$arr_7[$m],$main_header_3);
			$worksheet->formatmergeCellsMultiple($k,1,$k,1,"",$main_header_3);
			$worksheet->formatmergeCellsMultiple($k,2,$k,2,"",$main_header_3);
		}
	}
	else
	{
		while($rows14 = mysql_fetch_assoc($res14))
		{
			$k = $k+1;
			$worksheet->setRow($k,20.75);
			$worksheet->formatmergeCellsMultiple($k,0,$k,0,$rows14['ACTIVITYID'],$main_header_3);
			$worksheet->formatmergeCellsMultiple($k,1,$k,1,$rows14['ARRIVAL_COND'],$main_header_3);
			$worksheet->formatmergeCellsMultiple($k,2,$k,2,$rows14['DEPARTURE_COND'],$main_header_3);
		}
	}
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	
	$k = $k+1;
	$worksheet->setRow($k,20.75);
	$worksheet->formatmergeCellsMultiple($k,0,$k,3,"SIGNED SUBJECT TO TERMS/CONDITION AND EXCEPTIONS OF THE RELEVANT CHARTER PARTY/SALE PURCHASE CONTRACT",$main_header_2);
	
	for($i=0;$i<3;$i++)
	{
		$k = $k+1;
		$worksheet->setRow($k,20.75);
	}
	
	$worksheet->setRow($k,20.75);
	$worksheet->formatmergeCellsMultiple($k,0,$k,0,"MASTER",$main_header_2);
	$worksheet->formatmergeCellsMultiple($k,1,$k,1,$name." / AGENT",$main_header_2);
	$worksheet->formatmergeCellsMultiple($k,2,$k,2,"CHARTERER / AGENT",$main_header_2);
	$worksheet->formatmergeCellsMultiple($k,3,$k,3,"OWNER'S AGENT",$main_header_2);
	
	$workbook->send('SOF List ('.$port.' - '.$obj->getPortNameBasedOnID($portid).')_'.date("d-M-Y",time()).'.xls'); 
	$workbook->close();
}

function shipment_register_excel()
{
	$txtFrom_Date 	= $_REQUEST['txtFrom_Date'];
	$txtTo_Date  	= $_REQUEST['txtTo_Date'];
	$selDate_Type 	= $_REQUEST['selDate_Type'];
	$selBType 	    = $_REQUEST['selBType'];
	
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Shipment Register List');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text = & $workbook->addFormat(array('Size' => 7,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));		

	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(5);
	$worksheet->setPrintScale(70);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
//............................................	
	$worksheet->setColumn(0, 20,15.00);
	$worksheet->setRow(0,20.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,18,"Shipment Register List",$main_header);
	$worksheet->setRow(1,10.75);
	$worksheet->setRow(2,25.75);
	$worksheet->formatmergeCellsMultiple(2,0,2,0,"Nom ID",$header);
	$worksheet->formatmergeCellsMultiple(2,1,2,1,"Vessel",$header);
	$worksheet->formatmergeCellsMultiple(2,2,2,2,"CP Date",$header);
	$worksheet->formatmergeCellsMultiple(2,3,2,3,"Owner",$header);
	$worksheet->formatmergeCellsMultiple(2,4,2,4,"Broker",$header);
	$worksheet->formatmergeCellsMultiple(2,5,2,5,"Material Desc",$header);
	$worksheet->formatmergeCellsMultiple(2,6,2,6,"Last Updated Freight",$header);
	$worksheet->formatmergeCellsMultiple(2,7,2,7,"From Port",$header);
	$worksheet->formatmergeCellsMultiple(2,8,2,8,"To Port",$header);
	$worksheet->formatmergeCellsMultiple(2,9,2,9,"BL Date",$header);
	$worksheet->formatmergeCellsMultiple(2,10,2,10,"Qty",$header);
	$worksheet->formatmergeCellsMultiple(2,11,2,11,"Operational Expenses",$header);
	$worksheet->formatmergeCellsMultiple(2,12,2,12,"Port Expenses",$header);
	$worksheet->formatmergeCellsMultiple(2,13,2,13,"Bunker Expenses",$header);
	$worksheet->formatmergeCellsMultiple(2,14,2,14,"Voyage Earnings",$header);
	$worksheet->formatmergeCellsMultiple(2,15,2,15,"Daily Earnings / TCE",$header);
	$worksheet->formatmergeCellsMultiple(2,16,2,16,"Voyage Earnings + Demurrage",$header);
	$worksheet->formatmergeCellsMultiple(2,17,2,17,"Nett Daily Profit",$header);
	$worksheet->formatmergeCellsMultiple(2,18,2,18,"P/L",$header);
	$worksheet->freezePanes(array(1, 2));
	$sql = "select * from freight_cost_estimate_compare inner join freight_cost_estimete_master on freight_cost_estimete_master.FCAID= freight_cost_estimate_compare.FCAID WHERE freight_cost_estimate_compare.MODULEID='".$_SESSION['moduleid']."' AND freight_cost_estimate_compare.MCOMPANYID='".$_SESSION['company']."' and freight_cost_estimate_compare.FINAL_ID!='' and freight_cost_estimete_master.FIXED=1 and freight_cost_estimete_master.ESTIMATE_TYPE='".$selBType."' and freight_cost_estimate_compare.STATUS in (1,2) order by freight_cost_estimate_compare.COMID desc";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res); $j = 3;
	if($rec == 0)
	{
	}
	else
	{
		while($rows = mysql_fetch_assoc($res))
		{
			$fcaid = $obj->getLatestCostSheetID($rows['COMID']);
			$obj->viewFreightCostEstimationTempleteRecordsNew($fcaid);
			
			$quantity = $freight = 0;
			if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==1)
			{
				$quantity = $obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY");
				if($obj->getCompareEstimateData($rows['COMID'],"GAS_MARKET"))
				{
					$freight = $obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY");
				}
				else
				{
					$freight = number_format(($obj->getCompareEstimateData($rows['COMID'],"GAS_BALTIC") + $obj->getCompareEstimateData($rows['COMID'],"ADDNL_PRENIUM"))*$obj->getCompareEstimateData($rows['COMID'],"GAS_QUANTITY"), 2, '.', '');
				}
			}
			else if($obj->getCompareEstimateData($rows['COMID'],"ESTIMATE_TYPE")==2)
			{
				$quantity = $obj->getCompareEstimateData($rows['COMID'],"TANK_QUANTITY");
				$sql12 = "select sum(TOTAL_AMOUNT) as sum1 from freight_cost_estimete_slave12 where FCAID='".$obj->getCompareEstimateData($rows['COMID'],"FCAID")."'";
				$res12 = mysql_query($sql12);
				$row12 = mysql_fetch_assoc($res12);
				$freight  = $row12['sum1'];
			}
			else
			{
				if($obj->getCompareEstimateData($rows['COMID'],"QTY_TYPE_RADIO")== 1)
				{
					$quantity = $obj->getCompareEstimateData($rows['COMID'],"QUANTITY");
					$freight  = $obj->getCompareEstimateData($rows['COMID'],"NET_PAYABLE");
				}
				else
				{
					$sql12 = "select sum(QUANTITY) as sum, sum(NET_FREIGHT) as sum2 from freight_cost_estimete_slave7 where FCAID='".$obj->getCompareEstimateData($rows['COMID'],"FCAID")."'";
					$res12 = mysql_query($sql12);
					$row12 = mysql_fetch_assoc($res12);
					$quantity = $row12['sum'];
					$freight  = number_format($row12['sum2'], 2, '.', '');
				}
			}
			
			$addn_qty = $obj->getFun54();
			$dead_freight_qty = $obj->getFun53();				
			$arr  = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($rows['COMID'],$fcaid);
			
			$lp_name = ''; $dp_name = '';
			for($i=0;$i<count($arr);$i++)
			{
				$arr_portval = explode('@@',$arr[$i]);
				if($arr_portval[0] == "LP"){$lp_name .= $obj->getPortNameBasedOnID($arr_portval[1]).', ';}
				else{$dp_name .= $obj->getPortNameBasedOnID($arr_portval[1]).', ';}
			}
			
			$lp_name = substr($lp_name,0,-6);
			$dp_name = substr($dp_name,0,-6);
			$bl_date_arr = array();
			$bl_qty = 0;
			if($selDate_Type == 1)
			{
				$sql1 = "select * from sof_master where (BL_DATE BETWEEN '".date("Y-m-d",strtotime($txtFrom_Date))."' and '".date("Y-m-d",strtotime($txtTo_Date))."') and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$rows['COMID']."' and LOGIN = 'INTERNAL_USER'";
				$res1 = mysql_query($sql1);
				$res11 = mysql_query($sql1);
			    $rec1 = mysql_num_rows($res1);
				$bl_date_arr = array();
			    while($rows1 = mysql_fetch_assoc($res1))
				{
					for($i=0;$i<count($arr);$i++)
			        {
						$arr_portval = explode('@@',$arr[$i]);
						$sql2 = "select * from sof_master where SOFID='".$rows1['SOFID']."' and PORT='".$arr_portval[0]."' and PORTID='".$arr_portval[1]."' and RANDOMID='".$arr_portval[2]."'";
						$res2 = mysql_query($sql2);
						$rec2 = mysql_num_rows($res2);
						while($rows2 = mysql_fetch_assoc($res2))
						{
							$bl_date = date("d-M-Y",strtotime($rows2['BL_DATE']));
							$bl_date_arr[] = $arr_portval[0]."-".$obj->getPortNameBasedOnID($arr_portval[1])."-".$bl_date;
							$bl_qty = $bl_qty + $rows2['BL_QTY_LOADED'];
						}
					}
				}
			}
			else
			{
				$sql1 = "select * from freight_cost_estimete_master where (TRANS_DATE BETWEEN '".date("Y-m-d",strtotime($txtFrom_Date))."' and '".date("Y-m-d",strtotime($txtTo_Date))."') and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and COMID='".$rows['COMID']."' and FCAID='".$fcaid."' order by FCAID";
				$res1 = mysql_query($sql1);
				$res11 = mysql_query($sql1);
			    $rec1 = mysql_num_rows($res1);
			    $rows1 = mysql_fetch_assoc($res1);
				
				$bl_date_arr = array();
				for($i=0;$i<count($arr);$i++)
				{
					$arr_portval = explode('@@',$arr[$i]);
					$sql2 = "select * from sof_master where COMID='".$rows1['COMID']."' and PORT='".$arr_portval[0]."' and PORTID='".$arr_portval[1]."' and RANDOMID='".$arr_portval[2]."' and LOGIN = 'INTERNAL_USER'";
					$res2 = mysql_query($sql2) or die($sql2);
					$rec2 = mysql_num_rows($res2);
					while($rows2 = mysql_fetch_assoc($res2))
					{
						if($rows2['BL_DATE']!='' && $rows2['BL_DATE']!='0000-00-00' && $rows2['BL_DATE']!='1970-01-01' && $rows2['BL_DATE']!='01-01-1970')
						{
						   $bl_date = date("d-M-Y",strtotime($rows2['BL_DATE']));
						}
						else
						{
							$bl_date = '';
						}
						$bl_date_arr[] = $arr_portval[0]."-".$obj->getPortNameBasedOnID($arr_portval[1])."-".$bl_date;
						$bl_qty = $bl_qty + $rows2['BL_QTY_LOADED'];
					}
				}
			}
			
			
			if($rec1>0)
			{
				$sql7 = "select * from freight_cost_estimete_slave4 where FCAID='".$obj->getFun1()."' ";
				$res7 = mysql_query($sql7);
				$rec7 = mysql_num_rows($res7);
				$arrvendor = array();
				while($rows7 = mysql_fetch_assoc($res7))
				{
					$arrvendor[] = $obj->getVendorListNewBasedOnID($rows7['VENDORID']);
				}
				$worksheet->setRow($j,80.75);
				$worksheet->formatmergeCellsMultiple($j,0,$j,0,$obj->getCompareTableData($rows['COMID'],"MESSAGE"),$body_text);
				$worksheet->formatmergeCellsMultiple($j,1,$j,1,$obj->getVesselIMOData($obj->getFun5(),"VESSEL_NAME"),$body_text);
				$worksheet->formatmergeCellsMultiple($j,2,$j,2,(($obj->getFun112()=='' || $obj->getFun112()=='0000-00-00' || $obj->getFun112()=='1970-01-01')?'':date("d-m-Y",strtotime($obj->getFun112()))),$body_text);
				$worksheet->formatmergeCellsMultiple($j,3,$j,3,$obj->getVendorListNewBasedOnID($obj->getFun140()),$body_text);
				$worksheet->formatmergeCellsMultiple($j,4,$j,4,implode(', ',$arrvendor),$body_text);
				$worksheet->formatmergeCellsMultiple($j,5,$j,5,$obj->getMaterialCodeDesBasedOnCode($obj->getFun52()),$body_text);
				$worksheet->formatmergeCellsMultiple($j,6,$j,6,$freight,$body_text);
				$worksheet->formatmergeCellsMultiple($j,7,$j,7,$lp_name,$body_text);
				$worksheet->formatmergeCellsMultiple($j,8,$j,8,$dp_name,$body_text);
				$worksheet->formatmergeCellsMultiple($j,9,$j,9,implode(', ',$bl_date_arr),$body_text);
				$worksheet->formatmergeCellsMultiple($j,10,$j,10,$obj->getNumberFormat($quantity),$body_text);
				$worksheet->formatmergeCellsMultiple($j,11,$j,11,$obj->getNumberFormat($obj->getFun84()),$body_text);
				$worksheet->formatmergeCellsMultiple($j,12,$j,12,$obj->getNumberFormat($obj->getFun153()),$body_text);
				$worksheet->formatmergeCellsMultiple($j,13,$j,13,$obj->getNumberFormat($obj->getFun152()),$body_text);
				$worksheet->formatmergeCellsMultiple($j,14,$j,14,$obj->getNumberFormat($obj->getFun89()),$body_text);
				$worksheet->formatmergeCellsMultiple($j,15,$j,15,$obj->getNumberFormat($obj->getFun90()),$body_text);
				$worksheet->formatmergeCellsMultiple($j,16,$j,16,$obj->getNumberFormat($obj->getFun92()),$body_text);
				$worksheet->formatmergeCellsMultiple($j,17,$j,17,$obj->getNumberFormat($obj->getFun93()),$body_text);
				$worksheet->formatmergeCellsMultiple($j,18,$j,18,$obj->getNumberFormat($obj->getFun95()),$body_text);

				$j++;
			}
		}
	}
	
	$workbook->send('Shipment Register List ('.$selMat_Type.')_'.date("d-M-Y",time()).'.xls'); 
	$workbook->close();
}


function Total_vendor_excel()
{
	$workbook = new Spreadsheet_Excel_Writer(); // creating an object for excel sheets
	$obj = new data();
	$obj->funConnect();
	$worksheet =& $workbook->addWorksheet('Total Vendor List');
	// creating different different formats for displaying data 
	$header = & $workbook->addFormat(array('Size' => 9,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));	
	$main_header = & $workbook->addFormat(array('Size' => 10,'Bold' => 1,'Border' => 0,'FontFamily' => 'Verdana','align' => 'center', 'VAlign' => 'top','locked'=>1,'TextWrap'=>1));
	$body_text = & $workbook->addFormat(array('Size' => 8,'Bold' => 1,'Border' => 1,'FontFamily' => 'Verdana','align' => 'left', 'VAlign' => 'Vcenter','locked'=>1,'TextWrap'=>1));		

	
	// printable formats and settings
	$footer = 'Designed and Developed by Seven Oceans Holdings production';
	$worksheet->setLandscape();
	$worksheet->setPaper(5);
	$worksheet->setPrintScale(100);
	$worksheet->setMargins_LR(0.10);
	$worksheet->setMargins_TB(0.20);
	$worksheet->setHeader('',0.20);
	$worksheet->setFooter($footer,0.20);
	$worksheet->centerHorizontally(0);
	$worksheet->hideGridLines();
	
//............................................	
	$worksheet->setColumn(0, 3, 40.00);
	$worksheet->setRow(0,20.75);
	$worksheet->formatmergeCellsMultiple(0,0,0,3,"Total Vendor List",$main_header);
	$worksheet->setRow(1,10.75);
	$worksheet->setRow(2,20.75);
	$worksheet->formatmergeCellsMultiple(2,0,2,0,"Vendor Type",$header);
	$worksheet->formatmergeCellsMultiple(2,1,2,1,"Vendor Code",$header);
	$worksheet->formatmergeCellsMultiple(2,2,2,2,"Vendor Name",$header);
	$worksheet->formatmergeCellsMultiple(2,3,2,3,"Short Name",$header);
	
	$sql = "select * from vendor_master where STATUS=1 order by NAME";
	$res = mysql_query($sql);
	$i=3;
	$worksheet->freezePanes(array(3, 1));
	while($rows = mysql_fetch_assoc($res))
	{
		$worksheet->setRow($i,28.75);
		$worksheet->formatmergeCellsMultiple($i,0,$i,0,$obj->getVendorTypeNameBasedOnID($rows['VENDOR_TYPEID']),$body_text);
		$worksheet->formatmergeCellsMultiple($i,1,$i,1,$rows['CODE'],$body_text);
		$worksheet->formatmergeCellsMultiple($i,2,$i,2,$rows['NAME'],$body_text);
		$worksheet->formatmergeCellsMultiple($i,3,$i,3,$rows['SHORT_NAME'],$body_text);
		$i++;
	}
	$workbook->send('Vendor List'); 
	$workbook->close();
}

?>
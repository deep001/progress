<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$mappingid 	= $_REQUEST['mappingid'];
$page  		= $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}

if (@$_REQUEST['action'] == 'submit')
{
 	if($_REQUEST['upstatus'] == 1)
	{
		$msg = $obj->deleteBAFDetails();
	}
	else if($_REQUEST['upstatus'] == 2)
	{
		$msg = $obj->updateNewBAFDetails();
	}
	header('Location:./'.$page_link.'?msg='.$msg);
}
$obj->viewBAFCalculationRecords($mappingid);

$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&bafid=".$obj->getFun1()."&page=".$page;

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header" align="center"><?php echo strtoupper($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME"))." / ".(int)$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"NO_OF_LIFT")." LIFT / ".date("d M Y",strtotime($obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'COA_DATE')))." / ".$obj->getCOANumberBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"COA_NO"));?> :: BAF CALCULATION</h2>
                        </div><!-- /.col -->
                </div>
                <?php 
				if($obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'BAF') == 0)
				{
					echo '<div class="box">				
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                        	<thead>
                            	<tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>                                  
                                </tr>
                            </thead>
                            <tbody>';
					
					 	echo '<tr>
								<td align="center" colspan="4" valign="middle" style="color:#ff0000;letter-spacing:1px;">'.strtoupper("Please fulfill BAF details in COA.").'</td>
								</tr>';
						echo '</tbody>
						</table>
					</div>
				</div>';
				}
				else
				{
				?>
                
                <div class="box">				
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                        	<thead>
                            	<tr>
                                	<th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>                                    
                                </tr>
                            </thead>
                            <tbody>
                            	<tr>
                                	<td>Bunker Price IFO (USD/MT)</td>
                                    <td><input type="text" name="txtBPrice" id="txtBPrice" class="form-control" readonly value="<?php echo $obj->getFun2();?>" /></td>
                                    <td>BAF Terms (Days)</td>
                                    <td><input type="text" name="txtBAFTerms" id="txtBAFTerms" class="form-control" readonly value="<?php echo $obj->getFun3();?>" /></td>
                                    <td>BAF (USD/MT)</td>
                                    <td><input type="text" name="txtIncDec" id="txtIncDec" class="form-control" readonly value="<?php echo $obj->getFun4();?>" /></td>
                                </tr>
                            </tbody>
                         </table>
                    </div>
                </div>
                
                <div class="box">			
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Port</th>
                                    <th width="20%">BL Quantity (MT)</th>
                                    <th width="20%">BL Date</th>
                                    <th width="30%">Remarks</th>                                  
                                </tr>
                            </thead>
                           <tbody id="tblBLData">
                           	<?php 
							$sql = "select * from baf_slave1 where BAFID = ".$obj->getFun1();
							$res = mysql_query($sql);
							$rec = mysql_num_rows($res);
							$i=1;
							while($rows = mysql_fetch_assoc($res))
							{
							?>
								<tr id="blrow_<?php echo $i;?>">
                                	<td></td>
									<td><?php echo $obj->getPortNameBasedOnCode($rows['PORT_CODE']);?>
                                    </td>
									<td><input type="text" name="txtBLQty_<?php echo $i;?>" id="txtBLQty_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows['BL_QTY'];?>" onkeyup="getTotal();" /></td>
									<td><input type="text" name="txtBLDate_<?php echo $i;?>" id="txtBLDate_<?php echo $i;?>" class="form-control" readonly value="<?php echo date("d-m-Y",strtotime($rows['BL_DATE']));?>" placeholder="dd-mm-yyyy" /></td>
									<td><textarea class="form-control" name="txtRemarks_<?php echo $i;?>" id="txtRemarks_<?php echo $i;?>" placeholder="Remarks..." readonly cols="33" rows="1"><?php echo $rows['REMARKS'];?></textarea></td>
                                </tr>
                             <?php $i++; } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                <th align="left" colspan="2" ></th>
                                <th align="left" ><input type="text" name="txtTotalBLQty" id="txtTotalBLQty" class="form-control" readonly value="<?php echo $obj->getFun5();?>" /></th>
                                <th align="left" colspan="2" ></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                 </div>
                  <div class="box">			
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Date</th>
                                    <th width="20%">Day</th>
                                    <th width="20%">Rate IFO (USD/MT)</th>
                                    <th width="30%">Remarks</th>                                  
                                </tr>
                            </thead>
                          	<tbody id="tblDateData">
                            	<?php 
								$sql1 = "select * from baf_slave2 where BAFID = ".$obj->getFun1();
								$res1 = mysql_query($sql1);
								$rec1 = mysql_num_rows($res1);
								$i=1;
								while($rows1 = mysql_fetch_assoc($res1))
								{
								?>
                                <tr id="daterow_<?php echo $i;?>">
                                <td></td>
                                <td><input type="text" name="txtDate_<?php echo $i;?>" id="txtDate_<?php echo $i;?>" class="form-control" readonly value="<?php echo date("d-m-Y",strtotime($rows1['DATE']));?>" placeholder="dd-mm-yyyy" /></td>
                                <td><input type="text" name="txtDay_<?php echo $i;?>" id="txtDay_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows1['DAY'];?>" /></td>
                                <td><input type="text" name="txtRateIFO_<?php echo $i;?>" id="txtRateIFO_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows1['RATE_IFO'];?>" /></td>
                                <td><textarea class="form-control" readonly name="txtDRemarks_<?php echo $i;?>" id="txtDRemarks_<?php echo $i;?>" placeholder="Remarks.." cols="33" rows="1"><?php echo $rows1['REMARKS'];?></textarea></td>		
                                </tr>
                             <?php $i++; } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                            <th align="left" colspan="5" ></th>
                            </tr>
                            </tfoot>
                       </table>
                    </div>
                </div>
                <div class="box">
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="30%">#</th>
                                    <th width="40%"></th>
                                    <th width="40%"></th>                                  
                                </tr>
                            </thead>
                            	<?php 
								$sql2 = "select * from baf_slave1 where BAFID = ".$obj->getFun1();
								$res2 = mysql_query($sql2);
								$rec2 = mysql_num_rows($res2);
								$i=1;
								while($rows2 = mysql_fetch_assoc($res2))
								{
								?>
								<tbody>
									<tr>
                                    	<td colspan="3"style="color:#dc631e;font-size:16px; font-weight:bold;"><?php echo $obj->getPortNameBasedOnCode($rows2['PORT_CODE'])." / ".date("d-m-Y",strtotime($rows2['BL_DATE']))." / ".$rows2['BL_QTY'];?></td>
                                    </tr>
									<?php 
                                    $sql3 = "select * from baf_slave2 where BAFID = ".$obj->getFun1();
                                    $res3 = mysql_query($sql3);
                                    $rec3 = mysql_num_rows($res3);
                                    $j=1;
                                    $sum = $count = 0;
                                    while($rows3 = mysql_fetch_assoc($res3))
                                    {
                                    ?>
									<tr>
									<?php if($obj->getBAF_slave3Data($obj->getFun1(),$rows2['BAF_SLAVE1_ID'],$rows3['BAF_SLAVE2_ID'],"CHECK_STATUS") == 1)
                                    {
                                        $count++;
                                        $sum += $obj->getBAF_slave3Data($obj->getFun1(),$rows2['BAF_SLAVE1_ID'],$rows3['BAF_SLAVE2_ID'],"AMOUNT");
                                    ?>
                                    <td><input type="checkbox" name="checkApprovedbox_<?php echo $i;?>_<?php echo $j;?>" checked="checked" id="checkApprovedbox_<?php echo $i;?>_<?php echo $j;?>" class="regular-checkbox big-checkbox" value="1" onclick="getEnabled(<?php echo $i;?>,<?php echo $j;?>);" /></td>
                                	<td><input type="text" name="txtADate_<?php echo $i;?>_<?php echo $j;?>" id="txtADate_<?php echo $i;?>_<?php echo $j;?>" class="form-control" readonly value="<?php echo date("d-m-Y",strtotime($rows3['DATE']));?>" /></td>
                                	<td><input type="text" name="txtARateIFO_<?php echo $i;?>_<?php echo $j;?>" id="txtARateIFO_<?php echo $i;?>_<?php echo $j;?>" class="form-control" readonly value="<?php echo $rows3['RATE_IFO'];?>" /><input type="hidden" name="txtHidARateIFO_<?php echo $i;?>_<?php echo $j;?>" id="txtHidARateIFO_<?php echo $i;?>_<?php echo $j;?>" class="form-control" readonly value="<?php echo $obj->getBAF_slave3Data($obj->getFun1(),$rows2['BAF_SLAVE1_ID'],$rows3['BAF_SLAVE2_ID'],"AMOUNT");?>" /></td>
                                
                                <?php }else{?>	
							
									<td><input type="checkbox" name="checkApprovedbox_<?php echo $i;?>_<?php echo $j;?>" id="checkApprovedbox_<?php echo $i;?>_<?php echo $j;?>" class="regular-checkbox big-checkbox" value="1" onclick="getEnabled(<?php echo $i;?>,<?php echo $j;?>);" /></td>
									<td><input type="text" name="txtADate_<?php echo $i;?>_<?php echo $j;?>" id="txtADate_<?php echo $i;?>_<?php echo $j;?>" class="form-control" readonly value="<?php echo date("d-m-Y",strtotime($rows3['DATE']));?>" /></td>
									<td><input type="text" name="txtARateIFO_<?php echo $i;?>_<?php echo $j;?>" id="txtARateIFO_<?php echo $i;?>_<?php echo $j;?>" class="form-control" readonly value="<?php echo $rows3['RATE_IFO'];?>" /><input type="hidden" name="txtHidARateIFO_<?php echo $i;?>_<?php echo $j;?>" id="txtHidARateIFO_<?php echo $i;?>_<?php echo $j;?>" class="input-text" size="15" readonly value="" /></td>
				</tr>
				
									<?php }$j++;}?>
            
                                    <tr>
                                        <td></td>
                                        <td>Sum</td>
                                        <td><input type="text" name="txtARateIFOTotal_<?php echo $i;?>" id="txtARateIFOTotal_<?php echo $i;?>" class="form-control" readonly value="<?php echo number_format($sum,4,'.','');?>" /></td>
                                        </tr>
                                    <tr>
                                        <td></td>
                                        <td>Average</td>
                                        <td><input type="text" name="txtARateIFOAvg_<?php echo $i;?>" id="txtARateIFOAvg_<?php echo $i;?>" class="form-control" readonly value="<?php echo number_format($sum/$count,4,'.','');?>" /></td>
                                        </tr>
                                        <?php if($count == 0){?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><input type="text" name="txtADiffIFOAvg_<?php echo $i;?>" id="txtADiffIFOAvg_<?php echo $i;?>" class="form-control" readonly value="0.0000" /></td>
                                        </tr>
                                        
                                    <tr>
                                        <td></td>
                                        <td>BAF</td>
                                        <td><input type="text" name="txtABAF_<?php echo $i;?>" id="txtABAF_<?php echo $i;?>" class="form-control" readonly value="0.0000" /></td>
                                     </tr>
                                        <tr style="display:none;">
                                        <td></td>
                                        <td></td>
                                        <td><input type="text" name="txtHidAvgBAF_<?php echo $i;?>" id="txtHidAvgBAF_<?php echo $i;?>" class="form-control" readonly value="0.0000" /></td>
                                    </tr>
                                    <?php }else{?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><input type="text" name="txtADiffIFOAvg_<?php echo $i;?>" id="txtADiffIFOAvg_<?php echo $i;?>" class="form-control" readonly value="<?php echo number_format(($sum/$count) - $obj->getFun2(),4,'.','');?>" /></td>
                                    </tr>
                                        
                                    <tr>
                                        <td></td>
                                        <td>BAF</td>
                                        <td><input type="text" name="txtABAF_<?php echo $i;?>" id="txtABAF_<?php echo $i;?>" class="form-control" readonly value="<?php echo number_format((($sum/$count) - $obj->getFun2()) * $obj->getFun4(),4,'.','');?>" /></td>
                                    </tr>
                                        
                                    <tr style="display:none;">
                                        <td></td>
                                        <td></td>
                                        <td><input type="text" name="txtHidAvgBAF_<?php echo $i;?>" id="txtHidAvgBAF_<?php echo $i;?>" class="input-text" size="15" readonly value="<?php echo number_format(((($sum/$count) - $obj->getFun2()) * $obj->getFun4()) * $rows2['BL_QTY'],4,'.','');?>" /></td>
                                    </tr>
                                    <?php }?>
                                    <tr height="40"><td align="center" colspan="4"></td></tr>	
                                </tbody>
							<?php $i++;}?>
                            	<tfoot>
                                	<tr >
                                        <td></td>
                                        <td style="color:#1E85BE;">Average BAF</td>
                                        <td><input type="text" name="txtAvgBAF" id="txtAvgBAF" class="form-control" readonly value="<?php echo number_format($obj->getFun7(),4,'.','');?>" /></td>
                                	</tr>
                                </tfoot>                  
                            </table>
                        </div>
                	<div class="box">
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="30%">#</th>
                                    <th width="40%"></th>
                                    <th width="40%"></th>                                  
                                </tr>
                            </thead>
                            <tbody>
                            	<tr>
                                	<td></td>
                                    <td style="color:#dc631e; font-size:14px;">APPLICABLE BAF</td>
                                    <td ><input type="text" name="txtApplicableBAF" id="txtApplicableBAF" class="form-control" readonly value="<?php echo number_format($obj->getFun7(),4,'.','');?>" /></td>
                                </tr>
                                <tr><td colspan="3" style="color:red; font-size:13px; font-style:italic; font-weight:bold; background-color:#C1C1C1; text-align:center;">&nbsp;&nbsp;( APPLY BAF WILL AFFECT THE VOYAGE FINANCIALS CALCULATIONS.&nbsp;&nbsp;&nbsp;&nbsp;BAF CALCUTION WILL NOT BE EDITABLE.&nbsp;&nbsp;&nbsp;&nbsp;PLEASE APPLY AND SUBMIT WHEN FULLY CONFIRMED. )</td></tr>
                                <tr>
                                    <td></td>
                                    <td>Apply BAF</td>
                                    <td><?php if($obj->getFun6() == 1){$checked = "checked";}else{$checked = "";}?>
                                    <input type="checkbox" name="checkApplyBAF" id="checkApplyBAF" <?php echo $checked;?> class="regular-checkbox big-checkbox" value="1"  /><label for="checkApplyBAF"></label></td>
        						</tr>
              				</tbody>
                         </table>
                    </div>
                </div>
                
                <div class="box-footer" align="right">
                    <?php if($obj->getFun6() != 1){?>
                        <button type="button" class="btn btn-primary btn-flat" onclick="return getValidate(1);">Remove BAF</button>
                        <button type="button" class="btn btn-primary btn-flat" onclick="return getValidate(2);">Submit</button>
                        <input type="hidden" name="action" value="submit" />
                        <input type="hidden" name="upstatus" id="upstatus" value="" />
                    <?php }?>
                </div>	
				</form>
				<?php } ?>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
$('[id^=txtRemarks_],[id^=txtDRemarks_]').autosize({append: "\n"});
$("#txtBSSRate,#txtMiscRate").numeric();
}); 

function getEnabled(var1,var2)
{
	if ($("#checkApprovedbox_"+var1+"_"+var2).attr('checked')) 
	{
		$("#txtHidARateIFO_"+var1+"_"+var2).val($("#txtARateIFO_"+var1+"_"+var2).val());
		$("#txtARateIFOTotal_"+var1).val($("[id^=txtHidARateIFO_"+var1+"_]").sum().toFixed(4));
		
		var len = $("[id^=checkApprovedbox_"+var1+"_]:checked").length;
		
		var calc = parseFloat($("#txtARateIFOTotal_"+var1).val()) / parseFloat(len);
		$("#txtARateIFOAvg_"+var1).val(calc.toFixed(4));
		
		var calc1 = parseFloat(calc.toFixed(4)) - parseFloat($("#txtBPrice").val());
		$("#txtADiffIFOAvg_"+var1).val(calc1.toFixed(4));
		
		var calc2 = parseFloat(calc1.toFixed(4)) * parseFloat($("#txtIncDec").val());
		$("#txtABAF_"+var1).val(calc2.toFixed(4));
		
		var calc3 = parseFloat($("#txtBLQty_"+var1).val()) * parseFloat(calc2.toFixed(4));
		$("#txtHidAvgBAF_"+var1).val(calc3.toFixed(4));
		
		var calc4 = parseFloat($("[id^=txtHidAvgBAF_]").sum().toFixed(4)) / parseFloat($("#txtTotalBLQty").val());
		$("#txtAvgBAF,#txtApplicableBAF").val(calc4.toFixed(4));
		
		//getTotalFreight();
	}
	else
	{
		$("#txtHidARateIFO_"+var1+"_"+var2).val("");
		$("#txtARateIFOTotal_"+var1).val($("[id^=txtHidARateIFO_"+var1+"_]").sum().toFixed(4));
		
		var len = $("[id^=checkApprovedbox_"+var1+"_]:checked").length;
		if(parseFloat(len) > 0)
		{
			var calc = parseFloat($("#txtARateIFOTotal_"+var1).val()) / parseFloat(len);
			$("#txtARateIFOAvg_"+var1).val(calc.toFixed(4));
		
			var calc1 = parseFloat(calc.toFixed(4)) - parseFloat($("#txtBPrice").val());
			$("#txtADiffIFOAvg_"+var1).val(calc1.toFixed(4));
		
			var calc2 = parseFloat(calc1.toFixed(4)) * parseFloat($("#txtIncDec").val());
			$("#txtABAF_"+var1).val(calc2.toFixed(4));
		
			var calc3 = parseFloat($("#txtBLQty_"+var1).val()) * parseFloat(calc2.toFixed(4));
			$("#txtHidAvgBAF_"+var1).val(calc3.toFixed(4));
		
			var calc4 = parseFloat($("[id^=txtHidAvgBAF_]").sum().toFixed(4)) / parseFloat($("#txtTotalBLQty").val());
			$("#txtAvgBAF,#txtApplicableBAF").val(calc4.toFixed(4));
		}
		else
		{
			$("#txtARateIFOAvg_"+var1).val("0.0000");
			$("#txtADiffIFOAvg_"+var1).val("0.0000");
			$("#txtABAF_"+var1).val("0.0000");
			$("#txtHidAvgBAF_"+var1).val("0.0000");
			$("#txtAvgBAF,#txtApplicableBAF").val("0.0000");
		}
		//getTotalFreight();
	}	
}

/*function getTotalFreight()
{
	$("#txtAFriRate").val($("#txtBSSRate,#txtBAF,#txtMiscRate").sum().toFixed(2));
}*/

function getValidate(var1)
{
	if(var1 == 1)
	{
		if ($("#checkApplyBAF").attr('checked')) 
		{
			jAlert('Cannot delete! Apply BAF selected.', 'Alert');
			return false;		
		}
		else
		{
			jConfirm('Are you sure to remove this BAF permanently ?', 'Confirmation', function(r) {
				if(r){ 
					$("#upstatus").val(var1);
					document.frm1.submit();
				}
				else{return false;}
				});
		}
	}
	else
	{
		if ($("#checkApplyBAF").attr('checked')) 
		{
			jConfirm('APPLY BAF WILL AFFECT THE COST SHEET CALCULATIONS.<br>BAF CALCUTION WILL NOT BE EDITABLE.<br>PLEASE APPLY AND SUBMIT WHEN FULLY CONFIRMED. ', 'Confirmation', function(r) {
				if(r){ 
					$("#upstatus").val(var1);
					document.frm1.submit();
				}
				else{return false;}
				});
		}
		else
		{
			jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
				if(r){ 
					$("#upstatus").val(var1);
					document.frm1.submit();
				}
				else{return false;}
				});
		}	
	}
}

</script>
    </body>
</html>
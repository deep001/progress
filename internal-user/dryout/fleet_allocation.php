<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);

if(isset($_REQUEST['action_1']) && $_REQUEST['action_1'] == 0)
 {
 	$msg = $obj->VesselAllocation();
	header('Location:./fleet_allocation.php?msg='.$msg);
}
if(isset($_REQUEST['action_1']) && $_REQUEST['action_1'] == 1)
 {
 	$msg = $obj->VesselAllocationTab();
	header('Location:./fleet_allocation.php?msg='.$msg);
}

$msg = NULL;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}

</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-anchor"></i>&nbsp;Fleet&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active"> Fleet </li>
						<li class="active"> Fleet Allocation </li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content invoice">
					<!--   content put here.....-->
					<div class="row">
						<div class="col-md-12">
							<div align="right">
								<address>
									<a href="fleet.php"><button class="btn btn-info btn-flat">Back</button></a>
								</address>
							</div>
						</div>	
					</div>
					<?php if(isset($_REQUEST['msg'])){
					$msg = $_REQUEST['msg'];
					if($msg == 0){
					?>
					<div class="alert alert-success alert-dismissable">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<b>Congratulations!</b> Fleet allocated successfully.
					</div>
					<?php } ?>
					<?php } ?>
					<div class="row">
						<div class="col-md-12">
							<div align="center">
								<address>
									<h3>Fleet Allocation</h3>
								</address>
							</div>
						</div>	
					</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                        <div class="row">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#tab_1"> Non - Allocated </a></li>
                                    <li class=""><a data-toggle="tab" href="#tab_2"> Allocated </a></li>			
                                </ul>
                                <div class="tab-content">
                                    <div id="tab_1" class="tab-pane active">
                                        
                                            <div class="row invoice-info">
                                                <?php
                                                $mysql = "select * from vessel_imo_master where MCOMPANYID='".$_SESSION['company']."' and SHOW_STATUS!=1 order by VESSEL_NAME";
                                                $myres = mysql_query($mysql);
                                                while($myrows = mysql_fetch_assoc($myres))
                                                {
                                                    $mysql1 = "select * from vessel_allocation_master where VESSEL_IMO_ID=".$myrows['VESSEL_IMO_ID'];
                                                    $myres1 = mysql_query($mysql1);
                                                    $myrec1 = mysql_num_rows($myres1);
                                                    if($myrec1 == 0)
                                                    {?>
                                                    <div class="col-sm-6 invoice-col">
                                                        <address>
                                                        <input type="checkbox" name="checkIMObox[]" id="checkIMObox_<?php echo $myrows['VESSEL_IMO_ID'];?>" value="<?php echo $myrows['VESSEL_IMO_ID'];?>" />
                                                        
                                                        <?php echo $myrows['VESSEL_NAME'];?>&nbsp;&nbsp;&nbsp;(&nbsp;<?php echo $obj->getVesselTypeBasedOnID($myrows['VESSEL_TYPE']);?>
                                                        </address>
                                                        
                                                    </div><!-- /.col -->
                                                    <div class="col-sm-6 invoice-col">
                                                        <address>
                                                        <select data-placeholder="Choose a module here..." class="chzn-select" multiple name="selCProduct_<?php echo $myrows['VESSEL_IMO_ID'];?>[]" id="selCProduct_<?php echo $myrows['VESSEL_IMO_ID'];?>">
                                                            <?php	$obj->getProductListForUser(); ?>
                                                        </select>
                                                        </address>
                                                    </div><!-- /.col -->
                                                    
                                                <?php } ?>
                                            <?php } ?>
                                            </div>
                                            <div class="box-footer" align="right">
                                                <button type="button" class="btn btn-primary btn-flat" onClick="getValidate(0);">Submit</button>
                                                <input type="hidden" id="action_1" name="action_1" value="" />
                                            </div>
                                    </div>
                                    <div id="tab_2" class="tab-pane">
                                            <div class="row invoice-info">
                                                <?php
                                                $sql = "select * from vessel_imo_master where MCOMPANYID='".$_SESSION['company']."' and SHOW_STATUS!=1 order by VESSEL_NAME";
                                                $res = mysql_query($sql);
												$num = mysql_num_rows($res);
												$i = 0;
                                                while($rows = mysql_fetch_assoc($res))
                                                {
                                                $sql1 = "select * from vessel_allocation_master where VESSEL_IMO_ID=".$rows['VESSEL_IMO_ID'];
                                                $res1 = mysql_query($sql1);
                                                $rec1 = mysql_num_rows($res1);
                                                while($rows1 = mysql_fetch_assoc($res1))
                                                {$i = $i + 1;
                                                ?>
                                                <div class="col-sm-6 invoice-col">
                                                    <address>
                                                        <input type="checkbox" name="checkIMObox1_<?php echo $i;?>" id="checkIMObox1_<?php echo $i;?>" value="<?php echo $rows['VESSEL_IMO_ID'];?>" checked="checked"/>
                                                        
                                                        <?php echo $rows['VESSEL_NAME'];?>&nbsp;&nbsp;&nbsp;(&nbsp;<?php echo $obj->getVesselTypeBasedOnID($rows['VESSEL_TYPE']);?>&nbsp;)&nbsp;
                                                    </address>
                                                </div><!-- /.col -->
                                                
                                                <div class="col-sm-6 invoice-col">
                                                    <address>
                                                        <select data-placeholder="Choose Module..." class="input-text chzn-select" style="width:350px;" multiple name="selCProduct1_<?php echo $i;?>[]" id="selCProduct1_<?php echo $i;?>"><?php $obj->getProductListForUserForUpdate($rows1['MODULE_ID']);?></select>
                                                      
                                                    </address>
                                                </div><!-- /.col -->
                                                <?php } ?>
                                            <?php } ?>
                                            </div>
                                            <div class="box-footer" align="right">
                                                <button type="button" class="btn btn-primary btn-flat" onClick="getValidate1(1);">Submit</button>
                                                <input type="hidden" name="numvsl" id="numvsl" value="<?php echo $i;?>"/>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </form>
					<!----content ends here.......-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>

<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link type='text/css' href='../../css/basic.css' rel='stylesheet' media='screen' />
<script src="../../js/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" href="../../css/chosen.css" rel="stylesheet" />
<script type="text/javascript">
$(document).ready(function(){
	
$('[id^=selCProduct1_],[id^=selCProduct_]').chosen();

//$(".chzn-drop").css("width","258px");
//$(".default").css("width","258px");

});

function getValidate(val1)
{
	$("#action_1").val(val1);
	var arr = new Array(); 
	var arr1 = new Array();
	var i=0;
	$('[id^=checkIMObox_]').each(function(index) {
			if($(this).attr('checked')){arr[i] = this.value;}
			else{arr1[i] = this.value;}
			i++;
		});
	if(arr.length > 0)
	{
		var arr2 = new Array(); 
		var arr3 = new Array();
		var k=0;
		$('[id^=checkIMObox_]').each(function(index) {
		var rowid = this.id;
			if($(this).attr('checked'))
			{//alert($("#selIUser_"+rowid.split('_')[1]).val());	
				if($("#selCProduct_"+rowid.split('_')[1]).val() == null)
				{
					arr2[k] = rowid.split('_')[1];	
				}
			}
			k++;
		});
		
		if(arr2.length == 0)
		{
			document.frm1.submit();
			//return true;
		}
		else 
		{
			jAlert('Please select the module for vessel', 'Alert');
			//return false;
		}
	}
	else 
	{
		jAlert('Please allocate atleast one Vessel', 'Alert');
		//return false;
	}
}

function getValidate1(val1)
{
	$("#action_1").val(val1);
	var arr = new Array(); 
	var arr1 = new Array();
	var i=0;
	$('[id^=checkIMObox1_]').each(function(index) {
			if($(this).attr('checked')){arr[i] = this.value;}
			else{arr1[i] = this.value;}
			i++;
		});
	if(arr.length > 0)
	{
		var arr2 = new Array(); 
		var arr3 = new Array();
		var k=0;
		$('[id^=checkIMObox1_]').each(function(index) {
		var rowid = this.id;
			if($(this).attr('checked'))
			{//alert($("#selIUser_"+rowid.split('_')[1]).val());	
				if($("#selCProduct1_"+rowid.split('_')[1]).val() == null)
				{
					arr2[k] = rowid.split('_')[1];	
				}
			}
			k++;
		});
		
		if(arr2.length == 0)
		{
			document.frm1.submit();
			//return true;
		}
		else 
		{
			jAlert('Please select the module for vessel', 'Alert');
			//return false;
		}
	}
	else 
	{
		jAlert('Please allocate atleast one Vessel', 'Alert');
		//return false;
	}
}
</script>
</body>
</html>
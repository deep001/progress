<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$mappingid 	= $_REQUEST['mappingid'];
$page  		= $_REQUEST['page'];

if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}

if (@$_REQUEST['action'] == 'submit')
 {
	if (@$_REQUEST['txtStatus'] == '1')
 	{

		//$msg = $obj->deleteVesselOpenPositionsAttachment();
		//header('Location : ./updateVesselOpenPosition.php?id='.$_REQUEST['id'].'&msg='.$msg);
	}
	else if (@$_REQUEST['txtStatus'] == '2')
 	{
		$msg = $obj->changeVessel();
		header('Location:./'.$page_link.'?mappingid='.$mappingid.'&page=1');
	}
	
 }
$id = $obj->getMappingData($mappingid,"OPEN_VESSEL_ID");
$obj->viewVesselOpenPositionRecords($id);
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id."&page=".$page;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Change Vessel</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="in_ops_at_glance.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            CHANGE VESSEL    
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           Vessel Name
                            <address>
								<select  name="selVName" class="select form-control" id="selVName" >
									<?php 
									$obj->getVesselTypeListMemberWise();
									?>
								</select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                           Business Source Entity
                            <address>
                                <?php
									$sql = "SELECT * FROM open_vessel_entry_master where OPENVESSEL_ID='".$id."'";
									$res = mssql_query($sql);
									$rows = mssql_fetch_assoc($res);
									if($rows['LOGINTYPE'] != 'External User')
									{
								?>
									<select  name="selVendor" class="select form-control" id="selVendor" disabled="disabled">
										<?php 
											$_REQUEST['selVendor'] = $obj->getFun2();
											$obj->getVendorListNew();
										?>
									</select>
								<?php }else{
									
									$sql1 = "select USERNAME from login where LOGINID='".$rows['VENDORID']."'"; 
									$res1 = mysql_query($sql1);
									$rows1 = mysql_fetch_assoc($res1);
									echo strtoupper($rows1['USERNAME']);?>
									<input type="hidden" name="selVendor" id="selVendor" value="<?php echo $rows['VENDORID'];?>" />
								<?php }?>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                            Port Open
                            <address>
                               <select  name="selPort" onChange="getZoneData();" class="form-control" id="selPort" disabled="disabled">
								<?php 
                                $obj->getPortListNew();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Zone Open
                            <address>
                               <select  name="selZone"  class="form-control" id="selZone" disabled="disabled">
								<?php 
								$_REQUEST['selZone'] = $obj->getFun4();
								$obj->getZoneList();
								?>
                                </select>
                             </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                          COA / Spot
                            <address>
                                <select  name="selCOASpot" class="form-control" id="selCOASpot" onChange="getShow();" disabled="disabled">
								<?php 
                                $_REQUEST['selCOASpot'] = $obj->getFun16();
								$obj->getCOASpotList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                          Broker
                            <address>
                                <select name="selBroker" class="form-control" id="selBroker" disabled="disabled">
								<?php 
								$obj->getVendorListNewForCOA(12);
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                       
					</div>
                    
                    <?php if($obj->getFun16() == 2){?>
                    <div class="row invoice-info">
						<div class="col-sm-6 invoice-col">
                            Owner
                            <address>
                                <select  name="selOwner" class="form-control" id="selOwner">
								<?php 
                                $obj->getVendorListNewForCOA(11);
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->

                        <div class="col-sm-6 invoice-col">
                          COA Number
                            <address>
                               <select  name="selCOA" class="form-control" id="selCOA" onChange="getTotalShipments();">
								<?php 
								$_REQUEST['selCOA'] = $obj->getFun17();
                                $obj->getCOAList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-6 invoice-col">
                            <address>
                             &nbsp;
                             </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-6 invoice-col">
                           Number of Lift
                            <address>
                               <input type="text" name="txtNoLift" id="txtNoLift" class="form-control"  placeholder=" Number of Lift" autocomplete="off" value="<?php echo (int)$obj->getFun18();?>" disabled="disabled"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Total No. of Shipments
                            <address>
                                <strong><span id="ttl_shipment"></span></strong>
                             </address>
                        </div><!-- /.col -->
                        
                    </div>
                    <?php }else{ ?>
					<div class="row invoice-info" id="tr_coa" style="display:none;">
						<div class="col-sm-6 invoice-col">
                            Owner
                            <address>
                                <select name="selOwner" class="form-control" id="selOwner">
								<?php 
                                $obj->getVendorListNewForCOA(11);
                                ?>
                                </select>
                             </address>
                        </div><!-- /.col -->

                        <div class="col-sm-6 invoice-col">
                          COA Number
                            <address>
                               <select  name="selCOA" class="form-control" id="selCOA" onChange="getTotalShipments();">
								<?php 
								$obj->getCOAList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-6 invoice-col">
                           Number of Lift
                            <address>
                               <input type="text" name="txtNoLift" id="txtNoLift" class="form-control"  placeholder=" Number of Lift" autocomplete="off" value=""/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Total No. of Shipments
                            <address>
                                <strong><span id="ttl_shipment" ></span></strong>
                             </address>
                        </div><!-- /.col -->
                        
                    </div>	
					<?php } ?>
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           Laycan Start
                            <address>
                               <input type="text" name="txtFDate" id="txtFDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo date("d-m-Y",strtotime($obj->getFun5()));?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                            Laycan Finish
                            <address>
                                <input type="text" name="txtTDate" id="txtTDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo date("d-m-Y",strtotime($obj->getFun6()));?>"/>
                             </address>
                        </div><!-- /.col -->
                       
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-6 invoice-col">
                           ETA During Fixture
                            <address>
                               <input type="text" name="txtETADate" id="txtETADate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo date("d-m-Y",strtotime($obj->getFun13()));?>" disabled="disabled"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-6 invoice-col">
                           CP Date
                            <address>
                               <input type="text" name="txtCPDate" id="txtCPDate" class="form-control"  placeholder="dd-mm-yyyy" autocomplete="off" value="<?php echo date("d-m-Y",strtotime($obj->getFun14()));?>" disabled="disabled"/>
                            </address>
                        </div><!-- /.col -->
                        
					</div>
                    
                     <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                            Remarks
                            <address>
                               <textarea class="form-control areasize" name="txtNotes" id="txtNotes" rows="3" disabled="disabled" placeholder="Remarks ..." ><?php echo $obj->getFun7();?></textarea>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                           TC Description
                            <address>
                               <textarea class="form-control areasize" name="txtDirection" id="txtDirection" rows="3" disabled="disabled" placeholder="TC Description ..." ><?php echo $obj->getFun8();?></textarea>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    
                     <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                             &nbsp;
							<address>
							<?php if($obj->getFun15() != " "){
								$attachments = explode(",",$obj->getFun15());
								for($m=0;$m<count($attachments);$m++)
								{
									$s = "'";
									echo '<a href="../../attachment/'.$attachments[$m].'" target="_blank" >'.$attachments[$m].'</a>';
									echo "<br><br>";
								}
							}
							?>
							</address>
						</div><!-- /.col -->
                     </div>
				
					<?php if($rights == 1){ ?>
					<div class="box-footer" align="right">
						<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
						<input type="hidden" name="action" value="submit" />
						<input type="hidden" name="txtStatus" id="txtStatus" value="" />
					</div>
					<?php } ?>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$(".areasize").autosize({append: "\n"});
$("#txtNoLift").numeric();

$('#txtETADate,#txtCPDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true	
});


$('#txtFDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){
	$('#txtTDate').datepicker('setStartDate', new Date(getString($(this).val())));
 });
 
$('#txtTDate').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true,
	todayBtn: true,
	minuteStep: 1
 }).on('changeDate', function(){   });

function getString(var1)
{
  var var2 = var1.split('-');  
  return var2[2]+'/'+var2[1]+'/'+var2[0];
}

$("#selPort").val('<?php echo $obj->getFun3();?>');
$("#selOwner").val('<?php echo $obj->getFun11();?>');
$("#selBroker").val('<?php echo $obj->getFun12();?>');
$("#selVName").val('<?php echo $obj->getFun1();?>');

$("#frm1").validate({
	rules: {
		selVName:"required",
		selCOASpot:"required",
		selBroker:"required",
		txtCPDate:"required",
		txtFDate:"required",
		txtTDate:"required",
		selOwner: "required",
		selVCType: "required",
		selCOA: {  required: function(element) {return $("#selCOASpot").val() == 2;}},
		txtNoLift:{  required: function(element) {return $("#selCOASpot").val() == 2;},digits:true}
		},
	messages: {	
		selVName:"*",
		selCOASpot:"*",
		selBroker:"*",
		txtCPDate:"*",
		txtFDate:"*",
		txtTDate:"*",
		selOwner:"*",
		selVCType: "*",
		selCOA : "*",
		txtNoLift : {  required: "*",digits:"*"}
		},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

getTotalShipmentsOnLoad();
}); 

function getValidate()
{
	$("#txtStatus").val(2);
}

function getTotalShipmentsOnLoad()
{
	if($("#selCOA").val() != "")
	{
		var coa_list = <?php echo $obj->getCOAMasterDataJson();?>;
		$.each(coa_list[$("#selCOA").val()], function(index, array) {
				$("#ttl_shipment").html("");
				$("#ttl_shipment").html(array['NO_OF_SHIPMENT']);
			});
	}
	else
	{
		$("#ttl_shipment").html("");
	}
}

</script>
    </body>
</html>
<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$id = $_REQUEST['id'];
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->addMonthlyCOADetails();
	header('Location:./coa_list.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
$obj->viewCOARecords($id);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(4); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-truck"></i>&nbsp;Contract of Affreightment&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Contract of Affreightment</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="coa_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             COA DETAILS   
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Cargo/Material Type
                            <address>
                              <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getCargoTypeNameBasedOnID($obj->getFun25());?></strong>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Vessel Category
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getVesselCategoryTypeBasedOnId($obj->getFun26());?></strong>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            COA Number
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getFun1();?></strong>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            COA CP Date
                            <address>
                                <strong>&nbsp;&nbsp;&nbsp;  <?php echo date("d-M-Y",strtotime($obj->getFun2()));?></strong>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Cargo Qty(MT)
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getFun3();?></strong>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Owner
                            <address>
                                <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getVendorListNewBasedOnID($obj->getFun4())." ( ".$obj->getFun4()." )";?></strong>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Broker
                            <address>
                                <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getVendorListNewBasedOnID($obj->getFun5())." ( ".$obj->getFun5()." )";?></strong>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Charterer
                            <address>
                                <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getVendorListNewBasedOnID($obj->getFun6())." ( ".$obj->getFun6()." )";?></strong>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            No. of Shipments
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getFun7();?></strong>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Shipment Qty(MT)
                            <address>
                                <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getFun8();?></strong>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Tolerance ( % )
                            <address>
                               <strong>&nbsp;&nbsp;&nbsp;  <?php echo $obj->getFun9();?></strong>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    
                    
                  <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
								<div class="box-body no-padding">
								<table class="table table-striped">
								   <thead>
										 <tr>
											<th width="5%" style="text-align:center;">#</th>
											<th width="15%">Vessel Name</th>
											<th width="20%" >Laycan Start To Laycan Finish</th>
											<th width="15%" >Nomination</th>											
										 </tr>
									</thead>
									<tbody >
										<?PHP $sql = "select * from open_vessel_entry_master where COA_NO='".$id."' and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
										$res = mysql_query($sql);
										$rec = mysql_num_rows($res);
										$i=1;
										if($rec == 0)
										{
											
										}
										else
										{
										while($rows = mysql_fetch_assoc($res))
											{
												echo '<tr>';
												echo '<td align="center" valign="middle" class="input-text">'.$i.'.</td>';
												echo '<td align="left" valign="middle" class="input-text">'.strtoupper($obj->getVesselIMOData($rows['VESSEL_IMO_ID'],"VESSEL_NAME")).'</td>';
												echo '<td align="center" valign="middle" class="input-text">'.date("d-M-Y",strtotime($rows['FROM_DATE'])).' To '.date("d-M-Y",strtotime($rows['TO_DATE'])).'</td>';
												echo '<td align="center" valign="middle" class="input-text">'.$obj->getNomNameBasedOnOpenVesselID($rows['OPENVESSEL_ID']).'</td>';
												$i++;
											}
										}?>
									</tbody>
								</table>
								</div><!-- /.box-body -->
							</div>                          
                        </div><!-- /.col -->
                    </div>
                    
                    
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             REMARK BY MONTH(s)
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-primary">
								<div class="box-body no-padding">
								<table class="table table-striped">
								   <thead>
										 <tr>
											<th width="15%">Date in the month</th>
											<th width="20%" >Remark</th>
										 </tr>
									</thead>
									<tbody>
										<?PHP $sql1 = "select * from coa_monthly_remark where COAID='".$id."' and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."'";
                                        $res1 = mysql_query($sql1);
                                        $rec1 = mysql_num_rows($res1);
                                        $i=1;
                                        if($rec1 == 0)
                                        {?>
                                                <tbody id="tbodycoa">
                                                <tr id="row_1">
                                                <td align="center" valign="middle" class="input-text" ><input type="text" class="form-control" id="txtDate_1" name="txtDate_1" placeholder="dd-mm-yyyy" /></td>
                                                <td align="center" valign="middle" class="input-text" ><textarea class="form-control areasize" name="txtRemark_1" id="txtRemark_1" placeholder="Remarks ..." ></textarea></td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                <th align="left" valign="middle" class="input-text" colspan="2" ><button type="button" class="btn btn-primary btn-flat" onClick="addMonthlyRow();" >Add</button><input type="hidden" class="input-text" id="txtMCOAID" name="txtMCOAID" readonly size="10" value="1" /></th>
                                                </tr>
                                                </tfoot>
                                        <?php }
                                        else
                                        {
                                            echo '<tbody id="tbodycoa">';
                                        while($rows1 = mysql_fetch_assoc($res1))
                                            {
                                                echo '<tr id="row_'.$i.'">';
                                                echo '<td align="center" valign="middle" class="input-text"><input type="text" class="form-control" id="txtDate_'.$i.'" name="txtDate_'.$i.'" value="'.date("d-m-Y",strtotime($rows1['DATE'])).'"  placeholder="dd-mm-yyyy"/></td>';
                                                echo '<td align="center" valign="middle" class="input-text"><textarea name="txtRemark_'.$i.'" id="txtRemark_'.$i.'" class="form-control areasize">'.$rows1['REMARK'].'</textarea></td>';
                                                echo '</tr>';
                                                $i++;
                                            }
                                                echo '</tbody>';
                                                echo '<tfoot>';
                                                echo '<tr>';
                                                echo '<th align="left" valign="middle" class="input-text" colspan="2" ><button type="button" class="btn btn-primary btn-flat" onclick="addMonthlyRow();" >Add</button><input type="hidden" class="input-text" id="txtMCOAID" name="txtMCOAID" readonly="true" size="10" value="'.$rec1.'" /></th>';
                                                echo '</tr>';
                                                echo '</tfoot>';
                                        }?>
                                      </tbody>
								</table>
								</div><!-- /.box-body -->
							</div>                          
                        </div><!-- /.col -->
                    </div>
                    <div class="box-footer" align="right">
                        <button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
                        <input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
                    </div>
         
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("[id^=txtDate_]").datepicker({
    format: 'dd-mm-yyyy',
	autoclose:true
});
$(".areasize").autosize({append: "\n"});
});

function addMonthlyRow()
{
	var id = $("#txtMCOAID").val();
	if($("#txtDate_"+id).val() != "" && $("#txtRemark_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="row_'+id+'"><td align="center" valign="middle" class="input-text" ><input type="text" class="form-control" id="txtDate_'+id+'" name="txtDate_'+id+'" size="10" /></td><td align="center" valign="middle" class="input-text" ><textarea name="txtRemark_'+id+'" id="txtRemark_'+id+'" class="form-control areasize" ></textarea></td></tr>').appendTo("#tbodycoa");
		
		$('[id^=txtRemark_]').autosize({append: "\n"});
		$("[id^=txtDate_]").datepicker({
			format: 'dd-mm-yyyy'
		});
		$("#txtMCOAID").val(id);
	}
	else
	{
		jAlert('Please fill all records.', 'Alert');
		return false;
	}
}

function getValidate()
{
	var arr = new Array(); var arr1 = new Array();
	var i=0;
	$('[id^=txtDate_]').each(function(index) {
	var j = i+1;
			if($("#txtDate_"+j).val() == "" || $("#txtRemark_"+j).val() == "")
				{
					arr[i] = j;					
				}
			i++;
		});
	if(arr.length == 0)
	{
		return true;
	}
	else 
	{
		jAlert('Please fill all records', 'Alert');
		return false;
	}
}

function getMsg()
{
	jAlert("You are not authorised user for accessing this.");
	return false;
}

</script>
    </body>
</html>
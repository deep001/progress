<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];

if (@$_REQUEST['action1'] == 'submit')
 {
 	$msg = $obj->InsertRemarksDtatMGMT($id);
	header('Location:./sohdatamgmt_list.php?msg='.$msg);
 }
$obj->getDataMGMTData($id);
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(11); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-edit"></i>&nbsp;Jaldhi Post Fix Data Management&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Jaldhi Post Fix Data Management</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="sohdatamgmt_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             VIEW POST FIX DATA
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Vessel Name
                            <address>
                               &nbsp;&nbsp;&nbsp;<?php echo $obj->getFun1();?>
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
                            CP Date
                            <address>
                               &nbsp;&nbsp;&nbsp;<?php echo date('d-m-Y',strtotime($obj->getFun4()));?>
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-4 invoice-col">
                            Added By
                            <address>
                               &nbsp;&nbsp;&nbsp;<?php echo $obj->getUserDetailBaseOnId($obj->getFun5(),'CONTACT_PERSON');?>
                            </address>
                        </div><!-- /.col -->
						
                        <div class="col-sm-6 invoice-col">
                            Jaldhi Remarks
                            <address>
                                 &nbsp;&nbsp;&nbsp;<?php echo $obj->getFun6();?>
                            </address>
                        </div><!-- /.col -->
						
						<div class="col-sm-6 invoice-col">
						  
                           <?php if($obj->getFun7() != '')
									{ 
										$file = explode(",",$obj->getFun7()); 
										$name = explode(",",$obj->getFun8()); ?>										
									  Previous Attachments
										<address>
												<table cellpadding="1" cellspacing="1" border="0" width="100%" align="left">
												<?php
												$j =1;
												for($i=0;$i<sizeof($file);$i++)
												{
												?>
												<tr height="20" id="row_file1_<?php echo $j;?>">
													<td width="80%" align="left" class="input-text"  valign="top"><a href="../../attachment/<?php echo $file[$i]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="<?php echo $name[$i];?>"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $name[$i]; ?></a>
													<input type="hidden" name="file1_<?php echo $j;?>" id="file1_<?php echo $j;?>" value="<?php echo $file[$i]; ?>" />
													<input type="hidden" name="name1_<?php echo $j;?>" id="name1_<?php echo $j;?>" value="<?php echo $name[$i]; ?>" />
													</td>
													
												</tr>
												<?php $j++;}?>
											</table>
										</address>
									<?php }?>
                         </div>
                       <div class="col-sm-12 invoice-col">
                           SOH Remarks
                            <address>
                                 <textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" rows="3" placeholder="Remarks ..." ></textarea>
                            </address>
                        </div><!-- /.col -->
					
					</div>
				<div class="box-footer" align="right">
					<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
					<input type="hidden" name="action1" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
                    <input type="hidden" name="txtCRMFILE1" id="txtCRMFILE1" value="" />
					<input type="hidden" name="txtCRMNAME1" id="txtCRMNAME1" value="" />
				</div>
				</form>
                <br/><br/><br/>
                <div class="row invoice-info">
                      <div class="col-sm-12 invoice-col">
                       <strong>Jaldhi Remarks</strong>
                      
	                      <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                <th align="left" valign="middle" width="15%">Remarks Date</th>
                                <th align="left" valign="middle" width="15%">Remarks By</th>
                                <th align="left" valign="middle" width="70%">Remarks</th>
                                </tr>
                                </thead>
                               <?php 
							    $sql = "select * from post_fix_data_management_slave where DATAMGMTID='".$id."' and BY_USER=1  order by ADD_ON_DATE desc";
                                $res = mysql_query($sql);
                                $rec = mysql_num_rows($res);
                                $i = 1;
                               
                                echo '<tbody>';
                                    while($rows = mysql_fetch_assoc($res))
                                        {
                          
                                            echo '<tr>';			
                                            echo '<td align="left" valign="middle" class="input-text">'.date('d-m-Y',strtotime($rows['ADD_ON_DATE'])).'</td>';				
                                            echo '<td align="left" valign="middle" class="input-text">'.$obj->getUserDetailBaseOnId($rows['LOGINID'],'CONTACT_PERSON').'</td>';		 
											echo '<td align="left" valign="middle" class="input-text">'.addslashes($rows['REMARKS']).'</td>';	
                                            echo '</tr>';
                                            $i++;
                                    }
                                echo '</tbody>';
                               ?>
                                </table>	
                          </div>
                    </div>
                    
                   <div class="row invoice-info">
                      <div class="col-sm-12 invoice-col">
                          <strong>SOH Remarks</strong>
                          
                              <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                <th align="left" valign="middle" width="15%">Remarks Date</th>
                                <th align="left" valign="middle" width="15%">Remarks By</th>
                                <th align="left" valign="middle" width="70%">Remarks</th>
                                </tr>
                                </thead>
                               <?php 
							    $sql = "select * from post_fix_data_management_slave where DATAMGMTID='".$id."' and BY_USER=2 order by ADD_ON_DATE desc";
                                $res = mysql_query($sql);
                                $rec = mysql_num_rows($res);
                                $i = 1;
                               
                                echo '<tbody>';
                                    while($rows = mysql_fetch_assoc($res))
                                        {
                          
                                            echo '<tr>';			
                                            echo '<td align="left" valign="middle" class="input-text">'.date('d-m-Y',strtotime($rows['ADD_ON_DATE'])).'</td>';				
                                            echo '<td align="left" valign="middle" class="input-text">'.$obj->getUserDetailBaseOnId($rows['LOGINID'],'CONTACT_PERSON').'</td>';		 
											echo '<td align="left" valign="middle" class="input-text">'.addslashes($rows['REMARKS']).'</td>';	
                                            echo '</tr>';
                                            $i++;
                                    }
                                echo '</tbody>';
                               ?>
                                </table>
                          </div>
                    </div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
	$(".areasize").autosize({append: "\n"});
	$('#txtCPDate').datepicker({
		format: 'dd-mm-yyyy',
		autoclose:true
	});
	$("#frm1").validate({
		rules: {
			txtVesselName:"required",
			txtCPDate:"required",
			selShipper:"required",
			},
		messages: {	
			txtVesselName:"*",
			txtCPDate:"*",
			selShipper:"*",
			},
	submitHandler: function(form)  {
		
			jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
			$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
			$("#popup_content").css({"background":"none","text-align":"center"});
			$("#popup_ok,#popup_title").hide();  
			form.submit();
		}
	});
});

function Del_Upload1(var2)
{
	jConfirm('Are you sure you want to delete this upload permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file1_'+var2).remove();
	}
	});
}

function getValidate()
{
     var file_temp_name = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
	 $('#txtCRMFILE1').val(file_temp_name);
	 var file_actual_name = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
	 $('#txtCRMNAME1').val(file_actual_name);
	 //
	return true;
	document.frm1.submit();	
}

</script>
    </body>
</html>
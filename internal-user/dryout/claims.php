<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$id 		= explode(",",$_REQUEST['id']);
$mappingid 	= $id['4'];
$vendorid 	= $id['3'];
$page 		= $_REQUEST['page'];

if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertInvoiceClaimsDetails();
	$msg = explode(",",$msg);
	header('Location:./payment_grid.php?msg='.$msg[0].'&mappingid='.$msg[1].'&page='.$page);
}

$l_cost_sheet_id = $obj->getLatestCostSheetID($mappingid);
				   $obj->viewFreightEstimationRecords($mappingid,$l_cost_sheet_id);
$arr  			 = $obj->getLoadPortAndDischargePortArrBasedOnMappingidAndProcessWithoutTBN($mappingid,$l_cost_sheet_id);
$port_arr  		 = $obj->getLoadPortAndDischargePortArrBasedOnIDMappingidAndProcessWithoutTBN($mappingid,$l_cost_sheet_id);

$lp_name = ''; 
$dp_name = '';
for($j=0;$j<count($arr);$j++)
{
	if(substr($arr[$j],0,2) == "LP"){$lp_name .= substr($arr[$j],5).",";}
	else{$dp_name .= substr($arr[$j],5).",";}
}

$lp_name = substr($lp_name,0,-1);
$dp_name = substr($dp_name,0,-1);

$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&page=".$page;
$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;In Ops at a glance&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="payment_grid.php?mappingid=<?php echo $mappingid;?>&page=<?php echo  $page;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
				<?php
				$sql = "SELECT * from invoice_master where MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and NAMEID = '".$id[0]."' and GRADEID='".$id[2]."' and STATUS=0 LIMIT 1";
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				?>				
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								Invoice
								</h2>                            
							</div><!-- /.col -->
						</div>					
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Fixture Ref.
								<address>
									<label><?php echo $obj->getMappingData($mappingid,"NOM_NAME");?></label>
								</address>
							</div><!-- /.col -->							
							<div class="col-sm-4 invoice-col">
								Charterer
								<address>
								   <label><?php echo $obj->getVendorListNewBasedOnID($vendorid);?></label>
								   <input type="hidden" name="txtVendor" id="txtVendor" value="<?php echo $vendorid;?>" />
								   <input type="hidden" name="txtNameid" id="txtNameid" value="<?php echo $id[0];?>" />
								   <input type="hidden" name="txtGradeid" id="txtGradeid" value="<?php echo $id[2];?>" />
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Cost Type
								<address>
								   <label><?php echo $id[1];?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Vessel
								<address>
								   <label><?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Cargo
								<address>
								   <label><?php echo $obj->getCargoContarctForMapping($obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"CARGO_POSITION"));?></label>
								</address>
							</div><!-- /.col -->							
							<div class="col-sm-4 invoice-col">
								CP Date
								<address>
								   <label><?php echo date("d-m-Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"CP_DATE"))); ?></label>
								   <input type="hidden" name="txtCP_Date" id="txtCP_Date" size="10" value="<?php echo date("d-m-Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"CP_DATE"))); ?>" autocomplete="off" readonly="true"/>
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Loading Port
								<address>
								   <label><?php echo $lp_name;?></label>
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Discharging Port
								<address>
								   <label><?php echo $dp_name;?></label>
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Total BL Qty.
								<address>
								   <label><?php echo $obj->getFun15();?></label>
								</address>
							</div><!-- /.col -->						
						</div>
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Details</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Invoice Type</th>
											<th>Invoice Date</th>
											<th>Invoice No.</th>
											<th>Amount</th>
										</tr>
									</thead>
									<tbody>
									<?php
									$sql = "select * from invoice_master where MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and NAMEID = '".$id[0]."' and GRADEID='".$id[2]."'";
									$res = mysql_query($sql);
									$rec = mysql_num_rows($res);
									
									if($rec == 0)
									{
									?>
									<tr>
										<td valign="top" align="center" style="color:red;letter-spacing:1px;" colspan="4">SORRY CURRENTLY THERE ARE ZERO(0) RECORDS</td>
									</tr>
									<?php }else{while($rows = mysql_fetch_assoc($res)){?>
									<tr>
										<td><?php echo $rows['I_TYPE'];?></td>
										<td><?php echo date("d-M-Y",strtotime($rows['DATE']));?></td>
										<td><?php echo $rows['MESSAGE'];?></td>
										<td><?php echo $rows['NET'];?></td>
									</tr>
									<?php }}?>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Invoice Lifecycle</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding" style="overflow:auto;">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Owner/Charterer</th>
											<th>Rcvd Dem Docs Date</th>
											<th>Counter Date</th>
											<th>Counter Amt.(USD)</th>
											<th>Agreed Date</th>
											<th>Agreed Amt.(USD)</th>
											<th>Received Date</th>
											<th>Received Amt.(USD)</th>
											<th>Pending Amt.(USD)</th>
											<th>Invoice Status</th>
											<th>Del</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<tbody id="tbd">
	
												<?php
												$sql = "select * from invoice_claim_master where MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and NAMEID = '".$id[0]."' and GRADEID='".$id[2]."'";
												$res = mysql_query($sql);
												$rec = mysql_num_rows($res);
												$i = 0;
												if($rec == 0)
												{
												?>
												<tr id='row_1'>
													<td>
													<select  name="selType_1" class="select form-control" id="selType_1" >
														<option value = "" >---Select from list---</option>
														<?php $obj->getO_CType(); ?>
													</select></td>
													<td>
														<input type="text" class="form-control" name="txtRDDDate_1" id="txtRDDDate_1" value="" autocomplete="off"/>
													</td>
													<td>
														<input type="text" class="form-control" name="txtCDate_1" id="txtCDate_1" value="" autocomplete="off"/></td>
													<td>
														<input type="text" class="form-control" name="txtCAmt_1" id="txtCAmt_1" value="" autocomplete="off"/>
													</td>
													<td>
														<input type="text" class="form-control" name="txtADate_1" id="txtADate_1" value="" autocomplete="off" />
													</td>
													<td>
														<input type="text" class="form-control" name="txtAAmt_1" id="txtAAmt_1" value="" autocomplete="off"/>
													</td>
													<td>
														<input type="text" class="form-control" name="txtRDate_1" id="txtRDate_1" value="" autocomplete="off"/>
													</td>
													<td>
														<input type="text" class="form-control" name="txtRAmt_1" id="txtRAmt_1" value="" autocomplete="off"/>
													</td>
													<td>
														<input type="text" class="form-control" name="txtPAmt_1" id="txtPAmt_1" value="" autocomplete="off"/>
													</td>
													<td>
														<select  name="selIS_1" class="select form-control" id="selIS_1">
															<?php $obj->getInvoiceStatusList();?>
														</select>
													</td>
													<td>
														<a href="#1" onclick="Del_Row(1);"><img src="../../img/icon_delete.gif" /></a>
															<input type="hidden" class="input" name="txtROW_1" id="txtROW_1" value="1"/>
													</td>
												</tr>
												<?php $i++;}else{
													while($rows = mysql_fetch_assoc($res)){ 
													$i = $i+1;
													if($rows['RCVD_DEM_DOCS_DATE'] == '1970-01-01'){
														$rdd_date = '';
													}else{
														$rdd_date = date("d-M-Y",strtotime($rows['RCVD_DEM_DOCS_DATE']));
													}
													if($rows['COUNTER_DATE'] == '1970-01-01'){
														$c_date = '';
													}else{
														$c_date = date("d-M-Y",strtotime($rows['COUNTER_DATE']));
													}
													if($rows['AGREED_DATE'] == '1970-01-01'){
														$a_date = '';
													}else{
														$a_date = date("d-M-Y",strtotime($rows['AGREED_DATE']));
													}
													if($rows['RCVD_DATE'] == '1970-01-01'){
														$r_date = '';
													}else{
														$r_date = date("d-M-Y",strtotime($rows['RCVD_DATE']));
													}
												?>
												<tr id='row_<?php echo $i;?>'>
													<td>
														<select  name="selType_<?php echo $i;?>" class="select form-control" id="selType_<?php echo $i;?>">
															<option value = "" >---Select from list---</option>
															<?php $obj->getO_CType(); ?>
														</select>
													</td>
													<script>$('#selType_<?php echo $i;?>').val('<?php echo $rows['OWNER'];?>');</script>
													<td><input type="text" class="form-control" name="txtRDDDate_<?php echo $i;?>" id="txtRDDDate_<?php echo $i;?>" value="<?php echo $rdd_date;?>" readonly="true" /></td>
													<td><input type="text" class="form-control" name="txtCDate_<?php echo $i;?>" id="txtCDate_<?php echo $i;?>" value="<?php echo $c_date;?>" readonly="true" /></td>
													<td><input type="text" class="form-control" name="txtCAmt_<?php echo $i;?>" id="txtCAmt_<?php echo $i;?>" value="<?php echo $rows['COUNTER_AMT'];?>" autocomplete="off"/></td>
													<td><input type="text" class="input-text" name="txtADate_<?php echo $i;?>" id="txtADate_<?php echo $i;?>" value="<?php echo $a_date;?>" readonly="true" /></td>
													<td><input type="text" class="form-control" name="txtAAmt_<?php echo $i;?>" id="txtAAmt_<?php echo $i;?>" value="<?php echo $rows['AGREED_AMT'];?>" autocomplete="off"/></td>
													<td><input type="text" class="input-text" name="txtRDate_<?php echo $i;?>" id="txtRDate_<?php echo $i;?>" value="<?php echo $r_date;?>" readonly="true" /></td>
													<td><input type="text" class="form-control" name="txtRAmt_<?php echo $i;?>" id="txtRAmt_<?php echo $i;?>" value="<?php echo $rows['RCVD_AMT'];?>" autocomplete="off"/></td>
													<td><input type="text" class="form-control" name="txtPAmt_<?php echo $i;?>" id="txtPAmt_<?php echo $i;?>" value="<?php echo $rows['PENDING_AMT'];?>" autocomplete="off"/></td>
													<td><select  name="selIS_<?php echo $i;?>" class="select form-control" id="selIS_<?php echo $i;?>"  style="width:70px;">
													<?php $obj->getInvoiceStatusList();?>
													</select></td>
													<script>$('#selIS_<?php echo $i;?>').val(<?php echo $rows['INVOICE_STATUS'];?>);</script>
													<td><a href="#1" onclick="Del_Row(<?php echo $i;?>);"><img src="../../img/icon_delete.gif" /></a><input type="hidden" class="input" name="txtROW_<?php echo $i;?>" id="txtROW_<?php echo $i;?>" value="<?php echo $i;?>" size="5" /></td>
												</tr>
												<?php }}?>
												</tbody>
										</tr>
										<tr>
											<td colspan="11" align="left" valign="middle">
												<a href="#1" onclick="addUI_Row_2();"><img src="../../img/icon_add.gif" /></a>
												<input type="hidden" class="form-control" name="txtTBID" id="txtTBID" value="<?php echo $i;?>" />
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<?php if($rights == 1){ ?>
						<div class="box-footer" align="right">
							<button type="button" class="btn btn-primary btn-flat" onClick="return getSubmit()">Submit</button>
							<input type="hidden" name="action" id="action" value="submit" />
							<input type="hidden" name="txtCRM" id="txtCRM" value="" />
						</div>
						<?php } ?>
					</form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 

$("[id^=txtCAmt_], [id^=txtAAmt_], [id^=txtRAmt_], [id^=txtPAmt_]").numeric();

$('[id^=txtRDDDate_], [id^=txtCDate_], [id^=txtADate_], [id^=txtRDate_]').datepicker({
	format: 'dd-m-yyyy'
});

});

function addUI_Row_2()
{
	var id = $("#txtTBID").val();

	id  = (id - 1 )+ 2;
	$('<tr id="row_'+id+'"><td><select  name="selType_'+id+'" class="select form-control" id="selType_'+id+'"><option value = "" >---Select from list---</option><?php $obj->getO_CType();?></select></td><td><input type="text" class="form-control" name="txtRDDDate_'+id+'" id="txtRDDDate_'+id+'" value=""/></td><td align="left" valign="top"><input type="text" class="form-control" name="txtCDate_'+id+'" id="txtCDate_'+id+'" value=""/></td><td align="left" valign="top"><input type="text" class="form-control" name="txtCAmt_'+id+'" id="txtCAmt_'+id+'" value="" autocomplete="off"/></td><td align="left" valign="top"><input type="text" class="form-control" name="txtADate_'+id+'" id="txtADate_'+id+'" value=""/></td><td align="left" valign="top"><input type="text" class="form-control" name="txtAAmt_'+id+'" id="txtAAmt_'+id+'" value="" autocomplete="off"/></td><td align="left" valign="top"><input type="text" class="form-control" name="txtRDate_'+id+'" id="txtRDate_'+id+'" value=""/></td><td align="left" valign="top"><input type="text" class="form-control" name="txtRAmt_'+id+'" id="txtRAmt_'+id+'" value="" autocomplete="off"/></td><td align="left" valign="top"><input type="text" class="form-control" name="txtPAmt_'+id+'" id="txtPAmt_'+id+'" value="" autocomplete="off"/></td><td align="left" valign="top"><select  name="selIS_'+id+'" class="select form-control" id="selIS_'+id+'"  style="width:70px;"><?php $obj->getInvoiceStatusList();?></select></td><td align="center" valign="middle"><a href="#1" onclick="Del_Row('+id+');"><img src="../../img/icon_delete.gif" /></a><input type="hidden" name="txtROW_'+id+'" id="txtROW_'+id+'" value="'+id+'" /></td></tr>').appendTo("#tbd");
	$("#txtTBID").val(id);
	
	$("[id^=txtCAmt_], [id^=txtAAmt_], [id^=txtRAmt_], [id^=txtPAmt_]").numeric();

	$('[id^=txtRDDDate_], [id^=txtCDate_], [id^=txtADate_], [id^=txtRDate_]').datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-M-yy'
	});
}

function Del_Row(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently?', 'Confirmation', function(r) {
	if(r){
			$('#row_'+var1).remove();
		}
		else
		{
			return false;
		}
	});
}

function getSubmit()
{
	var var3 = $("[id^=txtROW_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRM').val(var3);
	//document.frm1.submit();
}

</script>
    </body>
</html>
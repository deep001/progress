<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$mappingid = $_REQUEST['mappingid'];
$page  = $_REQUEST['page'];

if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}

if($obj->getBAFCalculationRec($mappingid) == 1)
{
	header('Location:./baf_calculation1.php?mappingid='.$mappingid.'&page='.$page);
}

if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertBAFDetails();
	header('Location:./baf_calculation.php?msg='.$msg.'&mappingid='.$mappingid.'&page='.$page);
 }
$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&page=".$page;

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header" align="center"><?php echo strtoupper($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME"))." / ".(int)$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"NO_OF_LIFT")." LIFT / ".date("d-m-Y",strtotime($obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'COA_DATE')))." / ".$obj->getCOANumberBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"COA_NO"));?> :: BAF CALCULATION</h2>
                        </div><!-- /.col -->
                </div>
                <?php 
				if($obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'BAF') == 0)
				{
					echo '<div class="box">				
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                        	<thead>
                            	<tr>
                                	<th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>                                  
                                </tr>
                            </thead>
                            <tbody>';
					
					 	echo '<tr>
								<td align="center" colspan="5" valign="middle" style="color:#ff0000;letter-spacing:1px;">'.strtoupper("Please fulfill BAF details in COA.").'</td>
								</tr>';
						echo '</tbody>
						</table>
					</div>
				</div>';
				}
				else
				{
				?>
                
                <div class="box">				
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                        	<thead>
                            	<tr>
                                	<th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>                                    
                                </tr>
                            </thead>
                            <tbody>
                            	<tr>
                                	<td>Bunker Price IFO (USD/MT)</td>
                                    <td><input type="text" name="txtBPrice" id="txtBPrice" class="form-control" readonly value="<?php echo $obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'BUNKER_PRICE');?>" /></td>
                                    <td>BAF Terms (Days)</td>
                                    <td><input type="text" name="txtBAFTerms" id="txtBAFTerms" class="form-control" readonly value="<?php echo $obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'BAF_TERMS');?>" /></td>
                                    <td>BAF (USD/MT)</td>
                                    <td><input type="text" name="txtIncDec" id="txtIncDec" class="form-control" readonly value="<?php echo $obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'INCREASE_DECREASE');?>" /></td>
                                </tr>
                            </tbody>
                         </table>
                    </div>
                </div>
                
                <div class="box">			
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Port</th>
                                    <th width="20%">BL Quantity (MT)</th>
                                    <th width="20%">BL Date</th>
                                    <th width="30%">Remarks</th>                                  
                                </tr>
                            </thead>
                           <tbody id="tblBLData">
								<tr id="blrow_1">
                                	<td ><a href="#1" title="Remove Entry" onclick="getBLDelete(1);" ><img src="../../img/icon_delete.gif"  height="16" width="16" /></a></td>
									<td>
                                        <select  name="selPort_1" class="form-control" id="selPort_1">
                                            <?php 
                                            $obj->getPortListNew();
                                            ?>
                                        </select>
                                    </td>
									<td><input type="text" name="txtBLQty_1" id="txtBLQty_1" class="form-control" autocomplete="off" onkeyup="getTotal();"  placeholder="BL Quantity (MT)"/></td>
									<td><input type="text" name="txtBLDate_1" id="txtBLDate_1" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy"/></td>
									<td><textarea class="form-control" name="txtRemarks_1" id="txtRemarks_1" autocomplete="off" cols="33" rows="1" placeholder="Remarks..."></textarea></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                <th align="left" colspan="2" ><a href="#A" title="Add Row" onclick="getAddBLRow();" ><img src="../../img/icon_add.gif" height="16" width="16"/></a><input type="hidden" name="txtBLID" id="txtBLID" class="form-control" value="1" /></th>
                                <th align="left" ><input type="text" name="txtTotalBLQty" id="txtTotalBLQty" class="form-control" readonly value="" /></th>
                                <th align="left" colspan="2" ></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                 </div>
                  <div class="box">			
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Date</th>
                                    <th width="20%">Day</th>
                                    <th width="20%">Rate IFO (USD/MT)</th>
                                    <th width="30%">Remarks</th>                                  
                                </tr>
                            </thead>
                          	<tbody id="tblDateData">
                                <tr id="daterow_1">
                                <td><a href="#1" title="Remove Entry" onclick="getDateDelete(1);" ><img src="../../img/icon_delete.gif"  height="16" width="16" /></a></td>
                                <td><input type="text" name="txtDate_1" id="txtDate_1" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy"/></td>
                                <td><input type="text" name="txtDay_1" id="txtDay_1" class="form-control" autocomplete="off" placeholder="Day" readonly/></td>
                                <td><input type="text" name="txtRateIFO_1" id="txtRateIFO_1" class="form-control" autocomplete="off" placeholder="Rate IFO (USD/MT)"/></td>
                                <td><textarea name="txtDRemarks_1" id="txtDRemarks_1" class="form-control" cols="33" rows="1" placeholder="Remarks..."></textarea></td>		
                                </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                            <th align="left" colspan="5" ><a href="#A" title="Add Row" onclick="getAddDateRow();" ><img src="../../img/icon_add.gif" height="16" width="16"/></a><input type="hidden" name="txtDateID" id="txtDateID" readonly value="1" /></th>
                            </tr>
                            </tfoot>
                       </table>
                    </div>
                </div>
                 <?php } ?>
                <div class="box-footer" align="right">
                    <button type="button" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
                    <input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
                </div>	
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>




<script type="text/javascript">
$(document).ready(function(){
	$('[id^=txtRemarks_],[id^=txtDRemarks_]').autosize({append: "\n"});
	$("[id^=txtBLDate_]").datepicker({
		format: 'dd-mm-yyyy',
		autoclose:true
	});
	$("[id^=ui-datepicker-div]").hide();
	$("[id^=txtBLQty_],[id^=txtRateIFO_]").numeric();

	$("[id^=txtDate_]").datepicker({
		format: 'dd-mm-yyyy',
		autoclose:true,
	}).on('changeDate', function(){
		
		var rowid = this.id;
		var seldate = $(this).datepicker('getDate');
		seldate = seldate.toDateString();
		seldate = seldate.split(' ');
		var weekday=new Array();
		weekday['Mon']="Monday";
		weekday['Tue']="Tuesday";
		weekday['Wed']="Wednesday";
		weekday['Thu']="Thursday";
		weekday['Fri']="Friday";
		weekday['Sat']="Saturday";
		weekday['Sun']="Sunday";
		var dayOfWeek = weekday[seldate[0]];
		$('#txtDay_'+rowid.split('_')[1]).val(dayOfWeek);
	});
}); 


function getAddBLRow()
{
	var id = $("#txtBLID").val();
	if($("#selPort_"+id).val() != "" && $("#txtBLQty_"+id).val() != "" && $("#txtBLDate_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="blrow_'+id+'"><td><a href="#'+id+'" title="Remove Entry" onclick="getBLDelete('+id+');" ><img src="../../img/icon_delete.gif"  height="16" width="16" /></a></td><td><select  name="selPort_'+id+'" class="form-control" id="selPort_'+id+'"><?php $obj->getPortListNew();?></select></td><td><input type="text" name="txtBLQty_'+id+'" id="txtBLQty_'+id+'" class="form-control" autocomplete="off" onkeyup="getTotal();" placeholder="BL Quantity (MT)" /></td><td><input type="text" name="txtBLDate_'+id+'" id="txtBLDate_'+id+'" class="form-control" placeholder="dd-mm-yyyy" /></td><td><textarea class="form-control" name="txtRemarks_'+id+'" id="txtRemarks_'+id+'" cols="33" rows="1" placeholder="Remarks..."></textarea></td></tr>').appendTo("#tblBLData");		
		
		$("[id^=txtBLDate_]").datepicker({
			format: 'dd-mm-yyyy',
			autoclose:true
		});
		$('[id^=txtRemarks_]').autosize({append: "\n"});
		$("[id^=txtBLQty_]").numeric();
		$("[id^=ui-datepicker-div]").hide();
		$("#txtBLID").val(id);
	}
	else
	{
		jAlert('Please fill details.', 'Alert');
	}
}

function getBLDelete(rowid)
{
	var len = $("[id^=selPort_]option:selected").length;
	if(len == 1)
	{
		jAlert('Can not remove all entries.', 'Alert');
		return false;
	}
	else
	{
	jConfirm('Are you sure to remove this entry ?', 'Confirmation', function(r) {
		if(r){	
			$("#blrow_"+rowid).remove();
			$("#txtTotalBLQty").val($("[id^=txtBLQty_]").sum().toFixed(2));
		}
		else{return false;}
		});	
	}
}	

function getDateDelete(rowid)
{
	var len = $(":input[id^=txtDate_]").length;
	if(len == 1)
	{
		jAlert('Can not remove all entries.', 'Alert');
		return false;
	}
	else
	{
	jConfirm('Are you sure to remove this entry ?', 'Confirmation', function(r) {
		if(r){	
			$("#daterow_"+rowid).remove();
		}
		else{return false;}
		});	
	}
}	

function getAddDateRow()
{
	var id = $("#txtDateID").val();
	if($("#txtDate_"+id).val() != "" && $("#txtDay_"+id).val() != "" && $("#txtRateIFO_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="daterow_'+id+'"><td><a href="#1" title="Remove Entry" onclick="getDateDelete('+id+');" ><img src="../../img/icon_delete.gif"  height="16" width="16" /></a></td><td><input type="text" name="txtDate_'+id+'" id="txtDate_'+id+'" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy"/></td><td><input type="text" name="txtDay_'+id+'" id="txtDay_'+id+'" class="form-control" autocomplete="off" placeholder="Day" readonly="true" /></td><td><input type="text" name="txtRateIFO_'+id+'" id="txtRateIFO_'+id+'" class="form-control" placeholder="Rate IFO (USD/MT)" autocomplete="off"/></td><td><textarea class="form-control" autocomplete="off" name="txtDRemarks_'+id+'" id="txtDRemarks_'+id+'" cols="33" rows="1"></textarea></td>	</tr>').appendTo("#tblDateData");		
		
		$("[id^=txtDate_]").datepicker({
			format: 'dd-mm-yyyy',
			autoclose:true,
			}).on('changeDate', function(){
				var rowid = this.id;
				var seldate = $(this).datepicker('getDate');
				seldate = seldate.toDateString();
				seldate = seldate.split(' ');
				var weekday=new Array();
					weekday['Mon']="Monday";
					weekday['Tue']="Tuesday";
					weekday['Wed']="Wednesday";
					weekday['Thu']="Thursday";
					weekday['Fri']="Friday";
					weekday['Sat']="Saturday";
					weekday['Sun']="Sunday";
				var dayOfWeek = weekday[seldate[0]];
				$('#txtDay_'+rowid.split('_')[1]).val(dayOfWeek);
			});

		$('[id^=txtDRemarks_]').autosize({append: "\n"});
		$("[id^=txtRateIFO_]").numeric();
		$("[id^=ui-datepicker-div]").hide();
		$("#txtDateID").val(id);
	}
	else
	{
		jAlert('Please fill details.', 'Alert');
	}
}

function getTotal()
{
	$("#txtTotalBLQty").val($("[id^=txtBLQty_]").sum().toFixed(2));
}

function getValidate()
{
	var arr = new Array();
	var i=0;
	$('[id^=selPort_]').each(function(index) {
		var j = i+1;
		if($("#selPort_"+j).val() == "" || $("#txtBLQty_"+j).val() == "" || $("#txtBLDate_"+j).val() == "")
				{
					arr[i] = j;					
				}
			i++;
		});
	if(arr.length == 0)
	{
		var arr1 = new Array();
		var i=0;
		$('[id^=txtDate_]').each(function(index) {
			var j = i+1;
			if($("#txtDate_"+j).val() == "" || $("#txtDay_"+j).val() == "" || $("#txtRateIFO_"+j).val() == "")
					{
						arr1[i] = j;					
					}
				i++;
			});
		if(arr1.length == 0)
		{
			jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
			if(r){ 
				document.frm1.submit();
			}
			else{return false;}
			});	
		}
		else 
		{
			jAlert('Please fill all values', 'Alert');
			return false;
		}
	}
	else 
	{
		jAlert('Please fill all values', 'Alert');
		return false;
	}
}

</script>
    </body>
</html>
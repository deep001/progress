<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];

$cost_sheet_id = $obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($cost_sheet_id);
$vendorid = $obj->getFun140();

$vendosrs = array();
$sql11 = "select * from freight_cost_estimete_slave4 where FCAID='".$cost_sheet_id."'";
$res11 = mysql_query($sql11) or die($sql11);
while($rows11 = mysql_fetch_assoc($res11))
{
	$vendosrs[] = $obj->getVendorListNewData($rows11['VENDORID'],'NAME');
}

$vendosrs1 = array();
$sql2 = "select VENDORID from freight_cost_estimete_slave3 where FCAID='".$cost_sheet_id."' and IDENTIFY='ORC' and IDENTY_ID=2";
$res2 = mysql_query($sql2) or die($sql2);
$num2 = mysql_num_rows($res2);
while($rows2 = mysql_fetch_assoc($res2))
{
	$vendosrs1[] = $obj->getVendorListNewData($rows2['VENDORID'],'NAME');
}
$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
	$("#frm1").validate({
		rules: {
		selIType: {required: true},
		txtInvoiceNo: {required: true},
		txtDate: {required: true},
		txtDelLtTimeGMT:{required: true},
		txtReDelLtTimeGMT:{required: true},
		selDelPort:{required: true},
		selReDelPort:{required: true},
		txtTCCost:{required: true},
		txtReleaseHireDays:{required: true},
		selNOB:{required: true}
		},
	messages: {
		selIType: {required: "*"},
		txtInvoiceNo: {required: true},
		txtDate: {required: true},
		txtDelLtTimeGMT:{required: "*"},
		txtReDelLtTimeGMT:{required: "*"},
		selDelPort:{required: "*"},
		selReDelPort:{required: "*"},
		txtTCCost:{required: "*"},
		txtReleaseHireDays:{required: "*"},
		selNOB:{required: "*"}
		},
	submitHandler: function(form)  {
			jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
			$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
			$("#popup_content").css({"background":"none","text-align":"center"});
			$("#popup_ok,#popup_title").hide();  
			form.submit();
		}
	});
	
	$("[id^=txtHireDays_],[id^=txtHireDays_]").numeric();	
	
	$('#txtDate,#txtP_Date').datepicker({
		format: 'dd-mm-yyyy',
		autoclose:true
	});

});

function getTimeDiff(date2,date1)
{	
	if(date1 != '' &&  date2 != '')
	{
		end_actual_time  	 =  getDateWithSpecificFormat(date2);
		start_actual_time    =  getDateWithSpecificFormat(date1);
		
		start_actual_time 	= new Date(start_actual_time);
		end_actual_time 	= new Date(end_actual_time);
	
		var diff 			= end_actual_time - start_actual_time;
		var days			= (diff) / 86400000;
		return days.toFixed(5);	
	}
}


function getString(var1)
{
  var var2 = var1.split(' ');
  var var3 = var2[0].split('-');  
  return var3[2]+'/'+var3[1]+'/'+var3[0]+' '+var2[1];
}
 
function getDateWithSpecificFormat(sep)
{
	var dateSplit1  = sep.split(' ');
	var dateSplit   = dateSplit1[0].split('-');
	var currentDate = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0]+' '+dateSplit1[1];
	return currentDate;
}


function getValidate(var1)
{
	$("#txtStatus").val(var1);
}


function AddHireRows()
{
	var id = $("#txtOFFID").val();
	if($("#txtHireDays_"+id).val() != "" && $("#txtDesc_"+id).val()!='')
	{
		id = (id - 1) + 2;
		$('<tr id="off_Row_'+id+'"><td><input type="text" name="txtHireDays_'+id+'" id="txtHireDays_'+id+'" class="form-control"  placeholder="" autocomplete="off" value="" onKeyUp="getCalculateHireAmt();"/></td><td><input type="text" name="txtOffHire_'+id+'" id="txtOffHire_'+id+'" class="form-control"  placeholder="" autocomplete="off" value="" onKeyUp="getCalculateHireAmt();"/></td><td><input type="text" name="txtDesc_'+id+'" id="txtDesc_'+id+'" class="form-control" placeholder="" autocomplete="off" value=""/></td><td><input type="text" name="txtHireAmt_'+id+'" id="txtHireAmt_'+id+'" class="form-control"  placeholder="" autocomplete="off" readonly value=""/></td></tr>').appendTo("#tblOFF");
		$("#txtOFFID").val(id);
		$("[id^=txtHireDays_],[id^=txtOffHire_]").numeric();	
	}
}

function getCalculateHireAmt()
{
	var id = $("#txtOFFID").val();
	for(var i =1;i<=id;i++)
	{
		var hire = $("#txtHireRate").val();
		if(isNaN(hire)){hire = 0.00;}
		var hiredays = $("#txtHireDays_"+i).val();
		if(isNaN(hiredays) || hiredays==''){hiredays = 0.00;}
		var offhiredays = $("#txtOffHire_"+i).val();
		if(isNaN(offhiredays) || offhiredays==''){offhiredays = 0.00;}
		
		var totaldays = parseFloat(hiredays) - parseFloat(offhiredays);
		var invoiceamt = parseFloat(hire)*parseFloat(totaldays);
		if(isNaN(invoiceamt)){invoiceamt = 0.00;}
		$("#txtHireAmt_"+i).val(parseFloat(invoiceamt).toFixed(2));
	}
}

</script>
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(18); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="glyphicon glyphicon-credit-card"></i>&nbsp;Order To Cash &nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Order To Cash &nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;OC07</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
          
				<div align="right"><button class="btn btn-default" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button></a>&nbsp;&nbsp;&nbsp;<a href="oc07.php"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
							
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12" style="text-align:center;">
								<h2 class="page-header">
								OC07
                                <input type="hidden" name="txtFcaid" id="txtFcaid" value="<?php echo $obj->getFun1();?>"/>
                                <input type="hidden" name="txtMapId" id="txtMapId" value="<?php echo $comid;?>"/>
                                <input type="hidden" name="txtInID" id="txtInID" value="<?php echo $rows['INVOICEID'];?>"/>
                                <input type="hidden" name="txtVendorID" id="txtVendorID" value="<?php echo $vendorid;?>"/>
								</h2>                            
							</div><!-- /.col -->
						</div>
					    
                        
                        <div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
								Header Info
								</h3>                            
							</div><!-- /.col -->
						</div>
                        
						<div class="row invoice-info">
							<div class="col-sm-3 invoice-col">
								Nom ID
								<address>
								 <strong>&nbsp;&nbsp;<?php echo $obj->getCompareTableData($comid,"MESSAGE");?></strong>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-3 invoice-col">
								Vessel
								<address>
								 <strong>&nbsp;&nbsp;<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></strong>
								</address>
							</div><!-- /.col -->
							
                            <div class="col-sm-3 invoice-col">
								<span style="color:red;">SAP Sales Invoice Document No.</span>
								<address>
								   <input type="text" name="txtSAPPONO" id="txtSAPPONO" class="form-control" value=""/>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-3 invoice-col">
								 Date
								<address>
								   <input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo date("d-m-Y",time());?>"/>
								</address>
							</div><!-- /.col -->
						</div>
                        
                        <div class="row invoice-info">
							<div class="col-sm-3 invoice-col">
								Invoice Category
								<address>
								   <input type="text" name="txtINVCategory" id="txtINVCategory" class="form-control" value="Others" readonly/>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-3 invoice-col">
								Invoice Type
								<address>
								   <select name="txtSAPPONO" id="txtSAPPONO" class="form-control">
                                   <?php $obj->getFreightInvoiceType();?>
                                   </select>
								</address>
							</div><!-- /.col -->
                           
						</div>
                        
						<div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
								Details
								</h3>                            
							</div><!-- /.col -->
						</div>
                        
                        <div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Charter Type
								<address>
								    <strong>&nbsp;&nbsp;TC In - VC Out</strong>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
								Owner
								<address>
								 <strong>&nbsp;&nbsp;<?php echo $obj->getVendorListNewData($obj->getCompareEstimateData($comid,"DTCVENDORID"),"NAME");?></strong>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								GRT/NRT
								<address>
								   <strong>&nbsp;&nbsp;<?php echo $obj->getVesselParticularData('GT_INATERNATIONAL','vessel_master_1',$obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"));?></strong>
								</address>
							</div><!-- /.col -->
                            
						</div>
                        
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
								 BROKER
                                <address>
                                <strong>&nbsp;&nbsp;<?php echo implode(', ',$vendosrs);?></strong>
                                </address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								P & I Club
								<address>
								    <strong>&nbsp;&nbsp;<?php echo implode(', ',$vendosrs1);?></strong>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
								CP Date
								<address>
								   <strong>&nbsp;&nbsp;<?php
								   $cpdate = $obj->getCompareEstimateData($comid,"CP_DATE");
								   if($cpdate=="" || $cpdate=="0000-00-00"){$cpdate = ""; }
								   else{$cpdate = date("d-m-Y",strtotime($obj->getCompareEstimateData($comid,"CP_DATE"))); }
								    echo $cpdate; ?></strong>
								   <input type="hidden" name="txtCP_Date" id="txtCP_Date" class="input-text" size="10" value="<?php echo $cpdate; ?>" autocomplete="off" readonly/>
								</address>
							</div><!-- /.col -->
						</div>
						
                        <div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
								LOAD PORT
								</h3>                            
							</div><!-- /.col -->
						</div>
                        
                        <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Load Port(s)</th>
                                    <th>Load Rate&nbsp;<span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                                    <th>BL Date</th>
                                    <th>BL Quantity</th>
                                </tr>
                            </thead>
                            <tbody id="tblLoadPort">
                            <?php 
                            $sql1 = "select * from freight_cost_estimete_slave1 where FCAID='".$cost_sheet_id."' and LOAD_PORT_QTY>0";
                            
                            $res1 = mysql_query($sql1);
                            $num1 = mysql_num_rows($res1);
                              while($rows1 = mysql_fetch_assoc($res1))
                              {$i = $i + 1;
                              ?>
                                <?php
								$j = 0;
								
								$sql2 = "select * from sof_slave_1 inner join sof_master on sof_master.SOFID = sof_slave_1.SOFID where sof_master.PORT='LP' and sof_master.PORTID='".$rows1['FROM_PORT']."' and COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and LOGIN='INTERNAL_USER'";
								$res2 = mysql_query($sql2) or die($sql2);
								$rec2 = mysql_num_rows($res2);
								$bldate = $blqty = array();
								while($rows2 = mysql_fetch_assoc($res2))
								{ 
								    $bldate[] = date("d-M-Y",strtotime($rows2['BL_DATE']));
									$blqty[] = $rows2['BL_QTY'];
								}?>
                              <tr id="lp_Row_<?php echo $i;?>">
                                <td align="left" class="input-text" >
                                    <span id="spanLoadPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($rows1['FROM_PORT']);?></span>
                                </td>
                                <td align="left" class="input-text" >
                                    <?php echo $rows1['LOAD_PORT_RATE'];?>
                                </td>
                                <td align="left" class="input-text" >
                                    <?php echo implode(', ',$bldate);?>
                                </td>
                                <td align="left" class="input-text" >
                                    <?php echo implode(', ',$blqty);?>
                                </td>
                            </tr>
                          <?php }?>										
                            </tbody>
                          </table>
                        </div>
                        
                        <div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
								DISCHARGE PORT
								</h3>                            
							</div><!-- /.col -->
						</div>
                        
                        <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Discharge Port(s)</th>
                                    <th>Discharge Rate&nbsp;<span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                                    <th>Discharge Quantity</th>
                                </tr>
                            </thead>
                            <tbody id="tblLoadPort">
                            <?php 
                            $sql1 = "select * from freight_cost_estimete_slave1 where FCAID='".$cost_sheet_id."' and DISC_PORT_QTY>0";
                            
                            $res1 = mysql_query($sql1);
                            $num1 = mysql_num_rows($res1);
                              while($rows1 = mysql_fetch_assoc($res1))
                              {$i = $i + 1;
                              ?>
                              <tr id="lp_Row_<?php echo $i;?>">
                                <td align="left" class="input-text" >
                                    <span id="spanLoadPort_<?php echo $i;?>"><?php echo $obj->getPortNameBasedOnID($rows1['TO_PORT']);?></span>
                                </td>
                                <td align="left" class="input-text" >
                                    <?php echo $rows1['DISC_PORT_RATE'];?>
                                </td>
                                <td align="left" class="input-text" >
                                    <?php echo $rows1['DISC_PORT_QTY'];?>
                                </td>
                            </tr>
                          <?php }?>										
                            </tbody>
                          </table>
                        </div>
                        
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Quantity (MT)
								<address>
                                    <input type="text" name="txtQuantity" id="txtQuantity" class="form-control" readonly value="<?php echo $obj->getFun50();?>"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Freight Rate (USD/MT)
								<address>
                               
								  <input type="text" name="txtFreightRate" id="txtFreightRate" class="form-control" readonly value="<?php echo $obj->getFun47();?>"/>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
								Freight Amount (USD)
								<address>
                                    <input type="text" name="txtFreightAMT" id="txtFreightAMT" class="form-control" readonly value="<?php echo $obj->getFun66();?>"/>
								</address>
							</div><!-- /.col -->
						</div>
						
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Additional Freight Qty
								<address>
                                    <input type="text" name="txtAddFreightQty" id="txtAddFreightQty" class="form-control" readonly value="<?php echo $obj->getFun54();?>"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Freight Rate (Adln. Qty.)
								<address>
                               
								  <input type="text" name="txtAddFreightRate" id="txtAddFreightRate" class="form-control" readonly value="<?php echo $obj->getFun49();?>"/>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
								Additional Freight Amount (USD)
								<address>
                                    <input type="text" name="txtAddFreightAmt" id="txtAddFreightAmt" class="form-control" readonly value="<?php echo $obj->getFun72();?>"/>
								</address>
							</div><!-- /.col -->
						</div>
                        
                        
                        <div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Dead Freight Qty
								<address>
                                    <input type="text" name="txtDearFreightQty" id="txtAddFreightQty" class="form-control" readonly value="<?php echo $obj->getFun53();?>"/>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Freight Rate (DeadFreight Qty.)
								<address>
                               
								  <input type="text" name="txtAddFreightRate" id="txtAddFreightRate" class="form-control" readonly value="<?php echo $obj->getFun53();?>"/>
								</address>
							</div><!-- /.col -->
                            <div class="col-sm-4 invoice-col">
								Dead Freight Amount (USD)
								<address>
                                    <input type="text" name="txtHireageAmt" id="txtHireageAmt" class="form-control" readonly value="<?php echo $obj->getFun67();?>"/>
								</address>
							</div><!-- /.col -->
						</div>
						
                        
                       <div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
								Other Add
								</h3>                            
							</div><!-- /.col -->
						</div> 
                       
                       <div class="box">
                          <div class="box-body no-padding">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Description</th>
                                        
                                        <th>Amount<input type="hidden" name="txtAddID" id="txtAddID" value="1"/></th>
                                    </tr>
                                </thead>
                                <tbody id="tblAdd">
                                
                                    <tr id="add_Row_1">
                                       <td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                       <td><textarea class="form-control areasize" name="txtAddDes_1" id="txtAddDes_1" rows="1" placeholder="Description..."></textarea></td>
                                       <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td>
                                    </tr>
                                   
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td align="left"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewAddRow();">Add</button></td>
                                        <td>Total</td>
                                        <td><input type="text" name="txtAddDesTotalAmt" id="txtAddDesTotalAmt" class="form-control" readonly  placeholder="0.00" autocomplete="off" value=""/></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                      </div>
                      
                      <div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
								Other Less
								</h3>                            
							</div><!-- /.col -->
						</div> 
                       
                       <div class="box">
                          <div class="box-body no-padding">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Description</th>
                                        
                                        <th>Amount<input type="hidden" name="txtAddID" id="txtAddID" value="1"/></th>
                                    </tr>
                                </thead>
                                <tbody id="tblAdd">
                                
                                    <tr id="add_Row_1">
                                       <td align="center"><a href="#pr1" onClick="removeAddRow(1);"><i class="fa fa-times" style="color:red;"></i></a></td>
                                       <td><textarea class="form-control areasize" name="txtAddDes_1" id="txtAddDes_1" rows="1" placeholder="Description..."></textarea></td>
                                       <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="" onKeyUp="getFullClaculation();"/></td>
                                    </tr>
                                   
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td align="left"><button type="button" class="btn btn-primary btn-flat" onClick="AddNewAddRow();">Add</button></td>
                                        <td>Total</td>
                                        <td><input type="text" name="txtAddDesTotalAmt" id="txtAddDesTotalAmt" class="form-control" readonly  placeholder="0.00" autocomplete="off" value=""/></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                      </div>
                      
                      
                       
                        
                        <div class="box">
                          <div class="box-body no-padding">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                       <td align="center" width="70%">Total Freight Amount</td>
                                       <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="<?php echo $obj->getFun66();?>" onKeyUp="getFullClaculation();"/></td>
                                    </tr>
                                   
                                </tbody>
                            </table>
                        </div>
                      </div>
                      
                       <div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
								Deduction
								</h3>                            
							</div><!-- /.col -->
						</div>
                        
                       <div class="box">
                          <div class="box-body no-padding">
                            <table class="table table-striped">
                                
                                <tbody>
                                
                                    <tr>
                                       <td align="center">Ad Comm.(%)</td>
                                       <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="<?php echo $obj->getFun74();?>" /></td>
                                       <td align="center">Ad Comm. Amount</td>
                                       <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="<?php echo $obj->getFun75();?>" onKeyUp="getFullClaculation();"/></td>
                                    </tr>
                                   
                                </tbody>
                            </table>
                        </div>
                      </div>
                      
                      <div class="row">
							<div class="col-xs-12">
								<h3 class="page-header">
								Net Receivable
								</h3>                            
							</div><!-- /.col -->
						</div>
                        
                       <div class="box">
                          <div class="box-body no-padding">
                            <table class="table table-striped">
                                
                                <tbody>
                                
                                    <tr>
                                       <td align="center">Percentage (%)</td>
                                       <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="100" /></td>
                                       <td align="center">Net Receivable</td>
                                       <td><input type="text" name="txtAddDesAmt_1" id="txtAddDesAmt_1" class="form-control"  placeholder="Amount" autocomplete="off" value="<?php echo $obj->getFun66();?>" onKeyUp="getFullClaculation();"/></td>
                                    </tr>
                                   
                                </tbody>
                            </table>
                        </div>
                      </div>
						
                       <div class="box-footer" align="right">
							<input type="hidden" name="txtStatus" id="txtStatus" value="" />
							<button class="btn btn-primary btn-flat" type="submit" onClick="return getValidate(1);">Submit to Edit</button>
							<button class="btn btn-primary btn-flat" type="submit" onClick="return getValidate(2);">Submit to Close</button>
							<input type="hidden" name="action" id="action" value="submit" />
						    <input type="hidden" name="txtDEL_ID" id="txtDEL_ID" value="0"/>
				        </div>
												
					</form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

    </body>
</html>
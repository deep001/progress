<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$id = $_REQUEST['id'];
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->updatePortDataRecords();
	header('Location:./port_data.php?msg='.$msg);
 }
$obj->viewPortDataRecords($id);
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(3); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Port Data</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="port_data.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				     <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             UPDATE PORT DATA
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                          Port Name
                            <address>
                              <?php echo $obj->getPortNameBasedOnID($obj->getFun1()); ?>
								
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                          Terminal
                            <address>
                            <?php echo $obj->getTerminalNameBasedOnID($obj->getFun2()); ?>
                              
								
                            </address>
                        </div><!-- /.col -->
                        
                        <div class="col-sm-12 invoice-col">
                          Cargo
                            <address>
                              <?php echo $obj->getFun6(); ?>
								
                            </address>
                        </div><!-- /.col -->
                        
                         <div class="col-sm-12 invoice-col">
                          Remarks
                            <address>
                              <textarea class="form-control areasize" name="txtDesc" id="txtDesc" rows="3" placeholder="Remarks ..." ><?php echo $obj->getFun4(); ?></textarea>
								
                            </address>
                        </div><!-- /.col -->
                       
                       
                       
                       
                       
                        <div class="col-sm-4 invoice-col">
                          Attachment
                            <address>
								
                               <div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Attach Permit">
                                    <i class="fa fa-paperclip"></i> Attachment
                                    <input type="file" class="form-control" name="mul_file[]" id="mul_file" multiple title="" onChange="validateFile();"/>
                                </div>
                            </address>
                        </div><!-- /.col -->
						<?php if($obj->getFun5() != ''){ $file = explode(",",$obj->getFun5()); $name = explode(",",$obj->getFun7());?>
						<div class="col-sm-8 invoice-col">
                          Previous Attachments
                            <address>
									<table cellpadding="1" cellspacing="1" border="0" width="100%" align="left">
									<?php
									$j =1;
									for($i=0;$i<sizeof($file);$i++)
									{
									?>
									<tr height="20" id="row_file_<?php echo $j;?>">
										<td width="50%" align="left" class="input-text"  valign="top"><a href="../../attachment/<?php echo $file[$i]; ?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="Click to view file"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $name[$i];?></a><input type="hidden" name="file1_<?php echo $j;?>" id="file1_<?php echo $j;?>" value="<?php echo $file[$i]; ?>" /><input type="hidden" name="name1_<?php echo $j;?>" id="name1_<?php echo $j;?>" value="<?php echo $name[$i]; ?>" /></td>
										<td align="left" class="input-text"  valign="top"><a href="#1" onClick="Del_Upload(<?php echo $j;?>);"><i class="fa fa-times " style="color:red;"></i></a></td>
									</tr>
									<?php $j++;}?>
								</table>
                            </address>
                        </div><!-- /.col -->
						<?php }?>
                       
                        
                       
					</div>
                    
                    
                    
                    
                        
                   
                  
                    
				<div class="box-footer" align="right">
                            <input type="hidden" name="upstatus" id="upstatus" value="" />
							<input type="hidden" name="txtCRM1" id="txtCRM1" value="" />
							<input type="hidden" name="txtCRM2" id="txtCRM2" value="" />
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);" >Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
				</div>
         
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/chosen.css" rel="stylesheet" type="text/css" />
<script src="../../js/chosen.jquery.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$(".areasize").autosize({append: "\n"});

});


function getValidate(var1)
{
	$("#upstatus").val(var1); 
	var var4 = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRM1').val(var4);
	
	var var5 = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRM2').val(var5);
	
	var inp = document.getElementById('mul_file')
	if(var1 == 2 && var4 == '')
	{
		if(inp.files.length < 1)
		{
			jAlert('Please Attach Signed Permit Request', 'Alert');
			return false;
		}
	}
}


function validateFile()
{
        var sFileName = $("#mul_file");
		var inp = document.getElementById('mul_file');
		for (var i = 0; i < inp.files.length; ++i) 
		 {
	     var name = inp.files.item(i).name;
	     //alert("here is a file name: " + name);
			 if(name.length > 0) 
			   {
			   var blnValid = false;
			   var sCurExtension = "pdf";
			   if(name.substr(name.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase())
				 {
				   blnValid = true;
				 }
			   if(!blnValid) 
				{
				  jAlert("Sorry, Only PDFs Permitted");
				  $("#mul_file").val("");
				}
			}
		 }
}


function Del_Upload(var1)
{
	jConfirm('Are you sure you want to delete this upload permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file_'+var1).remove();
	}
	else{return false;}
	});
}

</script>
    </body>
</html>
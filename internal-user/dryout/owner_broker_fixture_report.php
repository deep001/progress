<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
if (isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$selBType = $_REQUEST['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	$selBType = '';
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(7,1); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-bar-chart-o"></i>&nbsp;Reports&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                         <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                         <li>Reports</li>
						 <li>Chartering</li>
						 <li class="active">Owner/Broker Fixture Report</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div class="box box-primary">
					<h3 style=" text-align:center;">Owner/Broker Wise Fixture Report</h3>
					<div class="row invoice-info" style="margin-left:10px;">
                        <div class="col-sm-3 invoice-col">
                           Label
                            <address>
								<select  name="selBOwner" class="select form-control" id="selBOwner" onchange="getOwnerData()" >
									<?php $obj->getLabelsForOwnerAndBroker();?>
								</select>
							</address>
						</div>
						<div class="col-sm-3 invoice-col">
                           Owner/Broker
                            <address>
								<select  name="selVendor" class="select form-control" id="selVendor" >
									<option value=''>---Select from list---</option>
								</select>
							</address>
						</div>
                        <div class="col-xs-3">
                             Business Type
                             <select name="selBType" id="selBType" class="form-control" >
                               <?php echo $obj->getBusinessTypeList1($selBType);?>
                              </select>                  
                        </div><!-- /.col -->
						<div class="col-sm-3 invoice-col">
							&nbsp;
                            <address>
								<button class="btn btn-info btn-flat" id="inner-login-button" type="button" onclick="getData()">Search</button>
							</address>
						</div>
					</div>
					<div align="right">
                         <a href="#" title="Pdf" onClick="getExcel();"><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i>Generate Excel</button></a>
						 <a href="#" title="Pdf" onClick="getPdf();"><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i>Generate PDF</button></a>

					</div>
					<div style="height:10px;"></div>
					<div id="DivOwner" class="box-body table-responsive" >
						<table id="Owner_broker_wise_fixture_report" class="table table-bordered table-striped dataTable" >
							<thead>
								<tr>
									<th align="left" valign="top">Sr. No</th>
									<th align="left" valign="top">Owner</th>
									<th align="left" valign="top">Vessel&nbsp;Type</th>
									<th align="left" valign="top">Nom ID</th>
									<th align="left" valign="top">Vessel Name</th>
									<th align="left" valign="top">CP Date</th>
									<th align="left" valign="top">Contract Qty</th>
									<th align="left" valign="top">Business Type</th>
									<th align="left" valign="top">Broker</th>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
						</table>
					</div>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$("#Owner_broker_wise_fixture_report").dataTable();
});

function getData()
{
	if($("#selVendor").val() != "" && $("#selBOwner").val() != "")
	{
		$('#DivOwner').html("");
		$('#DivOwner').html('<span style="text-align:center;display:block;"><img src="../../img/loading.gif" /></span>');
			$.post("options.php?id=18",{selVendor:""+$("#selVendor").val()+"",selBOwner:""+$("#selBOwner").val()+"",selBType:""+$("#selBType").val()+""}, function(html) {
				$('#DivOwner').html("");
				$('#DivOwner').html(html);
				$("#Owner_broker_wise_fixture_report").dataTable();
			});
	}else{
		
		jAlert('Please select both Label and Owner/Broker', 'Alert Dialog');
	}
}

function getOwnerData()
{
	if($("#selBOwner").val() != "")
	{
		var vendorlist 	= <?php echo $obj->getOwnerAndBrokerListJson(); ?>;
		var bowner 		= $('#selBOwner').val();  
		var sel1 		= $('#selVendor');	
		var options1 	= sel1.attr('option');
		$('option', sel1).remove();	
		if(bowner == 11){var label = "Owner";}else if(bowner == 12){var label = "Broker";}
		$("<option value = '' >---Select "+label+" from list---</option>").appendTo("#selVendor");
		$.each(vendorlist[bowner], function(index, array) {
			$("<option value = '"+array['key']+"' >"+array['value']+"</option>").appendTo("#selVendor");
		});
	}
	else
	{
		$("#selVendor").html("");
		$('#DivOwner').html("");
		$("#selVendor").html("<option value = '' >---Select from list---</option>");
	}
}

function getPdf()
{
	if($("#selBOwner").val() != "" && $("#selVendor").val() != "")
	{
		location.href='allPdf.php?id=32&selVendor='+$("#selVendor").val()+'&selBOwner='+$("#selBOwner").val()+'&selBType='+$("#selBType").val();
	}else{
		jAlert('Please select both Label and Owner/Broker ' , 'Alert Dialog')
	}
}

function getExcel()
{	
	if($("#selBOwner").val() != "" && $("#selVendor").val() != "")
	{
		location.href='allExcel.php?id=12&selVendor='+$("#selVendor").val()+'&selBOwner='+$("#selBOwner").val()+'&selBType='+$("#selBType").val();
	}
}
 
</script>
</body>
</html>
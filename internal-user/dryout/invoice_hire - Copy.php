<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$mappingid = $_REQUEST['id'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}

if (@$_REQUEST['action'] == 'submit')
{
	$msg = $obj->insertInvoiceHireDetails();
	$msg = explode(",",$msg);
	header('Location:./payment_grid.php?msg='.$msg[0].'&mappingid='.$msg[1].'&page='.$page);
}

$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&page=".$page;

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
.span span{
	font-weight:bold;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;">( 20 Jul 2015 4:40:44 PM )</div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&nbsp;In Ops at a glance&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="payment_grid.php?mappingid=<?php echo $mappingid;?>&page=<?php echo  $page;?>"><button class="btn btn-info btn-flat" type="button" >Back</button></a></div>
				<?php
				$sql = "SELECT * from invoice_hire_master where MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and STATUS=0 LIMIT 1";
				$res 	= mysql_query($sql);
				$rec 	= mysql_num_rows($res);
				$rows 	= mysql_fetch_assoc($res); 
				?>				
				<div style="height:10px;">&nbsp;</div>
					<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								Invoice
								</h2>                            
							</div><!-- /.col -->
						</div>
					
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Invoice Type
								<address>
									<select  name="selIType" class="select form-control" id="selIType" >
										<?php 
										$obj->getInvoiceType();
										?>
									</select>
									<script>$('#selIType').val('<?php echo $rows['ACC_TYPE'];?>');</script>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
							   Invoice No.
								<address>
									<input type="text" class="form-control" name="txtDNote" id="txtDNote" value="<?php echo $rows['PAYMENT_NO'];?>" />
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								CP Date
								<address>
								   <label><?php echo date("d-m-Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"CP_DATE"))); ?></label>
								   <input type="hidden" name="txtCP_Date" id="txtCP_Date" class="input-text" size="10" value="<?php echo date("d-m-Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"CP_DATE"))); ?>" autocomplete="off" readonly="true"/>
								</address>
							</div>
						</div>
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Charterer
								<address>
								   <select  name="selVendor" class="select form-control" id="selVendor" >
										<?php 
										$obj->getVendorListNewUpdate();
										?>
									</select>
									<script>$('#selVendor').val('<?php echo $rows['CHARTERER'];?>');</script>
								</address>
							</div><!-- /.col -->                     
							<div class="col-sm-4 invoice-col">
								 Invoice Date
								<address>
								   <?php if($rec == 0){?>
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo date("d-m-Y",time());?>"/>
									<?php }else{?>
									<input type="text" name="txtDate" id="txtDate" class="form-control" value="<?php echo date("d-m-Y",strtotime($rows['INVOICEDATE']));?>"/>
								<?php }?>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								 Fixture Ref.
								<address>
								   <label><?php echo $obj->getMappingData($mappingid,"NOM_NAME");?></label>
								</address>
							</div><!-- /.col -->
						</div>
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Vessel
								<address>
								   <label><?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?></label>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-8 invoice-col">
								Remarks
									<address>
										<textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" rows="3" placeholder="Remarks ..." ><?php echo $rows['REMARKS'];?></textarea>
									</address>
							</div>					
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Details</h3>
							</div>
							<table class="table table-striped">
								<tr>
									<th align="left" valign="middle" width="10%">#</th>
									<th align="left" valign="middle" width="30%">Details</th>
									<th align="left" valign="middle" width="20%">USD/Day</th>
									<th align="left" valign="middle" width="10%">Days</th>
									<th align="left" valign="middle" width="10%">Off Hire Days</th>
									<th align="left" valign="middle" width="20%">Amt</th>
								</tr>
								
								<tbody id="sortable" class="todo-list">
								<?php
								$sql1 = "select * from invoice_hire_master_1  where INVOICEID='".$rows['INVOICEID']."'";
								$res1 = mysql_query($sql1);
								$rec1 = mysql_num_rows($res1);
								$j = 0;
								if($rec1 == 0)
								{
								?>

								<tr id="row_1">
									<td align="left" valign="top" style="cursor:move;">
										<span class="handle">
											<i class="fa fa-ellipsis-v"></i><i class="fa fa-ellipsis-v"></i>
										</span> 
									</td>
									<td align="left" valign="middle"><textarea name="txtDetails_1" id="txtDetails_1" class="form-control" rows="1"></textarea></td>
									<td align="left" valign="middle"><input type="text" class="form-control" name="txtUSD_1" id="txtUSD_1" value="" autocomplete="off" onkeyup="getAmt(1)"/></td>
									<td align="left" valign="middle"><input type="text" class="form-control" name="txtDays_1" id="txtDays_1" value="" autocomplete="off" onkeyup="getAmt(1)"/></td>
									<td align="left" valign="middle"><input type="text" class="form-control" name="txtOHDays_1" id="txtOHDays_1" value="" autocomplete="off" onkeyup="getAmt(1)"/></td>
									<td align="left" valign="middle"><input type="text" class="form-control" name="txtAmt_1" id="txtAmt_1" value="" readonly="true"/><input type="hidden" class="input" name="txtROW_1" id="txtROW_1" value="1" size="5" /></td>
								</tr>
								<?php $j++;}else{ while($rows1 = mysql_fetch_assoc($res1)){$j++;?>
								<tr id="row_<?php echo $j;?>">
									<td align="left" valign="top" style="cursor:move;">
										<span class="handle">
											<i class="fa fa-ellipsis-v"></i><i class="fa fa-ellipsis-v"></i>
										</span> 
									</td>
									<td align="left" valign="middle"><textarea name="txtDetails_<?php echo $j;?>" id="txtDetails_<?php echo $j;?>" class="form-control" rows="1"><?php echo $rows1['DETAILS'];?></textarea></td>
									<td align="left" valign="middle"><input type="text" class="form-control" name="txtUSD_<?php echo $j;?>" id="txtUSD_<?php echo $j;?>" value="<?php echo $rows1['USD'];?>" autocomplete="off" onkeyup="getAmt(<?php echo $j;?>)"/></td>
									<td align="left" valign="middle"><input type="text" class="form-control" name="txtDays_<?php echo $j;?>" id="txtDays_<?php echo $j;?>" value="<?php echo $rows1['DAYS'];?>" autocomplete="off" onkeyup="getAmt(<?php echo $j;?>)"/></td>
									<td align="left" valign="middle"><input type="text" class="form-control" name="txtOHDays_<?php echo $j;?>" id="txtOHDays_<?php echo $j;?>" value="<?php echo $rows1['OFFHIRE'];?>" autocomplete="off" onkeyup="getAmt(<?php echo $j;?>)"/></td>
									<td align="left" valign="middle"><input type="text" class="form-control" name="txtAmt_<?php echo $j;?>" id="txtAmt_<?php echo $j;?>" value="<?php echo $rows1['AMT'];?>" readonly="true"/><input type="hidden" class="input" name="txtROW_<?php echo $j;?>" id="txtROW_<?php echo $j;?>" value="<?php echo $j;?>" size="5" /></td>
								</tr>

								<?php }}?>
								</tbody>
								
								<tr>
									<th align="left" valign="middle" width="90%" colspan="5">Total</th>
									<th align="left" valign="middle" width="10%"><input type="text" class="form-control" name="txtTtlAmt" id="txtTtlAmt" value="<?php echo $rows['TTL_AMT'];?>" readonly="true"/></th>
								</tr>
								<tr>
									<td colspan="7" align="left" valign="middle" ><button type="button" onClick="addUI_Row()"class="btn btn-primary btn-flat">Add</button><input type="hidden" class="input" name="txtTBID" id="txtTBID" value="<?php echo $j;?>" size="5" /></td>
								</tr>
							</table>
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Banking Details</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<td width="5%"></td>
											<td width="30%">Name Of Beneficiary</td>
											<td width="30%">
												<select  name="selNOB" class="select form-control" id="selNOB" onChange="getBankingDetailsData()">
													<?php 
													$obj->getBankingDetails();
													?>
												</select>
												<script>$('#selNOB').val('<?php echo $rows['NOB'];?>');</script>
											</td>
											<td width="35%"></td>
										</tr>
									</thead>
									<tbody id="row" style="display:none;">										
										<tr>
											<td></td>
											<td>Address</td>
											<td>
												<span id="span_1_0"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>Beneficiary A/C No.</td>
											<td>
												<span id="span_1_1"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>Beneficiary Bank</td>
											<td>
												<span id="span_1_2"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>Beneficiary Bank Address</td>
											<td>
												<span id="span_1_3"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>Beneficiary Bank Swift Code</td>
											<td>
												<span id="span_1_4"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>IBAN No.</td>
											<td>
												<span id="span_1_5"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>FED ABA</td>
											<td>
												<span id="span_1_6"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td colspan="4" style="background-color:grey; color:white;">CORRESPONDENT DETAILS</td>
										</tr>
										<tr>
											<td></td>
											<td>Correspondent Bank Name</td>
											<td>
												<span id="span_1_7"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>Swift Code</td>
											<td>
												<span id="span_1_8"></span>
											</td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td>Reference</td>
											<td>
												<span id="span_1_9"></span>
											</td>
											<td></td>
										</tr>
										
										<tr>
											<td align="right" colspan="4"><button type="button" class="btn btn-primary btn-flat" onClick="return getSubmit(0)">Submit</button>&nbsp;&nbsp;<button type="button" class="btn btn-primary btn-flat" onClick="return getSubmit(1)">Submit & Close</button><input type="hidden" name="action" id="action" value="submit" /><input type="hidden" name="txtCRM" id="txtCRM" value="" /><input type="hidden" name="txtStatus" id="txtStatus" class="input-text" size="10" value="0"/><input type="hidden" name="txtIID" id="txtIID" class="input-text" size="10" value="<?php echo $rows['INVOICEID'];?>"/>	</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">&nbsp;</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Invoice Type</th>
											<th>Hire No.</th>
											<th>Charterer</th>
											<th>Invoice Date</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
									<?php
									$sql = "SELECT * from invoice_hire_master where MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and STATUS='1'";
									$res = mysql_query($sql);
									$rec = mysql_num_rows($res);
									
									if($rec == 0)
									{
									?>
									<tr>
										<td colspan="7" align="center" valign="middle" style="color:red;letter-spacing:1px;">SORRY CURRENTLY THERE ARE ZERO(0) RECORDS</td>
									</tr>
									<?php }else{while($rows = mysql_fetch_assoc($res)){?>
									<tr>
										<td align="left" valign="middle"><?php echo $rows['ACC_TYPE'];?></td>
										<td align="left" valign="middle"><?php echo $rows['PAYMENT_NO'];?></td>
										<td align="left" valign="middle"><?php echo $obj->getVendorListNewBasedOnID($rows['CHARTERER']);?></td>
										<td align="left" valign="middle"><?php echo date("d-M-Y",strtotime($rows['INVOICEDATE']));?></td>
										<td align="left" valign="middle"><?php echo $rows['TTL_AMT'];?></td>
									</tr>
									<?php }}?>
									</tbody>
								</table>
							</div>
						</div>
					</form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="../../js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 

$("[id^=txtUSD_],[id^=txtDays_],[id^=txtOHDays_]").numeric();

$('[id^=txtDetails_], #txtRemarks').autosize({append: "\n"});

$('#txtDate').datepicker({
	format: 'dd-mm-yyyy'
});

getBankingDetailsData();
});

$(function() {
    "use strict";
    //jQuery UI sortable for the todo list
    $(".todo-list").sortable({
        placeholder: "sort-highlight",
        handle: ".handle",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
});

function getBankingDetailsData()
{
	var1 = $('#selNOB').val();
	$('#tbd_2').empty();
	$('#tbd_3').empty();
	
	if(var1 != '')
	{
		$('#row').show();
		var code = <?php echo $obj->getBankingDetailsDataWithJson(); ?>;
		$.each(code[var1], function(index, array) {
		for(i=0;i<10;i++)
		{ $('#span_1_'+i).text(array[i]);}
		});
		
		var code_1 = <?php echo $obj->getBankingDetailsDataWithJson_1(1); ?>;
		if(code_1 != null)
		{
			$.each(code_1[var1], function(index, array) {
			$('<tr height="5"><td colspan="10" align="left" valign="top" ></td></tr><tr><td align="left" valign="top">'+array['name']+'</td><td align="left" valign="top">:</td><td colspan="7" align="left" valign="top">'+array['value']+'</td></tr>').appendTo("#tbd_2");
			});
		}
		
		var code_2 = <?php echo $obj->getBankingDetailsDataWithJson_1(2); ?>;
		if(code_2 != null)
		{
			$.each(code_2[var1], function(index, array) {
			$('<tr height="5"><td colspan="10" align="left" valign="top" ></td></tr><tr><td align="left" valign="top">'+array['name']+'</td><td align="left" valign="top">:</td><td colspan="7" align="left" valign="top">'+array['value']+'</td></tr>').appendTo("#tbd_3");
			});
		}
	}
	else
	{
		$('#row').hide();
	}
}

function addUI_Row()
{
	var id = $("#txtTBID").val();
	if($('#txtDetails_'+id).val() != "" && $("#txtAmt_"+id).val() != "")
	{
		id  = (id - 1 )+ 2;
		$('<tr id="row_'+id+'"><td align="left" valign="top" style="cursor:move;"><span class="handle"><i class="fa fa-ellipsis-v"></i><i class="fa fa-ellipsis-v"></i></span></td><td align="left" valign="middle"><textarea name="txtDetails_'+id+'" id="txtDetails_'+id+'" class="form-control" rows="1"></textarea></td><td align="left" valign="middle"><input type="text" class="form-control" name="txtUSD_'+id+'" id="txtUSD_'+id+'" value="" autocomplete="off" onkeyup="getAmt('+id+')"/></td><td align="left" valign="middle"><input type="text" class="form-control" name="txtDays_'+id+'" id="txtDays_'+id+'" value="" autocomplete="off" onkeyup="getAmt('+id+')"/></td><td align="left" valign="middle"><input type="text" class="form-control" name="txtOHDays_'+id+'" id="txtOHDays_'+id+'" value="" autocomplete="off" onkeyup="getAmt('+id+')"/></td><td align="left" valign="middle"><input type="text" class="form-control" name="txtAmt_'+id+'" id="txtAmt_'+id+'" value="" readonly="true"/><input type="hidden" class="input" name="txtROW_'+id+'" id="txtROW_'+id+'" value="'+id+'" size="5" /></td></tr>').appendTo("#sortable");
		
		$("#txtTBID").val(id);
		
		$("[id^=txtUSD_],[id^=txtDays_],[id^=txtOHDays_]").numeric();

		$('[id^=txtDetails_]').autosize({append: "\n"});
	}
	else
	{
			jAlert('Please fill above entries.', 'Alert');
	}
}

function getAmt(var1)
{
	if($('#txtUSD_'+var1).val() != '' && $('#txtDays_'+var1).val() != '' && $('#txtOHDays_'+var1).val() != '')
	{
		var var2 = $('#txtUSD_'+var1).val();
		var var3 = $('#txtDays_'+var1).val();
		var var4 = $('#txtOHDays_'+var1).val();
		
		var5 = parseFloat(var3)-parseFloat(var4);
		
		var6 = (parseFloat(var2*var5)).toFixed(2);
		$('#txtAmt_'+var1).val(var6)
	}
	else
	{
		$('#txtAmt_'+var1).val('')
	}
	
	var sum = parseFloat($("[id^=txtAmt_]").sum());
	$("#txtTtlAmt").val(sum.toFixed(2));
}

function getSubmit(var1)
{
	var var3 = $("[id^=txtROW_]").map(function () {return this.value;}).get().join(",");
	$('#txtCRM').val(var3);
	
	if($('#selIType').val() != '' && $('#txtDate').val() != '' && $('#selVendor').val() != '' && $('#txtDNote').val() != '')
	{
		if($('#txtDetails_1').val() != '' && $('#txtAmt_').val() != '')
		{
			$('#txtStatus').val(var1);
			document.frm1.submit();
		}
		else
		{
			jAlert('Please fill atleast one entry at details section.', 'Alert');
			return false;
		}
	}
	else
	{
		jAlert('Please select Invoice Type & Invoice No. & Charterer & Invoice Date.', 'Alert');
		return false;
	}
}

</script>
    </body>
</html>
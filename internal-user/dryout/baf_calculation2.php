<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$mappingid  = $_REQUEST['mappingid'];
$page  		= $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}

if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->updateBAFDetails();
	header('Location:./baf_calculation.php?msg='.$msg.'&mappingid='.$mappingid.'&page='.$page);
 }
$obj->viewBAFCalculationRecords($mappingid);
$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&page=".$page;

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header" align="center"><?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME")." / ".date("d-m-Y",strtotime($obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'COA_DATE')));?>  BAF CALCULATION</h2>
                        </div><!-- /.col -->
                </div>
                <?php 
				if($obj->getCOANumberDataBasedOnID($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),'COA_NO'),'BAF') == 0)
				{
					echo '<div class="box">				
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                        	<thead>
                            	<tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>                                  
                                </tr>
                            </thead>
                            <tbody>';
					
					 	echo '<tr>
								<td align="center" colspan="4" valign="middle" style="color:#ff0000;letter-spacing:1px;">'.strtoupper("Please fulfill BAF details in COA.").'</td>
								</tr>';
						echo '</tbody>
						</table>
					</div>
				</div>';
				}
				else
				{
				?>
                
                <div class="box">				
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                        	<thead>
                            	<tr>
                                	<th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>                                    
                                </tr>
                            </thead>
                            <tbody>
                            	<tr>
                                	<td>Bunker Price IFO (USD/MT)</td>
                                    <td><input type="text" name="txtBPrice" id="txtBPrice" class="form-control" readonly value="<?php echo $obj->getFun2();?>" /></td>
                                    <td>BAF Terms (Days)</td>
                                    <td><input type="text" name="txtBAFTerms" id="txtBAFTerms" class="form-control" readonly value="<?php echo $obj->getFun3();?>" /></td>
                                    <td>BAF (USD/MT)</td>
                                    <td><input type="text" name="txtIncDec" id="txtIncDec" class="form-control" readonly value="<?php echo $obj->getFun4();?>" /></td>
                                </tr>
                            </tbody>
                         </table>
                    </div>
                </div>
                
                <div class="box">			
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Port</th>
                                    <th width="20%">BL Quantity</th>
                                    <th width="20%">BL Date</th>
                                    <th width="30%">Remarks</th>                                  
                                </tr>
                            </thead>
                           <tbody id="tblBLData">
							<?php 
								$sql = "select * from baf_slave1 where BAFID = ".$obj->getFun1();
								$res = mysql_query($sql);
								$rec = mysql_num_rows($res);
								$i=1;
								while($rows = mysql_fetch_assoc($res))
								{
								?>
								<tr id="blrow_<?php echo $i;?>">
								<td><a href="#<?php echo $i;?>" title="Remove Entry" onclick="getBLDelete(<?php echo $i;?>);" ><img src="../../img/icon_delete.gif"  height="16" width="16" /></a></td>
								<td><select  name="selPort_<?php echo $i;?>" class="form-control" id="selPort_<?php echo $i;?>" style="width:200px;">
									<?php $obj->getPortListNewForUpdate($rows['PORT_CODE']);?>
								</select></td>
								<td><input type="text" name="txtBLQty_<?php echo $i;?>" id="txtBLQty_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows['BL_QTY'];?>" onkeyup="getTotal();" /></td>
								<td><input type="text" name="txtBLDate_<?php echo $i;?>" id="txtBLDate_<?php echo $i;?>" class="form-control" readonly="true" value="<?php echo date("d-M-Y",strtotime($rows['BL_DATE']));?>" /></td>
								<td><textarea class="form-control" name="txtRemarks_<?php echo $i;?>" id="txtRemarks_<?php echo $i;?>" cols="33" rows="1"><?php echo $rows['REMARKS'];?></textarea></td>		
								</tr>
								<?php $i++;}?>
                            </tbody>
                            <tfoot>
                                <tr>
                                <th colspan="2" ><a href="#A" title="Add Row" onclick="getAddBLRow();" ><img src="../../img/icon_add.gif" height="16" width="16"/></a><input type="hidden" name="txtBLID" id="txtBLID" class="form-control" readonly="true" value="<?php echo $rec;?>" /></th>
                                <th><input type="text" name="txtTotalBLQty" id="txtTotalBLQty" class="form-control" readonly="true" value="<?php echo $obj->getFun5();?>" /></th>
                                <th align="left" colspan="2" ></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                 </div>
                  <div class="box">			
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Date</th>
                                    <th width="20%">Day</th>
                                    <th width="20%">Rate IFO</th>
                                    <th width="30%">Remarks</th>                                  
                                </tr>
                            </thead>
                          	<tbody id="tblDateData">
                            	<?php 
								$sql1 = "select * from baf_slave2 where BAFID = ".$obj->getFun1();
								$res1 = mysql_query($sql1);
								$rec1 = mysql_num_rows($res1);
								$i=1;
								while($rows1 = mysql_fetch_assoc($res1))
								{
								?>
                                <tr id="daterow_<?php echo $i;?>">
                                <td></td>
                                <td><input type="text" name="txtDate_<?php echo $i;?>" id="txtDate_<?php echo $i;?>" class="form-control" readonly value="<?php echo date("d-m-Y",strtotime($rows1['DATE']));?>" placeholder="dd-mm-yyyy" /></td>
                                <td><input type="text" name="txtDay_<?php echo $i;?>" id="txtDay_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows1['DAY'];?>" /></td>
                                <td><input type="text" name="txtRateIFO_<?php echo $i;?>" id="txtRateIFO_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows1['RATE_IFO'];?>" /></td>
                                <td><textarea class="form-control" readonly name="txtDRemarks_<?php echo $i;?>" id="txtDRemarks_<?php echo $i;?>" placeholder="Remarks.." cols="33" rows="1"><?php echo $rows1['REMARKS'];?></textarea></td>		
                                </tr>
                             <?php $i++; } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                            <th align="left" colspan="5" ></th>
                            </tr>
                            </tfoot>
                       </table>
                    </div>
                </div>
                
                <div class="box-footer" align="right">
                    <button type="button" class="btn btn-primary btn-flat" onclick="return getValidate();">Submit</button>
                    <input type="hidden" name="action" value="submit" />
                    <input type="hidden" name="upstatus" id="upstatus" value="" />
                </div>	
				</form>
				<?php } ?>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<link href="../../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
$('[id^=txtRemarks_],[id^=txtDRemarks_]').autosize({append: "\n"});
$("[id^=txtBLDate_]").datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd-M-yy'
});
$("[id^=ui-datepicker-div]").hide();
$("[id^=txtBLQty_],[id^=txtRateIFO_]").numeric();

$("[id^=txtDate_]").datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd-M-yy',
	onClose: function( selectedDate ) {
				var rowid = this.id;
				var seldate = $(this).datepicker('getDate');
				seldate = seldate.toDateString();
				seldate = seldate.split(' ');
				var weekday=new Array();
					weekday['Mon']="Monday";
					weekday['Tue']="Tuesday";
					weekday['Wed']="Wednesday";
					weekday['Thu']="Thursday";
					weekday['Fri']="Friday";
					weekday['Sat']="Saturday";
					weekday['Sun']="Sunday";
				var dayOfWeek = weekday[seldate[0]];
				$('#txtDay_'+rowid.split('_')[1]).val(dayOfWeek);
			}
});

}); 


function getAddBLRow()
{
	var id = $("#txtBLID").val();
	if($("#selPort_"+id).val() != "" && $("#txtBLQty_"+id).val() != "" && $("#txtBLDate_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="blrow_'+id+'"><td width="2%" align="left" class="text" ><a href="#'+id+'" title="Remove Entry" onclick="getBLDelete('+id+');" ><img src="../../images/icon_delete.gif"  height="16" width="16" /></a></td><td width="25%" align="left" class="text" ><select  name="selPort_'+id+'" class="select input-text" id="selPort_'+id+'" style="width:200px;"><?php $obj->getPortListNew();?></select></td><td width="20%" align="left" class="text" ><input type="text" name="txtBLQty_'+id+'" id="txtBLQty_'+id+'" class="input-text" size="15" autocomplete="off" value="" onkeyup="getTotal();" /></td><td width="20%" align="left" class="text" ><input type="text" name="txtBLDate_'+id+'" id="txtBLDate_'+id+'" class="input-text" size="15" readonly="true" value="" /></td><td width="35%" align="left" class="text" ><textarea class="input-text animated" name="txtRemarks_'+id+'" id="txtRemarks_'+id+'" cols="33" rows="1"></textarea></td></tr>').appendTo("#tblBLData");		
		
		$("[id^=txtBLDate_]").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-M-yy'
		});
		$('[id^=txtRemarks_]').autosize({append: "\n"});
		$("[id^=txtBLQty_]").numeric();
		$("[id^=ui-datepicker-div]").hide();
		$("#txtBLID").val(id);
	}
	else
	{
		jAlert('Please fill details.', 'Alert');
	}
}

function getBLDelete(rowid)
{
	var len = $(":select[id^=selPort_]").length;
	if(len == 1)
	{
		jAlert('Can not remove all entries.', 'Alert');
		return false;
	}
	else
	{
	jConfirm('Are you sure to remove this entry ?', 'Confirmation', function(r) {
		if(r){	
			$("#blrow_"+rowid).remove();
			$("#txtTotalBLQty").val($("[id^=txtBLQty_]").sum().toFixed(2));
		}
		else{return false;}
		});	
	}
}	

function getDateDelete(rowid)
{
	var len = $(":input[id^=txtDate_]").length;
	if(len == 1)
	{
		jAlert('Can not remove all entries.', 'Alert');
		return false;
	}
	else
	{
	jConfirm('Are you sure to remove this entry ?', 'Confirmation', function(r) {
		if(r){	
			$("#daterow_"+rowid).remove();
		}
		else{return false;}
		});	
	}
}	

function getAddDateRow()
{
	var id = $("#txtDateID").val();
	if($("#txtDate_"+id).val() != "" && $("#txtDay_"+id).val() != "" && $("#txtRateIFO_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="daterow_'+id+'"><td width="2%" align="left" class="text" ><a href="#1" title="Remove Entry" onclick="getDateDelete('+id+');" ><img src="../../images/icon_delete.gif"  height="16" width="16" /></a></td><td width="25%" align="left" class="text" ><input type="text" name="txtDate_'+id+'" id="txtDate_'+id+'" class="input-text" size="15" readonly="true" value="" /></td><td width="20%" align="left" class="text" ><input type="text" name="txtDay_'+id+'" id="txtDay_'+id+'" class="input-text" size="15" readonly="true" value="" /></td><td width="20%" align="left" class="text" ><input type="text" name="txtRateIFO_'+id+'" id="txtRateIFO_'+id+'" class="input-text" size="15" autocomplete="off" value="" /></td><td width="35%" align="left" class="text" ><textarea class="input-text animated" name="txtDRemarks_'+id+'" id="txtDRemarks_'+id+'" cols="33" rows="1"></textarea></td>	</tr>').appendTo("#tblDateData");		
		
		$("[id^=txtDate_]").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'dd-M-yy',
			onClose: function( selectedDate ) {
						var rowid = this.id;
						var seldate = $(this).datepicker('getDate');
						seldate = seldate.toDateString();
						seldate = seldate.split(' ');
						var weekday=new Array();
							weekday['Mon']="Monday";
							weekday['Tue']="Tuesday";
							weekday['Wed']="Wednesday";
							weekday['Thu']="Thursday";
							weekday['Fri']="Friday";
							weekday['Sat']="Saturday";
							weekday['Sun']="Sunday";
						var dayOfWeek = weekday[seldate[0]];
						$('#txtDay_'+rowid.split('_')[1]).val(dayOfWeek);
					}
		});

		$('[id^=txtDRemarks_]').autosize({append: "\n"});
		$("[id^=txtRateIFO_]").numeric();
		$("[id^=ui-datepicker-div]").hide();
		$("#txtDateID").val(id);
	}
	else
	{
		jAlert('Please fill details.', 'Alert');
	}
}

function getTotal()
{
	$("#txtTotalBLQty").val($("[id^=txtBLQty_]").sum().toFixed(2));
}

function getValidate()
{
	var arr = new Array();
	var i=0;
	$('[id^=selPort_]').each(function(index) {
		var j = i+1;
		if($("#selPort_"+j).val() == "" || $("#txtBLQty_"+j).val() == "" || $("#txtBLDate_"+j).val() == "")
				{
					arr[i] = j;					
				}
			i++;
		});
	if(arr.length == 0)
	{
		var arr1 = new Array();
		var i=0;
		$('[id^=txtDate_]').each(function(index) {
			var j = i+1;
			if($("#txtDate_"+j).val() == "" || $("#txtDay_"+j).val() == "" || $("#txtRateIFO_"+j).val() == "")
					{
						arr1[i] = j;					
					}
				i++;
			});
		if(arr1.length == 0)
		{
			jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
			if(r){ 
				document.frm1.submit();
			}
			else{return false;}
			});	
		}
		else 
		{
			jAlert('Please fill all values', 'Alert');
			return false;
		}
	}
	else 
	{
		jAlert('Please fill all values', 'Alert');
		return false;
	}
}

</script>
    </body>
</html>
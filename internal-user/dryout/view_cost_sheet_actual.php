<?php
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$mappingid 		= $_REQUEST['mappingid'];
$cost_sheet_id 	= $_REQUEST['cost_sheet_id'];
$page  			= $_REQUEST['page'];
$obj->viewFreightEstimationRecords($mappingid,$cost_sheet_id);
$pagename 		= basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&selFType=".$obj->getFun2().'&cost_sheet_id='.$cost_sheet_id;
$rdoMMarket 	= $obj->getFun8();
$rdoDWT 		= $obj->getFun7();
$rdoQty 	= $obj->getFun28();
$rdoCap     = $obj->getFun29();


?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}
form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}

</style>
</head>
    <body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onClick="getFinalCalculation();">Calculate</a>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp; In Ops at a glance&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Cost Sheet</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
					<!--   content put here..................-->
					<div align="right"><a href="view_cost_sheet_list.php?mappingid=<?php echo $mappingid;?>&page=<?php echo $page;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
					<div style="height:10px;">&nbsp;</div>
						
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            Voyage Financials
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
				
                    <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Fixture Type
								<address>
								<strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFreightFixtureOutBasedOnID(2);?></strong>
								</address>
							</div><!-- /.col -->
						</div>
                            
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								 Main Particulars
								</h2>                            
							</div><!-- /.col -->
						</div>
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Nom ID
								<address>
									<strong><?php echo $obj->getMappingData($mappingid,"NOM_NAME");?></strong>
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								 Vessel Name
								<address>
								   <strong><?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?></strong>
								</address>
							</div><!-- /.col -->
							 <div class="col-sm-4 invoice-col">
								 Vessel Type
								<address>
									<strong><?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_TYPE"));?></strong>
								</address>
							</div><!-- /.col -->
						</div>
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
							   Date
								<address>
									<strong><?php echo date("d-m-Y",strtotime($obj->getFun3()));?></strong>									
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Voyage No.
								<address>
									<strong><?php echo $obj->getFun6()?></strong>									
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								 Voyage Financials Name
								<address>
									<strong><?php echo $obj->getCostSheetNameBasedOnID($cost_sheet_id);?></strong>
								</address>
							</div><!-- /.col -->
							
						 </div>
                            
                            
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoDWT" class="checkbox" id="rdoDWT1" type="radio" value="1"  <?php if($rdoDWT == 1) echo "checked"; ?> />
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
								<input name="rdoDWT" class="checkbox" id="rdoDWT2" type="radio" value="2"  <?php if($rdoDWT == 2) echo "checked"; ?> />
								</address>
							</div><!-- /.col -->
						   
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>
								<address>
									<strong><?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUMMER_1");?></strong>
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>
								<address>
									<strong><?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"TROPICAL_1");?></strong>
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">&nbsp;</div><!-- /.col -->
							
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoCap" id="rdoCap1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoCap == 1) echo "checked"; ?> />
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								&nbsp;
								<address>
									<input name="rdoCap" id="rdoCap2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoCap == 2) echo "checked"; ?> />
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
								<address>
									<strong><?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"GRAIN");?></strong>
								</address>
							</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Bale Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
								<address>
									<strong><?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BALE");?></strong>
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-8 invoice-col">&nbsp;</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								SF <span style="font-size:10px; font-style:italic;">(CBM/MT)</span>
								<address>
									<strong><?php echo $obj->getFun30();?></strong>
								</address>
							</div><!-- /.col -->
						</div>
						
						<div class="row invoice-info">
							<div class="col-sm-8 invoice-col">&nbsp;</div><!-- /.col -->
							
							<div class="col-sm-4 invoice-col">
								Loadable <span style="font-size:10px; font-style:italic;">(MT)</span>
								<address>
									<strong><?php echo $obj->getFun31();?></strong>
								</address>
							</div><!-- /.col -->
						</div>
                             
						<div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								 Market
								</h2>                            
							</div><!-- /.col -->
						</div>
						 
						<div class="row invoice-info">
							<div class="col-sm-4 invoice-col">
								<address>
									<input name="rdoMMarket" class="checkbox" id="rdoMMarket1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoMMarket == 1) echo "checked"; ?>  />
								</address>
							</div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								<address>
									<input name="rdoMMarket" class="checkbox" id="rdoMMarket2" type="radio" style="cursor:pointer;" value="2" <?php if($rdoMMarket == 2) echo "checked"; ?> />									
								</address>
							</div><!-- /.col -->
						</div>
                        
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
								Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
								<address>
									<strong><?php echo $obj->getFun9();?></strong>
								</address>
                            </div><!-- /.col -->
							<div class="col-sm-4 invoice-col">
								Lumpsum <span style="font-size:10px; font-style:italic;">(USD)</span>
								<address>
									<strong><?php echo $obj->getFun10();?></strong>
								</address>
						   </div><!-- /.col -->
                               <div class="col-sm-4 invoice-col">
									Addnl Cargo Rate <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
										<strong><?php echo $obj->getFun26();?></strong>
                                    </address>
                               </div><!-- /.col -->
                         </div>
                        
                        <div class="row">
							<div class="col-xs-12">
								<h2 class="page-header">
								 Cargo
								</h2>                            
							</div><!-- /.col -->
						</div>
                         <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Quantity <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
										<strong><?php echo $obj->getFun15();?></strong>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                   Cargo Type
                                    <address>
										<strong><?php echo $obj->getCargoTypeNameBasedOnID($obj->getFun16());?></strong>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Cargo Name
                                    <address>
                                        <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCargoContarctForMapping($obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"CARGO_POSITION"));?></strong>
                                    </address>
                                </div><!-- /.col -->
                              </div>
							
							<div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  <span style="font-size:13px; font-style:italic; color:#dc631e">( Please put dead freight quantity / addnl quantity separately )</span>  
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <address>
										<input name="rdoQty" class="checkbox" id="rdoQty1" type="radio" value="1"  <?php if($rdoQty == 1) echo "checked"; ?> />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <address>
										<input name="rdoQty" class="checkbox" id="rdoQty2" type="radio" value="2" <?php if($rdoQty == 2) echo "checked"; ?> />
                                    </address>
                                </div><!-- /.col -->
                             </div>
							 
							 <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">&nbsp;</div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    DF Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
										<strong><?php echo $obj->getFun27();?></strong>
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                    Addnl Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                        <strong><?php echo $obj->getFun18();?></strong>
                                    </address>
                                </div><!-- /.col -->
                            </div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
									Sea Passage
									</h2>                            
								</div><!-- /.col -->
							</div>
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<?php 
											$sql = "select * from fca_tci_sea_passage where FCAID='".$obj->getFun1()."'";
											$res = mysql_query($sql);
											$rec = mysql_num_rows($res);
											$i = 1;
											?>
											<tr>
												<th>#</th>
												<th>From Port</th>
												<th>To Port</th>
												<th>Passage Type</th>
												<th>Distance</th>
												<th>Speed Adj.</th>
												<th>Margin<span style="font-size:10px; font-style:italic;">(days)</span></th>
												<input type="hidden" name="p_rotationID" id="p_rotationID" class="form-control" value="<?php echo $rec; ?>" /></th>
												<th>Estimated <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<th>Actual <span style="font-size:10px; font-style:italic;">(days)</span></th>
											</tr>
										</thead>
										<tbody id="tblPortRotation">
											<?php if($rec == 0){?>
											<tr id="PRrow_Empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
											<?php }else{ 
												$i=1;
												while($rows = mysql_fetch_assoc($res))
												{
												?>
												<tr id="pr_Row_<?php echo $i;?>">
												<td><?php echo $i.".";?></td>
												<td><?php echo $obj->getPortNameBasedOnID($rows['FROM_PORTID']);?></td>
												<td><?php echo $obj->getPortNameBasedOnID($rows['TO_PORTID']);?></td>
												<td><?php echo $obj->getPassageTypeNameBasedOnID($rows['PASSAGETYPEID'])." (".$obj->getPassageSpeedBasedOnID($rows['SPEEDID'])." )";?></td>
												<td><?php echo $rows['DISTANCE']." (".$obj->getDistanceTypeBasedOnID($rows['DISTANCE_TYPEID'])." )";?></td>
												<td><?php echo $rows['WEATHER'];?></td>
												<td><?php echo $rows['MARGIN'];?></td>
												<td><?php echo $obj->getEstimatedDays($mappingid,$rows['MARGIN'],$rows['DISTANCE'],$rows['SPEEDID'],$rows['PASSAGETYPEID'],$rows['WEATHER']);?></td>
												<td><span id="adaysLabel_<?php echo $i;?>"><?php if($actual_sp[$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_sp[$i]['ACTUAL_DAYS'];$ttl_actual += $actual_sp[$i]['ACTUAL_DAYS'];}?></span></td>
												</tr>
												<?php $i++;} }?>											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
									Load Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<?php
												$sql1 = "select * from fca_tci_load_port where FCAID='".$obj->getFun1()."'";
												$res1 = mysql_query($sql1,$connect);
												$rec1 = mysql_num_rows($res1);
											?>
											<tr>
												<th>#</th>
												<th>Load Port</th>
												<th>Cargo Name</th>
												<th>Qty MT</th>
												<th>Rate<span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
												<th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
												<th>Idle Days</th>
												<th>Work Days</th>
												<th>Estimated <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<th>Actual <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<input type="hidden" name="load_portID" id="load_portID" class="input" size="5" value="<?php echo $rec1;?>"/>
											</tr>
										</thead>
										<tbody id="tblLoadPort">
											<?php
											if($rec1 == 0){
											?>
											<tr id="LProw_Empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
											<?php 
											}else{
												$i=1;
												while($rows1 = mysql_fetch_assoc($res1))
												{?>
												<tr id="lp_Row_<?php echo $i;?>">
													<td><?php echo $i.".";?></td>
													<td><?php echo $obj->getPortNameBasedOnID($rows1['LOADPORTID']);?></td>
													<td><?php echo $obj->getCargoContarctForMapping($rows1['PURCHASE_ALLOCATIONID'],$obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"SHIPPING_STAGE"));?></td>
													<td><?php echo $rows1['QTY_MT'];?></td>
													<td><?php echo $rows1['RATE'];?></td>
													<td><?php echo $rows1['PORT_COST'];?></td>
													<td><?php echo $rows1['IDLE_DAYS'];?></td>
													<td><?php echo $rows1['WORK_DAYS'];?></td>
													<td><?php echo $rows1['IDLE_DAYS'] + $rows1['WORK_DAYS'];?></td>
													<td><?php if($actual_lp[$rows1['LOADPORTID']][$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_lp[$rows1['LOADPORTID']][$i]['ACTUAL_DAYS'];}?></td>
												</tr>
											<?php $i++;} }?>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Discharge Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<?php 
												$sql2 = "select * from fca_tci_disch_port where FCAID='".$obj->getFun1()."'";
												$res2 = mysql_query($sql2);
												$rec2 = mysql_num_rows($res2);
											?>
											<tr>
												<th>#</th>
												<th>Discharge Port</th>
												<th>Cargo Name</th>
												<th>Qty MT</th>
												<th>Rate<span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
												<th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
												<th>Idle Days</th>
												<th>Work Days</th>
												<th>Estimated <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<th>Actual <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<input type="hidden" name="dis_portID" id="dis_portID" class="form-control" value="<?php echo $rec2; ?>" />
											</tr>
										</thead>
										<tbody id="tblDisPort">
											<?php if($rec2 == 0){?>
											<tr id="DProw_Empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
											<?php 
												}else{
												$i=1;
												while($rows2 = mysql_fetch_assoc($res2))
												{
											?>
											<tr id="dp_Row_<?php echo $i;?>">
												<td><?php echo $i.".";?></td>
												<td><?php echo $obj->getPortNameBasedOnID($rows2['DIS_PORT_ID']);?></td>
												<td><?php echo $obj->getCargoContarctForMapping($rows2['PURCHASE_ALLOCID'],$obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"SHIPPING_STAGE"));?></td>
												<td><?php echo $rows2['QTY_MT'];?></td>
												<td><?php echo $rows2['RATE'];?></td>
												<td><?php echo $rows2['PORT_COST'];?></td>
												<td><?php echo $rows2['IDLE_DAYS'];?></td>
												<td><?php echo $rows2['WORK_DAYS'];?></td>
												<td><?php echo $rows2['IDLE_DAYS'] + $rows2['WORK_DAYS'];?></td>
												<td><?php if($actual_dp[$rows2['DIS_PORT_ID']][$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_dp[$rows2['DIS_PORT_ID']][$i]['ACTUAL_DAYS'];}?></td>
											</tr>
										<?php $i++;} }?>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Transit Port(s)
									</h2>                            
								</div><!-- /.col -->
							</div>
							<div class="box">
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<?php 
												$sql3 = "select * from fca_tci_transit_port where FCAID='".$obj->getFun1()."'";
												$res3 = mysql_query($sql3);
												$rec3 = mysql_num_rows($res3);
											?>
											<tr>
												<th>#</th>
												<th>Transit Port</th>
												<th>Port Costs<span style="font-size:10px; font-style:italic;">(USD)</span></th>
												<th>Idle Days</th>
												<th>Estimated <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<th>Actual <span style="font-size:10px; font-style:italic;">(days)</span></th>
												<input type="hidden" name="transit_portID" id="transit_portID" class="form-control" value="<?php echo $rec3; ?>" />
											</tr>
										</thead>
										
										<tbody id="tblTransitPort">
											<?php if($rec3 == 0){?>
											<tr id="TProw_Empty">
												<td valign="top" align="center" colspan="8" style="color:red;">Sorry , currently zero(0) records added.</td>
											</tr>
											<?php }else{
												$i=1;
												while($rows3 = mysql_fetch_assoc($res3))
												{
											?>
											<tr id="tp_Row_<?php echo $i;?>">
												<td><?php echo $i.".";?></td>
												<td><?php echo $obj->getPortNameBasedOnID($rows3['LOAD_PORTID']);?></td>
												<td><?php echo $rows3['PORT_COST'];?></td>
												<td><?php echo $rows3['IDLE_DAYS'];?></td>
												<td><?php echo $rows3['IDLE_DAYS'];?></td>
												<td><?php if($actual_tp[$rows3['LOAD_PORTID']][$i]['ACTUAL_DAYS'] == ""){echo "0";}else{echo $actual_tp[$rows3['LOAD_PORTID']][$i]['ACTUAL_DAYS'];}?></td>
											</tr>
											<?php $i++;} }?>
										</tbody>
									</table>
								</div>
							</div>
							
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">
										Totals
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Laden Dist
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"LADEN_DIST");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ballast Dist
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BALLAST_DIST");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Dist
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TOTAL_DIST");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Laden Days
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"LADEN_DAYS");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ballast Days
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BALLAST_DAYS");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Sea Days
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SEA_DAYS");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl Port Idle Days
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_IDLE_DAYS");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl Port Work Days
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_WORK_DAYS");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Total Days
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DAYS");?></strong>
                                    </address>
                                </div><!-- /.col -->
							</div>
							<div class="row invoice-info">	
								<div class="col-sm-4 invoice-col">
                                  Ttl FO Consp MT
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_FO_CONSP_MT");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl DO Consp MT
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DO_CONSP_MT");?></strong>
                                    </address>
                                </div><!-- /.col -->
							</div>
							<div class="row invoice-info">	
								<div class="col-sm-4 invoice-col">
                                  Ttl FO Consp Off Hire
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_FO_CONSP_OFFHIRE");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
								<div class="col-sm-4 invoice-col">
                                  Ttl DO Consp Off Hire
                                    <address>
										<strong><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DO_CONSP_OFFHIRE");?></strong>
                                    </address>
                                </div><!-- /.col -->
								
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Freight Adjustment</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="25%">&nbsp;&nbsp;</th>
												<th width="25%">Percent</th>
												<th width="25%">USD</th>
												<th width="25%">Per MT</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Gross Freight</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"GROSS_FREIGHT_USD");?></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"GROSS_FREIGHT_PERMT");?></td>
											</tr>
											
											<tr>
												<td>Dead Freight</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"DEAD_FREIGHT_USD");?></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"DEAD_FREIGHT_PERMT");?></td>
											</tr>
											
											<tr>
												<td>Addnl Freight</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"ADDNL_FREIGHT_USD");?></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"ADDNL_FREIGHT_PERMT");?></td>
											</tr>
											
											<tr>
												<td>Total Freight</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"TTL_FREIGHT_USD");?></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"TTL_FREIGHT_PERMT");?></td>
											</tr>
											
											<tr>
												<td>Address Commission</td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AC_PERCENT");?></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AC_USD");?></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AC_PERMT");?></td>
											</tr>
											
											<tr>
												<td>Brokerage</td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AGC_PERCENT");?></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"AGC_USD");?></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BALANCE_FREIGHT_USD");?></td>
											</tr>
											
											<tr>
												<td>Net Freight</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalTCIRecords($obj->getFun1(),"NETT_FRT_PAYABLE");?></td>
												<td></td>
											</tr>
											
										</tbody>
									</table>
									
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Freight Details</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="25%;"></th>
												<th width="25%;"></th>
												<th width="25%;"></th>
												<th width="25%;"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Final Nett Freight</td>
												<td><?php if($obj->getFreightEstimationTotalRecords($obj->getFun1(),"FGF_VENDORID") != " "){echo $obj->getVendorListNewBasedOnID($obj->getFreightEstimationTotalRecords($obj->getFun1(),"FGF_VENDORID"));}?></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"FINAL_NETT_FREIGHT_USD");?></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"FNF_MT");?></td>
											</tr>											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Bunker Adjustment</h3>
									<a href="javascript:void(0)" onClick="getShowHide();" id="BID" name="BID"><img src="../../img/close.png" title="Show Hide" /></a>
									<input type="hidden" id="txtbid" value="0" />
								</div>
								<div class="box-body no-padding table-responsive" style="overflow:auto;" id="bunker_adj">
									<table class="table table-striped">
										<thead>
											<tr class="GridviewScrollHeader">
												<th colspan="1">Select Bunker grade</th>
												<?php
													
													$sql = "SELECT * FROM bunker_grade_master where MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and STATUS=1";
													$res = mysql_query($sql);
													$rec = mysql_num_rows($res);
													
													if($rec == 0)
													{
												?>
												<tr>
													<td valign="top" align="center" colspan="6" style="color:red;">First fill the Bunker Grade Master Data.</td>
												</tr>
												<?php	
												}else{
												?>
												<?php $m=0; while($rows = mysql_fetch_assoc($res)){
													$ttl_bg[] = $rows['NAME'];
													$ttl_bg1[] = $rows['BUNKERGRADEID'];
												?>
													 <th colspan="3" style="text-align:center;"><?php echo $rows['NAME'];?>&nbsp;&nbsp;</th>
												<?php $m++; ?>
											<?php } ?>
											</tr>
										</thead>
										<tbody>
											<tr class="GridviewScrollHeader">
												<td>&nbsp;</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){?>
												<td align="center">MT</td>
												<td align="center">Price</td>
												<td align="center">Cost</td>
												<?php }?>
											</tr>
		
											<tr class="GridviewScrollItem">
												<td>Estimated</td>
												<?php $j=$k=0; for($i=0;$i<count($ttl_bg);$i++){			
												if($obj->getBunkerRec($obj->getFun1(),$mappingid,$ttl_bg1[$i]) > 0)
												{
												?>
												<td>
												<input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_1" id="txt<?php echo $ttl_bg[$i];?>_1_1" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_MT");?>" class="form-control" autocomplete="off" disabled style="width:150px;" readonly="true" />
												</td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_2" id="txt<?php echo $ttl_bg[$i];?>_1_2" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',1,this.value,<?php echo $ttl_bg1[$i];?>);" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;" readonly="true" /></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_3" id="txt<?php echo $ttl_bg[$i];?>_1_3" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"EST_COST");?>" class="form-control" autocomplete="off" disabled style="width:150px;" readonly="true"/></td>
												 <?php }else{?>
												 <td>
												<input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_1" id="txt<?php echo $ttl_bg[$i];?>_1_1" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',1,this.value);" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/>
												</td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_2" id="txt<?php echo $ttl_bg[$i];?>_1_2" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',1,this.value,<?php echo $ttl_bg1[$i];?>);"class="form-control" autocomplete="off" style="width:150px;" readonly="true" disabled /></td>
												<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_1_3" id="txt<?php echo $ttl_bg[$i];?>_1_3" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true" /></td>			 
												<?php }}?>
											</tr>
		
											<tr class="GridviewScrollItem">
												<td>Supply</td>
												<?php for($i=0;$i<count($ttl_bg);$i++){
													if($obj->getBunkerRec($obj->getFun1(),$mappingid,$ttl_bg1[$i]) > 0)
													{
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_1" id="txt<?php echo $ttl_bg[$i];?>_8_1" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',8,this.value);" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPPLY_MT");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_2" id="txt<?php echo $ttl_bg[$i];?>_8_2" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',8,this.value,<?php echo $ttl_bg1[$i];?>);" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPPLY_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true" /></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_3" id="txt<?php echo $ttl_bg[$i];?>_8_3" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPPLY_COST");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<?php }else{?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_1" id="txt<?php echo $ttl_bg[$i];?>_8_1" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',8,this.value);" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_2" id="txt<?php echo $ttl_bg[$i];?>_8_2" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',8,this.value,<?php echo $ttl_bg1[$i];?>);" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_8_3" id="txt<?php echo $ttl_bg[$i];?>_8_3" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
												<?php }}?>
											</tr>
											<tr class="GridviewScrollItem">
												<td>Supplier Adjustment</td>
													<?php for($i=0;$i<count($ttl_bg);$i++){
													if($obj->getBunkerRec($obj->getFun1(),$mappingid,$ttl_bg1[$i]) > 0)
													 {
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_1" id="txt<?php echo $ttl_bg[$i];?>_9_1" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',9,this.value);" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPP_ADJ_MT");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_2" id="txt<?php echo $ttl_bg[$i];?>_9_2" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',9,this.value,<?php echo $ttl_bg1[$i];?>);" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPP_ADJ_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_3" id="txt<?php echo $ttl_bg[$i];?>_9_3" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"SUPP_ADJ_COST");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<?php }else{?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_1" id="txt<?php echo $ttl_bg[$i];?>_9_1" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',9,this.value);" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_2" id="txt<?php echo $ttl_bg[$i];?>_9_2" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',9,this.value,<?php echo $ttl_bg1[$i];?>);" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_9_3" id="txt<?php echo $ttl_bg[$i];?>_9_3" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
												<?php }}?>
											</tr>
											<tr class="GridviewScrollItem">
												<td>Nett Supply </td>
													<?php for($i=0;$i<count($ttl_bg);$i++){
													if($obj->getBunkerRec($obj->getFun1(),$mappingid,$ttl_bg1[$i]) > 0)
													{
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_1" id="txt<?php echo $ttl_bg[$i];?>_10_1" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',10,this.value);" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"NETT_MT");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_2" id="txt<?php echo $ttl_bg[$i];?>_10_2" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',10,this.value,<?php echo $ttl_bg1[$i];?>);" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"NETT_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_3" id="txt<?php echo $ttl_bg[$i];?>_10_3" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"NETT_COST");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<?php }else{?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_1" id="txt<?php echo $ttl_bg[$i];?>_10_1" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',10,this.value);" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true" /></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_2" id="txt<?php echo $ttl_bg[$i];?>_10_2" onKeyUp="getCalculate('<?php echo $ttl_bg[$i];?>',10,this.value,<?php echo $ttl_bg1[$i];?>);" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true" /></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_10_3" id="txt<?php echo $ttl_bg[$i];?>_10_3" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true" /></td>
												<?php }}?>
											</tr>
											<tr class="GridviewScrollItem">
												<td>Actual Consumption</td>
													<?php for($i=0;$i<count($ttl_bg);$i++){
													if($obj->getBunkerRec($obj->getFun1(),$mappingid,$ttl_bg1[$i]) > 0)
													 {
													?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_1" id="txt<?php echo $ttl_bg[$i];?>_11_1" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',11,this.value);" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"ACTUAL_MT");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_2" id="txt<?php echo $ttl_bg[$i];?>_11_2" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"ACTUAL_PRICE");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true" /></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_3" id="txt<?php echo $ttl_bg[$i];?>_11_3" value="<?php echo $obj->getBunkerValues($obj->getFun1(),$mappingid,$ttl_bg1[$i],"ACTUAL_COST");?>" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<?php }else{?>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_1" id="txt<?php echo $ttl_bg[$i];?>_11_1" onKeyUp="getCalculate1('<?php echo $ttl_bg[$i];?>',11,this.value);" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_2" id="txt<?php echo $ttl_bg[$i];?>_11_2" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
													<td><input type="text"  name="txt<?php echo $ttl_bg[$i];?>_11_3" id="txt<?php echo $ttl_bg[$i];?>_11_3" class="form-control" autocomplete="off" style="width:150px;" disabled readonly="true"/></td>
												<?php }}?>
											</tr>
										<?php }?>									
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Bunkers Nett Supply</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="20%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="20%">Value/MT</th>
											</tr>
										</thead>
										<tbody id="tbbunkers">
											<?php 
											$query = "select * from fca_tci_bunker_nett where FCAID='".$obj->getFun1()."'";
											$qres = mysql_query($query);
											$qrec = mysql_num_rows($qres);
											if($qrec > 0)
											{
											while($qrows = mysql_fetch_assoc($qres)){
											?>
											<tr id="rowB_<?php echo $obj->getBunkerGradeBasedOnID($qrows['BUNKERGRADEID']);?>">
												<td><?php echo $obj->getBunkerGradeBasedOnID($qrows['BUNKERGRADEID']);?> Nett</td>
												<td></td>
												<td><?php echo $qrows['COST'];?></td>
												<td><?php if($qrows['VENDORID'] != " "){echo $obj->getVendorListNewBasedOnID($qrows['VENDORID']);}?></td>
												<td><?php echo $qrows['COST_MT'];?></td>
											</tr>
										<?php } ?>
										<?php }?>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Owner Related Costs (Others)</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
												<th width="20%">&nbsp;</th>
											</tr>
										</thead>
										<tbody id="tbodyBrokerage">
										<?php 
										$sql_brok = "select * from fca_tci_brokage_commission where FCAID='".$obj->getFun1()."'";
										
										$res_brok = mysql_query($sql_brok);
										$num_brok = mysql_num_rows($res_brok);
										if($num_brok==0)
										{$num_brok =1;
										?>
                                             <tr id="tbrRow_1">
                                                <td><a href="#tb1'" onClick="removeBrokerage(1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><input type="text" name="txtBrCommPercent_1" id="txtBrCommPercent_1" class="form-control" autocomplete="off" value="0.00" onKeyUp="getFinalCalculation();"  /></td>
												<td><input type="text" name="txtBrComm_1" id="txtBrComm_1" class="form-control" readonly value="0.00" /></td>
												<td><select  name="selBroVList_1" class="select form-control" id="selBroVList_1"></select>
													<script>$("#selBroVList_1").html($("#selVendor").html());$("#selBroVList_1").val('');</script>
												</td>
											</tr>
									<?php }
									     else
									      {$i=0;$num_brok = $num_brok;
										  while($rows_brok = mysql_fetch_assoc($res_brok))
										  {$i = $i + 1;?>
										  
										      <tr id="tbrRow_<?php echo $i;?>">
                                                <td><a href="#tb1'"><i class="fa fa-times" style="color:red;"></i></a></td>
												<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
												
												<td><?php echo $rows_brok['BROKAGE_PERCENT'];?></td>
												<td><?php echo $rows_brok['BROKAGE_AMT'];?></td>
												<td><?php echo $obj->getVendorListNewBasedOnID($rows_brok['VENDORID']);?></td>
											</tr>
										  <?php }} ?>
											
                                            </tbody>
											
										<tbody>
											<tr>
											<td><button type="button" class="btn btn-primary btn-flat">Add</button><input type="hidden" name="txtBRokageCount" id="txtBRokageCount" value="<?php echo $num_brok;?>"/></td>
											<td>Total Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
											
											<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM_PERCENT");?></td>
											<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"BROKERAGE_COMM");?></td>
											<td>
											</td>
										</tr>
										</tbody>
                                            
                                        <tbody>
											<?php 
												$sql4 = "select * from fca_tci_owner_related_cost where FCAID='".$obj->getFun1()."'";
												$res4 = mysql_query($sql4);
												$rec4 = mysql_num_rows($res4);
												$i=1;
												while($rows4 = mysql_fetch_assoc($res4))
												{											
											?>
											<tr>
												<td>
												<?php echo $obj->getOwnerRelatedCostNameBasedOnID($rows4['OWNER_RCOSTID']);?><input type="hidden" name="txtHidORCID_<?php echo $i;?>" id="txtHidORCID_<?php echo $i;?>" class="form-control" readonly value="<?php echo $rows4['OWNER_RCOSTID'];?>" />
												</td>
												<td></td>
												<td></td>
												<td>
												<?php if($rows4['AMOUNT'] > 0){?>
												
												<?php echo abs($rows4['AMOUNT']);?>
												
												<?php }else if($rows4['AMOUNT'] < 0){?>
												
												<span style="color:red;"><?php echo abs($rows4['AMOUNT']);?></span>
												
												<?php }else if($rows4['AMOUNT'] == 0){?>
												<?php if($obj->getOwnerRelatedCostData($rows4['OWNER_RCOSTID'],"RDO_STATUS") == 1){?>
													<?php echo abs($rows4['AMOUNT']);?>
													<?php }else if($obj->getOwnerRelatedCostData($rows4['OWNER_RCOSTID'],"RDO_STATUS") == 2){?>
													<span style="color:red;"><?php echo abs($rows4['AMOUNT']);?></span>
												<?php }?>
												<?php }?>
												</td>
												<td>
													<?php echo $obj->getVendorListNewBasedOnID($rows4['VENDORID']);?>											
												</td>
											</tr>								
											<?php $i++;}?>
											<tr>
												<td>Total Shipowner Expenses</td>
												<td></td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SHIPOWNER_EXP");?></td>
												<td></td>
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td style="font-weight:bold;">Value/MT</td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_SHIPOWNER_EXP_MT");?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Charterers' Costs</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%">Add Comm (%)</th>
												<th width="20%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="20%">Value/MT</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												$sql5 = "select * from fca_tci_charterer_cost where FCAID='".$obj->getFun1()."'";
												$res5 = mysql_query($sql5);
												$rec5 = mysql_num_rows($res5);
												$i=1;
												while($rows5 = mysql_fetch_assoc($res5))
												{
											?>
											<tr>
												<td><?php echo $obj->getChartererCostNameBasedOnID($rows5['CHARTERER_COSTID']);?></td>
												<td><?php echo $rows5['ADDCOMM'];?></td>
												<td><?php echo $rows5['ABSOLUTE'];?></td>
												<td><?php if($rows5['VENDORID'] != " "){echo $obj->getVendorListNewBasedOnID($rows5['VENDORID']);}?></td>
												<td><?php $ccvalue = $rows5['ABSOLUTE'] - (($rows5['ABSOLUTE'] * $rows5['ADDCOMM'])/100); echo number_format($ccvalue,2,'.','');;?></td>
											</tr>
											<?php $i++;}?>
											<tr>
												<td colspan="5"><input type="hidden" name="txtCC_id" id="txtCC_id" value="<?php echo $rec5;?>" /></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Port Costs</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%"></th>
												<th width="20%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="20%">Value/MT</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											$mysql = "select * from fca_tci_portcosts where FCAID='".$obj->getFun1()."' order by FCA_PORTCOSTID asc";
											$myres = mysql_query($mysql);
											$myrec = mysql_num_rows($myres);
											if($myrec > 0)
											{$i=$j=$k=1;
												while($myrows = mysql_fetch_assoc($myres))
												{
													if($myrows['PORT'] == "Load")
													{?>
													<tr id="oscLProw_<?php echo $i;?>">
														<td>Load Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
														<td></td>
														<td><?php echo $myrows['COST'];?></td>
														<td><?php echo $obj->getVendorListNewBasedOnID($myrows['VENDORID']);?></td>
														<td><?php echo $myrows['COST_MT'];?></td>
													</tr>
													<tr id="oscLProw1_<?php echo $i?>">
														<td colspan="5"></td>
													</tr>
													<?php $i++;}else if($myrows['PORT'] == "Discharge"){?>
													<tr id="oscDProw_<?php echo $j;?>">
														<td>Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
														<td></td>
														<td><?php echo $myrows['COST'];?></td>
														<td><?php echo $obj->getVendorListNewBasedOnID($myrows['VENDORID']);?></td>
														<td><?php echo $myrows['COST_MT'];?></td>
													</tr>
													<tr id="oscDProw1_<?php echo $j;?>" >
														<td colspan="5"></td>
													</tr>
													<?php $j++;}else if($myrows['PORT'] == "Transit"){?>
													<tr id="oscTProw_<?php echo $k;?>">
														<td>Transit Port   <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
														<td></td>
														<td><?php echo $myrows['COST'];?></td>
														<td><?php echo $obj->getVendorListNewBasedOnID($myrows['VENDORID']);?></td>
														<td><?php echo $myrows['COST_MT'];?></td>
													</tr>
													<?php $k++;}?>
												<?php }}?>
												<tr>
													<td>Total Port Costs</td>
													<td></td>
													<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_COSTS");?></td>
													<td></td>
													<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_PORT_COSTS_MT");?></td>
												</tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Demurrage Dispatch Ship Owner</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="15%">Add Comm (%)</th>
												<th width="15%">Estimated Cost</th>
                                                <th width="15%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="15%">Nett Value</th>
											</tr>
										</thead>
										<tbody id="tbDDSW" >
										<?php 
											$mysql2 = "select * from fca_tci_dd_shipowner where FCAID='".$obj->getFun1()."'";
											$myres2 = mysql_query($mysql2);
											$myrec2 = mysql_num_rows($myres2);
											if($myrec2 > 0)
											{$i=1;
												while($myrows2 = mysql_fetch_assoc($myres2)){
													if($myrows2['PORT_TYPE']== "Load")
													{
													$laytime_val = $obj->getLayTimeValuesBasedOnMappingID($mappingid,$myrows2['LOADPORTID'],"LP");
													$laytime_val = explode('@#@',$laytime_val);
													if($laytime_val[1]== 0){$laytime = $laytime_val[2];}else{$laytime = $laytime_val[1];}
											?>
											<tr id="ddswLProw_<?php echo $i;?>"> 
												<td>Load Port <?php echo $obj->getPortNameBasedOnID($myrows2['LOADPORTID']);?></td>
												<td><?php echo $myrows2['ADDCOMM'];?></td>
												
												<td><?php echo $myrows2['COST'];?></td>
												<?php if($laytime != ""){ $cost = $laytime; }else { $cost = $myrows2['COST']; } ?>
												<td><?php echo $cost; ?></td>
                                                <td><?php echo $obj->getVendorListNewBasedOnID($myrows2['VENDORID']); ?></td>
												<?php $omcvalue = $myrows2['COST'] - (($myrows2['COST'] * $myrows2['ADDCOMM'])/100);?>
												<td><?php echo $omcvalue;?></td>
                                              </tr>	
											<?php $i++;}} ?>
											<?php }?>
                                            
                                            <?php 
											$mysql2 = "select * from fca_tci_dd_shipowner where FCAID='".$obj->getFun1()."'";
											$myres2 = mysql_query($mysql2);
											$myrec2 = mysql_num_rows($myres2);
											if($myrec2 > 0)
											{$i=1;
												while($myrows2 = mysql_fetch_assoc($myres2))
												{
													if($myrows2['PORT_TYPE']== "Discharge")
													{
													 $laytime_val = $obj->getLayTimeValuesBasedOnMappingID($mappingid,$myrows2['LOADPORTID'],"DP");
													 $laytime_val = explode('@#@',$laytime_val);
													 if($laytime_val[1]== 0){$laytime = $laytime_val[2];}else{$laytime = $laytime_val[1];}
													?>
													<tr id="ddswDProw_<?php echo $i;?>">
													<td>Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows2['LOADPORTID']);?></td>
													<td><?php echo $myrows2['ADDCOMM'];?></td>
                                                    
													<td><?php echo $myrows2['COST'];?></td>
													<?php if($laytime != ""){ $cost = $laytime; }else { $cost = $myrows2['COST']; } ?>
													<td><?php echo $cost;?></td>
													<td><?php echo $obj->getVendorListNewBasedOnID($myrows2['VENDORID']); ?></td>
													<?php $omcvalue = $myrows2['COST'] - (($myrows2['COST'] * $myrows2['ADDCOMM'])/100);?>
													<td><?php echo $omcvalue;?></td>
                                                   </tr>	
											<?php $i++;}}}?>
											
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Other Misc. Income</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="20%"></th>
												<th width="20%">Add Comm (%)</th>
												<th width="20%">Cost</th>
												<th width="20%">Vendor</th>
												<th width="20%">Nett Value</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												$sql6 = "select * from fca_tci_other_misc_cost where FCAID='".$obj->getFun1()."'";
												$res6 = mysql_query($sql6);
												$rec6 = mysql_num_rows($res6);
												$i=1;
												while($rows6 = mysql_fetch_assoc($res6))
												{
											?>
											<tr>
												<td><?php echo $obj->getOtherMiscCostNameBasedOnID($rows6['OTHER_MCOSTID']);?></td>
												<td><?php echo $rows6['ADDCOMM'];?></td>
												<td><?php echo $rows6['AMOUNT'];?></td>
												<td><?php if($rows6['VENDORID'] != " "){echo $obj->getVendorListNewBasedOnID($rows6['VENDORID']);}?></td>
												<td><?php $omcvalue = $rows6['AMOUNT'] - (($rows6['AMOUNT'] * $rows6['ADDCOMM'])/100);echo number_format($omcvalue,2,'.','');?></td>
											</tr>
												<?php $i++; ?>
											<?php }?>
											<tr><td colspan="5" align="left" class="text" valign="top" ><input type="hidden" name="txtAOMC_id" id="txtAOMC_id" value="<?php echo $rec6;?>" /></td></tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="box">
								<div class="box-header">
									<h3 class="box-title">Results</h3>
								</div>
								<div class="box-body no-padding">
									<table class="table table-striped">
										<thead>
											<tr>
												<th width="25%"></th>
												<th width="25%"></th>
												<th width="25%"></th>
												<th width="25%"></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Revenue (Final Nett Freight)</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"REVENUE");?></td>
												<td></td>
											</tr>
											<tr>
												<td>Total Owners Expenses</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_OWNER_EXP");?></td>
												<td></td>
											</tr>	
											<tr>
												<td>Total Charterers' Expenses</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_CHARTERER_EXP");?></td>
												<td></td>
											</tr>
											<tr>
												<td>Voyage Earnings</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"VOYAGE_EARNING");?></td>
												<td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;" >Daily Earnings / TCE</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"DAILY_EARNING");?></td>
												<td></td>
											</tr>
											<tr>
												<td>Daily Time Charter (USD/Day)</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"DAILY_VO_EXP");?></td>
												<td><?php echo $obj->getVendorListNewBasedOnID( $obj->getFreightEstimationTotalRecords($obj->getFun1(),"VESSEL_OPR_VENDORID")); ?></td>
											</tr>
											<tr>
												<td style="color:#dc631e;" >G Total Voyage Earnings&nbsp;&nbsp;&nbsp;<span style="font-size:10px; font-style:italic;">(Voyage Earnings + Demurrage)</span></td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"GRAND_TTL_VOYAGE_EARNING");?></td>
												<td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">Nett Daily Earnings</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"NETT_DAILY_EARNING");?></td>
												<td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">Nett Daily Profit</td>
												<td></td>
												<td><?php echo $obj->getFreightEstimationTotalRecords($obj->getFun1(),"NETT_DAILY_PROFIT");?></td>
												<td></td>
											</tr>
											<tr>
												<td style="color:#dc631e;">P/L</td>
												<td></td>
												<td><?php echo round((float)($obj->getFreightEstimationTotalRecords($obj->getFun1(),"NETT_DAILY_PROFIT")*$obj->getFreightEstimationTotalRecords($obj->getFun1(),"TTL_DAYS")), 2); ?></td>
												<td></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
									<h2 class="page-header">&nbsp;
									
									</h2>                            
								</div><!-- /.col -->
							</div>
							
							<div class="row invoice-info" style="display:none;">
                                <div class="col-sm-4 invoice-col">
                                  Voyage Financials Status
                                    <address>
										<select  name="selVType" class="select form-control" id="selVType" >
											<?php 
												$_REQUEST['selVType'] = $obj->getFun4();
												$obj->getVoyageType();
											?>
										</select>
                                    </address>
                                </div><!-- /.col -->
							</div>
                  </form>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$("#TCI_18,#TCI_19").show();
});

function getPdf(var1,var2)
{
	//location.href='allPdf.php?id=4&mappingid='+var1+'&cost_sheet_id='+var2;
}
</script>
    </body>
</html>
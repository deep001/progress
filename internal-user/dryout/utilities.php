<?php 
session_start();
require_once("../includes/display_user.inc.php");
require_once("../includes/functions_user.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<link href="../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../js/timer.js" type="text/javascript"></script>
<link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.alerts.js"></script>
<script src="../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="../js/chosen.jquery.js" type="text/javascript"></script>
<link type="text/css" href="../css/chosen.css" rel="stylesheet" />
<script src='../js/jquery.autosize.js'></script>
<link href="../css/datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<script src="../js/plugins/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.numeric.js"></script>

</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(7); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                       <i class="fa fa-bar-chart-o"></i>&nbsp;Utilities&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Utilities</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				
			<div class="row">
                        <div class="col-md-3">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <i class="fa fa-certificate"></i>
                                    <h2 class="box-title">Vessel Info</h2>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="list-unstyled">
                                       <li>
                                            <ul>
                                                <li><a href="#A" >Insturctions</a></li>
                                                <li><a href="#A" >Vessel Information</a></li>
                                                <li><a href="#A" >Vessel Particulars</a></li>
                                                <li><a href="#A" >Cargo Information</a></li>
												<li><a href="#A" >Trading Certificates</a></li>
                                                <li><a href="#A" >Narcotics</a></li>
                                                <li><a href="#A" >Bonded Items</a></li>
                                                <li><a href="#A" >All Port Calls</a></li>
												<li><a href="#A" >Voyage Memo</a></li>
                                                <li><a href="#A" >IMO Form 161 Security</a></li>
										   </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- ./col -->
                        <div class="col-md-3">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <i class="fa fa-certificate"></i>
                                    <h3 class="box-title">Crew Info</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="list-unstyled">
                                       <li>
                                            <ul>
                                                <li><a href="#A" >Crew Data Entry</a></li>
                                                <li><a href="#A" >Personl Details</a></li>
                                                <li><a href="#A" >National Documents</a></li>
                                                <li><a href="#A" >Flag Documents</a></li>
												<li><a href="#A" >Tranning</a></li>
                                                <li><a href="#A" >Crew Data Complete</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- ./col -->
                        <div class="col-md-3">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <i class="fa fa-certificate"></i>
                                    <h3 class="box-title">Crew List</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="list-unstyled">
                                       <li>
                                            <ul>
                                                <li><a href="#A" >IMO Passport</a></li>
                                                <li><a href="#A" >IMO Passport with expiry date</a></li>
                                                <li><a href="#A" >IMO Seamans Book</a></li>
                                                <li><a href="#A" >IMO Seamans Book with expiry date</a></li>
												<li><a href="#A" >IMO Vaccination List</a></li>
                                                <li><a href="#A" >IMO Chinese Style</a></li>
                                                <li><a href="#A" >Singapore Crew List Info</a></li>
                                                <li><a href="#A" >IMO Offsigners List</a></li>
												<li><a href="#A" >Company ID Number</a></li>
                                                <li><a href="#A" >Crew List for Logbook</a></li>
                                                <li><a href="#A" >Crew List Body temp</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- ./col -->
						
						<div class="col-md-3">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <i class="fa fa-certificate"></i>
                                    <h3 class="box-title">Port Papers</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="list-unstyled">
                                       <li>
                                            <ul>
                                                <li><a href="portpaper_list.php" >PIC Bunkers</a></li>
                                                <li><a href="piccargo_list.php" >PIC Cargo</a></li>
                                                <li><a href="allnil_list.php" >All Nil List</a></li>
                                                <li><a href="nilpassenger_list.php" >Nil Passengers</a></li>
												<li><a href="nilstowaway_list.php" >Nil Stowaway</a></li>
                                                <li><a href="nilmail_list.php" >Nil Mail</a></li>
                                                <li><a href="nilarms_list.php" >Nil Arms & Ammo</a></li>
                                                <li><a href="nillivestock_list.php" >Nil Livestock</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>
					
					
					<div class="row">
                        <div class="col-md-3">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <i class="fa fa-certificate"></i>
                                    <h3 class="box-title">Port Arrival</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="list-unstyled">
                                       <li>
                                            <ul>
                                                <li><a href="#A" >IMO Bond</a></li>
                                                <li><a href="#A" >IMO Narcoties</a></li>
                                                <li><a href="#A" >IMO General</a></li>
                                                <li><a href="#A" >IMO Cargo</a></li>
												<li><a href="#A" >IMO Stores</a></li>
                                                <li><a href="#A" >Crew Effects 1 Pages</a></li>
                                                <li><a href="#A" >Crew Effects 2 Pages</a></li>
                                                <li><a href="#A" >Crew Currency Declaration</a></li>
												<li><a href="#A" >Vessel Currency Declaration</a></li>
                                                <li><a href="#A" >Maritime Health Declaration</a></li>
                                                <li><a href="#A" >MDH Page 2</a></li>

                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- ./col -->
                        <div class="col-md-3">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <i class="fa fa-certificate"></i>
                                    <h3 class="box-title">Terminal Req</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="list-unstyled">
                                       <li>
                                            <ul>
                                                <li><a href="#A" >EDP Letter</a></li>
                                                <li><a href="#A" >Application For COW</a></li>
                                                
                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- ./col -->
                        <div class="col-md-3">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <i class="fa fa-certificate"></i>
                                    <h3 class="box-title">Special Req</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="list-unstyled">
                                       <li>
                                            <ul>
                                                <li><a href="#A" >Islam Declaration</a></li>
                                                <li><a href="#A" >Blank Letter</a></li>
                                                <li><a href="#A" >Drugs List Nigeria</a></li>
                                                
                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- ./col -->
						
						<div class="col-md-3">
                            <div class="box box-solid">
                                <div class="box-header">
                                    <i class="fa fa-certificate"></i>
                                    <h3 class="box-title">Misc</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="list-unstyled">
                                       <li>
                                            <ul>
                                                <li><a href="#A" >Crew Cash Requirment</a></li>
                                                <li><a href="#A" >UAE Licence Date</a></li>
												<li><a href="#A" >Lop Water</a></li>
                                                <li><a href="#A" >Lop Rolling</a></li>
												<li><a href="#A" >Lop Blank</a></li>
                                                <li><a href="#A" >Lop Cargo Diff</a></li>
                                                
                                            </ul>
                                        </li>
                                    </ul>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
						
						
                    </div>
					
				</section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

</body>
</html>
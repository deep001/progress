<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
video::-internal-media-controls-download-button {
    display:none;
   }
   video::-webkit-media-controls-enclosure {
        overflow:hidden;
   }
   video::-webkit-media-controls-panel {
        width: calc(100% + 30px); /* Adjust as needed */
   }
</style>
</head>
    <body class="skin-blue fixed"  oncontextmenu="return false;">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(13); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
               <section class="content-header">
                    <h1>
                        <i class="fa fa-video-camera"></i>&nbsp;Module CBTs&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Module CBTs</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
					<div class="row invoice-info">	
						<div class="col-sm-6 invoice-col" align="center">
							<video width="100%" controls poster="../../helpdocs/videos/CBT_Charter_Out.png">
							  <source src="../../helpdocs/videos/CBT_Charter_Out.mp4" type="video/mp4">
							  Your browser does not support HTML5 video.
							</video>
                        </div><!-- /.col -->
						<div class="col-sm-6 invoice-col" align="center">
							<video width="100%" controls poster="../../helpdocs/videos/CBT_Post_fixture.png">
							  <source src="../../helpdocs/videos/CBT_Post_fixture.mp4" type="video/mp4">
							  Your browser does not support HTML5 video.
							</video>
                        </div><!-- /.col -->
					</div>
					
				 <!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/timer.js" type="text/javascript"></script>
<script>
function getRelocate(var1,fleet)
{
	if(fleet == 0 )
	{
		jAlert('Please choose fleet for this user', 'Alert');
	}
	else
	{
		if(var1 == 1){location.href = "./qms/";}
	}	
}
</script>
    </body>
</html>
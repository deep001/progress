<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

$mx	  = $_REQUEST['id'];
$comid = $_REQUEST['comid'];
//$id = $obj->getLatestCostSheetIDTC($comid);
//$obj->viewTCEstimationTempleteRecordsNew($id);

echo '<div id="tripdiv_'.$mx.'">
		   <div style="margin-bottom:7px; margin-top:8px; height:2px; background-color:#ccc;width:100%;"></div>
		   <div><strong style="font-size:15px;padding:0px 5px 0px 5px;""><a href="#tbl'.$mx.'" onClick="removeSheet('.$mx.');" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;Trip/Period '.$mx.'</strong></div>
		   <table width="100%" style="margin-bottom:10px; padding:0px 5px 0px 5px;">
			   <tr class="input-text">
				  <td >Del Date/Time&nbsp;<input type="hidden" name="txtCOUTSlaveIdentify_'.$mx.'" id="txtCOUTSlaveIdentify_'.$mx.'" value="add"/></td>
				  <td><input autocomplete="off" type="text" name="txtDeldate_'.$mx.'" id="txtDeldate_'.$mx.'" class="input-text" value="" style="width:100px;" placeholder="dd-mm-yy hh:mm" /></td>
				  <td >Re Del Date/Time&nbsp;</td>
				  <td><input autocomplete="off" type="text" name="txtReDeldate_'.$mx.'" id="txtReDeldate_'.$mx.'" class="input-text" value="" style="width:100px;" placeholder="dd-mm-yy hh:mm" /></td>
				  <td >TC Days&nbsp;</td>
				  <td><input autocomplete="off" type="text" name="txtTCdays_'.$mx.'" id="txtTCdays_'.$mx.'" class="input-text" placeholder="TC Days" style="width:100px;background-color:#EAEAEA;" readonly value=""/></td>
				  <td></td><td></td><td></td><td></td><td></td><td></td>
			   </tr>
			   <tr class="input-text">
				  <td colspan="6">
					 <table width="100%">
						 <tr class="input-text">
							<td colspan="5"><strong>Delivery</strong></td>
						 </tr>
						 <tr class="input-text">
						   <td>#</td>
						   <td>Bunker Grade</td>
						   <td>Qty(MT)</td>
						   <td>Bunker Date</td>
						   <td>Price USD/MT)</td>
						   <td>Amount(USD)</td>
						 </tr>
						 <tbody id="bunkerdelivery_'.$mx.'">
							<tr class="input-text" id="row_del_'.$mx.'_1">
							   <td><a href="#tbl1" onClick="removeDelBunker('.$mx.',1);" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td>
							   <td><select name="selDelBunker_'.$mx.'_1" class="input-text" style=" width:90px;" id="selDelBunker_'.$mx.'_1"></select></td>
							   <td><input type="text" name="txtDelQty_'.$mx.'_1" id="txtDelQty_'.$mx.'_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation('.$mx.');" autocomplete="off" /></td>
							   <td><input type="text" name="txtDelBunDate_'.$mx.'_1" id="txtDelBunDate_'.$mx.'_1" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
							   <td><input type="text" name="txtDelPrice_'.$mx.'_1" id="txtDelPrice_'.$mx.'_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation('.$mx.');" autocomplete="off" /></td>
							   <td><input type="text" name="txtDelAmount_'.$mx.'_1" id="txtDelAmount_'.$mx.'_1" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
							</tr>
							<script>$("#selDelBunker_'.$mx.'_1").html($("#selBunker").html());</script>
					  
						</tbody>
						<tbody>		
						<tr class="input-text">
							<td colspan="2"><button type="button" onClick="AddNewDel('.$mx.');">Add</button><input type="hidden" name="txtDELID_'.$mx.'" id="txtDELID_'.$mx.'" value="1"/></td>
							<td ></td>
							<td ></td>
							<td ></td>
							<td ><input type="text" name="txtTotalDelAmount_'.$mx.'" id="txtTotalDelAmount_'.$mx.'" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
							
						</tr>
					 </table>
				  
				  </td>
				  <td colspan="6">
					 <table width="100%">
						<tr class="input-text">
							<td colspan="5"><strong>Re-Delivery</strong></td>
						</tr>
						<tr class="input-text">
						   <td>#</td>
						   <td>Bunker Grade</td>
						   <td>Qty(MT)</td>
						   <td>Bunker Date</td>
						   <td>Price USD/MT)</td>
						   <td>Amount(USD)</td>
						</tr>
						<tbody id="bunkerRedelivery_'.$mx.'">
							<tr class="input-text" id="row_Redel_'.$mx.'_1">
							   <td><a href="#tbl1" onClick="removeReDelBunker('.$mx.',1);" ><i class="fa fa-times" style="color:red;"></i></a>&nbsp;&nbsp;</td>
							   <td><select name="selReDelBunker_'.$mx.'_1" class="input-text" style=" width:90px;" id="selReDelBunker_'.$mx.'_1"></select></td>
							   <td><input type="text" name="txtReDelQty_'.$mx.'_1" id="txtReDelQty_'.$mx.'_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation('.$mx.');" autocomplete="off" /></td>
							   <td><input type="text" name="txtReDelBunDate_'.$mx.'_1" id="txtReDelBunDate_'.$mx.'_1" class="input-text" style="width:85px;" value="" placeholder="DD-MM-YYYY" autocomplete="off" /></td>
							   <td><input type="text" name="txtReDelPrice_'.$mx.'_1" id="txtReDelPrice_'.$mx.'_1" class="input-text numeric" style="width:85px;" value="" placeholder="0.00" onKeyUp="getFinalCalculation('.$mx.');" autocomplete="off" /></td>
							   <td><input type="text" name="txtReDelAmount_'.$mx.'_1" id="txtReDelAmount_'.$mx.'_1" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
							</tr>
						</tbody>
						<tbody>		
						<tr class="input-text">
							<td colspan="2"><button type="button" onClick="AddNewReDel('.$mx.');">Add</button><input type="hidden" name="txtREDELID_'.$mx.'" id="txtREDELID_'.$mx.'" value="1"/></td>
							<td ></td>
							<td ></td>
							<td ></td>
							<td ><input type="text" name="txtTotalReDelAmount_'.$mx.'" id="txtTotalReDelAmount_'.$mx.'" readonly class="input-text numeric" style="width:85px;background-color:#E1E1E1;" value="" placeholder="0.00" autocomplete="off"/></td>
						</tr>
						</tbody>
					 </table>
				  
				  </td>
			   </tr>
			   <tr class="input-text">
				<td colspan="12">&nbsp;</td>
			   </tr>
				<tr class="input-text">
				   <td></th>
				   <td>Off Hire Reason</td>
				   <td>Off Hire From</td>
				   <td>Off Hire To</td>
				   <td>Off Hire Days</td>
				   <td>Off Hire Rate/Day(USD)</td>
				   <td>Off Hire(USD)</td>
				   <td></td>
				   <td >Utilisation Days&nbsp;</td>
				   <td><input autocomplete="off" type="text" name="txtUtilisationDays_'.$mx.'" id="txtUtilisationDays_'.$mx.'" class="input-text" placeholder="Utilisation Days" style="width:90px;background-color:#EAEAEA;" readonly value=""></td>
				   <td >HFO/MGO Diff.(USD)&nbsp;</td>
				   <td><input autocomplete="off" type="text" name="txtBunkerDifference_'.$mx.'" id="txtBunkerDifference_'.$mx.'" class="input-text" placeholder="0.00" style="width:90px;background-color:#EAEAEA;" readonly value=""></td>
				   <td ></td>
				</tr>
				<tbody id="offhirebody_'.$mx.'">
					<tr class="input-text" id="row_offhire_'.$mx.'_1">
						<td><a href="#tb1" onClick="removeOffHire('.$mx.',1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
						<td valign="top"><textarea class="input-text areasize" name="txtOffHireReason_'.$mx.'_1" style="width:120px;" id="txtOffHireReason_'.$mx.'_1" rows="3" placeholder="Off Hire Reason..."></textarea></td>
						<td valign="top"><input autocomplete="off" type="text" name="txtOffHireFrom_'.$mx.'_1" id="txtOffHireFrom_'.$mx.'_1" class="input-text" value="" style="width:100px;" placeholder="dd-mm-yyyy HH:MM" /></td>
						<td valign="top"><input autocomplete="off" type="text" name="txtOffHireTo_'.$mx.'_1" id="txtOffHireTo_'.$mx.'_1" class="input-text" value="" style="width:100px;" placeholder="dd-mm-yyyy HH:MM" /></td>
						<td valign="top"><input autocomplete="off" type="text" name="txtOffHireDays_'.$mx.'_1" id="txtOffHireDays_'.$mx.'_1" class="input-text" placeholder="Off Hire Days" style="width:90px;background-color:#EAEAEA;" readonly value="" ></td>
						<td valign="top"><input autocomplete="off" type="text" name="txtOffHireRate_'.$mx.'_1" id="txtOffHireRate_'.$mx.'_1" class="input-text" placeholder="Rate/Day(USD)" style="width:100px;" onKeyUp="getFinalCalculation('.$mx.');" value="" ></td>
						<td valign="top"><input autocomplete="off" type="text" name="txtOffHireAmt_'.$mx.'_1" id="txtOffHireAmt_'.$mx.'_1" class="input-text" placeholder="Off Hire(USD)" style="width:90px;background-color:#EAEAEA;" readonly value="" ></td>
						<td></td><td></td><td></td><td></td><td></td>
					</tr>
				</tbody>
				<tbody>		
				<tr class="input-text">
					<td ><button type="button" onClick="AddNewOffHire('.$mx.');">Add</button><input type="hidden" name="txtOFFID_'.$mx.'" id="txtOFFID_'.$mx.'" value="1"/></td>
					<td colspan="11"></td>
					
				</tr>
				</tbody>
			 </table>
		  <!----------------------/////////------------LEFT SIDE PART------------/////////------------------------------>	
		   <table width="100%" style="margin-bottom:5px; padding:0px 5px 0px 5px;">
		   <tr class="input-text">
			   <td width="50%"><strong>Revenue Calculations - USD</strong></td>
			   <td width="50%"><strong>Expense Calculations - USD</strong></td>
		   </tr>
		   <tr>
			   <td width="50%" style="padding-top:4px; vertical-align:top;">
				 <table width="100%">
					<tbody>
					<tr class="input-text">
						<td>Hire PDPR Currency</td>
						<td><select  name="selExchangeCurrency_'.$mx.'" id="selExchangeCurrency_'.$mx.'" onChange="getCurrencySpan('.$mx.');" class="input-text" style="width:100px;" ></select></td>
						<td>Exchange Rate To USD</td>
						<td><input type="text" name="txtExchangeRate_'.$mx.'" id="txtExchangeRate_'.$mx.'" class="input-text numeric" style="width:100px;" value="1" placeholder="Exchange Rate" autocomplete="off" onKeyUp="getFinalCalculation('.$mx.');"/></td>
					</tr>
					<tr class="input-text">
						<td>Hire Fixed Period PDPR <span id="currencyspan_'.$mx.'"></span></td>
						<td></td>
						<td><input autocomplete="off" type="text" name="txtDailyGrossHireUSD_'.$mx.'" id="txtDailyGrossHireUSD_'.$mx.'" class="input-text" style="width:100px;" value="" onKeyUp="getFinalCalculation('.$mx.');"/></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td>Hire Fixed Period PDPR(USD)</td>
						<td></td>
						<td><input autocomplete="off" type="text" name="txtDailyGrossHireUSD1_'.$mx.'" id="txtDailyGrossHireUSD1_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value=""/></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td >Add Comm(%)</td>
						<td><input autocomplete="off" type="text" name="txtAddCommPerct_'.$mx.'" id="txtAddCommPerct_'.$mx.'" class="input-text" style="width:100px;" value="'.$obj->getFun68().'" onKeyUp="getFinalCalculation('.$mx.');"/></td>
						<td><input autocomplete="off" type="text" name="txtAddComm1_'.$mx.'" id="txtAddComm1_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value=""/></td>
						<td><select name="selAddCommVendor_'.$mx.'" class="input-text" style="width:100px;" id="selAddCommVendor_'.$mx.'" ></select></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td >Broker’s Comm.(%)</td>
						
						<td><input autocomplete="off" type="text" name="txtChartererAcc_'.$mx.'" id="txtChartererAcc_'.$mx.'" class="input-text" style="width:100px;" value="'.$obj->getFun69().'" onKeyUp="getFinalCalculation('.$mx.');"/></td>
						<td><input autocomplete="off" type="text" name="txtBrokerComm_'.$mx.'" id="txtBrokerComm_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value=""/></td>
						<td><select name="selBroCommVendor_'.$mx.'" class="input-text" style="width:100px;" id="selBroCommVendor_'.$mx.'" ></select></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td >Nett Hire(USD/day)</td>
						<td></td>
						<td><input autocomplete="off" type="text" name="txtNettHire_'.$mx.'" id="txtNettHire_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="" ></td>
						<td></td><td></td>
					</tr>
					<tr class="input-text">
						<td >Nett Rev(USD)</td>
						<td></td>
						<td><input autocomplete="off" type="text" name="txtNettRev_'.$mx.'" id="txtNettRev_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="" /></td>
						<td></td><td></td>
					</tr>
					<tr class="input-text">
						<td >Less Off hire</td>
						<td></td>
						<td><input autocomplete="off" type="text" name="txtLessOffHire_'.$mx.'" id="txtLessOffHire_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="" ></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td >CVE(USD/Month)</td>
						<td><input autocomplete="off" type="text" autocomplete="off" name="txtCVEM_'.$mx.'" id="txtCVEM_'.$mx.'" class="input-text" style="width:100px;" value="1250" onKeyUp="getFinalCalculation('.$mx.');"/></td>
						<td><input autocomplete="off" type="text" name="txtCVE_'.$mx.'" id="txtCVE_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value=""/></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td ><strong>Nett Hire to invoice(USD)</strong></td>
						<td></td>
						<td><input autocomplete="off" type="text" name="txtNETHire_'.$mx.'" id="txtNETHire_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value=""/></td>
						<td><select name="selESTtcVendor_'.$mx.'" class="input-text" style="width:100px;" id="selESTtcVendor_'.$mx.'" ></select></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td ><strong>Other Income</strong></td>
						<td></td>
						<td></td><td></td>
					</tr>
				</tbody>		
				<tbody id="otherIncomebody_'.$mx.'">
					<tr class="input-text" id="row_otherincome_'.$mx.'_1">
						<td><a href="#tb1" onClick="removeOtherIncome('.$mx.',1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
						<td ><input autocomplete="off" type="text" name="txtOtherIncomeText_'.$mx.'_1" id="txtOtherIncomeText_'.$mx.'_1" class="input-text" style="width:100px;" placeholder="Description" value="" /></td>
						<td><input autocomplete="off" type="text" name="txtOtherIncome_'.$mx.'_1" id="txtOtherIncome_'.$mx.'_1" class="input-text" style="width:100px;" onKeyUp="getFinalCalculation('.$mx.');" placeholder="Other Income" value="" /></td>
						<td><select name="selOthIncVendor_'.$mx.'_1" class="input-text" style="width:100px;" id="selOthIncVendor_'.$mx.'_1" ></select></td>
					</tr>
				</tbody>
				<tbody>		
					<tr class="input-text">
						<td ><button type="button" onClick="addOtherIncomeRow('.$mx.');">Add</button><input type="hidden" name="hddnOtherID_'.$mx.'" id="hddnOtherID_'.$mx.'" value="1"/></td>
						<td></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td><strong>Total Rev</strong></td>
						<td></td>
						<td></td>
						<td><input autocomplete="off" type="text" name="txtTotalRev_'.$mx.'" id="txtTotalRev_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" readonly value="" /></td>
						<td></td>
						
					</tr>
				</tbody>
				 </table>
			   </td>
			   
	<!---------------------------------------------RIGHT SIDE PART----------------------------------------------------->					   
			   <td width="50%" style="padding-top:4px; vertical-align:top;">
					<table width="100%">
					
					<tr class="input-text">
						<td colspan="6"><strong>Other Expense</strong></td>
					</tr>
					<tr class="input-text">
						<td></td>
						<td>Expense Type</td>
						<td>Expense Descp.</td>
						<td>Add to TTL</td>
						<td>Expense Amt</td>
						<td>Vendor</td>
					</tr>
				   
					<tbody id="otherExpensebody_'.$mx.'">
						<tr class="input-text" id="row_otherExpense_'.$mx.'_1">
							<td><a href="#tb1" onClick="removeOtherExpense('.$mx.',1);" ><i class="fa fa-times" style="color:red;"></i></a></td>
							<td ><select name="selExpenseType_'.$mx.'_1" id="selExpenseType_'.$mx.'_1" class="input-text" style="width:100px;"></select></td>
							<td><input autocomplete="off" type="text" name="txtExpenseDesc_'.$mx.'_1" id="txtExpenseDesc_'.$mx.'_1" class="input-text" style="width:100px;" placeholder="Expense Descp." value="" /></td>
							<td><input name="ChkAddToTTL_'.$mx.'_1" class="checkbox" id="ChkAddToTTL_'.$mx.'_1" type="checkbox" value="1"/><input type="hidden" name="chkVal_'.$mx.'_1" id="chkVal_'.$mx.'_1" value="" /></td>
							<td><input autocomplete="off" type="text" name="txtOtherExpense_'.$mx.'_1" id="txtOtherExpense_'.$mx.'_1" class="input-text" style="width:100px;" onKeyUp="getFinalCalculation('.$mx.');" placeholder="Other Income(USD)" value="" /></td>
							<td><select name="selOthExpVendor_'.$mx.'_1" class="input-text" style="width:100px;" id="selOthExpVendor_'.$mx.'_1" ></select></td>
						</tr>
					</tbody>
					<tr class="input-text">
						<td colspan="5"><button type="button" onClick="addOtherExpenseRow('.$mx.');">Add</button><input type="hidden" name="hddnOtherExID_'.$mx.'" id="hddnOtherExID_'.$mx.'" value="1"/></td>
					</tr>
					<tr class="input-text">
						<td colspan="5">&nbsp;</td>
					</tr>
					<tr class="input-text">
						<td colspan="5"><strong>TC In Expenses</strong></td>
					</tr>
					<tr class="input-text">
						<td>TC In (USD/Day)</td>
						<td><input autocomplete="off" type="text" name="txtTCPerDay_'.$mx.'" id="txtTCPerDay_'.$mx.'" class="input-text" style="width:100px;" placeholder="TC In (USD/Day)" value="" onKeyUp="getFinalCalculation('.$mx.');"/></td>
						<td>Total Hireage(USD) </td>
						<td><input autocomplete="off" type="text" name="txtTotalHireage_'.$mx.'" id="txtTotalHireage_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="Total Hireage" readonly value=""/></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td>Add. Comm(%)</td>
						<td><input autocomplete="off" type="text" name="txtAddComm_'.$mx.'" id="txtAddComm_'.$mx.'" class="input-text" style="width:100px;" placeholder="Add. Comm(%)" value="" onKeyUp="getFinalCalculation('.$mx.');"/></td>
						<td>Add. Comm(USD)</td>
						<td><input autocomplete="off" type="text" name="txtAddCommAmt_'.$mx.'" id="txtAddCommAmt_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="Add. Comm(USD)" readonly value=""/></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td></td>
						<td></td>
						<td>Nett Hireage(USD)</td>
						<td><input autocomplete="off" type="text" name="txtNetHireage_'.$mx.'" id="txtNetHireage_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="Nett Hireage(USD)" readonly value=""/></td>
						<td><select name="selTcInExpVendor_'.$mx.'" class="input-text" style="width:100px;" id="selTcInExpVendor_'.$mx.'" ></select></td>
					</tr>
					<tr class="input-text">
						<td colspan="5">&nbsp;</td>
					</tr>
					<tr class="input-text">
						<td colspan="2"><strong>Total Expenses</strong></td>
						<td></td>
						<td><input autocomplete="off" type="text" name="txtTotalExpenses_'.$mx.'" id="txtTotalExpenses_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="Total Expenses" readonly value=""/></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td colspan="2"><strong>TC Earnings</strong></td>
						<td></td>
						<td><input autocomplete="off" type="text" name="txtVoyageEarnings_'.$mx.'" id="txtVoyageEarnings_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="TC Earnings" value=""/></td>
						<td></td>
					</tr>
					<tr class="input-text">
						<td colspan="2"><strong>Profit / Day&nbsp;</strong></td>
						<td></td>
						<td><input autocomplete="off" type="text" name="txtProfitPerDay_'.$mx.'" id="txtProfitPerDay_'.$mx.'" class="input-text" style="width:100px;background-color:#EAEAEA;" placeholder="Voy Earnings/Utilisation Days" value=""/></td>
						<td></td>
					</tr>
				</table>
			   </td>
		   </tr>
		   </table>';

?>

<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertElibraryReferencesDetails();
	header('Location:./elibrary.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(6); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-desktop"></i>&nbsp;E-Library&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">E-Library</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="elibrary.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             ADD REFRENCES
    					    </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Category
                            <address>
                               <select  name="selCategory" class="form-control" id="selCategory" >
								<?php 
                                $obj->getElibraryCategoryList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Reference Type
                            <address>
                                <select  name="selRefType" class="form-control" id="selRefType" >
									<?php 
                                    $obj->getElibraryReferenceTypeList();
                                    ?>
                                    </select>
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
                            Date
                            <address>
                                <input type="text" name="txtDate" id="txtDate" class="form-control"  placeholder="Date" autocomplete="off" value="<?php echo date("d-M-Y",time());?>" readonly/>
                             </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                       <div class="col-sm-4 invoice-col">
                         	 Name
                            <address>
                               <input type="text" name="txtName" id="txtName" class="form-control" autocomplete="off" placeholder="Name" value=""/>
                            </address>
                        </div><!-- /.col -->
						<div class="col-sm-4 invoice-col">
                         	 Source
                            <address>
                               <input type="text" name="txtSource" id="txtSource" class="form-control" autocomplete="off" placeholder="Source" value=""/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                     <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                            Description
                            <address>
                               <textarea class="form-control areasize" name="txtDirection" id="txtDirection" rows="3" placeholder="Description ..." ></textarea>
                            </address>
                        </div><!-- /.col -->
                    </div>
					<div class="row invoice-info">
                        <div class="col-sm-12 invoice-col">
                           &nbsp;
                            <address>
							<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add file">
                                    <i class="fa fa-paperclip"></i> Attachment
                                    <input type="file" class="form-control" multiple name="mul_file[]" id="mul_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                </div>
							</address>
                        </div><!-- /.col -->
					</div>
                    
				
				<div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate(0);">Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="txtStatus" id="txtStatus" value="1" /><input type="hidden" name="txtFileName" id="txtFileName" value="" />
				</div>
         
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$('#txtDate').datepicker({
    format: 'dd-mm-yyyy'
});
$(".areasize").autosize({append: "\n"});
$("#frm1").validate({
	rules: {
	selCategory:"required",
	selRefType:"required",
	txtName:"required"
	},
messages: {	
	selCategory:"*",
	selRefType:"*",
	txtName:"*"
	},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

});


function getShow()
{
	if ($("#checkBAFbox").iCheck('check')) 
	{
		$("#rowBAF").show();
	}
	else
	{
		$("#rowBAF").hide();
		$("#txtBPrice,#txtBAFTerms,#txtIncDec").val("");
	}
}

</script>
    </body>
</html>
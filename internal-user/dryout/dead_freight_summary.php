<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
if (isset($_REQUEST['selBType']) && $_REQUEST['selBType']!="")
{
	$selBType = $_REQUEST['selBType'];
}
else if(isset($_SESSION['selBType']))
{
	$selBType = $_SESSION['selBType'];
}
else
{
	$selBType = '';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(7,2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-bar-chart-o"></i>&nbsp;Reports&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li>Reports</li>
						<li>Operations</li>
						<li class="active">Dead Freight Summary Report</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div class="box box-primary">
				<h3 style=" text-align:center;">Dead Freight Summary Report</h3>
				<div style="height:10px;"></div>
					<div class="row invoice-info" style="margin-left:5px;margin-right:5px;">
						<div class="col-sm-2 invoice-col">
							Date From <span style="font-size:10px; font-style:italic;">  (financial year)</span>
							<address>
								<input type="text" name="txtFrom_Date" id="txtFrom_Date" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy"> 
							</address>
						</div>
				
						<div class="col-sm-2 invoice-col">
							Date To <span style="font-size:10px; font-style:italic;">  (financial year)</span>
							<address>
								<input type="text" name="txtTo_Date" id="txtTo_Date" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy"> 
							</address>
						</div>
						<div class="col-sm-2 invoice-col">
							Remove Zeros
							<address>
								<input type="checkbox" name="checkbox" id="checkbox" class="minimal" /><!-- /.col -->
							</address>
						</div>
                        <div class="col-xs-2">
                             Business Type
                             <select name="selBType" id="selBType" class="form-control" >
                               <?php echo $obj->getBusinessTypeList1($selBType);?>
                              </select>                  
                        </div><!-- /.col -->
						<div class="col-sm-2 invoice-col">
							&nbsp;
							<address>
								<button type="button" class="btn btn-primary btn-flat" onClick="getData();" >Search</button>
							</address>
						</div><!-- /.col -->
						<div class="col-sm-2 invoice-col">
							&nbsp;
							<address>
								&nbsp;
							</address>
						</div><!-- /.col -->
					</div>
					<div align="right">	
                        <a href="#" title="Pdf" onClick="getExcel();"><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i>Generate Excel</button></a>
                        <a href="#" title="Pdf" onClick="getPdf();"><button class="btn btn-default" style="margin-right: 5px;" type="button"><i class="fa fa-download"></i>Generate PDF</button></a>
					</div>
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
						<div id="DivOwner" class="box-body table-responsive">
							<table id="df_list"  class="table table-bordered table-striped" >
								<thead>
								<tr>
                                    <th align="left" valign="top" width="9%">Sr. No</th>
                                    <th align="left" valign="top" width="9%">Nom ID</th>
                                    <th align="left" valign="top" width="9%">Vessel Name</th>
                                    <th align="left" valign="top" width="9%">CP Date</th>	
                                    <th align="left" valign="top" width="9%">Shipper</th>
                                    <th align="left" valign="top" width="9%">Load Port</th>
                                    <th align="left" valign="top" width="9%">Discharge Port</th>
                                    <th align="left" valign="top" width="9%">Contract Qty (MT)</th>
                                    <th align="left" valign="top" width="9%">Dead freight Qty</th>	
                                    <th align="left" valign="top" width="10%">Dead freight Amount</th>
								</tr>
								</thead>
									<tbody>
									</tbody>
							</table>
						</div>
					</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
	$('#txtFrom_Date,#txtTo_Date').datepicker({
		format: 'dd-mm-yyyy',
		autoclose: true
	});
	
	$("#df_list").dataTable();
	
});

function getData()
{
	var chk;
	if($("#checkbox").attr('checked'))
	{chk = 1;}else{chk = 0;}
	    var table = $('#df_list').dataTable();
	    if(table != null)table.fnDestroy();
	if($("#txtFrom_Date").val() != "" && $("#txtTo_Date").val() != "")
	{
		$('#DivOwner').html('<span style="text-align:center;display:block;"><img src="../../img/loading.gif" /></span>');
		$.post("options.php?id=15",{txtFrom_Date:""+$("#txtFrom_Date").val()+"",txtTo_Date:""+$("#txtTo_Date").val()+"",chk:""+chk+"",selBType:""+$("#selBType").val()+""}, function(html) {
			$('#DivOwner').empty();
			$('#DivOwner').append(html);
			$("#df_list").dataTable();
		});
	}else{
		jAlert('Please select Date From & Date To', 'Alert Dialog');
	}
}

function getPdf()
{
	var chk;
	if($("#checkbox").attr('checked'))
	{
		chk = 1;
	}else{
		chk = 0;
	}
	location.href='allPdf.php?id=34&txtFrom_Date='+$("#txtFrom_Date").val()+'&txtTo_Date='+$("#txtTo_Date").val()+'&chk='+chk+'&selBType='+$("#selBType").val();
}

function getExcel()
{
	var chk;
	if($("#checkbox").attr('checked'))
	{chk = 1;}else{chk = 0;}
	location.href='allExcel.php?id=14&txtFrom_Date='+$("#txtFrom_Date").val()+'&txtTo_Date='+$("#txtTo_Date").val()+'&chk='+chk+'&selBType='+$("#selBType").val();
}
</script>
		
</body>
</html>
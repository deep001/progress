<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_tc.php";}else if($page == 2){$page_link = "vessel_in_post_tc.php";}else {$page_link = "vessel_in_history_tc.php";}

$id = $obj->getLatestCostSheetIDTC($comid);
$obj->viewTCEstimationTempleteRecordsNew($id);
$result = mysql_query("select MESSAGE from chartering_estimate_tc_compare where COMID='".$comid."'") or die;
$rows1 = mysql_fetch_assoc($result);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
					<?php if($page == 1){$page_link = "in_ops_tc.php";}else if($page == 2){$page_link = "vessel_in_post_tc.php";}else {$page_link = "vessel_in_history_tc.php";}?>
					
					<div align="right">
						<a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
					<div style="height:10px;">&nbsp;</div>
					
				<div class="box box-primary">
					<h3 style=" text-align:center;">Payment / Invoice Grid : <?php echo $obj->getVesselIMOData($obj->getFun2(),"VESSEL_NAME");?></h3>
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>					 
						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Operational Costs (Others)</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="50%">Name</th>
											<th width="25%">Vendor</th>
											<th width="25%">&nbsp;</th>											
										</tr>
									</thead>
									<tbody>
										<?php 
										$sql_brok = "select * from chartering_estimate_tc_slave5 where TCOUTID='".$obj->getFun1()."' ";
										
										$res_brok = mysql_query($sql_brok);
										$num_brok = mysql_num_rows($res_brok);
										while($rows_brok = mysql_fetch_assoc($res_brok))
										 {
										$id = '1,Brokerage Commission,0,0,'.$rows_brok['VENDOR'].','.$comid.','.$rows_brok["BROKERAGE_AMT"];
										?>
										<tr>
											<td>Brokerage Commission <span style="font-size:10px; font-style:italic;">(%)</span></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($rows_brok['VENDOR']);?></td>
											<td>
												<?php if($obj->getVendorListNewBasedOnID($rows_brok['VENDOR']) != '') {?>
													<a href="request_port_costtc.php?senddata=<?php echo $id;?>&name=Operational Costs(Brokerage Commission)&page=<?php echo $page;?>" ><button type="button" class="btn btn-info btn-flat">Payment</button></a>
												<?php }?>
											</td>
										</tr>
										<?php } ?>
										
										<?php 
										$sql4 = "select * from chartering_estimate_tc_slave4 where TCOUTID='".$obj->getFun1()."' ";
										$res4 = mysql_query($sql4);
										$rec4 = mysql_num_rows($res4);$i=0;
										while($rows4 = mysql_fetch_assoc($res4))
										{$i= $i +1;
											$id = '2,Operational Costs,'.$rows4['OPID'].',0,'.$rows4['VENDORID'].','.$comid.','.$rows4['AMOUNT'];
										?>
										<tr>
											<td><?php echo $obj->getOwnerRelatedCostNameBasedOnID($rows4['OPID']);?></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($rows4['VENDORID']);?></td>
											<td><?php if($obj->getVendorListNewBasedOnID($rows4['VENDORID']) != '') {?>
												<a href="request_port_costtc.php?senddata=<?php echo $id;?>&name=Operational Costs(<?php echo $obj->getOwnerRelatedCostNameBasedOnID($rows4['OPID']);?>)&page=<?php echo $page;?>" ><button class="btn btn-info btn-flat" type="button">Payment</button></a>
											<?php }?>
											</td>
										</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
						</div>						
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Port Costs</h3>
							</div><!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-striped">
									<thead>
										<tr>
											<th width="50%">Name</th>
											<th width="25%">Vendor</th>
											<th width="25%">&nbsp;</th>											
										</tr>
									</thead>
									<tbody>
									<?php 
									$mysql = "select * from chartering_estimate_tc_slave1 where TCOUTID='".$obj->getFun1()."' order by TC_SLAVE1ID";
								
									$myres = mysql_query($mysql) or die($mysql);
									$myrec = mysql_num_rows($myres);
									if($myrec > 0)
									{$i=1;
										while($myrows = mysql_fetch_assoc($myres))
										{
											if($myrows['PORT_COSTLP_VENDOR']!=NULL && $myrows['PORT_COSTLP_VENDOR']!='')
											{
												$id = '3,Load Port Costs,'.$myrows['FROM_PORT'].','.$myrows['RANDOMID'].','.$myrows['PORT_COSTLP_VENDOR'].','.$comid.','.$myrows['LOAD_PORT_COST'];
												?>
												<tr>
												<td>Load Port  <?php echo $obj->getPortNameBasedOnID($myrows['FROM_PORT']);?></td>
												<td><?php echo $obj->getVendorListNewBasedOnID($myrows['PORT_COSTLP_VENDOR']);?></td>
												<td>
												<?php if( $obj->getVendorListNewBasedOnID($myrows['PORT_COSTLP_VENDOR']) != '') {?>
												<a href="request_port_costtc.php?senddata=<?php echo $id;?>&name=Load Port <?php echo $obj->getPortNameBasedOnID($myrows['FROM_PORT']);?>&page=<?php echo $page;?>" ><button class="btn btn-info btn-flat" type="button">Payment</button></a>
												<?php }?>
												</td>
												</tr>
												<?php 
											}
											if($myrows['PORT_COSTDP_VENDOR']!=NULL && $myrows['PORT_COSTDP_VENDOR']!='')
											{
												$id = '3,Discharge Port Costs,'.$myrows['TO_PORT'].','.$myrows['RANDOMID'].','.$myrows['PORT_COSTDP_VENDOR'].','.$comid.','.$myrows['DISC_PORT_COST'];
												?>
												<tr>
												<td>Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows['TO_PORT']);?></td>
												<td><?php echo $obj->getVendorListNewBasedOnID($myrows['PORT_COSTDP_VENDOR']);?></td>
												<td>
												<?php if( $obj->getVendorListNewBasedOnID($myrows['PORT_COSTDP_VENDOR']) != '') {?>
												<a href="request_port_costtc.php?senddata=<?php echo $id;?>&name=Discharge Port <?php echo $obj->getPortNameBasedOnID($myrows['FROM_PORT']);?>&page=<?php echo $page;?>" ><button class="btn btn-info btn-flat" type="button">Payment</button></a>
												<?php }?>
												</td>
												</tr>
											    <?php 
											}
											
											if($myrows['PORT_COSTTP_VENDOR']!=NULL && $myrows['PORT_COSTTP_VENDOR']!='')
											{
												$id = '3,Transit Port Costs,'.$myrows['TO_PORT'].','.$myrows['RANDOMID'].','.$myrows['PORT_COSTTP_VENDOR'].','.$comid.','.$myrows['DISC_PORT_COST'];
											?>
											<tr>
											<td>Transit Port  <?php echo $obj->getPortNameBasedOnID($myrows['TO_PORT']);?></td>
											<td><?php echo $obj->getVendorListNewBasedOnID($myrows['PORT_COSTTP_VENDOR']);?></td>
											<td>
											<?php if( $obj->getVendorListNewBasedOnID($myrows['PORT_COSTTP_VENDOR']) != '') {?>
											<a href="request_port_costtc.php?senddata=<?php echo $id;?>&name=Transit Port <?php echo $obj->getPortNameBasedOnID($myrows['FROM_PORT']);?>&page=<?php echo $page;?>" ><button class="btn btn-info btn-flat" type="button">Payment</button></a>
											<?php }?>
											</td>
											</tr>
                                            <?php }?>
											<?php 
											$i++;}?>
									<?php }?>
									</tbody>
								</table>
							</div>
						</div>
                    </form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
		
</body>
</html>
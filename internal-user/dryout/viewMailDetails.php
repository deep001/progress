<?php 
set_time_limit(40000);
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_mailinbox.inc.php");
$objemaildata = new emaildata();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
$msgdata = $objemaildata->getMailDetails($_REQUEST['msgid'],$_REQUEST['mailtype']);
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>Gmail</title><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="google" value="notranslate"><meta name="application-name" content="Gmail"><meta name="description" content="Google&#39;s approach to email"><meta name="application-url" content="https://mail.google.com/mail/u/0"><meta name="google" content="notranslate">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
table {border-collapse:separate;}
select{height:19px;}
.select {
    font-family: Verdana, Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-weight: bold;
    color: #333333;
    /* color: #FBC763; */
    text-decoration: none;
    line-height: 13px;
	height:13px;
}

.input-text {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	font-weight: normal;
	color: #333333;
	text-decoration: none;
	/*text-transform:uppercase;
	line-height: 15px;*/
}

.input-textsmall {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-weight: normal;
	color: #333333;
	text-decoration:none;
	text-transform:uppercase;
}


.input 
{
font-family: Verdana, Arial, Helvetica, sans-serif;
font-size: 10px;
font-weight: normal;
color: #333333;
text-decoration: none;
}



</style>
</head>
	<div id="basic-modal-content" style="display:none;" align="center">
	<table cellpadding="1" cellspacing="4" border="0" align="center">
	<tr><td align="center"><img src="../../img/loading.gif" /><br /></td></tr>
	</table>
	</div>
	
    <body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onClick="getFinalCalculation();">Calculate</a>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(9); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="glyphicon glyphicon-envelope"></i>&nbsp;Mail Inbox&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">View Mail</li>
                    </ol>
                </section>
                
                <div align="right" style="margin-top:5px;margin-bottom:5px;margin-right:5px;"><a href="mailinbox.php?mailtype=<?=$_REQUEST['mailtype'];?>"><button class="btn btn-info btn-flat">Back</button></a></div>
                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                   <div class="mailbox row">
                        <div class="col-xs-12">
                            <div class="box box-solid">
                                <div class="box-body">
                                   <div class="row">
                                        <div class="col-md-3 col-sm-4">
                                            <!-- BOXES are complex enough to move the .box-header around.
                                                 This is an example of having the box header within the box body -->
                                            <div class="box-header">
                                            </div>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li class="header"><strong>EMAIL DETAILS</strong></li>
                                                    <li class="active"><a href="#A" style="color:red;">From&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;<?= $msgdata['fromaddress'];?></a></li>
                                                     <li class="active"><a href="#A" style="color:red;">To&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $msgdata['toaddress'];?></a></li>
                                                     <li class="active"><a href="#A"> Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= date("d M Y H:i:s",strtotime($msgdata['date']));?></a></li>
                                                      <li class="active"><a href="#A"> Reply to&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $msgdata['reply_toaddress'];?></a></li>
                                                      
                                                </ul>
                                            </div>
                                        </div><!-- /.col (LEFT) -->
                                        <div class="col-md-9 col-sm-8">
                                            <h2 class="page-header">
                                                <?=$msgdata['subject'];?>        
                                            </h2>                            
                                            
                                            <div class="row">                        
                                                <div class="col-md-12" style="overflow:auto;">
                                                    <!-- The time line -->
                                                
                                                    <ul class="timeline">
                                                        <!-- timeline time label -->
                                                        <li>
                                                            <i class="fa fa-envelope bg-blue"></i>
                                                            <div class="timeline-item">
                                                                <div class="timeline-body" style="white-space:pre-line;">
                                                                    <ul style="list-style:none;">
                                                                        <?php 
																		if(!empty($msgdata['attachments']))
																		{
																			echo '<strong>Attachments</strong><br/>';
																			$i = 1;
																			foreach($msgdata['attachments'] as $attach)
																			{
																				echo '<a target="_blank" href="../../attachment/'.$attach.'"><i class="fa fa-download"></i>&nbsp;&nbsp;'.$attach.'</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;';
																			}
																			
																			echo '<br/><br/>';
																		}
																		?>
                                                                		<li><?= nl2br($msgdata['message']);?></li>
                                                                        
                                                              		</ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                
                                                        <!-- timeline time label -->
                                                        <li>
                                                            <i class="fa fa-clock-o"></i>
                                                        </li>		
                                                    </ul>
                                                </div><!-- /.col -->
                                            </div>
                                                   
                                                </div><!-- /.col -->
                                        </div>
                                        </div><!-- /.col (RIGHT) -->
                                    </div>
                                </div>
                             </div>
                          </div>
                      </div>
                
                </form>
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>

<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$(".navbar-btn").click();
});


</script>
    </body>
</html>
<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['comid'];
$gen_agency_id = $_REQUEST['gen_agency_id'];
$vendorid = $_REQUEST['vendorid'];
$page  = $_REQUEST['page'];
$tabs  = $_REQUEST['tabs'];
if($page == 1){$page_link = "in_ops_at_glance.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "vessel_in_post_ops.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "vessel_in_history.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "coa_in_ops_at_glance.php"; $page_bar="In Ops at a glance - COA";$nav=20;$subtitle = 'COA';}else if($page == 5){$page_link = "coa_in_post_ops.php"; $page_bar="In Post Ops at a glance - COA";$nav=20;$subtitle = 'COA';}else{$page_link = "coa_in_history.php"; $page_bar="Vessels in History - COA";$nav=20;$subtitle = 'COA';}

if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertLoadPortCostDetails();
	header('Location:./nominate_agent.php?comid='.$_REQUEST['comid'].'&page='.$page."&tabs=".$tabs);
 }
$cost_sheet_id = $obj->getLatestCostSheetID($comid);
$arr  = $obj->getLoadPortAndDischargePortArrBasedOnCommidAndProcessWithoutTBN($comid,$cost_sheet_id);

$pagename = basename($_SERVER['PHP_SELF'])."?comid=".$comid."&gen_agency_id=".$gen_agency_id."&page=".$page."&tabs=".$tabs;
$msg = NULL;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
if($page == 1)
{
	$linkid = 4;
}
else
{
	$linkid = 5;
}
$rights = $obj->getUserRights($uid,$moduleid,$linkid);
$rdoExchange = $obj->getLoadPortCostData($gen_agency_id,"RDO_EXCHANGE");
if($rdoExchange=='' || $rdoExchange==NULL)
{
	$rdoExchange = 1;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$('[id^=txtRemarksOperator_],[id^=txtRemarksAgent_],#txtBDetails').autosize({append: "\n"});
$("#txtERate,[id^=txtEstmdCost_],[id^=txtEstmdCostUSD_],[id^=txtActualCost_],[id^=txtActualCostUSD_],#txtAdvActualCost,#txtFinalPayment,#txtAdvRequestedCost1,#txtAdvRequestedCost2").numeric();
});

function calculateEstmdCostUSD(var1)
{
	if($("#rdoExchange1").is(":checked"))
	{
		if($("#txtERate").val() == ""){$e_rate = 0;}else{$e_rate = $("#txtERate").val();}
		if($("#txtActualCost_"+var1).val() == ""){$cost = 0;}else{$cost = $("#txtActualCost_"+var1).val();}
		
		var calc = parseFloat($cost) / parseFloat($e_rate);
		$("#txtActualCostUSD_"+var1).val(calc.toFixed(2));
		$("#txtttlActualCost").val($("[id^=txtActualCost_]").sum().toFixed(2));
	}
	$("#txtttlActualCostUSD").val($("[id^=txtActualCostUSD_]").sum().toFixed(2));
}

function calculateTTLEstmdCost()
{
	if($("#rdoExchange1").is(":checked"))
	{
		$('[id^=txtActualCost_]').each(function(index) {
			var rowid = this.id;
			var var1 = rowid.split('_')[1];
			if($("#txtERate").val() == ""){$e_rate = 0;}else{$e_rate = $("#txtERate").val();}
			if($("#txtActualCost_"+var1).val() == ""){$cost = 0;}else{$cost = $("#txtActualCost_"+var1).val();}
			
			var calc = parseFloat($cost) / parseFloat($e_rate);
			$("#txtActualCostUSD_"+var1).val(calc.toFixed(2));
		});
		$("#txtttlActualCost").val($("[id^=txtActualCost_]").sum().toFixed(2));
	}
	$("#txtttlActualCostUSD").val($("[id^=txtActualCostUSD_]").sum().toFixed(2));
}

function getValidate(var1,var2)
{
	if(var1==2)
	{
		if($("#rdoExchange1").is(":checked"))
	    {
			if($("#txtERate").val() == "" || $("#txtERate").val() == 0 || $("#txtttlActualCost").val() == "" || $("#txtttlActualCostUSD").val() == 0)
			{
				jAlert('Please fill Exchange Rate and Actual Cost.', 'Alert');
				return false;
			}
			else
			{
				jConfirm('Are you sure you want to submit this data?', 'Confirmation', function(r) 
				{
					if(r)
					{
						$("#txtSubmitid").val(var1);
						$("#upstatus").val(var2);
						document.frm1.submit();
					} 
					else 
					{
						return false;
					}
				});
			}
		}
		else
		{
			if($("#txtttlActualCost").val() == "" || $("#txtttlActualCostUSD").val() == 0)
			{
				jAlert('Please fill Actual Cost.', 'Alert');
				return false;
			}
			else
			{
				jConfirm('Are you sure you want to submit this data?', 'Confirmation', function(r) 
				{
					if(r)
					{
						$("#txtSubmitid").val(var1);
						$("#upstatus").val(var2);
						document.frm1.submit();
					} 
					else 
					{
						return false;
					}
				});
			}
		}
	}
	else
	{
		jConfirm('Are you sure you want to submit this data?', 'Confirmation', function(r) 
		{
			if(r)
			{
				$("#txtSubmitid").val(var1);
				$("#upstatus").val(var2);
				document.frm1.submit();
			} 
			else 
			{
				return false;
			}
		});
	}
}

</script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                       <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                         <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<?php 
					if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
							<div class="alert alert-success alert-dismissable">
								<i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<b>Congratulations!</b> Agency Letter Generation added/updated successfully.
							</div>
						<?php }?>
						<?php if($msg == 1){?>
						<div class="alert alert-success alert-dismissable">
							<i class="fa fa-ban"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<b>Sorry!</b> this agent is already exists for this port.
						</div>
						<?php }?>
				<?php }?>
				<!--   content put here..................-->
				<div align="right"><a href="<?php echo $page_link;?>?comid=<?php echo $comid;?>&page=<?php echo $page;?>&tabs=<?php echo $tabs;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
				<?php 
				$mysql = "select * from generate_agency_letter where COMID='".$comid."' and MODULEID='".$_SESSION['moduleid']."' AND MCOMPANYID='".$_SESSION['company']."' and GEN_AGENCY_ID='".$gen_agency_id."' and STATUS=2";
				$myres = mysql_query($mysql);
				$myrec = mysql_num_rows($myres);
				$myrows  = mysql_fetch_assoc($myres);
				?>			
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header" align="center">
                             <?php echo $obj->getAgencyLetterData($gen_agency_id,"PORT")." - ".$obj->getPortNameBasedOnID($obj->getAgencyLetterData($gen_agency_id,"PORTID"));?> Port Costs  
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Date
                            <address>
                               <?php if(date("d-M-Y",strtotime($obj->getLoadPortCostData($gen_agency_id,"DATE"))) == "01-Jan-1970"){?>
								<input type="text" name="txtDate" id="txtDate" class="form-control" readonly value="<?php echo date("d-M-Y",time());?>" />
								<?php }else{?>
								<input type="text" name="txtDate" id="txtDate" class="form-control" readonly value="<?php echo date("d-M-Y",strtotime($obj->getLoadPortCostData($_SESSION['uid'],"DATE")));?>" />
								<?php }?>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Agent
                            <address>
                                <?php echo strtoupper($obj->getVendorListNewBasedOnID($vendorid));?>
                            </address>
                        </div><!-- /.col -->
                      </div>
					
					<div class="row invoice-info">
						 <div class="col-sm-4 invoice-col">
                           Vessel
                            <address>
                                <input type="text" name="txtVessel" id="txtVessel" class="form-control" readonly value="<?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?>" />
                            </address>
                        </div><!-- /.col -->
                       
                        <div class="col-sm-4 invoice-col">
                           Port
                            <address>
                                <input type="text" name="txtVoyage" id="txtVoyage" class="form-control" readonly value="<?php echo $obj->getPortNameBasedOnID($obj->getAgencyLetterData($gen_agency_id,"PORTID"));?>" />
                            </address>
                        </div><!-- /.col -->
					</div>
					
					<div class="row invoice-info">
						 <div class="col-sm-4 invoice-col">
                           Country
                            <address>
                                <?php echo $obj->getCountryNameBasedOnID($obj->getAgencyLetterData($gen_agency_id,"COUNTRY_ID"));?>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Currency
                            <address>
                              <?php echo $obj->getCountryData($obj->getAgencyLetterData($gen_agency_id,"COUNTRY_ID"),"CURRENCY_NAME");?>
                            </address>
                        </div><!-- /.col -->
                        
					</div>
					
					<div class="row invoice-info">
                         <div class="col-sm-4 invoice-col"><br/>
                        	<input name="rdoExchange" id="rdoExchange1" type="radio" style="cursor:pointer;" value="1" class="input-text"  <?php if($rdoExchange == 1) echo "checked"; ?>/>&nbsp;&nbsp;Local Currency&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="rdoExchange" id="rdoExchange2" type="radio" style="cursor:pointer;" value="2" class="input-text"  <?php if($rdoExchange ==2) echo "checked"; ?>/>&nbsp;&nbsp;USD&nbsp;&nbsp;
                        </div>
                        <div class="col-sm-4 invoice-col">
                           Exchange Rate <span style="font-size:10px; font-style:italic;">( Local to USD )</span>
                            <address>
                              <?php if($obj->getLoadPortCostData($gen_agency_id,"FINAL_EXCHANGE_RATE") == 0){?>
                               <?php if($obj->getLoadPortCostData($gen_agency_id,"EXCHANGE_RATE")==0){$exchangerate ='';}else{$exchangerate =$obj->getLoadPortCostData($gen_agency_id,"EXCHANGE_RATE");}?>
								<input type="text" name="txtERate" id="txtERate" onKeyUp="calculateTTLEstmdCost();" class="form-control" readonly value="<?php echo $exchangerate?>" />
								<?php }else{?>
                                <?php if($obj->getLoadPortCostData($gen_agency_id,"FINAL_EXCHANGE_RATE")==0){$exchangerate ='';}else{$exchangerate =$obj->getLoadPortCostData($gen_agency_id,"FINAL_EXCHANGE_RATE");}?>
								<input type="text" name="txtERate" id="txtERate" onKeyUp="calculateTTLEstmdCost();" class="form-control" autocomplete="off" value="<?php echo $exchangerate;?>" />
								<?php }?>
                            </address>
                        </div><!-- /.col -->
						 <div class="col-sm-4 invoice-col" style="color:#dc631e;">
                          Initial Exchange Rate
                            <address>
                                <?php if($obj->getLoadPortCostData($gen_agency_id,"EXCHANGE_RATE")==0){echo '';}else{echo $obj->getLoadPortCostData($gen_agency_id,"EXCHANGE_RATE");}?>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-8 invoice-col"></div><!-- /.col -->
                       </div>
					
					<div>
					<?php echo $obj->getPDAPortCostsBasedOnCountryID($obj->getAgencyLetterData($gen_agency_id,"COUNTRY_ID"),$gen_agency_id);?>
					</div>
                   
					<div class="row invoice-info">
						 <div class="col-sm-4 invoice-col">
                           Bank Details <span style="font-size:9px; font-style:italic;">( Agency )</span>
                            <address>
                                <textarea class="form-control areasize" name="txtBDetails" id="txtBDetails"><?php echo $obj->getLoadPortCostData($gen_agency_id,"BANK_DETAILS")?></textarea>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Prepared By <span style="font-size:9px; font-style:italic;">( Agency )</span>
                            <address>
                             <input type="text" name="txtPreparedBy" id="txtPreparedBy" class="form-control" autocomplete="off" value="<?php echo $obj->getLoadPortCostData($gen_agency_id,"PREPARED_BY")?>" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Authorized By <span style="font-size:9px; font-style:italic;">( Agency )</span>
                            <address>
                               <input type="text" name="txtAuthorizedBy" id="txtAuthorizedBy" class="form-control" autocomplete="off" value="<?php echo $obj->getLoadPortCostData($gen_agency_id,"AUTHORIZED_BY")?>" />
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    <div class="row invoice-info">
						 <div class="col-sm-4 invoice-col">
                           Agent's Remarks
                            <address>
                                <textarea class="form-control areasize" name="txtAgentRemarks" id="txtAgentRemarks" placeholder="Agent's Remarks" disabled ><?php echo $obj->getLoadPortCostData($gen_agency_id,"AGENT_REMARKS")?></textarea>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Operator's Remarks
                            <address>
                             <textarea class="form-control areasize" name="txtOperatorRemarks" id="txtOperatorRemarks" placeholder="Operator's Remarks" ><?php echo $obj->getLoadPortCostData($gen_agency_id,"OPERATOR_REMARKS")?></textarea>
                            </address>
                        </div><!-- /.col -->
					</div>
				    
                    <div class="box-footer" align="right">
					<?php if($obj->getLoadPortCostData($gen_agency_id,"STATUS") == 1 && $obj->getLoadPortCostData($gen_agency_id,"SUBMITID") != 2){ ?>
                        <button type="button" class="btn btn-primary btn-flat" onClick="return getValidate(0,1);">Submit</button>&nbsp;&nbsp;<button class="btn btn-primary btn-flat" id="inner-login-button" type="button"  onclick="return getValidate(0,2);">Submit & Close</button>
                    <?php }?>
                   
					<?php if( $obj->getLoadPortCostData($gen_agency_id,"STATUS") == 3 && $obj->getLoadPortCostData($gen_agency_id,"SUBMITID") != 2){?>
							<button type="button" class="btn btn-primary btn-flat" onClick="return getValidate(1,3);">Submit</button>
					&nbsp;&nbsp;<button class="btn btn-primary btn-flat" id="inner-login-button" type="button"  onclick="return getValidate(2,3);">Submit & Close</button>
                    <?php } ?>
                    
                    <input type="hidden" name="action" value="submit" />
                    <input type="hidden" name="txtSubmitid" id="txtSubmitid" value=""/><input type="hidden" name="upstatus" id="upstatus" value="" />
					</div>
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
    </body>
</html>
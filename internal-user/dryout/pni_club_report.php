<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(7,2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-bar-chart-o"></i>&nbsp;Reports&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li>Reports</li>
						<li>Operations</li>
						<li class="active">PNI Club Declaration Report</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div class="box box-primary">
				<h3 style=" text-align:center;">PNI Club Declaration Report</h3>
				<div style="height:10px;"></div>
					<div class="row invoice-info" style="margin-left:5px;margin-right:5px;">
						<div class="col-sm-3 invoice-col">
							Date From <span style="font-size:10px; font-style:italic;">  (financial year)</span>
							<address>
								<input type="text" name="txtFrom_Date" id="txtFrom_Date" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy"> 
							</address>
						</div>
				
						<div class="col-sm-3 invoice-col">
							Date To <span style="font-size:10px; font-style:italic;">  (financial year)</span>
							<address>
								<input type="text" name="txtTo_Date" id="txtTo_Date" class="form-control" autocomplete="off" placeholder="dd-mm-yyyy"> 
							</address>
						</div>
						<div class="col-sm-2 invoice-col">
							Remove Zeros
								<address>
							<input type="checkbox" name="checkbox" id="checkbox" class="minimal" /><!-- /.col -->
							</address>
						</div>
						<div class="col-sm-2 invoice-col">
						&nbsp;
							<address>
							  <button type="button" class="btn btn-primary btn-flat" onClick="getData();" >Search</button>
							</address>
						</div><!-- /.col -->
					</div>
					<div align="right">
						<a href="#?" title="Excel" onClick="getExcel();"><img src="../../img/excel.gif" height="16" width="16"></a>
						<a href="#?" title="Pdf" onClick="getPdf();"><img src="../../img/pdf_button.png" width="25" height="25"></a>
					</div>
					
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
						<div class="box-body table-responsive">
							<table width="100%" cellpadding="1" cellspacing="1" border="0"  class="table table-bordered table-striped" id="pni_list">
								<thead>
									<tr valign="top">
										<th align="left" valign="top">Sr. No</th>
										<th align="left" valign="top">Nom ID</th>
										<th align="left" valign="top">Vessel Name</th>
										<th align="left" valign="top">CP Date</th>	
										<th align="left" valign="top">Material Code</th>
										<th align="left" valign="top">PNI Club Vendor</th>
										<th align="right" valign="top">Amount</th>
										<th align="right" valign="top">Paid</th>
										<th align="right" valign="top">Balance</th>
									</tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
						</div>
					</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script type="text/javascript">
$(document).ready(function(){

$('#txtFrom_Date,#txtTo_Date').datepicker({
	format: 'dd-mm-yyyy',
	autoclose: true
});
	$("#pni_list").dataTable();

});

function getData()
{
	var chk;
	if($("#checkbox").attr('checked'))
	{
		chk = 1;
	}else{
		chk = 0;
	}
	
	if($("#txtFrom_Date").val() != "" && $("#txtTo_Date").val() != "")
	{
		$('#pni_list tbody').html('<tr><td align="center" colspan="9"><img src="../../img/loading.gif" /></td></tr>');
		$.post("options.php?id=22",{txtFrom_Date:""+$("#txtFrom_Date").val()+"",txtTo_Date:""+$("#txtTo_Date").val()+"",chk:""+chk+""}, function(html) {
			$('#pni_list tbody').empty();
			$('#pni_list tbody').append(html);
		});
	}else{
		jAlert('Please select Date From & Date To', 'Alert Dialog');
	}
}

function getPdf()
{
	var chk;
	if($("#checkbox").attr('checked'))
	{chk = 1;}else{chk = 0;}
	
	location.href='allPdf.php?id=33&txtFrom_Date='+$("#txtFrom_Date").val()+'&txtTo_Date='+$("#txtTo_Date").val()+'&chk='+chk;
}

function getExcel()
{	
	var chk;
	if($("#checkbox").attr('checked'))
	{chk = 1;}else{chk = 0;}
	
	location.href='allExcel.php?id=13&txtFrom_Date='+$("#txtFrom_Date").val()+'&txtTo_Date='+$("#txtTo_Date").val()+'&chk='+chk;
}
 
</script>
		
</body>
</html>
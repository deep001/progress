<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$mappingid = $_REQUEST['mappingid'];

if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->FixedMapping();
	header('Location:./nomination_at_glance.php?msg='.$msg);
}
$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid;

$uid	  = $_SESSION['uid'];
$moduleid = $_SESSION['moduleid'];
$rights = $obj->getUserRights($uid,$moduleid,3);

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Nominations at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="nomination_at_glance.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             Convert to Fixture
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
                    
                    
                    <div class="row invoice-info" >
                        <div class="col-sm-4 invoice-col">
							Nom ID
                            <address>
                              <input type="text" name="txtNomID" id="txtNomID" value="" class="form-control" placeholder="File Name" autocomplete="off" />
                            </address>
                        </div><!-- /.col -->
                 
						<?php if($rights == 1){ ?>
						<div class="col-sm-4 invoice-col">
                        &nbsp;
                            <address><input type="hidden" name="action" id="action" value="submit" />
							<button type="submit" class="btn btn-primary btn-flat">Convert to Fixture</button>
				           </address>
                        </div><!-- /.col -->
						<?php } ?>
					</div>               
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){ 
$("#frm1").validate({
	rules: {
		txtNomID:{required: true,remote:"options.php?id=11"}
	},
	
	messages: {	
		txtNomID:{required: "*",remote:"Already Exist."}
	}
});
});
</script>
    </body>
</html>
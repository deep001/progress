<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$mappingid = $_REQUEST['mappingid'];
$cost_sheet_id = $_REQUEST['cost_sheet_id'];

if (@$_REQUEST['action'] == 'submit')
 { 
 	$msg = $obj->updateTCIDetails();
	header('Location:./nomination_at_glance.php?msg='.$msg);
 }

$obj->viewFreightEstimationRecords($mappingid,$cost_sheet_id);
$rdoMMarket = $obj->getFun8();
$rdoDWT = $obj->getFun7();
$rdoQty = $obj->getFun28();
$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid.'&cost_sheet_id='.$cost_sheet_id;


$b_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"B_FULL_SPEED");
if($b_full_speed == "" ){$bfs = 0;}else{$bfs = $b_full_speed;}
$b_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"B_ECO_SPEED1");
if($b_ech_speed1 == "" ){$bes1 = 0;}else{$bes1 = $b_ech_speed1;}
$b_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"B_ECO_SPEED2");
if($b_ech_speed2 == "" ){$bes2 = 0;}else{$bes2 = $b_ech_speed2;}
$fo_b_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_FO_FULL_SPEED");
if($fo_b_full_speed == ""){$fo_bfs = 0;}else{$fo_bfs = $fo_b_full_speed;}
$fo_b_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_FO_ECO_SPEED1");
if($fo_b_ech_speed1 == ""){$fo_bes1 = 0;}else{$fo_bes1 = $fo_b_ech_speed1;}
$fo_b_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_FO_ECO_SPEED2");
if($fo_b_ech_speed2 == ""){$fo_bes2 = 0;}else{$fo_bes2 = $fo_b_ech_speed2;}
$do_b_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_DO_FULL_SPEED");
if($do_b_full_speed == ""){$do_bfs = 0;}else{$do_bfs = $do_b_full_speed;}
$do_b_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_DO_ECO_SPEED1");
if($do_b_ech_speed1 == ""){$do_bes1 = 0;}else{$do_bes1 = $do_b_ech_speed1;}
$do_b_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BP_DO_ECO_SPEED2");
if($do_b_ech_speed2 == ""){$do_bes2 = 0;}else{$do_bes2 = $do_b_ech_speed2;}


$l_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"L_FULL_SPEED");
if($l_full_speed == "" ){$lfs = 0;}else{$lfs = $l_full_speed;}
$l_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"L_ECO_SPEED1");
if($l_ech_speed1 == "" ){$les1 = 0;}else{$les1 = $l_ech_speed1;}
$l_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"L_ECO_SPEED2");
if($l_ech_speed2 == "" ){$les2 = 0;}else{$les2 = $l_ech_speed2;}
$fo_l_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_FO_FULL_SPEED");
if($fo_l_full_speed == ""){$fo_lfs = 0;}else{$fo_lfs = $fo_l_full_speed;}
$fo_l_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_FO_ECO_SPEED1");
if($fo_l_ech_speed1 == ""){$fo_les1 = 0;}else{$fo_les1 = $fo_l_ech_speed1;}
$fo_l_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_FO_ECO_SPEED2");
if($fo_l_ech_speed2 == ""){$fo_les2 = 0;}else{$fo_les2 = $fo_l_ech_speed2;}
$do_l_full_speed = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_DO_FULL_SPEED");
if($do_l_full_speed == ""){$do_lfs = 0;}else{$do_lfs = $do_l_full_speed;}
$do_l_ech_speed1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_DO_ECO_SPEED1");
if($do_l_ech_speed1 == ""){$do_les1 = 0;}else{$do_les1 = $do_l_ech_speed1;}
$do_l_ech_speed2 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"LP_DO_ECO_SPEED2");
if($do_l_ech_speed2 == ""){$do_les2 = 0;}else{$do_les2 = $do_l_ech_speed2;}

$fo_inport_idle = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PI_FO_FULL_SPEED");
if($fo_inport_idle == ""){$foidle = 0;}else{$foidle = $fo_inport_idle;}
$fo_inport_wrking = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PW_FO_FULL_SPEED");
if($fo_inport_wrking == ""){$fowrking = 0;}else{$fowrking = $fo_inport_wrking;}

$do_inport_idle = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PI_DO_FULL_SPEED");
if($do_inport_idle == ""){$doidle = 0;}else{$doidle = $do_inport_idle;}
$do_inport_wrking = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"PW_DO_FULL_SPEED");
if($do_inport_wrking == ""){$dowrking = 0;}else{$dowrking = $do_inport_wrking;}

$submitid1 = $obj->getVesselParameterDataNominationWise($mappingid,$obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUBMITID");
if($submitid1 == ""){$submitid = 0;}else{$submitid = $submitid1;}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
</style>
</head>
    <body class="skin-blue fixed">
		<a href="#" id="trigger1" title="Calculate" onclick="getFinalCalculation();">Calculate</a>
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Nominations at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="nomination_at_glance.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
						
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             Voyage Financials : Estimate
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
				
                        <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Fixture Type
                                    <address>
                                    <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFreightFixtureBasedOnID($obj->getFun2());?></strong>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     Main Particulars
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                   Nom ID
                                    <address>
                                    <input type="text" name="txtNomID" id="txtNomID" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getMappingData($mappingid,"NOMINATION_ID");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                     Vessel Name
                                    <address>
                                       <input type="text" name="txtVName" id="txtVName" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?>" />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                     Vessel Type
                                    <address>
                                       <input type="text" name="txtVType" id="txtVType" class="form-control" autocomplete="off" placeholder="Vessel Type" readonly value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_TYPE"));?>" />
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                   Date
                                    <address>
                                    <input type="text" name="txtDate" id="txtDate" class="form-control" autocomplete="off" placeholder="Date (for financial year)" value="<?php if($obj->getFun3() == ""){ echo "dd-mm-yy"; }else{ echo date("d-m-Y",strtotime($obj->getFun3())); } ?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                     Voyage No.
                                    <address>
                                        <input type="text" name="txtVNo" id="txtVNo" class="form-control" autocomplete="off" placeholder="Voyage No." value="<?php echo $obj->getFun10();?>" />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                     Cost Sheet Name
                                    <address>
                                        <input type="text" name="txtENo" id="txtENo" class="form-control" autocomplete="off" value="<?php echo $obj->getCostSheetNameBasedOnID($cost_sheet_id);?>" placeholder="Cost Sheet Name" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                            
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  
                                    <address>
                                   <input name="rdoDWT" class="checkbox" id="rdoDWT1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoDWT == 1) echo "checked"; ?> onClick="showDWTField();"  />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    
                                    <address>
                                     <input name="rdoDWT" class="checkbox" id="rdoDWT2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoDWT == 2) echo "checked"; ?> onClick="showDWTField();" />
                                    </address>
                                </div><!-- /.col -->
                               
                             </div>
                            
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>
                                    <address>
                                   <input type="text" name="txtDWTS" id="txtDWTS"  class="form-control" autocomplete="off" placeholder="DWT" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUMMER_1");?>"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                  DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>
                                    <address>
                                        <input type="text" name="txtDWTT" id="txtDWTT" class="form-control" autocomplete="off" placeholder="DWT" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"TROPICAL_1");?>" ><input type="hidden" name="txtTCNo" id="txtTCNo" class="input-text" size="10" autocomplete="off" value="<?php echo $obj->getFun11();?>" />
                             </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                  
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  <input name="rdoCap" class="checkbox" id="rdoCap1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoCap == 1) echo "checked"; ?> onClick="showCapField();"  />
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <input name="rdoCap" class="checkbox" id="rdoCap2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoCap == 2) echo "checked"; ?> onClick="showCapField();" />
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                   
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Grain Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
                                    <address>
                                    <input type="text" name="txtGCap" id="txtGCap" class="form-control" autocomplete="off" placeholder="Grain Cap" value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"GRAIN");?>" disabled/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                   Bale Cap.<span style="font-size:10px; font-style:italic;">(CBM)</span>
                                    <address>
                                        <input type="text" name="txtBCap" id="txtBCap" class="form-control" autocomplete="off" placeholder="Bale Cap" value="<?php echo $obj->getVesselMaster8Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"BALE");?>" disabled />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                  SF <span style="font-size:10px; font-style:italic;">(CBM/MT)</span>
                                   <address>
                                        <input type="text" name="txtSF" id="txtSF" class="form-control" placeholder="SF" autocomplete="off" value="<?php echo $obj->getFun9();?>"  onkeyup="getTotalDWT(),getTotalDWT1()"/>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 &nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    &nbsp;
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                  Loadable <span style="font-size:10px; font-style:italic;">(MT)</span>
                                   <address>
                                        <input type="text" name="txtLoadable" id="txtLoadable" class="form-control" placeholder="Loadable" readonly autocomplete="off" value="<?php echo $obj->getFun24();?>"/>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     Market
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 <address>
                                  <input name="rdoMMarket" id="rdoMMarket1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoMMarket == 1) echo "checked"; ?>  onclick="showMMarketField();"  />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                  <address>
                                       <input name="rdoMMarket" class="checkbox" id="rdoMMarket2" type="radio" style="cursor:pointer;" value="2" <?php if($rdoMMarket == 2) echo "checked"; ?> onClick="showMMarketField();" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                
                                </div><!-- /.col -->
                             </div>
                             
                        
                        
                        <div class="row invoice-info">
                              <div class="col-sm-4 invoice-col">
                                  Agreed Gross Freight <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
                                     <input type="text" name="txtMTCPDRate" id="txtMTCPDRate" class="form-control" placeholder="Agreed Gross Freight" autocomplete="off" value=""  onkeyup="getFinalCalculation();" disabled/>
                                    </address>
                               </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                   Lumpsum <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
                                     <input type="text" name="txtMLumpsum" id="txtMLumpsum" class="form-control" placeholder="Lumpsum" autocomplete="off" disabled value=""  onkeyup="getFinalCalculation();"/>
                                    </address>
                               </div><!-- /.col -->
                               <div class="col-sm-4 invoice-col">
                                   Addnl Cargo Rate <span style="font-size:10px; font-style:italic;">(USD/MT)</span>
                                    <address>
                                     <input type="text"  name="txtAddnlCRate" id="txtAddnlCRate" class="form-control" placeholder="Addnl Cargo Rate" autocomplete="off" value=""  onkeyup="getFinalCalculation();"/>
                                    </address>
                               </div><!-- /.col -->
                         </div>
                        
                        <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     Cargo
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                        <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Quantity <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                    <input type="text" name="txtCQMT" id="txtCQMT" class="form-control" placeholder="Quantity" autocomplete="off" value="<?php echo $obj->getGreaterCargoQuantity($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));?>"  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                            
                                <div class="col-sm-4 invoice-col">
                                   Cargo Type
                                    <address>
                                    <select  name="selCType" class="form-control" id="selCType">
									<?php 
                                    $obj->getCargoTypeList();
                                    ?>
                                    </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Cargo Name
                                    <address>
                                        <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCargoContarctForMapping($obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"CARGO_POSITION"));?></strong>
                                    </address>
                                </div><!-- /.col -->
                                
                            </div>
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  <span style="font-size:13px; font-style:italic; color:#dc631e">( Please put dead freight quantity / addnl quantity separately )</span>  
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                     
                                    <address>
                                        <input name="rdoQty" class="checkbox" id="rdoQty1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoQty == 1) echo "checked"; ?>  onclick="showQtyField();"  />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                    <address>
                                        <input name="rdoQty" class="checkbox" id="rdoQty2" type="radio" style="cursor:pointer;" value="2" <?php if($rdoQty == 2) echo "checked"; ?> onClick="showQtyField();" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                            
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  &nbsp;
                                    <address>
                                 <input type="hidden" name="txtAQMT" id="txtAQMT" value=""  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                     DF Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                     <input type="text" name="txtDFQMT" id="txtDFQMT" class="form-control" placeholder="DF Qty" autocomplete="off" value="0.00"  onkeyup="getFinalCalculation();"/>
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                    Addnl Qty <span style="font-size:10px; font-style:italic;">(MT)</span>
                                    <address>
                                        <input type="text" name="txtAddnlQMT" id="txtAddnlQMT" class="form-control" placeholder="Addnl Qty " autocomplete="off" readonly value="0.00"  onkeyup="getFinalCalculation();"/>
                                        <select  name="selPort" id="selPort" style="display:none;" >
										<?php 
                                        $obj->getPortList();
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                           
                           <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     Load Port
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                           
                             <div class="row invoice-info">
                                <div class="col-sm-3 invoice-col">
                                  Load Port
                                    <address>
                                     <select  name="selLoadPort" class="form-control" id="selLoadPort">
									<?php 
                                    $obj->getPortList();
                                    ?>
                                    </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                    Cargo
                                    <address>
                                       <select  name="selLPCName" class="form-control" id="selLPCName" onChange="getLOadPortQty();" >
										<?php 
                                        $obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-3 invoice-col">
                                     Qty MT
                                    <address>
                                        <input type="text" name="txtLPQMT" id="txtLPQMT" class="form-control" placeholder="Qty MT" onKeyUp="getLoadPortCalculation();" autocomplete="off" value="" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                     Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                                    <address>
                                        <input type="text" name="txtLPRMTDay" id="txtLPRMTDay" class="form-control" placeholder="Rate "  onkeyup="getLoadPortCalculation();" autocomplete="off" value="" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                               <div class="col-sm-3 invoice-col">
                                     Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
                                        <input type="text" name="txtLPPortCosts" id="txtLPPortCosts" class="form-control" placeholder="Port Costs" autocomplete="off" value="" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                    Work Days <span style="font-size:10px; font-style:italic;">(Qty/Rate)</span>
                                    <address>
                                        <input type="text" name="txtLPWorkDay" id="txtLPWorkDay" class="form-control" placeholder="Work Days" autocomplete="off" value="" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                   &nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                   &nbsp;
                                    <address>
                                    
                                        <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addLoadPortDetails()">ADD</button>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             
                             
                             
                             
                       <div class="row">
                        <div class="col-xs-12">
                          <div class="box box-primary">
							<div class="box-body no-padding">
							<table class="table table-striped">
							<?php 
                                $sql = "select * from fca_vci_load_port where FCAID='".$obj->getFun1()."'";
                                $res = mysql_query($sql);
                                $rec = mysql_num_rows($res); 
                            ?>
                            <thead>
                            <tr>
                            <th width="3%" align="center">#</th>
                            <th width="7%" align="left">Load Port</th>
                            <th width="10%" align="left">Cargo Name</th>
                            <th width="7%" align="left">Qty MT</th>
                            <th width="7%" align="left">Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
                            <th width="7%" align="left">Work Days<input type="hidden" name="load_portID" id="load_portID" class="input" size="5" value="<?php echo $rec;?>" /></th>
                            <th width="8%" align="left">Port Costs</th>
                            </tr>
                            </thead>
                            <tbody id="tblLoadPort">
                            <?php if($rec == 0){?>
                            
                            <?php }else{
                            $i=1;
                            while($rows = mysql_fetch_assoc($res))
                            {
                            ?>
                            <tr id="lp_Row_<?php echo $i;?>">
                            <td align="center" ><a href="#lp<?php echo $i;?>" onClick="removeLoadPort('<?php echo $i;?>');" ><i class="fa fa-times" style="color:red;"></i></a></td>
                            <td align="left" ><?php echo $obj->getPortNameBasedOnID($rows['LOADPORTID']);?><input type="hidden" name="txtLoadPort_<?php echo $i;?>" id="txtLoadPort_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows['LOADPORTID'];?>"/></td>
                            <td align="left" ><?php echo $obj->getCargoContarctForMapping($rows['PURCHASE_ALLOCATIONID'],$obj->getMappingData($mappingid,"CARGO_POSITION"));?><input type="hidden" name="txtLPCID_<?php echo $i;?>" id="txtLPCID_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows['PURCHASE_ALLOCATIONID'];?>"/></td>
                            <td align="left" ><?php echo $rows['QTY_MT'];?><input type="hidden" name="txtLpQMT_<?php echo $i;?>" id="txtLpQMT_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows['QTY_MT'];?>"/></td>
                            <td align="left" ><?php echo $rows['RATE'];?><input type="hidden" name="txtLpRate_<?php echo $i;?>" id="txtLpRate_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows['RATE'];?>"/></td>
                            <td align="left" ><?php echo $rows['WORK_DAYS'];?><input type="hidden" name="txtLpBLWorkDays_<?php echo $i;?>" id="txtLpBLWorkDays_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows['WORK_DAYS'];?>"/></td>
                            <td align="left" ><?php echo $rows['PORTCOSTS'];?><input type="hidden" name="txtLPPCosts_<?php echo $i;?>" id="txtLPPCosts_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows['PORTCOSTS'];?>"/></td>
                            </tr>
                            <?php $i++;} }?>
                            </tbody>
                            </table>
                         </div>
                        </div>
                       </div>
                     </div>
                            
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     Discharge Port(s)
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                           
                             <div class="row invoice-info">
                                <div class="col-sm-3 invoice-col">
                                  Discharge Port
                                    <address>
                                     <select  name="selDisPort" class="form-control" id="selDisPort">
									<?php 
                                    $obj->getPortList();
                                    ?>
                                    </select>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                    Cargo
                                    <address>
                                       <select  name="selDPCName" class="form-control" id="selDPCName" onChange="getDisPortQty();" >
										<?php 
                                        $obj->getContractIdBasedOnIDList($obj->getMappingData($mappingid,"CARGO_POSITION"),$obj->getMappingData($mappingid,"CARGO_IDS"));
                                        ?>
                                        </select>
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-3 invoice-col">
                                     Qty MT
                                    <address>
                                        <input type="text" name="txtDPQMT" id="txtDPQMT" class="form-control" placeholder="Qty MT" onKeyUp="getDisPortCalculation();" autocomplete="off" value="" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                     Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span>
                                    <address>
                                        <input type="text" name="txtDPRMTDay" id="txtDPRMTDay" class="form-control" placeholder="Rate "  onkeyup="getDisPortCalculation();" autocomplete="off" value="" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                               <div class="col-sm-3 invoice-col">
                                     Port Costs <span style="font-size:10px; font-style:italic;">(USD)</span>
                                    <address>
                                        <input type="text" name="txtDPPortCosts" id="txtDPPortCosts" class="form-control" placeholder="Port Costs" autocomplete="off" value="" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                    Work Days <span style="font-size:10px; font-style:italic;">(Qty/Rate)</span>
                                    <address>
                                        <input type="text" name="txtDPWorkDay" id="txtDPWorkDay" class="form-control" placeholder="Work Days" autocomplete="off" value="" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                   &nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-3 invoice-col">
                                   &nbsp;
                                    <address>
                                    
                                        <button class="btn btn-primary btn-flat" id="inner-login-button" type="button" onClick="addDisPortDetails()">ADD</button>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row">
                                <div class="col-xs-12">
                                  <div class="box box-primary">
                                    <div class="box-body no-padding">
                                    <table class="table table-striped">
                                    <?php 
										$sql1 = "select * from fca_vci_disch_port where FCAID='".$obj->getFun1()."'";
										$res1 = mysql_query($sql1);
										$rec1 = mysql_num_rows($res1); 
									?>
									<thead>
									<tr>
									<th width="3%" align="center">#</th>
									<th width="7%" align="left">Discharge Port</th>
									<th width="10%" align="left">Cargo Name</th>
									<th width="7%" align="left">Qty MT</th>
									<th width="7%" align="left">Rate <span style="font-size:10px; font-style:italic;">(MT/Day)</span></th>
									<th width="7%" align="left">Work Days<input type="hidden" name="dis_portID" id="dis_portID" class="input" size="5" value="<?php echo $rec1;?>" /></th>
									<th width="7%" align="left">Port Costs</th>
									</tr>
									</thead>
									<tbody id="tblDisPort">
									<?php if($rec1 == 0){?>
									
									<?php }else{
									$i=1;
									while($rows1 = mysql_fetch_assoc($res1))
									{
									?>
									<tr id="dp_Row_<?php echo $i;?>">
									<td align="center"><a href="#dp<?php echo $i;?>" onClick="removeDisPort('<?php echo $i;?>');" ><i class="fa fa-times" style="color:red;"></i></a></td>
									<td align="left"><?php echo $obj->getPortNameBasedOnID($rows1['DIS_PORT_ID']);?><input type="hidden" name="txtDisPort_<?php echo $i;?>" id="txtDisPort_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows1['DIS_PORT_ID'];?>"/></td>
									<td align="left"><?php echo $obj->getCargoContarctForMapping($rows1['PURCHASE_ALLOCID'],$obj->getMappingData($mappingid,"CARGO_POSITION"));?><input type="hidden" name="txtDPCID_<?php echo $i;?>" id="txtDPCID_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows1['PURCHASE_ALLOCID'];?>"/></td>
									<td align="left"><?php echo $rows1['QTY_MT'];?><input type="hidden" name="txtDpQMT_<?php echo $i;?>" id="txtDpQMT_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows1['QTY_MT'];?>"/></td>
									<td align="left"><?php echo $rows1['RATE'];?><input type="hidden" name="txtDpRate_<?php echo $i;?>" id="txtDpRate_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows1['RATE'];?>"/></td>
									<td align="left"><?php echo $rows1['WORK_DAYS'];?><input type="hidden" name="txtDpBLWorkDays_<?php echo $i;?>" id="txtDpBLWorkDays_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows1['WORK_DAYS'];?>"/></td>
									<td align="left"><?php echo $rows1['PORTCOSTS'];?><input type="hidden" name="txtDPPCosts_<?php echo $i;?>" id="txtDPPCosts_<?php echo $i;?>" class="input" size="10" value="<?php echo $rows1['PORTCOSTS'];?>"/></td>
									</tr>
									<?php $i++;} }?>
									</tbody>
									</table>
                                 </div>
                                </div>
                               </div>
                             </div>
                             
                             
                             
                             
                             
                             
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     Freight Adjustment
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                             
                             
                             
                             
                             <div class="box-body no-padding">
                               <table class="table table-striped">
                                <thead>
                                <tr>
                                <th width="43%"></th>
                                <th width="17%">Percent</th>
                                <th width="18%">USD</th>
                                <th width="22%">Per MT</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <td width="43%">Gross Freight</td>
                                <td ></td>
                                <td width="18%"><input type="text"  name="txtFrAdjUsdGF" id="txtFrAdjUsdGF" class="form-control" autocomplete="off" size="10" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"GROSS_FREIGHT_USD");?>" />
                                </td>
                                <td width="22%"><input type="text"  name="txtFrAdjUsdGFMT" id="txtFrAdjUsdGFMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"GROSS_FREIGHT_PERMT");?>" />
                                </td>
                                </tr>
                                <tr>
                                <td width="43%" align="left">Dead Freight</td>
                                <td ></td>
                                <td width="18%" align="center"><input type="text"  name="txtFrAdjUsdDF" id="txtFrAdjUsdDF" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DEAD_FREIGHT_USD");?>" />
                                </td>
                                <td width="22%" align="center"><input type="text"  name="txtFrAdjUsdDFMT" id="txtFrAdjUsdDFMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DEAD_FREIGHT_PERMT");?>" />
                                </td>
                                </tr>
                                <tr>
                                <td width="43%" align="left" >Addnl Freight</td>
                                <td ></td>
                                <td width="18%" align="center"><input type="text"  name="txtFrAdjUsdAF" id="txtFrAdjUsdAF" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"ADDNL_FREIGHT_USD");?>" />
                                </td>
                                <td width="22%" align="center"><input type="text"  name="txtFrAdjUsdAFMT" id="txtFrAdjUsdAFMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"ADDNL_FREIGHT_PERMT");?>" />
                                </td>
                                </tr>
                                <tr>
                                <td width="43%" align="left">Total Freight</td>
                                <td ></td>
                                <td width="18%" align="center"><input type="text"  name="txtFrAdjUsdTF" id="txtFrAdjUsdTF" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_FREIGHT_USD");?>" />
                                </td>
                                <td width="22%" align="center"><input type="text"  name="txtFrAdjUsdTFMT" id="txtFrAdjUsdTFMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_FREIGHT_PERMT");?>" />
                                </td>
                                </tr>
                                <tr>
                                <td width="43%" align="left" class="input-text"  valign="top" >Address Commission</td>
                                <td width="17%" align="left" class="input-text" valign="top"><input type="text"  name="txtFrAdjPerAC" id="txtFrAdjPerAC" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"AC_PERCENT");?>" onKeyUp="getFinalCalculation();"/></td>
                                <td width="18%" align="center" class="input-text" valign="top"><input type="text"  name="txtFrAdjUsdAC" id="txtFrAdjUsdAC" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"AC_USD");?>"  /></td>
                                <td width="22%" align="center" class="input-text" valign="top"><input type="text"  name="txtFrAdjUsdACMT" id="txtFrAdjUsdACMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"AC_PERMT");?>"  />
                                </td>
                                </tr>
                                <tr>
                                <td width="43%" align="left">Brokerage</td>
                                <td align="left"><input type="text"  name="txtFrAdjPerAgC" id="txtFrAdjPerAgC" class="form-control" autocomplete="off" value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"AGC_PERCENT");?>" onKeyUp="getFinalCalculation(),getValue()" /></td>
                                <td width="18%" align="center"><input type="text"  name="txtFrAdjUsdAgC" id="txtFrAdjUsdAgC" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"AGC_USD");?>"   />
                                <td></td>
                                </td>
                                </tr>
                                <tr>
                                <td width="43%" align="left">Net Freight Payable</td>
                                <td align="left"></td>
                                <td width="18%" align="center"><input type="text"  name="txtFrAdjUsdFP" id="txtFrAdjUsdFP" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"NETT_FRT_PAYABLE");?>"   />
                                </td>
                                <td></td>
                                </tr>
                                
                                </tbody>
                                </table>
                                </div>
                            
                             
                             
                            
                             
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     Owner Related Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                             
                             
                             
                              <div class="row invoice-info">
                               <div class="col-sm-4 invoice-col">
                                    Freight Payment
                                    <address>
                                        <input type="text"  name="txtTTLORCFP" id="txtTTLORCFP" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"FREIGHT_PAYMENT");?>"  />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Cost/MT
                                    <address>
                                       <input type="text" name="txtTTLORCFPCostMT" id="txtTTLORCFPCostMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"ORC_COSTMT");?>" />
                                    </address>
                                </div><!-- /.col -->
                              </div>
                             
                             
                             
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     All Other Shipping Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                             <div class="box-body no-padding">
                               <table class="table table-striped">
                                <thead>
                                <tr>
                                <th width="12%"></th>
                                <th width="14%">Cost</th>
                                <th width="13%">Cost/MT</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $sql2 = "select * from fca_vci_other_shipping_cost where FCAID='".$obj->getFun1()."'";
                                $res2 = mysql_query($sql2);
                                $rec2 = mysql_num_rows($res2);
                                $i=1;
                                while($rows2 = mysql_fetch_assoc($res2))
                                {
                                ?>
                                <tr>
                                <td width="35%" align="left"><?php echo $obj->getOtherShippingCostNameBasedOnID($rows2['OTHER_SCOSTID']);?><input type="hidden" name="txtHidOSCID_<?php echo $i;?>" id="txtHidOSCID_<?php echo $i;?>" value="<?php echo $rows2['OTHER_SCOSTID'];?>" /></td>
                                <td width="14%" align="left"><input type="text"  name="txtOSCAbs_<?php echo $i;?>" id="txtOSCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows2['ABSOLUTE'];?>" onKeyUp="getOSCostCalculate1(<?php echo $i;?>),getFinalCalculation()"/></td>
                                <td width="13%" align="left"><input type="text" name="txtOSCCostMT_<?php echo $i;?>" id="txtOSCCostMT_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows2['COST_MT'];?>" /></td>
                                </tr>
                                <?php $i++;}?>
                                </tbody>
                                <tfoot >
                                <tr height="10"><td colspan="3" align="left" class="text" valign="top" ><input type="hidden" name="txtAOSC_id" id="txtAOSC_id" value="<?php echo $rec2;?>" /></td></tr>
                                </tfoot>
                                </table>
                                </div>
                             
                            
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     All Other Miscellaneous Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                             <div class="box-body no-padding">
                               <table class="table table-striped">
                                <thead>
                                <tr>
                                <th width="12%"></th>
                                <th width="14%">Cost</th>
                                <th width="13%">Cost/MT</th>
                                </tr>
                                </thead>
                                <tbody>
                               <?php 
								$sql3 = "select * from fca_vci_other_misc_cost where FCAID='".$obj->getFun1()."'";
								$res3 = mysql_query($sql3);
								$rec3 = mysql_num_rows($res3);
								$i=1;
								while($rows3 = mysql_fetch_assoc($res3))
								{
								?>
								<tr>
								<td align="left"><?php echo $obj->getOtherMiscCostNameBasedOnID($rows3['OTHER_MCOSTID']);?><input type="hidden" name="txtHidOMCID_<?php echo $i;?>" id="txtHidOMCID_<?php echo $i;?>" value="<?php echo $rows3['OTHER_MCOSTID'];?>" /></td>
								<td align="left" ><input type="text"  name="txtOMCAbs_<?php echo $i;?>" id="txtOMCAbs_<?php echo $i;?>" class="form-control" autocomplete="off" value="<?php echo $rows3['AMOUNT'];?>" onKeyUp="getOMCCalculate(<?php echo $i;?>),getFinalCalculation()" /></td>
								<td align="left"><input type="text"  name="txtOMCCostMT_<?php echo $i;?>" id="txtOMCCostMT_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows3['COST_MT'];?>" /></td>
								</tr>
								<?php $i++;}?>
								</tbody>
								<tfoot>
								<tr height="10"><td colspan="3" align="left" class="text" valign="top" ><input type="hidden" name="txtAOMC_id" id="txtAOMC_id" value="<?php echo $rec3;?>" /><input type="hidden"  name="txtTTLOtherCost" id="txtTTLOtherCost" readonly value="" placeholder="0.00" /><input type="hidden"  name="txtTTLOtherCostMT" id="txtTTLOtherCostMT" value="" placeholder="0.00" /></td></tr>
								</tfoot>
                                </table>
                                </div>
                                
                                
                                
                                
                                <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Port Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                             <div class="box-body no-padding">
                               <table class="table table-striped">
                                <thead>
                                <tr>
                                <th width="12%"></th>
                                <th width="14%">Cost</th>
                                <th width="13%">Cost/MT</th>
                                </tr>
                                </thead>
                                <tbody>
                               <?php 
						$mysql = "select * from fca_vci_portcosts where FCAID='".$obj->getFun1()."' order by FCA_PORTCOSTID asc";
						$myres = mysql_query($mysql);
						$myrec = mysql_num_rows($myres);
						if($myrec > 0)
						{$i=$j=$k=1;
							while($myrows = mysql_fetch_assoc($myres))
							{
								if($myrows['PORT'] == "Load")
								{?>
								<tr>
								<td align="left">Load Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
								<td align="left" ><input type="text"  name="txtLPOSCCost_<?php echo $i?>" id="txtLPOSCCost_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $myrows['COST'];?>" /></td>
								<td align="left"><input type="text" name="txtLPOSCCostMT_<?php echo $i;?>" id="txtLPOSCCostMT_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $myrows['COST_MT'];?>" /></td>
								</tr>
				<?php $i++;}else if($myrows['PORT'] == "Discharge")
			               {?>
                           <tr id="oscDProw_<?php echo $j;?>">
                            <td width="35%" align="left">Discharge Port  <?php echo $obj->getPortNameBasedOnID($myrows['LOADPORTID']);?></td>
                            <td width="14%" align="left"><input type="text"  name="txtDPOSCCost_<?php echo $j;?>" id="txtDPOSCCost_<?php echo $j;?>" class="form-control" autocomplete="off" readonly value="<?php echo $myrows['COST'];?>"  /></td>
                            <td width="13%" align="left"><input type="text" name="txtDPOSCCostMT_<?php echo $j;?>" id="txtDPOSCCostMT_<?php echo $j;?>" class="form-control" autocomplete="off" readonly value="<?php echo $myrows['COST_MT'];?>"  /></td>
                            </tr>
                        <?php $j++;}?>
                        <?php }}?>
								</tbody>
								<tfoot>
                                <tr >
                                <td width="35%" align="left" class="input-text"  valign="top" >Total Port Costs</td>
                                
                                <td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtTTLPortCosts" id="txtTTLPortCosts" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_PORT_COSTS");?>"  /></td>
                                <td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtTTLPortCostsMT" id="txtTTLPortCostsMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_PORT_COSTS_MT");?>"  /></td>
                                </tr>
                                </tfoot>
                                </table>
                                </div>
                        
                           <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Total Freight Costs
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                             <div class="box-body no-padding">
                             <table class="table table-striped">
                             <tbody>
                                <tr>
                                <td width="35%" align="left">Total Freight Costs</td>
                                <td width="14%" align="left"><input type="text"  name="txtTTLShippingCost" id="txtTTLShippingCost" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_SHIPPING_COST");?>" /></td>
                                <td width="13%" align="left" class="input-text" valign="top"><input type="text"  name="txtTTLShippingCostMT" id="txtTTLShippingCostMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"TTL_SHIPPING_COSTMT");?>" /></td>
                                </tr>
                                </tbody>
                             </table>
                             </div>
                             
                             
                             
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Demurrage Dispatch Ship Owner
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                             <div class="box-body no-padding">
                             <table class="table table-striped">
                             <thead>
                                <tr>
                                <th width="12%" align="center"></th>
                                <th width="14%" align="center">Cost</th>
                                <th width="13%" align="center">Cost/MT</th>
                                </tr>
                                </thead>
                             <tbody id="tbDDSW" >
                              <?php 
                                $sql4 = "select * from fca_vci_shipowner where FCAID='".$obj->getFun1()."'";
                                $res4 = mysql_query($sql4);
                                $rec4 = mysql_num_rows($res4);
                                $i=$j=1;
                                while($rows4 = mysql_fetch_assoc($res4))
                                {
                                if($rows4['PORT'] == "Load")
                                        {
                                        ?>
                                            <tr id="ddswLProw_<?php echo $i;?>">
                                            <td width="35%" align="left">Load Port   <?php echo $obj->getPortNameBasedOnID($rows4['LOADPORTID']);?></td>
                                            <td width="14%" align="left"><input type="text"  name="txtddswLPCost_<?php echo $i;?>" id="txtddswLPCost_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows4['COST'];?>"/></td>
                                            <td width="13%" align="left"><input type="text" name="txtddswLPCostMT_<?php echo $i;?>" id="txtddswLPCostMT_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows4['COST_MT'];?>" /></td>
                                            </tr>
                                <?php $i++;}else if($rows4['PORT'] == "Discharge")
                                        {?>
                            
                                            <tr id="ddswDProw_<?php echo $j;?>">
                                            <td width="35%" align="left">Discharge Port   <?php echo $obj->getPortNameBasedOnID($rows4['LOADPORTID']);?></td>
                                            <td width="14%" align="left"><input type="text"  name="txtddswDPCost_<?php echo $j;?>" id="txtddswDPCost_<?php echo $j;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows4['COST'];?>" /></td>
                                            <td width="13%" align="left"><input type="text" name="txtddswDPCostMT_<?php echo $j;?>" id="txtddswDPCostMT_<?php echo $j;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows4['COST_MT'];?>" /></td>
                                            </tr>
                                            
                                <?php $j++;}?>
                                <?php }?>
                                </tbody>
                             </table>
                             </div>
                            
                            
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                    Demurrage Dispatch Shipper
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                             <div class="box-body no-padding">
                             <table class="table table-striped">
                             <thead>
                                <tr>
                                <th width="12%" align="center"></th>
                                <th width="14%" align="center">Cost</th>
                                <th width="13%" align="center">Cost/MT</th>
                                </tr>
                                </thead>
                             <tbody id="tbDDShipper" >
                              <?php 
								$sql5 = "select * from fca_vci_dd_shipper where FCAID='".$obj->getFun1()."'";
								$res5 = mysql_query($sql5);
								$rec5 = mysql_num_rows($res5);
								$i=1;
								while($rows5 = mysql_fetch_assoc($res5))
								{
								?>
									<tr id="ddshipLProw_<?php echo $i;?>">
									<td width="35%" align="left">Load Port   <?php echo $obj->getPortNameBasedOnID($rows5['LOADPORTID']);?></td>
									<td width="14%" align="left"><input type="text"  name="txtddshipLPCost_<?php echo $i;?>" id="txtddshipLPCost_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows5['COST'];?>"/></td>
									<td width="13%" align="left"><input type="text" name="txtddshipLPCostMT_<?php echo $i;?>" id="txtddshipLPCostMT_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows5['COST_MT'];?>" /></td>
									</tr>
								<?php $i++;}?>		
                                </tbody>
                             </table>
                             </div>
                             
                             
                             
                              <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                   Demurrage Dispatch Receiver
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                             <div class="box-body no-padding">
                             <table class="table table-striped">
                             <thead>
                                <tr>
                                <th width="12%" align="center"></th>
                                <th width="14%" align="center">Cost</th>
                                <th width="13%" align="center">Cost/MT</th>
                                </tr>
                                </thead>
                            <tbody id="tbDDReceiver" >
                            <?php 
                            $sql6 = "select * from fca_vci_dd_receiver where FCAID='".$obj->getFun1()."'";
                            $res6 = mysql_query($sql6);
                            $rec6 = mysql_num_rows($res6);
                            $i=1;
                            while($rows6 = mysql_fetch_assoc($res6))
                            {
                            ?>
                                <tr id="DDReceiDProw_<?php echo $i;?>">
                               <td width="35%" align="left">Discharge Port   <?php echo $obj->getPortNameBasedOnID($rows6['LOADPORTID']);?></td>
                               <td width="14%" align="left"><input type="text"  name="txtDDReceiDPCost_<?php echo $i;?>" id="txtDDReceiDPCost_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows6['COST'];?>" /></td>
                               <td width="13%" align="left"><input type="text" name="txtDDReceiDPCostMT_<?php echo $i;?>" id="txtDDReceiDPCostMT_<?php echo $i;?>" class="form-control" autocomplete="off" readonly value="<?php echo $rows6['COST_MT'];?>"  /></td>
                               </tr>
                            <?php $i++;}?>	
                              </tbody>
                            </table>
                           </div>
                  
                  
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     Demurrage Dispatch Nett Results
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="box-body no-padding">
                             <table class="table table-striped">
                             <thead>
                                <tr>
                                <th width="12%" align="center"></th>
                                <th width="14%" align="center">Cost</th>
                                <th width="13%" align="center">Cost/MT</th>
                                </tr>
                                </thead>
                            <tbody id="tbDDNR" >
                              <tr>
                               <td width="35%" align="left">Load Port </td>
                               <td width="14%" align="left"><input type="text"  name="txtDDNRLPCost" id="txtDDNRLPCost" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DDNR_LP_COST");?>" /></td>
                               <td width="13%" align="left"><input type="text" name="txtDDNRLPCostMT" id="txtDDNRLPCostMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DDNR_LP_COSTMT");?>"  /></td>
                               </tr>
                               <tr>
                               <td width="35%" align="left">Discharge Port</td>
                               <td width="14%" align="left"><input type="text"  name="txtDDNRDPCost" id="txtDDNRDPCost" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DDNR_DP_COST");?>" /></td>
                               <td width="13%" align="left"><input type="text" name="txtDDNRDPCostMT" id="txtDDNRDPCostMT" class="form-control" autocomplete="off" readonly value="<?php echo $obj->getFreightEstimationTotalVCIRecords($obj->getFun1(),"DDNR_DP_COSTMT");?>" /></td>
                               </tr>
                              </tbody>
                            </table>
                           </div>
							<div class="row invoice-info">
                                <div class="col-sm-3 invoice-col">
                                 CS Status
                                    <address>
                                     <select  name="selVType" class="form-control" id="selVType">
									<?php 
                                   $_REQUEST['selVType'] = $obj->getFun4();
	                               $obj->getVoyageType();
                                    ?>
                                    </select>
                                    </address>
                                </div><!-- /.col -->
                              </div>
                  
                        <div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
							<input type="hidden" name="action" id="action" value="submit" />
				        </div>
                  </form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
if(<?php echo $bfs;?> == 0 || <?php echo $bes1;?> == 0 || <?php echo $bes2;?> == 0 || <?php echo $lfs;?> == 0 || <?php echo $les1;?> == 0 || <?php echo $les2;?> == 0 || <?php echo $submitid;?> != 2)
{
	jAlert('Please ensure commercial parameters are completed before proceeding', 'Alert', function(r) {
		if(r){ 
			location.href = "edit_nominations.php?mappingid="+<?php echo $mappingid;?>+"&tab=2";
			
		}
		else{return false;}
		});		
}

$("#TCI_18,#TCI_19").show();

$("#txtMTCPDRate,#txtMLumpsum ,#txtWSFR, #txtWSFD ,#txtCQMT ,#txtMinQMT ,#txtAddnlQMT ,#txtWeather, #txtMargin ,#txtDistance ,#txtPCosts ,#txtQMT ,#txtRate ,#txtWDays1 ,#txtIDays ,#txtDPCosts ,#txtDQMT ,#txtDRate ,#txtDWDays1 ,#txtDIDays ,#txtTLPCosts ,#txtTLIDays ,#txtTFOCOffHire ,#txtTDOCOffHire ,#txtFrAdjPerAC ,#txtDiversionCost ,#txtDemCost ,#txtExtraDues ,#txtBrCommPercent ,[id^=txtORCAmt_],[id^=txtCCAbs_] ,#txtDailyVesselOperatingExpenses ,[id^=txtMCWS_],[id^=txtMCDistanceLeg_],[id^=txtMCTotalDistance_],[id^=txtOvrWS_],[id^=txtOvrDistanceLeg_],[id^=txtOvrTotalDistance_],[id^=txtAdditionAmt_],[id^=txtDeductionAmt_],#txtFrAdjUsdGF,#txtFrAdjPerACTF,#txtFrAdjPerACGF,[id^=txtOMCAbs_]").numeric();

$("#txtDate").datepicker({
	dateFormat: 'dd-m-yy'
});

$('#panel1').slidePanel({
		triggerName: '#trigger1',
		position: 'fixed',
		triggerTopPos: '500px',
		panelTopPos: '500px'
	});

$('[id^=txtFPort_]').each(function(index) {
	var rowid = this.id;
	var lasrvar1 = rowid.split('_')[1];
	var text = $("#selFPort [value='"+$("#txtFPort_"+lasrvar1).val()+"']").text();
	//for load port.....................
	$('<option value="'+$("#txtFPort_"+lasrvar1).val()+'">'+text+'</option>').appendTo("#selLoadPort");
	//.........ends...................
	//for transit port.....................
	$('<option value="'+$("#txtFPort_"+lasrvar1).val()+'">'+text+'</option>').appendTo("#selTLoadPort");
	//.........ends...................
});
$('[id^=txtTPort_]').each(function(index) {
	var rowid = this.id;
	var lasrvar1 = rowid.split('_')[1];
	var text = $("#selTPort [value='"+$("#txtTPort_"+lasrvar1).val()+"']").text();
	//for discharge port.....................
	$('<option value="'+$("#txtTPort_"+lasrvar1).val()+'">'+text+'</option>').appendTo("#selDisPort");
	//.........ends...................
});

});

function showDWTField()
{
	if($("#rdoDWT1").is(":checked"))
	{
		$("#txtDWTS").attr({'disabled':''});
		$("#txtDWTT").attr({'disabled':'disabled'});
	}
	if($("#rdoDWT2").is(":checked"))
	{
		$("#txtDWTS").attr({'disabled':'disabled'});
		$("#txtDWTT").attr({'disabled':''});
	}
}

function showMMarketField1()
{
	if($("#rdoMMarket1").is(":checked"))
	{
		$("#txtMTCPDRate").attr({'disabled':''});
		$("#txtMLumpsum").attr({'disabled':'disabled'});
	}
	if($("#rdoMMarket2").is(":checked"))
	{
		$("#txtMTCPDRate").attr({'disabled':'disabled'});
		$("#txtMLumpsum").attr({'disabled':''});
	}
}

function showQtyField()
{
	if($("#rdoQty1").is(":checked"))
	{
		$("#txtDFQMT").attr({'readonly':''});
		$("#txtAddnlQMT").attr({'readonly':'readonly'});
		$("#txtDFQMT,#txtAddnlQMT").val("0.00");
		$("#txtDFQMT").focus();
	}
	if($("#rdoQty2").is(":checked"))
	{
		$("#txtDFQMT").attr({'readonly':'readonly'});
		$("#txtAddnlQMT").attr({'readonly':''});
		$("#txtDFQMT,#txtAddnlQMT").val("0.00");
		$("#txtAddnlQMT").focus();
	}
}

function showMMarketField()
{
	if($("#rdoMMarket1").is(":checked"))
	{
		$("#txtMTCPDRate").attr({'disabled':''});
		$("#txtMLumpsum").attr({'disabled':'disabled'});
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtMTCPDRate").focus();
		getFinalCalculation();
	}
	if($("#rdoMMarket2").is(":checked"))

	{
		$("#txtMTCPDRate").attr({'disabled':'disabled'});
		$("#txtMLumpsum").attr({'disabled':''});
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtMLumpsum").focus();
		getFinalCalculation();
	}
}

function getToPort()
{
	if($('#selWSPCFP').val() != "" && $('#selWSPCTP').val() != "")
	{
		$('#txtWSFR,#txtWSFD').val("");
		$.post("options.php?id=58",{selFPort:""+$("#selWSPCFP").val()+"",selTPort:""+$("#selWSPCTP").val()+""}, function(html) {
		var var1 = html.split("#");
		$('#txtWSFR').val(var1[0]);
		$('#txtWSFD').val(var1[3]);
		getFinalCalculation();
		});
	}
	else
	{
		$('#txtWSFR,#txtWSFD').val("0.00");
	}
}

function getCalculateAddnl()
{
	if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}
	if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
	var diff  = parseFloat(cargo_qty) - parseFloat(min_qty); 
	if(diff.toFixed(2) < 0){$("#txtAddnlQMT").val("0.00");}
	else{$("#txtAddnlQMT").val(diff.toFixed(2));}
	
}

function getDistance()
{
	$("#txtDistance").val("");
	$("#loader1").show();
	if($('#selFPort').val() != "" && $('#selTPort').val() != "" && $('#selDType').val() != "")
	{
		$("#txtDistance").val("");
		$.post("options.php?id=2",{selFPort:""+$("#selFPort").val()+"",selTPort:""+$("#selTPort").val()+"",selDType:""+$("#selDType").val()+""}, function(data) 
		{
				$('#txtDistance').val(data);
				$("#loader1").hide();
		});
	}
	else
	{
		$('#txtDistance').val("");
		$("#loader1").hide();
	}
}

//..............for Port Rotation details...............................................
function addPortRotationDetails()
{
	if($("#selFPort").val() != "" && $("#selTPort").val() != "" && $("#selPType").val() != "" && $("#txtDistance").val() != "" && $("#selSSpeed").val() != "")
	{
			var id = $("#p_rotationID").val();
			var lasrvar1 = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar1 = rowid.split('_')[1];
			});
		if($("#selFPort").val() == $("#txtTPort_"+lasrvar1).val() || id == 0 || lasrvar1 == "")
		{
			getVoyageTime();
			id = (id - 1) + 2;
			$("#PRrow_Empty").remove();
			var frm_port = document.getElementById("selFPort").selectedIndex;
			var frm_port_text = document.getElementById("selFPort").options[frm_port].text;
			var to_port = document.getElementById("selTPort").selectedIndex;
			var to_port_text = document.getElementById("selTPort").options[to_port].text;
			var p_type = document.getElementById("selPType").selectedIndex;
			var p_type_text = document.getElementById("selPType").options[p_type].text;
			
			var d_type = document.getElementById("selDType").selectedIndex;
			var d_type_text = document.getElementById("selDType").options[d_type].text;
			
			var speed_type = document.getElementById("selSSpeed").selectedIndex;
			var speed_type_text = document.getElementById("selSSpeed").options[speed_type].text;
			
			$('<tr id="pr_Row_'+id+'"><td align="center" class="input-text" ><a href="#pr'+id+'" id="spcancel_'+id+'" onclick="removePortRotation('+id+','+$("#selFPort").val()+','+$("#selTPort").val()+');" ><img src="../../images/icon_delete.gif" height="14" width="12" /></a></td><td align="left" class="input-text" >'+frm_port_text+'<input type="hidden" name="txtFPort_'+id+'" id="txtFPort_'+id+'" class="input" size="10" value="'+$("#selFPort").val()+'"/></td><td align="left" class="input-text" >'+to_port_text+'<input type="hidden" name="txtTPort_'+id+'" id="txtTPort_'+id+'" class="input" size="10" value="'+$("#selTPort").val()+'"/></td><td align="left" class="input-text" >'+p_type_text+'('+speed_type_text+')<input type="hidden" name="txtPType_'+id+'" id="txtPType_'+id+'" class="input" size="10" value="'+$("#selPType").val()+'"/><input type="hidden" name="txtSSpeed_'+id+'" id="txtSSpeed_'+id+'" class="input" size="10" value="'+$("#selSSpeed").val()+'"/></td><td align="left" class="input-text" >'+parseFloat($("#txtDistance").val())+'('+d_type_text+')<input type="hidden" name="txtDistance_'+id+'" id="txtDistance_'+id+'" class="input" size="10" value="'+parseFloat($("#txtDistance").val())+'"/><input type="hidden" name="txtDType_'+id+'" id="txtDType_'+id+'" class="input" size="10" value="'+$("#selDType").val()+'"/></td><td align="left" class="input-text" >'+$("#txtWeather").val()+'<input type="hidden" name="txtWeather_'+id+'" id="txtWeather_'+id+'" class="input" size="10" value="'+$("#txtWeather").val()+'"/></td><td align="left" class="input-text" >'+$("#txtMargin").val()+'<input type="hidden" name="txtMargin_'+id+'" id="txtMargin_'+id+'" class="input" size="10" value="'+$("#txtMargin").val()+'"/></td></tr>').appendTo("#tblPortRotation");
			
			//for load port.....................
			$('<option value="'+$("#selFPort").val()+'">'+frm_port_text+'</option>').appendTo("#selLoadPort");
			//.........ends...................
			//for discharge port.....................
			$('<option value="'+$("#selTPort").val()+'">'+to_port_text+'</option>').appendTo("#selDisPort");
			//.........ends...................
			//for transit port.....................
			$('<option value="'+$("#selFPort").val()+'">'+frm_port_text+'</option>').appendTo("#selTLoadPort");
			//.........ends...................
			$("#p_rotationID").val(id);
			$("#selFPort").val($("#selTPort").val());
			$("#selTPort,#selPType,#txtDistance,#txtWeather,#txtMargin,#selSSpeed").val("");
			$("#selDType").val("1");
			
			var lasrvar = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar = rowid.split('_')[1];
			});
			$("[id^=spcancel_]").hide();
			$("#spcancel_"+lasrvar).show();
			
			//getTTLFreight();
		}
		else
		{
			jAlert('Please select in sequence of ports', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Sea Passage', 'Alert');
	}
}

function getVoyageTime()
{
	if($("#txtMargin").val() == ""){var margin = 0;}else{var margin = $("#txtMargin").val();}
	if($("#txtDistance").val() == ""){var distance = 0;}else{var distance = $("#txtDistance").val();}
	if($("#txtWeather").val() == ""){var speed_adj = 0;}else{var speed_adj = $("#txtWeather").val();}
	if($("#selPType").val() == 1)
	{
		if($("#selSSpeed").val() == 1)
		{
			var ballast = <?php echo $bfs;?>;
			var fo_ballast = <?php echo $fo_bfs;?>;
			var do_ballast = <?php echo $do_bfs;?>;
		}
		if($("#selSSpeed").val() == 2)
		{
			var ballast = <?php echo $bes1;?>;
			var fo_ballast = <?php echo $fo_bes1;?>;
			var do_ballast = <?php echo $do_bes1;?>;
		}
		if($("#selSSpeed").val() == 3)
		{
			var ballast = <?php echo $bes2;?>;
			var fo_ballast = <?php echo $fo_bes2;?>;
			var do_ballast = <?php echo $do_bes2;?>;
		}
		
		var calc = ($("#txtDistance").val() /  (parseFloat(ballast) + parseFloat(speed_adj)))/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) + parseFloat(calc) + parseFloat(margin);
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		var ttl_days = parseFloat(calc) + parseFloat($("#txtBDays").val()) + parseFloat(margin);
		$("#txtBDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ballast));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ballast));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat(distance) + parseFloat($("#txtBDist").val());
		$("#txtBDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
	if($("#selPType").val() == 2)
	{
		if($("#selSSpeed").val() == 1)
		{
			var ladan = <?php echo $lfs;?>;
			var fo_ladan = <?php echo $fo_lfs;?>;
			var do_ladan = <?php echo $do_lfs;?>;
		}
		if($("#selSSpeed").val() == 2)
		{
			var ladan = <?php echo $les1;?>;
			var fo_ladan = <?php echo $fo_les1;?>;
			var do_ladan = <?php echo $do_les1;?>;
		}
		if($("#selSSpeed").val() == 3)
		{
			var ladan = <?php echo $les2;?>;
			var fo_ladan = <?php echo $fo_les2;?>;
			var do_ladan = <?php echo $do_les2;?>;
		}
		
		var calc = ($("#txtDistance").val() / (parseFloat(ladan) + parseFloat(speed_adj)) )/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) + parseFloat(calc) + parseFloat(margin);
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		var ttl_days = parseFloat(calc) + parseFloat($("#txtLDays").val()) + parseFloat(margin);
		$("#txtLDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ladan));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat((parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ladan));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat(distance) + parseFloat($("#txtLDist").val());
		$("#txtLDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
	//getTTLFreight();
}

function removePortRotation(var1,portid,disportid)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){	
			//for load port.....................
			$("#selLoadPort option[value='"+$("#txtFPort_"+var1).val()+"']").remove();
			$('[id^=txtLoadPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == portid)
				{
					if($("#txtPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtPCosts_"+rowid.split('_')[1]).val();}
					if($("#txtLPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtLPIDays_"+rowid.split('_')[1]).val();}
					if($("#txtLPWDays_"+rowid.split('_')[1]).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtLPWDays_"+rowid.split('_')[1]).val();}
					
					var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
					$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
					$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDays(rowid.split('_')[1]);
					
					$("#lp_Row_"+rowid.split('_')[1]).remove();
					$("#oscLProw_"+rowid.split('_')[1]).remove();
					$("#oscLProw1_"+rowid.split('_')[1]).remove();
					$("#ddswLProw_"+rowid.split('_')[1]).remove();
					$("#ddswLProw1_"+rowid.split('_')[1]).remove();
					
					if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
					$("#txtDDNRLPCost").val($("[id^=txtDDSLPOSCCost_]").sum().toFixed(2));
					var ttlcostMT = parseFloat($("#txtDDNRLPCost").val()) / parseFloat(c_qty);
					$("#txtDDNRLPCostMT").val(ttlcostMT.toFixed(2));
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					//getTTLFreight();
				}
			});
			//.........ends...................	
			//for discharge port.....................
			$("#selDisPort option[value='"+$("#txtTPort_"+var1).val()+"']").remove();
			$('[id^=txtDisPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == disportid)
				{
					if($("#txtDPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtDPCosts_"+rowid.split('_')[1]).val();}
					if($("#txtDPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtDPIDays_"+rowid.split('_')[1]).val();}
					if($("#txtDPWDays_"+rowid.split('_')[1]).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtDPWDays_"+rowid.split('_')[1]).val();}
					
					var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
					$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
					$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDaysDP(rowid.split('_')[1]);
					
					$("#dp_Row_"+rowid.split('_')[1]).remove();
					$("#oscDProw_"+rowid.split('_')[1]).remove();
					$("#oscDProw1_"+rowid.split('_')[1]).remove();			
					//$("#oscDDRDProw_"+rowid.split('_')[1]).remove();
					//$("#oscDDRDProw1_"+rowid.split('_')[1]).remove();
					
					
					if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
					$("#txtDDNRDPCost").val($("[id^=txtDDRDPOSCCost_]").sum().toFixed(2));
					var ttlcostMT = parseFloat($("#txtDDNRDPCost").val()) / parseFloat(c_qty);
					$("#txtDDNRDPCostMT").val(ttlcostMT.toFixed(2));
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					//getTTLFreight();
				}
			});
			//.........ends...................		
			//for transit port.....................
			$("#selTLoadPort option[value='"+$("#txtFPort_"+var1).val()+"']").remove();
			$('[id^=txtTLoadPort_]').each(function(index) {
				var rowid = this.id;
				if(this.value == portid)
				{
					if($("#txtTPCosts_"+rowid.split('_')[1]).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtTPCosts_"+rowid.split('_')[1]).val();}
					if($("#txtTPIDays_"+rowid.split('_')[1]).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtTPIDays_"+rowid.split('_')[1]).val();}
								
					var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
					$("#txtTtPIDays").val(ttl_idays.toFixed(2));
					$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
					
					getRemoveTTLVoyageDaysTP(rowid.split('_')[1]);
					
					$("#tp_Row_"+rowid.split('_')[1]).remove();
					$("#oscTProw_"+rowid.split('_')[1]).remove();
					$("#oscTProw1_"+rowid.split('_')[1]).remove();
					
					var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
					$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
					$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
					//getTTLFreight();
				}
			});
			//.........ends...................
			getRemoveVoyageTime(var1);	
			$("#pr_Row_"+var1).remove();
			var lasrvar = "";
			$('[id^=txtFPort_]').each(function(index) {
				var rowid = this.id;
				lasrvar = rowid.split('_')[1];
			});
			$("#selTPort,#selPType,#txtDistance,#txtWeather,#txtMargin,#selSSpeed").val("");
			$("#selFPort").val($("#txtTPort_"+lasrvar).val());
			$("[id^=spcancel_]").hide();
			$("#spcancel_"+lasrvar).show();
			//getTTLFreight();
		}
		else{return false;}
		});	
}

function getRemoveVoyageTime(var1)
{
	if($("#txtMargin_"+var1).val() == ""){var margin = 0;}else{var margin = $("#txtMargin_"+var1).val();}
	if($("#txtDistance_"+var1).val() == ""){var distance = 0;}else{var distance = $("#txtDistance_"+var1).val();}
	if($("#txtWeather_"+var1).val() == ""){var speed_adj = 0;}else{var speed_adj = $("#txtWeather_"+var1).val();}
	if($("#txtPType_"+var1).val() == 1)
	{
		if($("#txtSSpeed_"+var1).val() == 1)
		{
			var ballast = <?php echo $bfs;?>;
			var fo_ballast = <?php echo $fo_bfs;?>;
			var do_ballast = <?php echo $do_bfs;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 2)
		{
			var ballast = <?php echo $bes1;?>;
			var fo_ballast = <?php echo $fo_bes1;?>;
			var do_ballast = <?php echo $do_bes1;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 3)
		{
			var ballast = <?php echo $bes2;?>;
			var fo_ballast = <?php echo $fo_bes2;?>;
			var do_ballast = <?php echo $do_bes2;?>;
		}
		
		var calc = ($("#txtDistance_"+var1).val() / (parseFloat(ballast) + parseFloat(speed_adj)) )/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));	
		
		var ttl_days = parseFloat($("#txtBDays").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtBDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var diff_fo = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ballast);
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(diff_fo.toFixed(2));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		
		var diff_do = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ballast);
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(diff_do.toFixed(2));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat($("#txtBDist").val()) - parseFloat(distance);
		$("#txtBDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));	
	}
	if($("#txtPType_"+var1).val() == 2)
	{
		if($("#txtSSpeed_"+var1).val() == 1)
		{
			var ladan = <?php echo $lfs;?>;
			var fo_ladan = <?php echo $fo_lfs;?>;
			var do_ladan = <?php echo $do_lfs;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 2)
		{
			var ladan = <?php echo $les1;?>;
			var fo_ladan = <?php echo $fo_les1;?>;
			var do_ladan = <?php echo $do_les1;?>;
		}
		if($("#txtSSpeed_"+var1).val() == 3)
		{
			var ladan = <?php echo $les2;?>;
			var fo_ladan = <?php echo $fo_les2;?>;
			var do_ladan = <?php echo $do_les2;?>;
		}
		
		var calc = ($("#txtDistance_"+var1).val() / (parseFloat(ladan) + parseFloat(speed_adj)) )/24;
		var voyage_time = parseFloat($("#txtVoyageTime").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtVoyageTime,#txtTTLVoyageDays").val(voyage_time.toFixed(2));
		
		var ttl_days = parseFloat($("#txtLDays").val()) - (parseFloat(calc) + parseFloat(margin));
		$("#txtLDays").val(ttl_days.toFixed(2));
		$("#txtTSDays").val($("#txtBDays,#txtLDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		
		var diff_fo = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(fo_ladan);
		var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(diff_fo.toFixed(2));
		$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));	
		
		var diff_do = (parseFloat(calc) + parseFloat(margin)) *  parseFloat(do_ladan);
		var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(diff_do.toFixed(2));
		$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
		var ttl_dis = parseFloat($("#txtLDist").val()) - parseFloat(distance);
		$("#txtLDist").val(ttl_dis.toFixed(2));
		$("#txtTDist").val($("#txtBDist,#txtLDist").sum().toFixed(2));
	}
	//getTTLFreight();
}
//...................Port Rotation details ends here........................................


function getLOadPortQty()
{
	if($("#selLPCName").val() != "")
	{
		if($("#txtCQMT").val() != "")
		{
			$("#txtQMT").val(parseFloat($("#txtCQMT").val()) - parseFloat($("[id^=txtLpQMT_]").sum()));
			getLoadPortCalculation();
		}
		else
		{
			$("#txtQMT").val(0);
		}
	}
	else
	{
		$("#txtQMT,#txtRate,#txtWDays,#txtWDays1").val("");
	}
}

function getLoadPortCalculation()
{
	if($("#txtQMT").val() != "" && $("#txtRate").val() != "") 
	{
		var value = ($("#txtQMT").val() / $("#txtRate").val()) / 24;
		$("#txtWDays").val(value.toFixed(2));
	}
	else
	{
		$("#txtWDays").val('0.00');
	}
}

//..............for Load Port details...............................................
function addLoadPortDetails()
{
	if($("#selLoadPort").val() != "" && $("#txtQMT").val() != "" && $("#txtRate").val() != "" && $("#selLPCName").val() != "")
	{
		if(parseFloat($("#txtCQMT").val()) >= parseFloat($("[id^=txtLpQMT_],#txtQMT").sum()))
		{
			getTTLVoyageDays();
			var id = $("#load_portID").val();
			id = (id - 1) + 2;
			$("#LProw_Empty").remove();
			var load_port = document.getElementById("selLoadPort").selectedIndex;
			var load_port_text = document.getElementById("selLoadPort").options[load_port].text;
			
			var cargo = document.getElementById("selLPCName").selectedIndex;
			var cargo_text = document.getElementById("selLPCName").options[cargo].text;
			
			$('<tr id="lp_Row_'+id+'"><td align="center" class="input-text" ><a href="#lp'+id+'" onclick="removeLoadPort('+id+');" ><img src="../../images/icon_delete.gif" height="14" width="12" /></a></td><td align="left" class="input-text" >'+load_port_text+'<input type="hidden" name="txtLoadPort_'+id+'" id="txtLoadPort_'+id+'" class="input" size="10" value="'+$("#selLoadPort").val()+'"/></td><td align="left" class="input-text" >'+cargo_text+'<input type="hidden" name="txtLPCID_'+id+'" id="txtLPCID_'+id+'" class="input" size="10" value="'+$("#selLPCName").val()+'"/></td><td align="left" class="input-text" >'+$("#txtQMT").val()+'<input type="hidden" name="txtLpQMT_'+id+'" id="txtLpQMT_'+id+'" class="input" size="10" value="'+$("#txtQMT").val()+'"/></td><td align="left" class="input-text" >'+$("#txtRate").val()+'<input type="hidden" name="txtLPRate_'+id+'" id="txtLPRate_'+id+'" class="input" size="10" value="'+$("#txtRate").val()+'"/></td><td align="left" class="input-text" >'+$("#txtPCosts").val()+'<input type="hidden" name="txtPCosts_'+id+'" id="txtPCosts_'+id+'" class="input" size="10" value="'+$("#txtPCosts").val()+'"/></td><td align="left" class="input-text" >'+$("#txtIDays").val()+'<input type="hidden" name="txtLPIDays_'+id+'" id="txtLPIDays_'+id+'" class="input" size="10" value="'+$("#txtIDays").val()+'"/></td><td align="left" class="input-text" >'+$("#txtWDays").val()+'<input type="hidden" name="txtLPWDays_'+id+'" id="txtLPWDays_'+id+'" class="input" size="10" value="'+$("#txtWDays").val()+'"/></td></tr>').appendTo("#tblLoadPort");
			
			if($("#txtPCosts").val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtPCosts").val();}
			var lpcostMT = parseFloat(lp_cost) / parseFloat($("#txtCQMT").val());
			var lp = "'LP'";		
		$('<tr id="oscLProw_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >Load Port   '+load_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtLPOSCCost_'+id+'" id="txtLPOSCCost_'+id+'" class="input-text" size="10" readonly="true" value="'+$("#txtPCosts").val()+'" /></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtLPOSCCostMT_'+id+'" id="txtLPOSCCostMT_'+id+'" class="input-text" size="10" readonly="true" value="'+lpcostMT.toFixed(2)+'" /></td></tr><tr id="oscLProw1_'+id+'" height="5"><td colspan="10" align="left" class="text" valign="top" ></td></tr>').appendTo("#tbPortCosts");
		
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
			$('<tr id="ddswLProw_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >Load Port   '+load_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtddswLPCost_'+id+'" id="txtddswLPCost_'+id+'" class="input-text" size="10" readonly="true" value="0.00"/></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtddswLPCostMT_'+id+'" id="txtddswLPCostMT_'+id+'" class="input-text" size="10" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDSW");
			
			$("#txtTtPIDays").val($("#txtTtPIDays,#txtIDays").sum().toFixed(2));
			$("#txtTtPWDays").val($("#txtTtPWDays,#txtWDays").sum().toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			$("[id^=txtDDSLPOSCCost_],[id^=txtDDNRLPOSCCost_]").numeric();
			$("#load_portID").val(id);
			$("#txtQMT,#txtRate,#selLoadPort,#txtPCosts,#txtIDays,#txtWDays,#txtWDays1,#txtLPDraftM,#selLPCName,#selCrType").val("");
			//getTTLFreight();
		}
		else
		{
			jAlert('Loaded quantity is greater than Cargo Qty.', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Load Port', 'Alert');
	}
}

function getTTLVoyageDays()
{
	if($("#txtIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtIDays").val();}
	if($("#txtWDays").val() == ""){var w_days = 0;}else{var w_days = $("#txtWDays").val();}
	
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
	var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
	
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
		
	//getTTLFreight();
}


function removeLoadPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){ 
			if($("#txtPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtPCosts_"+var1).val();}
			if($("#txtLPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtLPIDays_"+var1).val();}
			if($("#txtLPWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtLPWDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
			var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			getRemoveTTLVoyageDays(var1);
			
			$("#lp_Row_"+var1).remove();
			$("#oscLProw_"+var1).remove();
			$("#oscLProw1_"+var1).remove();
			$("#ddswLProw_"+var1).remove();
			$("#ddswLProw1_"+var1).remove();
			
			if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
			$("#txtDDNRLPCost").val($("[id^=txtDDSLPOSCCost_]").sum().toFixed(2));
			var ttlcostMT = parseFloat($("#txtDDNRLPCost").val()) / parseFloat(c_qty);
			$("#txtDDNRLPCostMT").val(ttlcostMT.toFixed(2));
			
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			//getTTLFreight();
		}
		else{return false;}
		});	
}

function getRemoveTTLVoyageDays(var1)
{
	if($("#txtLPIDays_"+var1).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtLPIDays_"+var1).val();}
	if($("#txtLPWDays_"+var1).val() == ""){var w_days = 0;}else{var w_days = $("#txtLPWDays_"+var1).val();}
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) -  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
	
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2)));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
	var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
	
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2)));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	//getTTLFreight();
}

function getDisPortQty()
{
	if($("#selDPCName").val() != "")
	{
		if($("#txtCQMT").val() != "")
		{
			$("#txtDQMT").val(parseFloat($("#txtCQMT").val()) - parseFloat($("[id^=txtDpQMT_]").sum()));
			getDisPortCalculation();
		}
		else
		{
			$("#txtDQMT").val(0);
		}
	}
	else
	{
		$("#txtDQMT,#txtDRate,#txtDWDays,#txtDWDays1").val("");
	}
}

function getDisPortCalculation()
{
	if($("#txtDQMT").val() != "" && $("#txtDRate").val() != "") 
	{
		var value = ($("#txtDQMT").val() / $("#txtDRate").val()) / 24;
		$("#txtDWDays").val(value.toFixed(2));
	}
	else
	{
		$("#txtDWDays,#txtDWDays1").val('0.00');
	}
}

//..............for Dis Port details...............................................
function addDisPortDetails()
{
	if($("#selDisPort").val() != "" && $("#txtDQMT").val() != "" && $("#txtDRate").val() != "" && $("#selDPCName").val() != "" )
	{
		if(parseFloat($("#txtCQMT").val()) >= parseFloat($("[id^=txtDpQMT_],#txtDQMT").sum()))
		{
			getTTLVoyageDaysDP();
			var id = $("#dis_portID").val();
			id = (id - 1) + 2;
			$("#DProw_Empty").remove();
			var dis_port = document.getElementById("selDisPort").selectedIndex;
			var dis_port_text = document.getElementById("selDisPort").options[dis_port].text;
			
			var cargo = document.getElementById("selDPCName").selectedIndex;
			var cargo_text = document.getElementById("selDPCName").options[cargo].text;
						
			$('<tr id="dp_Row_'+id+'"><td align="center" class="input-text" ><a href="#dp'+id+'" onclick="removeDisPort('+id+');" ><img src="../../images/icon_delete.gif" height="14" width="12" /></a></td><td align="left" class="input-text" >'+dis_port_text+'<input type="hidden" name="txtDisPort_'+id+'" id="txtDisPort_'+id+'" class="input" size="10" value="'+$("#selDisPort").val()+'"/></td><td align="left" class="input-text" >'+cargo_text+'<input type="hidden" name="txtDPCID_'+id+'" id="txtDPCID_'+id+'" class="input" size="10" value="'+$("#selDPCName").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDQMT").val()+'<input type="hidden" name="txtDpQMT_'+id+'" id="txtDpQMT_'+id+'" class="input" size="10" value="'+$("#txtDQMT").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDRate").val()+'<input type="hidden" name="txtDPRate_'+id+'" id="txtDPRate_'+id+'" class="input" size="10" value="'+$("#txtDRate").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDPCosts").val()+'<input type="hidden" name="txtDPCosts_'+id+'" id="txtDPCosts_'+id+'" class="input" size="10" value="'+$("#txtDPCosts").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDIDays").val()+'<input type="hidden" name="txtDPIDays_'+id+'" id="txtDPIDays_'+id+'" class="input" size="10" value="'+$("#txtDIDays").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDWDays").val()+'<input type="hidden" name="txtDPWDays_'+id+'" id="txtDPWDays_'+id+'" class="input" size="10" value="'+$("#txtDWDays").val()+'"/></td></tr>').appendTo("#tblDisPort");
			
		if($("#txtDPCosts").val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPCosts").val();}
		var dpcostMT = parseFloat(dp_cost) / parseFloat($("#txtCQMT").val());
		var dp = "'DP'";
		$('<tr id="oscDProw_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >Discharge Port   '+dis_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtDPOSCCost_'+id+'" id="txtDPOSCCost_'+id+'" class="input-text" size="10" readonly="true" value="'+$("#txtDPCosts").val()+'" /></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtDPOSCCostMT_'+id+'" id="txtDPOSCCostMT_'+id+'" class="input-text" size="10" readonly="true" value="'+dpcostMT.toFixed(2)+'"  /></td></tr><tr id="oscDProw1_'+id+'" height="5"><td colspan="10" align="left" class="text" valign="top" ></td></tr>').appendTo("#tbPortCosts");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
			$("#txtTtPIDays").val($("#txtTtPIDays,#txtDIDays").sum().toFixed(2));
			$("#txtTtPWDays").val($("#txtTtPWDays,#txtDWDays").sum().toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			$("#dis_portID").val(id);
			$("#selDisPort,#txtDQMT,#txtDRate,#txtDPDraftM,#txtDPCosts,#txtDIDays,#txtDWDays,#txtDWDays1,#selDPCName,#selDPCrType").val("");
			//getTTLFreight();
		}
		else
		{
			jAlert('Discharged quantity is greater than Cargo Qty.', 'Alert');
		}
	}
	else
	{
		jAlert('Please fill all the records for Discharge Port', 'Alert');
	}
}

function getTTLVoyageDaysDP()
{
	if($("#txtDIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtDIDays").val();}
	if($("#txtDWDays").val() == ""){var w_days = 0;}else{var w_days = $("#txtDWDays").val();}
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
		
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
	var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
		
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));
	
	//getTTLFreight();
}

function removeDisPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){ 
			if($("#txtDPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtDPCosts_"+var1).val();}
			if($("#txtDPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtDPIDays_"+var1).val();}
			if($("#txtDPWDays_"+var1).val() == ""){var w_days = "0.00";}else{var w_days = $("#txtDPWDays_"+var1).val();}
			
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
			var ttl_wdays = parseFloat($("#txtTtPWDays").val()) - parseFloat(w_days);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTtPWDays").val(ttl_wdays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			getRemoveTTLVoyageDaysDP(var1);
			
			$("#dp_Row_"+var1).remove();

			$("#oscDProw_"+var1).remove();
			$("#oscDProw1_"+var1).remove();			
			//$("#oscDDRDProw_"+var1).remove();
			//$("#oscDDRDProw1_"+var1).remove();
			
			if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
			$("#txtDDNRDPCost").val($("[id^=txtDDRDPOSCCost_]").sum().toFixed(2));
			var ttlcostMT = parseFloat($("#txtDDNRDPCost").val()) / parseFloat(c_qty);
			$("#txtDDNRDPCostMT").val(ttlcostMT.toFixed(2));
			
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			//getTTLFreight();
		}
		else{return false;}
		});	
}

function getRemoveTTLVoyageDaysDP(var1)
{
	if($("#txtDPIDays_"+var1).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtDPIDays_"+var1).val();}
	if($("#txtDPWDays_"+var1).val() == ""){var w_days = 0;}else{var w_days = $("#txtDPWDays_"+var1).val();}
	var ttl_days1 = parseFloat(idle_days) +  parseFloat(w_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) -  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var wrkingdays_fo = parseFloat(w_days) * <?php echo $fowrking;?>;
		
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(parseFloat(margin_fo.toFixed(2)) + parseFloat(wrkingdays_fo.toFixed(2)));
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;
	var wrkingdays_do = parseFloat(w_days) * <?php echo $dowrking;?>;		
		
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(parseFloat(margin_do.toFixed(2)) + parseFloat(wrkingdays_do.toFixed(2)));
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	//getTTLFreight();
}

function addTransitPortDetails()
{
	if($("#selTLoadPort").val() != "")
	{
		getTTLVoyageDaysTP();
		var id = $("#transit_portID").val();
		id = (id - 1) + 2;
		$("#TProw_Empty").remove();
		var load_port = document.getElementById("selTLoadPort").selectedIndex;
		var load_port_text = document.getElementById("selTLoadPort").options[load_port].text;
				
		$('<tr id="tp_Row_'+id+'"><td align="center" class="input-text" ><a href="#dp'+id+'" onclick="removeTransitPort('+id+');" ><img src="../../images/icon_delete.gif" height="14" width="12" /></a></td><td align="left" class="input-text" >'+load_port_text+'<input type="hidden" name="txtTLoadPort_'+id+'" id="txtTLoadPort_'+id+'" class="input" size="10" value="'+$("#selTLoadPort").val()+'"/></td><td align="left" class="input-text" >'+$("#txtTLPCosts").val()+'<input type="hidden" name="txtTPCosts_'+id+'" id="txtTPCosts_'+id+'" class="input" size="10" value="'+$("#txtTLPCosts").val()+'"/></td><td align="left" class="input-text" >'+$("#txtTLIDays").val()+'<input type="hidden" name="txtTPIDays_'+id+'" id="txtTPIDays_'+id+'" class="input" size="10" value="'+$("#txtTLIDays").val()+'"/></td></tr>').appendTo("#tblTransitPort");
		
		if($("#txtTLPCosts").val() == ""){var tp_cost = 0;}else{var tp_cost = $("#txtTLPCosts").val();}
		var tpcostMT = parseFloat(tp_cost) / parseFloat($("#txtCQMT").val());
		var tp = "'TP'";
		$('<tr id="oscTProw_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >Transit Port   '+load_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtTPOSCCost_'+id+'" id="txtTPOSCCost_'+id+'" class="input-text" size="10" readonly="true" value="'+$("#txtTLPCosts").val()+'" /></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtTPOSCCostMT_'+id+'" id="txtTPOSCCostMT_'+id+'" class="input-text" size="10" readonly="true" value="'+tpcostMT.toFixed(2)+'" /></td></tr><tr height="5" id="oscTProw1_'+id+'"><td colspan="10" align="left" class="text" valign="top" ></td></tr>').appendTo("#tbPortCosts");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
		$("#txtTtPIDays").val($("#txtTtPIDays,#txtTLIDays").sum().toFixed(2));
		$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
		$("#transit_portID").val(id);
		$("#selTLoadPort,#txtTLPCosts,#txtTLIDays").val("");
		//getTTLFreight();
	}
	else
	{
		jAlert('Please fill all the records for Tansit Port', 'Alert');
	}
}

function getTTLVoyageDaysTP()
{
	if($("#txtTLIDays").val() == ""){var idle_days = 0;}else{var idle_days = $("#txtTLIDays").val();}
	var ttl_days1 = parseFloat(idle_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) +  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) + parseFloat(margin_fo);
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;	
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) + parseFloat(margin_do);
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	//getTTLFreight();
}

function removeTransitPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){
			if($("#txtTPCosts_"+var1).val() == ""){var port_costs = "0.00";}else{var port_costs = $("#txtTPCosts_"+var1).val();}
			if($("#txtTPIDays_"+var1).val() == ""){var idleDays = "0.00";}else{var idleDays = $("#txtTPIDays_"+var1).val();}
						
			var ttl_idays = parseFloat($("#txtTtPIDays").val()) - parseFloat(idleDays);
			$("#txtTtPIDays").val(ttl_idays.toFixed(2));
			$("#txtTDays").val($("#txtTSDays,#txtTtPIDays,#txtTtPWDays").sum().toFixed(2));
			
			getRemoveTTLVoyageDaysTP(var1);
			 
			$("#tp_Row_"+var1).remove();
			$("#oscTProw_"+var1).remove();
			$("#oscTProw1_"+var1).remove();
			
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum())/ parseFloat($("#txtCQMT").val());
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_],[id^=txtTPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			//getTTLFreight();
		}
		else{return false;}
		});	
}

function getRemoveTTLVoyageDaysTP(var1)
{
	if($("#txtTPIDays_"+var1).val() == ""){var idle_days = 0;}else{var idle_days = $("#txtTPIDays_"+var1).val();}
	var ttl_days1 = parseFloat(idle_days);
	var ttl_days = parseFloat($("#txtTTLVoyageDays").val()) -  parseFloat(ttl_days1);
	$("#txtTTLVoyageDays").val(ttl_days.toFixed(2));
	
	var margin_fo = parseFloat(idle_days) * <?php echo $foidle;?>;
	var consp_fo =  parseFloat($("#txtTTLFoConsp").val()) - parseFloat(margin_fo);
	$("#txtTTLFoConsp,#txtTFUMT").val(consp_fo.toFixed(2));
	
	var margin_do = parseFloat(idle_days) * <?php echo $doidle;?>;		
	var consp_do =  parseFloat($("#txtTTLDoConsp").val()) - parseFloat(margin_do);
	$("#txtTTLDoConsp,#txtTDUMT").val(consp_do.toFixed(2));	
	
	//getTTLFreight();
}

function getShowHide()
{
	if($("#txtbid").val() == 0)
	{
		document.BID.src = "../../images/open.png";
		$('#BID').attr('title', 'Open Panel');
		$("#bunker_adj").hide();
		$("#txtbid").val(1);
	}
	else
	{
		document.BID.src = "../../images/close.png";
		$('#BID').attr('title', 'Close Panel');
		$("#bunker_adj").show();
		$("#txtbid").val(0);
	}
}

function getEnabled(var1,var2)
{
	if($("#checkIFO_"+var1).attr('checked'))
	{
		$("[id^=txt"+$("#txtBHID_"+var1).val()+"_]").attr("disabled","");
		$("[id^=txt"+$("#txtBHID_"+var1).val()+"_]").numeric();
		$("#txtTTLEst_"+var1).val("0");
		
		var str=var2.substr(0,3);
		if(str == 'IFO')
		{
			$("[id^=txt"+var2+"_1_1]").val($("#txtTFUMT").val());
		}
		else if(str == 'MDO')
		{
			$("[id^=txt"+var2+"_1_1]").val($("#txtTDUMT").val());
		}
		else if(str == 'MGO')
		{
			$("[id^=txt"+var2+"_1_1]").val($("#txtTDUMT").val());
		}
		else
		{
			$("[id^=txt"+var2+"_1_1]").val('0.00');
		}
		getBunkerTable(var2);
	}
	else
	{
		$("[id^=txt"+$("#txtBHID_"+var1).val()+"_]").attr("disabled","disabled");
		$("[id^=txt"+$("#txtBHID_"+var1).val()+"_]").val("");
		$("#txtTTLEst_"+var1).val("0");
		$("[id^=txt"+var2+"_1_1]").val('');
		removeBunkerTable(var2);
	}
	
}

function getBunkerTable(var1)
{
	$('<tr id="rowB_'+var1+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >'+var1+' Nett</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtBunkerCost_'+var1+'" id="txtBunkerCost_'+var1+'" class="input-text" size="10" readonly="true"  placeholder="0.00"  /></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtBunkerCostMT_'+var1+'" id="txtBunkerCostMT_'+var1+'" class="input-text" size="10" readonly="true" value="" placeholder="0.00" /></td></tr>').appendTo("#tbbunkers");
}

function removeBunkerTable(var1)
{
	$("#rowB_"+var1).remove();
}

function getCalculate1(var1,var2,var3)
{
	$("#txt"+var1+"_"+var2+"_1").val(var3);
	if(var3 == ""){var first = 0;}else{var first = var3;}
	if($("#txt"+var1+"_"+var2+"_2").val() == ""){var second = 0;}else{var second = $("#txt"+var1+"_"+var2+"_2").val();}
	var calc = parseFloat(first) * parseFloat(second);
	$("#txt"+var1+"_"+var2+"_3").val(calc.toFixed(2));
	//$("#txtTTLEst_"+var4).val($("#txt"+var1+"_1_3").val());	
}


function getCalculate(var1,var2,var3,var4)
{
	$("#txt"+var1+"_"+var2+"_2").val(var3);
	if($("#txt"+var1+"_"+var2+"_1").val() == ""){var first = 0;}else{var first = $("#txt"+var1+"_"+var2+"_1").val();}
	if(var3 == ""){var second = 0;}else{var second = var3;}
	var calc = parseFloat(first) * parseFloat(second);
	$("#txt"+var1+"_"+var2+"_3").val(calc.toFixed(2));
	$("#txtTTLEst_"+var4).val($("#txt"+var1+"_1_3").val());	
}


function getORCCalculate(status,var1)
{
	if(status == 1)
	{
		$("#txtHidORCAmt_"+var1).val($("#txtORCAmt_"+var1).val());
	}
	if(status == 2)
	{
		$("#txtHidORCAmt_"+var1).val("-"+$("#txtORCAmt_"+var1).val());
	}
	getFinalCalculation();
}

function getOMCCalculate(var1)
{
	if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}
	if(parseFloat(cargo_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	
	if($("#txtOMCAbs_"+var1).val() == ""){var omc_abs = 0;}else{var omc_abs = $("#txtOMCAbs_"+var1).val();}
	var calc = parseFloat(omc_abs) / parseFloat(per_mt);
	$("#txtOMCCostMT_"+var1).val(calc.toFixed(2));
	getFinalCalculation();	
}

function getCharterersCostCalculate(var1)
{
	if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}
	if(parseFloat(cargo_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}
	
	if($("#txtCCAbs_"+var1).val() == ""){var cc_abs = 0;}else{var cc_abs = $("#txtCCAbs_"+var1).val();}
	var calc = parseFloat(cc_abs) / parseFloat(per_mt);
	$("#txtCCostMT_"+var1).val(calc.toFixed(2));
	getFinalCalculation();	
}

function getPortCostSum(var1,port)
{
	if(port == 'LP')
	{
		if($("#txtLPOSCCost_"+var1).val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtLPOSCCost_"+var1).val();}
		var lpcostMT = parseFloat(lp_cost) / parseFloat($("#txtCQMT").val());
		$("#txtLPOSCCostMT_"+var1).val(lpcostMT.toFixed(2));
	}
	else if(port == 'DP')
	{
		if($("#txtDPOSCCost_"+var1).val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPOSCCost_"+var1).val();}
		var dpcostMT = parseFloat(dp_cost) / parseFloat($("#txtCQMT").val());
		$("#txtDPOSCCostMT_"+var1).val(dpcostMT.toFixed(2));
	}
	else if(port == 'TP')
	{
		if($("#txtTPOSCCost_"+var1).val() == ""){var tp_cost = 0;}else{var tp_cost = $("#txtTPOSCCost_"+var1).val();}
		var tpcostMT = parseFloat(tp_cost) / parseFloat($("#txtCQMT").val());
		$("#txtTPOSCCostMT_"+var1).val(tpcostMT.toFixed(2));
	}
	getFinalCalculation();			
}



function getFinalCalculation()
{
	<!----------------- Quantity  ------------------->
	if($("#txtCQMT").val() == ""){var cargo_qty = 0;}else{var cargo_qty = $("#txtCQMT").val();}
	
	if(parseFloat(cargo_qty))
	{
		var per_mt = parseFloat(cargo_qty);
	}

	<!----------------- Agreed Gross Freight  ------------------->
	if($("#txtMTCPDRate").val() == ""){var ws_rate = 0;}else{var ws_rate = $("#txtMTCPDRate").val();}
	
	<!-----------------  Addnl Qty  ------------------->
	if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
	
	<!----------------- Quantity  ------------------->
	var per_amt = $("#txtCQMT").val();
	if($("#rdoMMarket1").is(":checked"))
	{
		<!----------------- Agreed Gross Freight  ------------------->
		if($("#txtMTCPDRate").val() == ""){var agr_gross_fr = 0;}else{var agr_gross_fr = $("#txtMTCPDRate").val();}
		//if($("#txtAQMT").val() == ""){var actual_qty = 0;}else{var actual_qty = $("#txtAQMT").val();}
		
		<!----------------- (Freight Adjustment) Gross Freight  ------------------->
		var gross_fr = parseFloat(agr_gross_fr) * parseFloat(per_amt);
		$("#txtFrAdjUsdGF").val(gross_fr.toFixed(2));
		
		<!----------------- (Freight Adjustment) Gross Freight(MT)  ------------------->
		var gross_fr_mt = parseFloat(gross_fr) / parseFloat(per_amt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		
		<!-----------------  DF Qty (MT)  ------------------->
		if($("#txtDFQMT").val() == "" || $("#txtDFQMT").val() == 0)
		{
			var df_qty = 0;
			$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
			
		}
		else
		{
			<!-----------------  Dead Freight(USD)  ------------------->
			var df_qty = $("#txtDFQMT").val();
			var dead_fr = parseFloat(agr_gross_fr) * parseFloat(df_qty);
			$("#txtFrAdjUsdDF").val(dead_fr.toFixed(2));
			<!-----------------  Dead Freight(MT)  ------------------->
			var dead_fr_mt = parseFloat(dead_fr) / parseFloat(df_qty);
			$("#txtFrAdjUsdDFMT").val(dead_fr_mt.toFixed(2));
		}
		
		<!-----------------  Addnl Cargo Rate (USD/MT)  ------------------->
		
		if($("#txtAddnlCRate").val() == ""){var addnl_rate = 0;}else{var addnl_rate = $("#txtAddnlCRate").val();}
		//if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
		
		
		if($("#txtAddnlQMT").val() == "" || $("#txtAddnlQMT").val() == 0 )
		{
			var addnl_qty = 0;
			$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		}
		else
		{
			<!-----------------  Addnl Qty (MT)  ------------------->
			var addnl_qty = $("#txtAddnlQMT").val();
			
			<!-----------------(Freight Adjustment)  Addnl Freight(USD)  ------------------->
			var addnl_fr = parseFloat(addnl_rate) * parseFloat(addnl_qty);
			$("#txtFrAdjUsdAF").val(addnl_fr.toFixed(2));
			
			<!-----------------(Freight Adjustment)  Addnl Freight(MT)  ------------------->
			var addnl_fr_mt = parseFloat(addnl_fr) / parseFloat(addnl_qty);
			$("#txtFrAdjUsdAFMT").val(addnl_fr_mt.toFixed(2));
		}
		
		<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum().toFixed(2));
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / (parseFloat(per_amt) + parseFloat(df_qty) + parseFloat(addnl_qty));
		<!-----------------(Freight Adjustment)  Total Freight(MT)  ------------------->
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
	}
	
	if($("#rdoMMarket2").is(":checked"))
	{
		<!-----------------(Freight Adjustment)  Gross Freight(USD)  ------------------->
		$("#txtFrAdjUsdGF").val($("#txtMLumpsum").val());
		
		<!-----------------(Freight Adjustment)  Gross Freight(MT)  ------------------->
		var gross_fr_mt = parseFloat($("#txtFrAdjUsdGF").val()) / parseFloat(per_amt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		
		$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
		$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		
		<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum());
		
		<!-----------------(Freight Adjustment)  Gross Freight(MT)  ------------------->
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / parseFloat(per_amt);
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
	}
	
	<!-----------------(Freight Adjustment)  Total Freight(USD)  ------------------->
	if($("#txtFrAdjUsdTF").val() == ""){var ttl_fr = 0;}else{var ttl_fr = $("#txtFrAdjUsdTF").val();}
	
	<!-----------------(Freight Adjustment) Address Commission(Percent)  ------------------->
	if($("#txtFrAdjPerAC").val() == ""){var ac_per = 0;}else{var ac_per = $("#txtFrAdjPerAC").val();}
	
	<!-----------------(Freight Adjustment) Address Commission(USD)  ------------------->
	var address_commission = (parseFloat(ttl_fr) * parseFloat(ac_per)) / 100;
	$("#txtFrAdjUsdAC").val(address_commission.toFixed(2));
	
	<!-----------------(Freight Adjustment) Address Commission(MT)  ------------------->
	var address_commission_mt = parseFloat($("#txtFrAdjUsdAC").val()) / parseFloat(per_amt);
	$("#txtFrAdjUsdACMT").val(address_commission_mt.toFixed(2));
	
	<!-----------------(Freight Adjustment) Brokerage(Persent)  ------------------->
	if($("#txtFrAdjPerAgC").val() == ""){var ag_per = 0;}else{var ag_per = $("#txtFrAdjPerAgC").val();}
	var agent_commission = (parseFloat(ttl_fr) * parseFloat(ag_per)) / 100;
	
	<!-----------------(Freight Adjustment) Brokerage(USD)  ------------------->
	$("#txtFrAdjUsdAgC").val(agent_commission.toFixed(2));
	
	<!-----------------(Freight Adjustment) Net Freight Payable(USD)  ------------------->
	var net_fr = parseFloat(ttl_fr) - parseFloat($("#txtFrAdjUsdAC,#txtFrAdjUsdAgC").sum());
	$("#txtFrAdjUsdFP,#txtFGFFD").val(net_fr.toFixed(2));
	
	<!-----------------(Freight Adjustment) Net Freight Payable(MT)  ------------------->
	var net_fr_mt = parseFloat(net_fr) / parseFloat(per_amt);
	$("#txtFGFFDMT").val(net_fr_mt.toFixed(2));

	if($("#txtFGFFD").val() == ""){var final_nett_frt = 0;}else{var final_nett_frt = $("#txtFGFFD").val();}
	var rev = parseFloat($("[id^=txtOMCAbs_]").sum().toFixed(2)) + parseFloat(final_nett_frt);
	$("#txtRevenue").val(rev.toFixed(2));
	
	/*if($("#rdoMMarket2").is(":checked"))
	{
		$("#txtFrAdjUsdTF,#txtFrAdjUsdGF").val($("#txtMLumpsum").val());
		$("#txtFrAdjFixedDiff").val("0.00");
	}*/
	
	/*if($("#txtFrAdjPerACTF").val() == ""){var less_address_comm_tf = 0;}else{var less_address_comm_tf = $("#txtFrAdjPerACTF").val();}
	if($("#txtFrAdjUsdTF").val() == ""){var ttl_freight = 0;}else{var ttl_freight = $("#txtFrAdjUsdTF").val();}
	
	var add_comm_usd_tf = (parseFloat(ttl_freight) * parseFloat(less_address_comm_tf))/100;
	$("#txtFrAdjUsdACTF").val(add_comm_usd_tf.toFixed(2));
	
	
	if($("#txtFrAdjPerACGF").val() == ""){var less_address_comm_gf = 0;}else{var less_address_comm_gf = $("#txtFrAdjPerACGF").val();}
	if($("#txtFrAdjUsdGF").val() == ""){var gross_freight = 0;}else{var gross_freight = $("#txtFrAdjUsdGF").val();}
	
	var add_comm_usd_gf = (parseFloat(gross_freight) * parseFloat(less_address_comm_gf))/100;
	$("#txtFrAdjUsdACGF").val(add_comm_usd_gf.toFixed(2));*/		
	
	
	//if($("#txtFrAdjUsdACTF").val() == ""){var add_comm_usd_tf1 = 0;}else{var add_comm_usd_tf1 = $("#txtFrAdjUsdACTF").val();}
	//if($("#txtFrAdjUsdACGF").val() == ""){var add_comm_usd_gf1 = 0;}else{var add_comm_usd_gf1 = $("#txtFrAdjUsdACGF").val();}
	
	
	//var balance_freight = parseFloat(gross_freight) - (parseFloat(add_comm_usd_tf1) + parseFloat(add_comm_usd_gf1));
	//$("#txtBalanceFreight").val(balance_freight.toFixed(2));
	
	//var final_nett_frt = parseFloat(balance_freight.toFixed(2)) + parseFloat($("[id^=txtAdditionAmt_]").sum().toFixed(2)) - parseFloat($("[id^=txtDeductionAmt_]").sum().toFixed(2));
	//$("#txtFGFFD").val(final_nett_frt.toFixed(2));
	
	//if($("#txtFGFFD").val() == ""){var final_nett_freight = 0;}else{var final_nett_freight = $("#txtFGFFD").val();}
	//var final_nett_freight_mt = parseFloat(final_nett_freight) / parseFloat(per_mt);
	//$("#txtFGFFDMT").val(final_nett_freight_mt.toFixed(2));
	
	if($("#txtBrCommPercent").val() == ""){var brokerage_comm = 0;}else{var brokerage_comm = $("#txtBrCommPercent").val();}
	if($("#txtFrAdjUsdTF").val() == ""){var t_frt = 0;}else{var t_frt = $("#txtFrAdjUsdTF").val();}
	
	var brokerage_comm_usd = (parseFloat(t_frt) * parseFloat(brokerage_comm))/100;
	$("#txtBrComm").val(brokerage_comm_usd.toFixed(2));
	
	$("#txtTTLORCAmt").val($("[id^=txtHidORCAmt_],#txtBrComm").sum());
	var calc = parseFloat($("#txtTTLORCAmt").val()) / parseFloat(per_mt);
	$("#txtTTLORCCostMT").val(calc.toFixed(2));
	
	$("#txtTTLOwnersExpenses").val($("[id^=txtTTLEst_],#txtTTLPortCosts,#txtTTLORCAmt").sum().toFixed(2));
	$("#txtTTLCharterersExpenses").val($("[id^=txtCCAbs_]").sum().toFixed(2));
	
	if($("#txtRevenue").val() == ""){var revenue = 0;}else{var revenue = $("#txtRevenue").val();}
	if($("#txtTTLOwnersExpenses").val() == ""){var owners_expenses = 0;}else{var owners_expenses = $("#txtTTLOwnersExpenses").val();}
	var voyage_earning = parseFloat(revenue) - parseFloat(owners_expenses);
	$("#txtVoyageEarnings,#txtGTTLVoyageEarnings").val(voyage_earning.toFixed(2));
	
	if($("#txtTDays").val() == 0){var ttl_days = 1;}else{var ttl_days = $("#txtTDays").val();}
	var daily_earnings = parseFloat(voyage_earning) / parseFloat(ttl_days);
	$("#txtDailyEarnings,#txtNettDailyEarnings").val(daily_earnings.toFixed(2));
	
	if($("#txtDailyVesselOperatingExpenses").val() == ""){var daily_vessel_exp = 0;}else{var daily_vessel_exp = $("#txtDailyVesselOperatingExpenses").val();}
	var nett_daily_profit = parseFloat($("#txtNettDailyEarnings").val()) - parseFloat(daily_vessel_exp);
	$("#txtNettDailyProfit").val(nett_daily_profit.toFixed(2));	
}

function getValidate()
{
	if($("#rdoMMarket1").is(":checked"))
	{
		var arr = new Array(); var arr1 = new Array();
		var i=0;
		$('[id^=txtFreightSpecs_]').each(function(index) {
					var j = i+1;
					if($("#txtFreightSpecs_"+j).val() == "" || $("#txtMinCargo_"+j).val() == "" || $("#txtMCWSFlatRate_"+j).val() == "" || $("#txtMCWS_"+j).val() == "" || $("#txtMCAmount_"+j).val() == "" || parseFloat($("#txtMCAmount_"+j).val()) == "0.00")
					{
						arr[i] = j;		
					}			
					i++;
			});
		if(arr.length == 0)
		{
		jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
			if(r){
					getFinalCalculation();
					document.frm1.submit();
				 }
			else{return false;}
			});
		}
		else
		{
			jAlert('Please fill mandatory fields', 'Alert');
			return false;
		}
	}
	if($("#rdoMMarket2").is(":checked"))
	{
		jConfirm('Are you sure you have checked each entry ?', 'Confirmation', function(r) {
			if(r){
					getFinalCalculation();
					document.frm1.submit();
				 }
			else{return false;}
			});
	}
}

function getRemoveFrtAdj(var1)
{
	var arr = new Array(); var arr1 = new Array();
	var i=0;
	$('[id^=txtFreightSpecs_]').each(function(index) {
				var j = i+1;
				arr[i] = j;					
				i++;
		});
	if(arr.length != 1)
	{
		jConfirm('Are you sure you want to remove this entry permanently ?', 'Confirmation', function(r) {
			if(r){
					$("#frt_adj_"+var1).remove();
					getFinalCalculation();			
				 }
			else{return false;}
			});
	}
	else 
	{
		jAlert('Atleast one Freight Specs is required.', 'Alert');
		return false;
	}
}


function AddFrtAdjRow()
{
	var id = $("#txtFSID").val();
	if($("#txtFreightSpecs_"+id).val() != "" && $("#txtMinCargo_"+id).val() != "" && $("#txtMCWSFlatRate_"+id).val() != "" && $("#txtMCAmount_"+id).val() != "" && $("#txtMCAmount_"+id).val() != "0.00")
	{
		id = (id - 1) + 2;
			
		$('<tbody id="frt_adj_'+id+'"><tr height="35"><td align="left" class="input-text" valign="top" colspan="10" ></td></tr><tr height="25"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="15%" align="left" class="input-text"  valign="top" >Freight Specs</td><td width="1%" align="left" class="input-text" valign="top">:</td><td width="23%" align="left" class="input-text" valign="top"><input type="text"  name="txtFreightSpecs_'+id+'" id="txtFreightSpecs_'+id+'" class="input-text" size="18"  autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top">WS Flat Rate</td><td width="11%" align="left" class="input-text" valign="top">WS</td><td width="11%" align="left" class="input-text" valign="top">Distance Leg</td><td width="11%" align="left" class="input-text" valign="top">Total Distance</td><td width="11%" align="left" class="input-text" valign="top">Amount</td><td width="6%" align="left" class="input-text" valign="top"><a href="#del" style="cursor:pointer;" onclick="getRemoveFrtAdj('+id+');"  title="Remove Entry" ><img src="../../images/icon_delete.gif"  /></a></td></tr><tr  height="25"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="15%" align="left" class="input-text"  valign="top" >Min Cargo (MT)</td><td width="1%" align="left" class="input-text" valign="top">:</td><td width="23%" align="left" class="input-text" valign="top"><input type="text"  name="txtMinCargo_'+id+'" id="txtMinCargo_'+id+'" class="input-text" size="18" readonly="true" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtMCWSFlatRate_'+id+'" id="txtMCWSFlatRate_'+id+'" class="input-text" size="4" readonly="true" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtMCWS_'+id+'" id="txtMCWS_'+id+'" class="input-text" size="4" onkeyup="getFinalCalculation();" autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtMCDistanceLeg_'+id+'" id="txtMCDistanceLeg_'+id+'" class="input-text" size="4"  autocomplete="off" onkeyup="getFinalCalculation();" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtMCTotalDistance_'+id+'" id="txtMCTotalDistance_'+id+'" class="input-text" size="4" onkeyup="getFinalCalculation();"   autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtMCAmount_'+id+'" id="txtMCAmount_'+id+'" class="input-text" size="4" readonly="true" value=""/></td><td width="6%" align="left" class="input-text" valign="top"></td></tr><tr height="25"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="15%" align="left" class="input-text"  valign="top" >Overage (MT)</td><td width="1%" align="left" class="input-text" valign="top">:</td><td width="23%" align="left" class="input-text" valign="top"><input type="text"  name="txtOverage_'+id+'" id="txtOverage_'+id+'" class="input-text" size="18" readonly="true" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtOvrWSFlatRate_'+id+'" id="txtOvrWSFlatRate_'+id+'" class="input-text" size="4" readonly="true" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtOvrWS_'+id+'" id="txtOvrWS_'+id+'" class="input-text" size="4" onkeyup="getFinalCalculation();" autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtOvrDistanceLeg_'+id+'" id="txtOvrDistanceLeg_'+id+'" class="input-text" size="4" onkeyup="getFinalCalculation();"  autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtOvrTotalDistance_'+id+'" id="txtOvrTotalDistance_'+id+'" class="input-text" size="4" onkeyup="getFinalCalculation();"  autocomplete="off" value=""/></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtOvrAmount_'+id+'" id="txtOvrAmount_'+id+'" class="input-text" size="4" readonly="true" value=""/></td><td width="6%" align="left" class="input-text" valign="top"></td></tr><tr height="25"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="15%" align="left" class="input-text"  valign="top" >Total Cargo Qty (MT)</td><td width="1%" align="left" class="input-text" valign="top">:</td><td width="23%" align="left" class="input-text" valign="top"><input type="text"  name="txtTotalCargoQty_'+id+'" id="txtTotalCargoQty_'+id+'" class="input-text" size="18"  readonly="true" value=""/></td><td width="11%" align="left" class="input-text" valign="top"></td><td width="11%" align="left" class="input-text" valign="top"></td><td width="11%" align="left" class="input-text" valign="top"></td><td width="11%" align="left" class="input-text" valign="top"></td><td width="11%" align="left" class="input-text" valign="top"><input type="text"  name="txtTotalAmount_'+id+'" id="txtTotalAmount_'+id+'" class="input-text" size="4"  readonly="true" value=""/></td><td width="6%" align="left" class="input-text" valign="top"></td></tr></tbody>').appendTo("#tbl_frt_adj");
	
		if($("#txtMinQMT").val() == ""){var min_qty = 0;}else{var min_qty = $("#txtMinQMT").val();}
		if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
		if($("#txtWSFR").val() == ""){var ws_flat_rate = 0;}else{var ws_flat_rate = $("#txtWSFR").val();}
		
		$("#txtMinCargo_"+id).val(min_qty);$("#txtOverage_"+id).val(addnl_qty);
		$("#txtMCWSFlatRate_"+id).val(ws_flat_rate);$("#txtOvrWSFlatRate_"+id).val(ws_flat_rate);
		$("#txtTotalCargoQty_"+id).val($("#txtMinCargo_"+id+" , #txtOverage_"+id+"").sum());
		
		$("[id^=txtMCWS_],[id^=txtMCDistanceLeg_],[id^=txtMCTotalDistance_],[id^=txtOvrWS_],[id^=txtOvrDistanceLeg_],[id^=txtOvrTotalDistance_]").numeric();	
		$("#txtFSID").val(id);
	}
	else
	{
		jAlert('Please fill data for Freight Specs', 'Alert');
		return false;
	}
}

function addAddition()
{
	var id = $("#txtADDID").val();
	if($("#txtAddition_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="add_row_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="43%" align="left" class="input-text"  valign="top" ><input type="text"  name="txtAddition_'+id+'" id="txtAddition_'+id+'" class="input-text" size="30" autocomplete="off" value="" /></td><td width="17%" align="left" class="input-text" valign="top"></td><td width="18%" align="center" class="input-text" valign="top"><input type="text"  name="txtAdditionAmt_'+id+'" id="txtAdditionAmt_'+id+'" class="input-text" size="10" autocomplete="off" onkeyup="getFinalCalculation();" value="0.00" /></td><td width="22%" align="center" class="input-text" valign="top"></td></tr>').appendTo("#tbody_add");
	
		$("[id^=txtAdditionAmt_]").numeric();	
		$("#txtADDID").val(id);
	}
	else
	{
		jAlert('Please fill data for Additions', 'Alert');
		return false;
	}
}

function addDeductions()
{
	var id = $("#txtDEDID").val();
	if($("#txtDeduction_"+id).val() != "")
	{
		id = (id - 1) + 2;
		$('<tr id="ded_row_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="43%" align="left" class="input-text"  valign="top" ><input type="text"  name="txtDeduction_'+id+'" id="txtDeduction_'+id+'" class="input-text" size="30" autocomplete="off" value="" /></td><td width="17%" align="left" class="input-text" valign="top"></td><td width="18%" align="center" class="input-text" valign="top"><input type="text"  name="txtDeductionAmt_'+id+'" id="txtDeductionAmt_'+id+'" class="input-text" size="10" autocomplete="off" onkeyup="getFinalCalculation();" value="0.00" /></td><td width="22%" align="center" class="input-text" valign="top"></td></tr>').appendTo("#tbody_ded");
	
		$("[id^=txtDeductionAmt_]").numeric();	
		$("#txtDEDID").val(id);
	}
	else
	{
		jAlert('Please fill data for Deductions', 'Alert');
		return false;
	}
}
</script>
    </body>
</html>
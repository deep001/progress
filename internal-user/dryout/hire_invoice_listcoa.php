<?php 
session_start();
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_dryout.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$comid = $_REQUEST['id'];
$page  = $_REQUEST['page'];
if($page == 1){$page_link = "in_ops_at_glance.php"; $page_bar="In Ops at a glance";$nav=5;$subtitle = 'VC';}else if($page == 2){$page_link = "vessel_in_post_ops.php"; $page_bar="Vessels in Post Ops";$nav=5;$subtitle = 'VC';}else if($page == 3){$page_link = "vessel_in_history.php"; $page_bar="Vessels in History";$nav=5;$subtitle = 'VC';}else if($page == 4){$page_link = "coa_in_ops_at_glance.php"; $page_bar="In Ops at a glance - COA";$nav=20;$subtitle = 'COA';}else if($page == 5){$page_link = "coa_in_post_ops.php"; $page_bar="In Post Ops at a glance - COA";$nav=20;$subtitle = 'COA';}else{$page_link = "coa_in_history.php"; $page_bar="Vessels in History - COA";$nav=20;$subtitle = 'COA';}
if(@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->deleteHireInvoiceRecords();
	header('Location:./hire_invoice_listcoa.php?msg='.$msg."&id=".$comid."&page=".$page);
 }
if(@$_REQUEST['action1'] == 'submit1')
{
	$msg = $obj->insertHirePaymentReceivedDetails();
	header('Location:./hire_invoice_listcoa.php?msg='.$msg[0].'&id='.$comid.'&page='.$page);
}
$l_cost_sheet_id = $obj->getLatestCostSheetID($comid);
$obj->viewFreightCostEstimationTempleteRecordsNew($comid,$l_cost_sheet_id);

$pagename = basename($_SERVER['PHP_SELF'])."?id=".$comid."&page=".$page;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu($nav); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Ops <?php echo $subtitle;?>&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;<?php echo $page_bar;?>&nbsp;&nbsp;&nbsp;</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div class="box box-primary">
					<h3 style=" text-align:center;">Hire Statement Grid : <?php echo $obj->getVesselIMOData($obj->getCompareEstimateData($comid,"VESSEL_IMO_ID"),"VESSEL_NAME");?></h3>
                    <div align="right">
						<span>Nom ID : <?php echo $obj->getCompareTableData($comid,"MESSAGE");?></span><br/>
						<a href="payment_gridcoa.php?comid=<?php echo $comid;?>&page=<?php echo  $page;?>"><button class="btn btn-info btn-flat">Back</button></a>
                        <a href="invoice_hirecoa.php?id=<?php echo $comid;?>&page=<?php echo  $page;?>"><button class="btn btn-info btn-flat">Add New/Existing</button></a></div>
					<div style="height:10px;">&nbsp;</div>
					<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>					 
						<?php $obj->displayHireInvioceListList($comid, $page);?>
						<input type="hidden" name="tblid" id="tblid" value=""/>
				        <input type="hidden" name="action" id="action" value="submit"/>
					</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								</div>
								<div class="modal-body">
									<form name="frm2" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post"/>
                                        <div id="divPayment">
                                            
                                        </div>
                                        <div class="box-footer" align="right">
                                            <button type="submit" id="btnhide" class="btn btn-primary btn-flat" onClick="return getValid();" >Submit</button>
                                            <input type="hidden" id="action1" name="action1" value="submit1" />
                                            <input type="hidden" name="txtCRMFILE1" id="txtCRMFILE1" value="" />
                                            <input type="hidden" name="txtCRMNAME1" id="txtCRMNAME1" value="" />
                                        </div>
									</form>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div>
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<script src='../../js/jquery.autosize.js'></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.calculation-min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#invoice_list").dataTable();
});


function getDelete(depid)
{
	jConfirm('Are you sure you want to delete this statement permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#tblid").val(depid);
			document.frm1.submit();
		}
		else{return false;}
		});
}

function getValid()
{
	if($('#txtP_PR').val() == '' || $('#txtP_Date').val() =='' || $('#txtP_Remarks').val()=='')
	{
		jAlert('Please fill the Payment Received & Date & Remarks', 'Alert');
		return false;
	}
	else
	{
		var file_temp_name = $("[id^=file1_]").map(function () {return this.value;}).get().join(",");
	    $('#txtCRMFILE1').val(file_temp_name);
	    var file_actual_name = $("[id^=name1_]").map(function () {return this.value;}).get().join(",");
	    $('#txtCRMNAME1').val(file_actual_name);
		document.frm1.submit();
		return true;
	}
}

function Del_Upload1(var2)
{
	jConfirm('Are you sure you want to delete this upload permanently ?', 'Confirmation', function(r) {
	if(r){ 
		$('#row_file1_'+var2).remove();
	}
	});
}

function onkeyUp1()
{
	if(parseFloat($('#txtP_PR').val()) > parseFloat($('#txtP_Amt').val()))
	{
		jAlert('Payment Received is more than Amount', 'Alert');
		$('#txtP_PR').val(0.00);
	}
}


function openWin(var1)
{
	$("#divPayment").empty();
	$("#divPayment").html('<div><img src="../../img/ajax-loader.gif" /><br><b>Loading...</b></div>'); 
	$.post("options.php?id=45",{invoiceid:""+var1+""}, function(html) {
		$("#divPayment").empty();
		$("#divPayment").append(html);
		
		$("#txtP_PR").numeric();
        $('#txtP_Remarks').autosize({append: "\n"});
		$('#txtP_Date').datepicker({
			format: 'dd-mm-yyyy',
			autoclose:true
		});
	});
}


</script>
		
</body>
</html>
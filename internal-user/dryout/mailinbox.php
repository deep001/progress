<?php 
set_time_limit(40000);
session_start();
date_default_timezone_set('Etc/UTC');
require_once("../../includes/display_internal_user_dryout.inc.php");
require_once("../../includes/functions_internal_user_mailinbox.inc.php");
require_once('../../PHPMailer-5.2.26/PHPMailerAutoload.php');
$objemaildata = new emaildata();
$display = new display();
$display->logout_iu();
$arrtype = array('INBOX'=>1,'Sent Mail'=>2,'Drafts'=>3,'Trash'=>4,'Starred'=>5,'Spam'=>6);
if(!empty($_REQUEST['mailtype']))
{
	$mailtype = $_REQUEST['mailtype'];
	$type = $arrtype[$mailtype];
}
else
{
	$mailtype = 'INBOX';
	$type = $arrtype[$mailtype];
}
if(@$_REQUEST['action'] == "submit")
{
	$ch = curl_init();
	$upload2 = $objemaildata->getUpload($_FILES['mul_file']);
	$name2 = $objemaildata->getUploadName($_FILES['mul_file']);
	$filesname = array();
	for($i =0;$i<count($upload2);$i++)
	{
	  $filesname[] = array('url'=>'../../attachment/'.$upload2[$i],'name'=>$name2[$i]); 
	}
	
    $data = array(
        'txtMailCC' =>$_REQUEST['txtMailCC'],
        'txtMessages' => $_REQUEST['txtMessages'],
        'txtSubject' => $_REQUEST['txtSubject'],
        'txtMailBcc' => $_REQUEST['txtMailBcc'],
        'txtMailTo' => $_REQUEST['txtMailTo'],
		'filesname' => $filesname,
    );

    $data = http_build_query($data);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_URL, "http://111.118.180.210:8012/sohvctc/internal-user/dryout/sendmails.php");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);

    $output = curl_exec($ch);
	$pos = strpos($output, 'Message sent!');
	if ($pos === false) {
		header('Location:./mailinbox.php?msg=1');
	} else {
		header('Location:./mailinbox.php?msg=0');
	}
}
$pagename = basename($_SERVER['PHP_SELF']);
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='text/html; charset=UTF-8,width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
#chartdiv {
	
	font-size	: 11px;
}	
</style>

</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(16); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
               <section class="content-header">
                    <h1>
                        <i class="glyphicon glyphicon-envelope"></i>&nbsp;Mail Inbox&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Mail Inbox</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
                <?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Mail Sent successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while sending mail.
				</div>
				<?php }}?>
                <form name="frm1" accept-charset="utf-8" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
				<!--   content put here..................-->
                    <div class="mailbox row">
                        <div class="col-xs-12">
                            <div class="box box-solid">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-9" style="overflow:auto;">&nbsp;
                                        </div>
                                        <div class="col-md-3" style="overflow:auto;"><strong><span id="fromnum">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;of&nbsp;&nbsp;&nbsp;<span id="totalmail">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;</strong><button class="paginate_button current" id="frommailicon" type="button" onClick="getData1(2);"><i class="glyphicon glyphicon-chevron-left"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class="paginate_button current" id="tomailicon" type="button" onClick="getData1(1);"><i class="glyphicon glyphicon-chevron-right"></i></button>
                                        <input type="hidden" name="start" id="start" value="" />
                                        <input type="hidden" name="end" id="end" value="" />
                                         <input type="hidden" name="txtTotalMail" id="txtTotalMail" value="<?=$noofmail;?>" />
                                         <input type="hidden" name="txtMailType" id="txtMailType" value="<?=$mailtype;?>" />
                                         <input type="hidden" name="txtTypeid" id="txtTypeid" value="<?=$type;?>" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2">
                                            <!-- BOXES are complex enough to move the .box-header around.
                                                 This is an example of having the box header within the box body -->
                                            <div class="box-header">
                                                <button class="btn btn-primary btn-flat" data-toggle="modal" data-target="#compose-mail" title="Compose Mail" style="width:100%;" type="button">Compose Mail</button>
                                            </div>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li class="active" id="li_1" onClick="getData('INBOX',1);"><a href="#A"><i class="fa fa-bookmark" style="color:#dc631e;"></i> Inbox</a></li>
                                                    <li id="li_2"><a href="#" onClick="getData('Sent Mail',2);"><i class="glyphicon glyphicon-play" style="color:#dc631e;"></i> Sent</a></li>
                                                    <li id="li_3"><a href="#" onClick="getData('Drafts',3);"><i class="glyphicon glyphicon-file" style="color:#dc631e;"></i> Drafts</a></li>
                                                    <li id="li_4"><a href="#" onClick="getData('Trash',4);"><i class="glyphicon glyphicon-trash" style="color:#dc631e;"></i> Trash</a></li>
                                                    <li id="li_5"><a href="#" onClick="getData('Starred',5);"><i class="glyphicon glyphicon-star" style="color:#dc631e;"></i> Starred</a></li>
                                                    <li id="li_6"><a href="#" onClick="getData('Spam',6);"><i class="glyphicon glyphicon-exclamation-sign" style="color:#dc631e;"></i> Spam</a></li>
                                                </ul>
                                            </div>
                                        </div><!-- /.col (LEFT) -->
                                        <div class="col-md-10 col-sm-10">
                                        <div class="box-body table-responsive">
                                            <table id="inbox_list" class="table table-hover" style="width:100%">
                                                <tbody id="inbox_tbody">
                                                </tbody>
                                            </table>
                                        </div><!-- /.col (RIGHT) -->
                                    </div><!-- /.row -->
                                    </div>
                                </div><!-- /.box-body -->                                
                            </div><!-- /.box -->
                        </div><!-- /.col (MAIN) -->
                    </div>
                    
				<!--   content ends here..................-->
                </form>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			<div class="modal fade" id="compose-mail" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" style="width:80%;">
                    <div class="modal-content">
                        <div class="modal-header">
                        <form name="frm2" accept-charset="utf-8" method="post" id="frm2" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <br/>
                            <h4 class="modal-title"><i class="glyphicon glyphicon-envelope"></i>&nbsp;&nbsp;&nbsp;New Message</h4><br/>
                            <div class="row">
                                <div class="col-sm-1 invoice-col">
                                    <address>
                                       To
                                    </address>
                                </div>
                                <div class="col-sm-11 invoice-col">
                                    <address>
                                        <input type="text" name="txtMailTo" id="txtMailTo" class="form-control" placeholder="To" value="">
                                    </address>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1 invoice-col">
                                    <address>
                                    CC
                                    </address>
                                </div>
                                <div class="col-sm-11 invoice-col">
                                    <address>
                                        <input type="text" name="txtMailCC" id="txtMailCC" class="form-control" placeholder="CC" value="">
                                    </address>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1 invoice-col">
                                    <address>
                                    BCC
                                    </address>
                                </div>
                                <div class="col-sm-11 invoice-col">
                                  <address>
                                  <input type="text" name="txtMailBcc" id="txtMailBcc" class="form-control" placeholder="BCC" value="">
                                  </address>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="col-sm-1 invoice-col">
                                    <address>
                                    Subject
                                    </address>
                                </div>
                                <div class="col-sm-11 invoice-col">
                                    <address>
                                    <textarea class="form-control autosize" name="txtSubject" id="txtSubject" placeholder="Subject" rows="1" ></textarea>
                                    </address>	
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1 invoice-col">
                                    <address>
                                    Message
                                    </address>
                                </div>
                                <div class="col-sm-11 invoice-col">
                                    <address>
                                        <textarea class="form-control autosize redactor" style="min-height:200px;" name="txtMessages" id="txtMessages" placeholder="Message" rows="15" ></textarea>
                                    </address>	
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1 invoice-col">
                                    <address>
                                    Attachment
                                    </address>
                                </div>
                                <div class="col-sm-11 invoice-col">
                                    <address>
                                        <div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Attachment">
                                            <i class="fa fa-paperclip"></i> Attachment
                                            <input type="file" class="form-control" multiple name="mul_file[]" id="mul_file" title="" data-widget="Add Attachment" data-toggle="tooltip" data-original-title="Add Attachment"/>
                                        </div>
                                    </address>	
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 invoice-col">
                                    <address>
                                        <input type="hidden" name="action" id="action" value="submit"/>
                                        <button type="submit" class="btn btn-primary btn-flat" style="float:right;">Send Mail</button>
                                    </address>	
                                </div>
                            </div>
                        </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<link href="../../css/redactor.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/redactor.js"></script>
<script>
$(document).ready(function(){ 
    $(".areasize").autosize({append: "\n"});
	getData("<?=$mailtype;?>",<?=$type;?>,1);
	$('.redactor').redactor();
	$("#frm2").validate({
	rules: {
	txtMailTo:{"required":true,"email":true},
	txtSubject:"required"
	},
messages: {	
	
	},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});	
});

function getData(mailtype,type,val)
{
	if(mailtype !=$("#txtMailType").val())
	{
		$("#start").val('');
		$("#end").val('');
		$("#txtMailType").val(mailtype);
	    $("#txtTypeid").val(type);
		$("#fromnum").html("1 - 20");
		$("#totalmail").html("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
	}
	
	getData1(val);
}

function getData1(val)
{
	var liid = $("#txtTypeid").val();
	$("[id^=li_]").removeClass('active');
	$("#li_"+liid).addClass('active');
	$("#frommailicon,#tomailicon").attr('disabled',true);
	$('#inbox_tbody').html('<tr><td align="center" colspan="6"><img src="../../img/loading.gif" /></td></tr>');
		$.post("options.php?id=58",{mailtype:""+$("#txtMailType").val()+"",start:""+$("#start").val()+"",val:""+val+"",itemperpage:20}, function(html) {
			var jsondata1 = html.split("@@@@@@");
			var jsondata = JSON.parse(jsondata1[0]);
			$('#inbox_tbody').empty();
			var i =0;
			$.each(jsondata, function(index, array){
				i = parseInt(i) + 1;var opening = closing = '';
				if(array['seen']=="unseen")
				{opening = '<strong>';closing = '</strong>';}else{opening = '';closing = '';}
				$('<tr><td>'+opening+''+array['fromaddress']+''+closing+'</td><td>'+opening+''+array['subject']+''+closing+'</td><td><span class="label label-primary">'+array['date']+'</span></td><td><a href="viewMailDetails.php?msgid='+array['Msgno']+'&mailtype='+$("#txtMailType").val()+'"><strong>Details</strong></a></td></tr>').appendTo("#inbox_tbody");
			});
			$("#totalmail").html(jsondata1[1]);
			$("#txtTotalMail").val(jsondata1[1]);	
			$("#frommailicon,#tomailicon").removeAttr('disabled');
			$("#start").val(jsondata1[2]);
			$("#end").val(parseInt(jsondata1[2]) + 19);
			if($("#start").val()==1 || parseInt($("#start").val())<0)
			{
				$("#frommailicon").attr('disabled',true);
			}
			
			if(parseInt($("#end").val())>=jsondata1[1])
			{
				$("#tomailicon").attr('disabled',true);
				$("#end").val(parseInt(jsondata1[1]) - parseInt($("#start").val()) + 1);
			}
			$("#fromnum").html($("#start").val()+" - "+$("#end").val());
			if(i==0)
			{
				$('<tr><td colspan="4" align="center" style="color:red;">Sorry!!!, mail not found</td></tr>').appendTo("#inbox_tbody");
			}
	});
}
</script>
    </body>
</html>
<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$id = $_REQUEST['id'];
$display->logout_iu();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->updateCOADetails();
	header('Location : ./coa_list.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
$obj->viewCOARecords($id);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(4); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-truck"></i>&nbsp;Contract of Affreightment&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Contract of Affreightment</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="coa_list.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">			
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                            UPDATE COA
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
					<div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Cargo/Material Type
                            <address>
                               <select  name="selCargoType" class="form-control" id="selCargoType" >
								<?php 
                                $obj->getCargoTypeList();
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                           Vessel Category
                            <address>
                                <select  name="selVCType" class="form-control" id="selVCType" >
									<?php 
                                    $obj->getVesselCategoryList();
                                    ?>
                                    </select>
                            </address>
                        </div><!-- /.col -->
                     
					</div>
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            COA Number
                            <address>
                               <input type="text" name="txtCOANo" id="txtCOANo" class="form-control"  placeholder="COA Number" autocomplete="off" value="<?php echo $obj->getFun1();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            COA CP Date
                            <address>
                                <input type="text" name="txtCOADate" id="txtCOADate" class="form-control"  placeholder="COA CP Date" autocomplete="off" value="<?php echo date("d-m-Y",strtotime($obj->getFun2()));?>"/>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Cargo Qty(MT)
                            <address>
                               <input type="text" name="txtCQty" id="txtCQty" class="form-control" autocomplete="off" placeholder="Cargo Qty(MT)" value="<?php echo $obj->getFun3();?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                          Owner
                            <address>
                                <select  name="selOwner" class="form-control" id="selOwner">
								<?php 
                                $obj->getVendorListNewForCOA(11);
                                ?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Broker
                            <address>
                                <select  name="selBroker" class="form-control" id="selBroker">
								<?php 
								$obj->getVendorListNewForCOA(12);
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                          Charterer
                            <address>
                                <select  name="selCharterer" class="form-control" id="selCharterer">
								<?php 
								$obj->getVendorListNewForCOA(7);
								?>
                                </select>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                            No. of Shipments
                            <address>
                               <input type="text" name="txtNoofShipment" id="txtNoofShipment" class="form-control"  placeholder="No. of Shipments" autocomplete="off" value="<?php echo $obj->getFun7();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Shipment Qty(MT)
                            <address>
                                <input type="text" name="txtSQty" id="txtSQty" class="form-control"  placeholder="Shipment Qty(MT)" autocomplete="off" value="<?php echo $obj->getFun8();?>"/>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Tolerance ( % )
                            <address>
                               <input type="text" name="txtTolerance" id="txtTolerance" class="form-control" autocomplete="off" placeholder="Tolerance ( % )" value="<?php echo $obj->getFun9();?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    
                    
                  <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Shipment ( s )
                            <address>
                               <input type="text" name="txtShipment" id="txtShipment" class="form-control"  placeholder="Shipment ( s )" autocomplete="off" value="<?php echo $obj->getFun10();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Per/Month
                            <address>
                                <input type="text" name="txtper" id="txtper" class="form-control"  placeholder="Per/Month" autocomplete="off" value="<?php echo $obj->getFun11();?>"/>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                           Starting Date
                            <address>
                               <input type="text" name="txtStartDate" id="txtStartDate" class="form-control" autocomplete="off" placeholder="Starting Date" value="<?php echo date("d-M-Y",strtotime($obj->getFun12()));?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Notice ( Days )
                            <address>
                               <input type="text" name="txtNotice" id="txtNotice" class="form-control"  placeholder="Notice ( Days )" autocomplete="off" value="<?php echo $obj->getFun13();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Freight Rate (USD/MT)
                            <address>
                                <input type="text" name="txtFRate" id="txtFRate" class="form-control"  placeholder="Freight Rate (USD/MT)" autocomplete="off" value="<?php echo $obj->getFun14();?>"/>
                             </address>
                        </div><!-- /.col -->
                        
                         <div class="col-sm-2 invoice-col">
						   <?php if($obj->getFun15() != " "){?>
                           Previous Attachments
                           <address>
                            <a href="../../attachment/<?php echo $obj->getFun15();?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="Click to view file"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $obj->getFun15();?></a>
                            </address>
                            <?php }?>
                            </div>
                             <div class="col-sm-2 invoice-col">
                            <address>
								<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Inspector's Report">
                                    <i class="fa fa-paperclip"></i> Freight Matrix
                                    <input type="file" class="form-control" multiple name="fre_matrix" id="fre_matrix" title="" data-widget="Freight Matrix" data-toggle="tooltip" data-original-title="Freight Matrix"/>
                                </div>
							</address>
						</div>
                        
						
					</div>
                    
                   
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           Demurrage Rate
                            <address>
                               <input type="text" name="txtDemuRate" id="txtDemuRate" class="form-control"  placeholder="Demurrage Rate" autocomplete="off" value="<?php echo $obj->getFun16();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            Dispatch Rate
                            <address>
                                <input type="text" name="txtDisRate" id="txtDisRate" class="form-control"  placeholder="Dispatch Rate" autocomplete="off" value="<?php echo $obj->getFun17();?>"/>
                             </address>
                        </div><!-- /.col -->
                        
					  <div class="col-sm-4 invoice-col">
                            Add Comm ( % )
                        <address>
                        <input type="text" name="txtAddComm" id="txtAddComm" class="form-control" autocomplete="off" placeholder="Add Comm ( % )" value="<?php echo $obj->getFun18();?>"/>
                          </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                           BAF
                            <address>
                            <?php if($obj->getFun19() == "1"){$checked = 'checked="checked"';}else{$checked = '';} ?>
                               <input type="checkbox" name="checkBAFbox" id="checkBAFbox" onclick="getShow();" <?php echo $checked;?> value="1" />
                            </address>
                        </div><!-- /.col -->
                        
					</div>
                    
                    
                    <div class="row invoice-info" style="display:none;" id="rowBAF">
                        <div class="col-sm-4 invoice-col">
                           Bunker Price IFO  ( USD/MT )
                            <address>
                               <input type="text" name="txtBPrice" id="txtBPrice" class="form-control"  placeholder="Bunker Price IFO ( USD/MT )" autocomplete="off" value="<?php echo $obj->getFun20();?>"/>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            BAF Terms ( Days )
                            <address>
                                <input type="text" name="txtBAFTerms" id="txtBAFTerms" class="form-control"  placeholder="BAF Terms ( Days )" autocomplete="off" value="<?php echo $obj->getFun21();?>"/>
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-4 invoice-col">
                            BAF ( USD/MT )
                            <address>
                               <input type="text" name="txtIncDec" id="txtIncDec" class="form-control" autocomplete="off" placeholder="BAF ( USD/MT )" value="<?php echo $obj->getFun22();?>"/>
                            </address>
                        </div><!-- /.col -->
					</div>
                    
                    
                     <div class="row invoice-info">
                        <div class="col-sm-8 invoice-col">
                            Remarks
                            <address>
                               <textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" rows="3" placeholder="Remarks ..." ><?php echo $obj->getFun23();?></textarea>
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-2 invoice-col">
						   <?php if($obj->getFun24() != " "){?>
                           Previous Attachments
                           <address>
                            <a href="../../attachment/<?php echo $obj->getFun24();?>" target="_blank" style="color:blue;" data-toggle="tooltip" data-original-title="Click to view file"><i class="fa fa-external-link"></i>&nbsp;&nbsp;<?php echo $obj->getFun24();?></a>
                            </address>
                            <?php }?>
                            </div>
                       <div class="col-sm-2 invoice-col">
                           &nbsp;
                            <address>
								<div class="btn btn-success btn-file btn-flat" data-toggle="tooltip" data-original-title="Add Inspector's Report">
                                    <i class="fa fa-paperclip"></i> Attachment
                                    <input type="file" class="form-control" multiple name="attach_file" id="attach_file" title="" data-widget="Attachment" data-toggle="tooltip" data-original-title="Attachment"/>
                                </div>
							</address>
                        </div><!-- /.col -->
					</div>
                    
                     
				
				<div class="box-footer" align="right">
							<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button>
							<input type="hidden" name="action" value="submit" /><input type="hidden" name="upstatus" id="upstatus" value="" />
				</div>
         
				
					
				</form>
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#selOwner").val('<?php echo $obj->getFun4();?>');
$("#selBroker").val('<?php echo $obj->getFun5();?>');
$("#selCharterer").val('<?php echo $obj->getFun6();?>');
$("#selCargoType").val('<?php echo $obj->getFun25();?>');
$("#selVCType").val('<?php echo $obj->getFun26();?>');
$("[id^=txtDate_]").datepicker({
    format: 'dd-mm-yyyy'
});
$(".areasize").autosize({append: "\n"});
$("#frm1").validate({
rules: {
	selCargoType:{required:true},
	selVCType:{required:true},
	txtCOANo:{required:true},
	txtCOADate:{required:true},
	txtCQty:{required:true, number:true},
	selOwner:{required:true},
	selBroker:{required:true},
	selCharterer:{required:true},
	txtNoofShipment:{required:true, number:true},
	txtSQty:{required:true, number:true},
	txtTolerance:{required:true},
	txtShipment:{required:true, number:true},
	txtper:{required:true, number:true},
	txtStartDate:{required:true},
	txtNotice:{number:true},
	txtFRate:{number:true},
	//txtDemuRate:{number:true},
	txtDisRate:{number:true},
	txtAddComm:{number:true},
	txtBPrice:{required : "#checkBAFbox:checked" , number:true},
	txtBAFTerms:{required : "#checkBAFbox:checked" , number:true},
	txtIncDec:{required : "#checkBAFbox:checked" , number:true}
	},
messages: {
	selCargoType:{required:"*"},
	selVCType:{required:"*"},
	txtCOANo:{required:"*"},
	txtCOADate:{required:"*"},
	txtCQty:{required:"*"},
	selOwner:{required:"*"},
	selBroker:{required:"*"},
	selCharterer:{required:"*"},
	txtNoofShipment:{required:"*"},
	txtSQty:{required:"*"},
	txtTolerance:{required:"*"},
	txtShipment:{required:"*"},
	txtper:{required:"*"},
	txtStartDate:{required:"*"},
	txtNotice:{number:"*"},
	txtFRate:{number:"*"},
	//txtDemuRate:{number:"*"},
	txtDisRate:{number:"*"},
	txtAddComm:{number:"*"},
	txtBPrice:{required : "*" },
	txtBAFTerms:{required : "*" },
	txtIncDec:{required : "*" }
	}
});	
getShow();
});


function getShow()
{
	if ($("#checkBAFbox").iCheck('check')) 
	{
		$("#rowBAF").show();
	}
	else
	{
		$("#rowBAF").hide();
		$("#txtBPrice,#txtBAFTerms,#txtIncDec").val("");
	}
}

</script>
    </body>
</html>
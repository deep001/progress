<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$mappingid = $_REQUEST['mappingid'];
$cost_sheet_id = $_REQUEST['cost_sheet_id'];
if($obj->getFreightEstimationStatus1($mappingid) == 1 )
{
	header('Location : ./voyage_estimation.php?mappingid='.$mappingid.'&cost_sheet_id='.$cost_sheet_id);
}
if($obj->getFreightEstimationRecProcessWise($mappingid,$cost_sheet_id) > 0 )
{
	if($obj->getFreightEstimationStatus($mappingid,$cost_sheet_id) == 1 )
	{
		header('Location : ./update_voyage_estimation.php?mappingid='.$mappingid.'&cost_sheet_id='.$cost_sheet_id);
	}
	else
	{
		header('Location : ./update_voyage_estimation1.php?mappingid='.$mappingid.'&cost_sheet_id='.$cost_sheet_id);
	}
}
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->insertVCIDetails();
	header('Location : ./nomination_at_glance.php?msg='.$msg);
 }
if($obj->getLastCostSheetID($mappingid) > 0)
{
	$obj->viewFreightEstimationRecords($mappingid,$obj->getLastCostSheetID($mappingid));
	$rdoMMarket = $obj->getFun12();
	$rdoCap = $obj->getFun6();
	$rdoDWT = $obj->getFun21();
	$rdoQty = $obj->getFun29();
	$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&selFType=".$obj->getFun2().'&cost_sheet_id='.$cost_sheet_id;
}
else
{
	$rdoMarket = $rdoMMarket = $rdoCap = $rdoDWT = $rdoQty = 1;
	$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid.'&cost_sheet_id='.$cost_sheet_id;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Nominations at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				<!--   content put here..................-->
				<div align="right"><a href="nomination_at_glance.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
						
				<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                             Cost Sheet : Estimate
                            </h2>                            
                        </div><!-- /.col -->
                    </div>
					
				
                        <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Fixture Type
                                    <address>
                                    <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getFreightFixtureBasedOnID($obj->getFun2());?></strong>
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                     Main Particulars
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                   Nom ID
                                    <address>
                                    <input type="text" name="txtNomID" id="txtNomID" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getMappingData($mappingid,"NOMINATION_ID");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                     Vessel Name
                                    <address>
                                       <input type="text" name="txtVName" id="txtVName" class="form-control" autocomplete="off" placeholder="Nom ID" readonly value="<?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME");?>" />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                     Vessel Type
                                    <address>
                                       <input type="text" name="txtVType" id="txtVType" class="form-control" autocomplete="off" placeholder="Vessel Type" readonly value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_TYPE"));?>" />
                                    </address>
                                </div><!-- /.col -->
                            </div>
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                   Date<span style="font-size:10px; font-style:italic;"> (for financial year)</span>
                                    <address>
                                    <input type="text" name="txtDate" id="txtDate" class="form-control" autocomplete="off" placeholder="Date (for financial year)" value="<?php echo date("d-m-Y",strtotime($obj->getFun3()));?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                     Voyage No.
                                    <address>
                                        <input type="text" name="txtVNo" id="txtVNo" class="form-control" autocomplete="off" placeholder="Voyage No." value="<?php echo $obj->getFun10();?>" />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                     Cost Sheet Name
                                    <address>
                                        <input type="text" name="txtENo" id="txtENo" class="form-control" autocomplete="off" value="<?php echo $obj->getCostSheetNameBasedOnID($cost_sheet_id);?>" placeholder="Cost Sheet Name" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                            
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  
                                    <address>
                                   <input name="rdoDWT" class="checkbox" id="rdoDWT1" type="radio" style="cursor:pointer;" value="1"  <?php if($rdoDWT == 1) echo "checked"; ?> onclick="showDWTField();"  />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    
                                    <address>
                                     <input name="rdoDWT" class="checkbox" id="rdoDWT2" type="radio" style="cursor:pointer;" value="2"  <?php if($rdoDWT == 2) echo "checked"; ?> onclick="showDWTField();" />
                                    </address>
                                </div><!-- /.col -->
                               
                             </div>
                            
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  DWT<span style="font-size:10px; font-style:italic;">(Summer)</span>
                                    <address>
                                   <input type="text" name="txtDWTS" id="txtDWTS"  class="form-control" autocomplete="off" placeholder="DWT" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"SUMMER_1");?>" readonly/>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                  DWT<span style="font-size:10px; font-style:italic;">(Tropical)</span>
                                    <address>
                                        <input type="text" name="txtDWTT" id="txtDWTT" class="form-control" autocomplete="off" placeholder="DWT" value="<?php echo $obj->getVesselMaster1Data($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"TROPICAL_1");?>" disabled="disabled" readonly ><input type="hidden" name="txtTCNo" id="txtTCNo" class="input-text" size="10" autocomplete="off" value="<?php echo $obj->getFun11();?>" />
                             </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                  
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  &nbsp;
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    LayCan Starts
                                    <address>
                                       <?php if(date("d-m-Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"FROM_DATE"))) == "01-01-1970")
                                        {
                                            echo '<input type="text" name="txtDPLayCanStarts" id="txtDPLayCanStarts" class="form-control" value="" />';
                                        }
                                        else
                                        {
                                            echo '<input type="text" name="txtDPLayCanStarts" id="txtDPLayCanStarts" class="form-control" value="'.date("d-m-Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"FROM_DATE"))).'" />';
                                        }
                                        ?>	
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                     LayCan Finishes
                                    <address>
                                      <?php if(date("d-m-Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"TO_DATE"))) == "01-01-1970")
                                        {
                                            echo '<input type="text" name="txtDPLayCanFinishes" id="txtDPLayCanFinishes" class="form-control" value="" />';
                                        }
                                        else
                                        {
                                            echo '<input type="text" name="txtDPLayCanFinishes" id="txtDPLayCanFinishes" class="form-control" value="'.date("d-m-Y",strtotime($obj->getVesselOpenEntryDeta($obj->getMappingData($mappingid,"OPEN_VESSEL_ID"),"TO_DATE"))).'" />';
                                        }
                                        ?>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             
                             
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                        Discharge Port           
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Discharge Port
                                    <address>
                                    <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getContractLoadPortBasedOnID($obj->getMappingData($mappingid,"CARGO_IDS"),"DISPORT"); ?></strong>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Cargo Name
                                    <address>
                                        <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCargoContarctForMapping($obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"CARGO_POSITION"));?></strong>
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                     Cargo Qty MT
                                    <address>
                                        <strong>&nbsp;&nbsp;&nbsp;<?php echo $obj->getCargoContarctQTYForMapping1($obj->getMappingData($mappingid,"CARGO_IDS"),$obj->getMappingData($mappingid,"CARGO_POSITION"));?></strong>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Date of Delivery
                                  <address>
                                  <?php if(date("d-m-Y",strtotime($obj->getNomDetailsData($mappingid,"DATE_OF_DELIVERY"))) == "01-01-1970")
                                    {
                                        echo '<input type="text" name="txtDateofDelivery" id="txtDateofDelivery" placeholder="dd-mm-yyyy" class="form-control" value="" />';
                                    }
                                    else
                                    {
                                        echo '<input type="text" name="txtDateofDelivery" id="txtDateofDelivery" class="form-control" value="'.date("d-m-Y",strtotime($obj->getNomDetailsData($mappingid,"DATE_OF_DELIVERY"))).'" />';
                                    }
                                    ?>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Place of Delivery
                                    <address>
                                       <input type="text" name="txtPOD" id="txtPOD" class="form-control" placeholder="Place of Delivery" autocomplete="off" value="<?php echo $obj->getNomDetailsData($mappingid,"PLACE_OF_DELIVERY");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Alarm <span style="font-size:10px; font-style:italic; color:#ccc;">(No. of Days before Re-Del)</span>
                                    <address>
                                      <input type="text" name="txtAlarm" id="txtAlarm" class="form-control" autocomplete="off" value="<?php echo $obj->getNomDetailsData($mappingid,"ALARM");?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                        
                        
                        <div class="row invoice-info">
                           <div class="col-sm-4 invoice-col">
                                   Nomination ID
                                    <address>
                                      <strong>&nbsp;&nbsp;&nbsp; <?php echo $obj->getMappingData($mappingid,"NOMINATION_ID");?></strong>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                        
                        <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                        TC CP Terms : Sea Passage
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                        <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    Wind Force
                                    <address>
                                    <input type="text" name="txtCPWindForce" id="txtCPWindForce" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_WIND_FORCE");?>" />
                                    </address>
                                </div><!-- /.col -->
                            
                                <div class="col-sm-4 invoice-col">
                                   Speed Laden(Kts)
                                    <address>
                                    <input type="text" name="txtCPSpeedLaden" id="txtCPSpeedLaden" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_SPEED_LADEN");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Speed Ballast (Kts)
                                    <address>
                                        <input type="text" name="txtCPSpeedBallast" id="txtCPSpeedBallast" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_SPEED_BALLAST");?>" />
                                    </address>
                                </div><!-- /.col -->
                                
                            </div>
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                   FO Cons Laden ( MT/Day)
                                    <address>
                                    <input type="text" name="txtCPFOConspLaden" id="txtCPFOConspLaden" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_FO_CONSP_LADEN");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                     DO Cons Laden ( MT/Day)
                                    <address>
                                        <input type="text" name="txtCPDOConspLaden" id="txtCPDOConspLaden" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DO_CONSP_LADEN");?>" />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                     FO Cons Ballast ( MT/Day)
                                    <address>
                                        <input type="text" name="txtCPFOConspBallast" id="txtCPFOConspBallast" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_FO_CONSP_BALLAST");?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                            
                            
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                   DO Cons Ballast ( MT/Day)
                                    <address>
                                   <input type="text" name="txtCPDOConspBallast" id="txtCPDOConspBallast" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DO_CONSP_BALLAST");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                     FO Cons Cleaning( MT/Day)
                                    <address>
                                     <input type="text" name="txtCPFOTkCleaning" id="txtCPFOTkCleaning" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_FO_TK_CLEANING");?>" />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                    FO Cons G Freeing ( MT/Day)FO Cons G Freeing ( MT/Day)
                                    <address>
                                        <input type="text" name="txtCPFOGasFreeing" id="txtCPFOGasFreeing" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_FO_GAS_FREEING");?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                           
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  FO Cons Inerting ( MT/Day)
                                    <address>
                                     <input type="text" name="txtCPFOInerting" id="txtCPFOInerting" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_FO_INSERTING");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    FO Cons Heating( MT/Day)
                                    <address>
                                        <input type="text" name="txtCPFOCargoHeating" id="txtCPFOCargoHeating" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_FO_CARGO_HEATING");?>" />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                     DO Cons Cleaning( MT/Day)
                                    <address>
                                        <input type="text" name="txtCPDOTkCleaning" id="txtCPDOTkCleaning" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DO_TK_CLEANING");?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                               <div class="col-sm-4 invoice-col">
                                    DO Cons G Freeing ( MT/Day)
                                    <address>
                                      <input type="text" name="txtCPDOGasFreeing" id="txtCPDOGasFreeing" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DO_GAS_FREEING");?>" />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                     DO Cons Inerting ( MT/Day)
                                    <address>
                                      <input type="text" name="txtCPDOInerting" id="txtCPDOInerting" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DO_INSERTING");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                     DO Cons Heating( MT/Day)
                                    <address>
                                      <input type="text" name="txtCPDOCargoHeating" id="txtCPDOCargoHeating" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DO_CARGO_HEATING");?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                               <div class="col-sm-4 invoice-col">
                                   Speed Allowance(Kts)
                                    <address>
                                      <input type="text" name="txtSpeedAllowances" id="txtSpeedAllowances" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_SPEED_ALLOWANCES");?>" />
                                    </address>
                                </div><!-- /.col -->
                                 
                             </div>
                             
                             
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                        TC CP Terms : Port     
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Man Pressure(Kg/sq cm)
                                    <address>
                                    <input type="text" name="txtCPManPressure" id="txtCPManPressure" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_MAN_PRESSURE");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Load Rate (MT/Hr)
                                    <address>
                                        <input type="text" name="txtCPLoadRate" id="txtCPLoadRate" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_LOAD_RATE");?>" />
                                    </address>
                                </div><!-- /.col -->
                                 <div class="col-sm-4 invoice-col">
                                    Disch Rate (MT/Hr)
                                    <address>
                                        <input type="text" name="txtCPDischRate" id="txtCPDischRate" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DISCH_RATE");?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                   FO Cons Ldg ( MT/Day)
                                  <address>
                                   <input type="text" name="txtCPFOConspLoading" id="txtCPFOConspLoading" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_FO_CONSP_LOADING");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    DO Cons Ldg ( MT/Day)
                                    <address>
                                       <input type="text" name="txtCPDOConspLoading" id="txtCPDOConspLoading" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DO_CONSP_LOADING");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                   FO Cons Disch ( MT/Day)
                                    <address>
                                      <input type="text" name="txtCPFOConspDisch" id="txtCPFOConspDisch" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_FO_CONSP_DISCH");?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                   DO Cons Disch ( MT/Day)
                                  <address>
                                   <input type="text" name="txtCPDOConspDisch" id="txtCPDOConspDisch" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DO_CONSP_DISCH");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    FO Cons Idle ( MT/Day)
                                    <address>
                                       <input type="text" name="txtCPFOConspIdling" id="txtCPFOConspIdling" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_FO_CONSP_IDLING");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                   DO Consp Idle (MT/Day)
                                    <address>
                                      <input type="text" name="txtCPDOConspIdling" id="txtCPDOConspIdling" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DO_CONSP_IDLING");?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                   Load Rate ( MT/Day)
                                  <address>
                                   <input type="text" name="txtCPLoadRate_1" id="txtCPLoadRate_1" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_LOAD_RATE_1");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Disch Rate ( MT/Day)
                                    <address>
                                       <input type="text" name="txtDischRate_1" id="txtDischRate_1" class="form-control" autocomplete="off" value="<?php echo $obj->getTC_CP_TERMS_Data($mappingid,"CP_DISCH_RATE_1");?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                      
                        
                        
                        
                        
                            <?php 
                            $mysql = "select * from commercial_parameters_nomnation_wise where MAPPINGID='".$mappingid."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."' and VESSEL_IMO_ID='".$vessel_id."'";
                             //echo $mysql;die();
                            $myres = mysql_query($mysql);
                            $myrec = mysql_num_rows($myres);
                            if($myrec == 0)
                            {
                            ?>
                            <div class="row invoice-info">
                               
                                <div class="col-sm-4 invoice-col">
                                   Nomination ID
                                    <address>
                                      <strong>&nbsp;&nbsp;&nbsp; <?php echo $obj->getMappingData($mappingid,"NOMINATION_ID");?></strong>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                            
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                        COMMERCIAL - PARAMETERS
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Vessel Name
                                    <address>
                                   <input type="text" name="txtVName" id="txtVName" class="form-control" readonly value="<?php echo $name;?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Vessel Type
                                    <address>
                                        <input type="text" name="txtVType" id="txtVType" class="form-control" readonly value="<?php echo $type;?>" />
                                    </address>
                                </div><!-- /.col -->
                                 
                             </div>
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                  Date
                                    <address>
                                   <?php if($obj->getVesselParameterData($vessel_imo_id,"MAIN_DATA_DATE",$user) != ""){?>
                                    <input type="text" name="txtDate" id="txtDate" class="form-control" readonly value="<?php echo date("d-m-Y",strtotime($obj->getVesselParameterData($vessel_imo_id,"MAIN_DATA_DATE",$user)));?>" />
                                    <?php }else{?>
                                    <input type="text" name="txtDate" id="txtDate" class="form-control" readonly value="<?php echo date("d-m-Y",time())?>" />
                                    <?php }?>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    DWT <span style="font-size:10px; font-style:italic;">(Summer)</span>
                                    <address>
                                        <input type="text" name="txtDWTS" id="txtDWTS" class="form-control"  value="<?php echo $obj->getVesselIMOData($vessel_imo_id,"DWT");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 Draft <span style="font-size:10px; font-style:italic;">(Summer)</span>
                                    <address>
                                   <input type="text" name="txtDraft" id="txtDraft" class="form-control"  value="<?php echo $obj->getVesselIMOData($vessel_imo_id,"DRAFTM");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    TPC
                                    <address>
                                        <input type="text" name="txtTPC" id="txtTPC" class="form-control"  value="<?php echo $obj->getVesselParticularData('SUMMER_3','vessel_master_1',$vessel_imo_id);?>" readonly />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                        Speed Data
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            
                             <div class="box-body no-padding">
                               <table class="table table-striped">
                                <thead>
                                <tr>
                                <td width="31%" align="left" ></td>
                                <td width="23%" align="right" valign="top">Full Speed</td>
                                <td width="23%" align="right" valign="top">Eco Speed 1</td>
                                <td width="23%" align="right" valign="top">Eco Speed 2</td>
                               
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <td align="left">Ballast Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
                                <td align="right" valign="top" ><input type="text" name="txtBFullSpeed" id="txtBFullSpeed" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"B_FULL_SPEED",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtBEcoSpeed1" id="txtBEcoSpeed1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"B_ECO_SPEED1",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtBEcoSpeed2" id="txtBEcoSpeed2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"B_ECO_SPEED2",$user);?>" /></td>
                                </tr>
                                <tr>
                                <td align="left">Laden Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
                                <td align="right" valign="top"><input type="text" name="txtLFullSpeed" id="txtLFullSpeed" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"L_FULL_SPEED",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtLEcoSpeed1" id="txtLEcoSpeed1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"L_ECO_SPEED1",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtLEcoSpeed2" id="txtLEcoSpeed2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"L_ECO_SPEED2",$user);?>" /></td>
                                </tr>
                                </tbody>
                                </table>
                                </div>
                                
                                <div class="row">
                                  <div class="col-xs-12">
                                    <h2 class="page-header">
                                        FO Consumption MT/Day
                                    </h2>                            
                                  </div><!-- /.col -->
                               </div>
                               <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 FO Grade
                                    <address>
                                     <input type="text" name="txtFOGrade" id="txtFOGrade" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"FO_GRADE",$user);?>" />
                                    </address>
                                </div><!-- /.col -->
                                
                             </div>
                               <div class="box-body no-padding">
                               <table class="table table-striped">
                                <thead>
                                <tr>
                                <td width="31%" align="left" ></td>
                                <td width="23%" align="right" valign="top">Full Speed</td>
                                <td width="23%" align="right" valign="top">Eco Speed 1</td>
                                <td width="23%" align="right" valign="top">Eco Speed 2</td>
                               
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <td align="left">Ballast Passage</td>
                                <td align="right" valign="top" ><input type="text" name="txtFOBPassageFS" id="txtFOBPassageFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"BP_FO_FULL_SPEED",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtFOBPassageES1" id="txtFOBPassageES1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"BP_FO_ECO_SPEED1",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtFOBPassageES2" id="txtFOBPassageES2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"BP_FO_ECO_SPEED2",$user);?>" /></td>
                                </tr>
                                <tr>
                                <td align="left">Laden Passage</td>
                                <td align="right" valign="top"><input type="text" name="txtFOLPassageFS" id="txtFOLPassageFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"LP_FO_FULL_SPEED",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtFOLPassageES1" id="txtFOLPassageES1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"LP_FO_ECO_SPEED1",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtFOLPassageES2" id="txtFOLPassageES2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"LP_FO_ECO_SPEED2",$user);?>" /></td>
                                </tr>
                                </tbody>
                                </table>
                                </div>
                                
                               <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 In Port - Idle
                                    <address>
                                   <input type="text" name="txtFOIPIdleFS" id="txtFOIPIdleFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"PI_FO_FULL_SPEED",$user);?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    In Port - Working
                                    <address>
                                        <input type="text" name="txtFOIPWorkingFS" id="txtFOIPWorkingFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"PW_FO_FULL_SPEED",$user);?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Others
                                    <address>
                                        <input type="text" name="txtFOOthersFS" id="txtFOOthersFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"OTH_FO_FULL_SPEED",$user);?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             
                             
                             
                             <div class="row">
                                  <div class="col-xs-12">
                                    <h2 class="page-header">
                                        DO Consumption per MT/Day
                                    </h2>                            
                                  </div><!-- /.col -->
                               </div>
                               <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 DO Grade
                                    <address>
                                     <input type="text" name="txtDOGrade" id="txtDOGrade" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"DO_GRADE",$user);?>" />
                                    </address>
                                </div><!-- /.col -->
                                
                             </div>
                                <div class="box-body no-padding">
                               <table class="table table-striped">
                                <thead>
                                <tr>
                                <td width="31%" align="left" ></td>
                                <td width="23%" align="right" valign="top">Full Speed</td>
                                <td width="23%" align="right" valign="top">Eco Speed 1</td>
                                <td width="23%" align="right" valign="top">Eco Speed 2</td>
                               
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <td align="left">Ballast Passage</td>
                                <td align="right" valign="top"><input type="text" name="txtDOBPassageFS" id="txtDOBPassageFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"BP_DO_FULL_SPEED",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtDOBPassageES1" id="txtDOBPassageES1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"BP_DO_ECO_SPEED1",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtDOBPassageES2" id="txtDOBPassageES2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"BP_DO_ECO_SPEED2",$user);?>" /></td>
                                </tr>
                                <tr>
                                <td align="left">Laden Passage</td>
                                <td align="right" valign="top"><input type="text" name="txtDOLPassageFS" id="txtDOLPassageFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"LP_DO_FULL_SPEED",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtDOLPassageES1" id="txtDOLPassageES1" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"LP_DO_ECO_SPEED1",$user);?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtDOLPassageES2" id="txtDOLPassageES2" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"LP_DO_ECO_SPEED2",$user);?>" /></td>
                                </tr>
                                </tbody>
                                </table>
                                </div>
                                
                               <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 In Port - Idle
                                    <address>
                                   <input type="text" name="txtDOIPIdleFS" id="txtDOIPIdleFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"PI_DO_FULL_SPEED",$user);?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    In Port - Working
                                    <address>
                                        <input type="text" name="txtDOIPWorkingFS" id="txtDOIPWorkingFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"PW_DO_FULL_SPEED",$user);?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Others
                                    <address>
                                        <input type="text" name="txtDOOthersFS" id="txtDOOthersFS" class="form-control" autocomplete="off" value="<?php echo $obj->getVesselParameterData($vessel_imo_id,"OTH_DO_FULL_SPEED",$user);?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             <div class="row">
                                  <div class="col-xs-12">
                                    <h2 class="page-header">
                                        Source / Other Details
                                    </h2>                            
                                  </div><!-- /.col -->
                               </div>
                               <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 Remarks
                                    <address>
                                   <textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" cols="90%" rows="5"><?php echo $obj->getVesselParameterData($vessel_imo_id,"REMARKS",$user);?></textarea>
                                    </address>
                                </div><!-- /.col -->
                               
                             </div>
                             
                              <div class="box-footer" align="right"> 
                            <button type="submit" class="btn btn-primary btn-flat" onclick="return getSubmit3(1);">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary btn-flat" onclick="return getSubmit3(2);">Submit & Close</button>
                <input type="hidden" name="action3" id="action3" value="submit" />
                         </div> 
                             <?php }else{
                            $myrows = mysql_fetch_assoc($myres);
                            ?>
                             <div class="row invoice-info">
                               <div class="col-sm-4 invoice-col">
                                   Nomination ID
                                    <address>
                                      <strong>&nbsp;&nbsp;&nbsp; <?php echo $obj->getMappingData($mappingid,"NOMINATION_ID");?></strong>
                                    </address>
                                </div><!-- /.col -->
                             </div>
                            
                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                        COMMERCIAL - PARAMETERS
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                        Main Data
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 Vessel Name
                                    <address>
                                     <input type="text" name="txtVName" id="txtVName" class="form-control" readonly value="<?php echo $obj->getVesselIMOData($myrows['VESSEL_IMO_ID'],"VESSEL_NAME");?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Vessel Type
                                    <address>
                                        <input type="text" name="txtVType" id="txtVType" class="form-control" readonly value="<?php echo $obj->getVesselTypeBasedOnID($obj->getVesselIMOData($myrows['VESSEL_IMO_ID'],"VESSEL_TYPE"));?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 Date
                                    <address>
                                    <?php if($myrows['MAIN_DATA_DATE'] != ""){?>
                                    <input type="text" name="txtDate" id="txtDate" class="form-control" readonly value="<?php echo date("d-m-Y",strtotime($myrows['MAIN_DATA_DATE']));?>" />
                                    <?php }else{?>
                                    <input type="text" name="txtDate" id="txtDate" class="form-control" readonly value="<?php echo date("d-m-Y",time())?>" />
                                    <?php }?>
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                   DWT <span style="font-size:10px; font-style:italic;">(Summer)</span>
                                    <address>
                                        <input type="text" name="txtDWTS" id="txtDWTS" class="form-control"  value="<?php echo $obj->getVesselIMOData($vessel_imo_id,"DWT");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 Draft <span style="font-size:10px; font-style:italic;">(Summer)</span>
                                    <address>
                                    <input type="text" name="txtDraft" id="txtDraft" class="form-control"  value="<?php echo $obj->getVesselIMOData($vessel_imo_id,"DRAFTM");?>" readonly />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                   TPC
                                    <address>
                                        <input type="text" name="txtTPC" id="txtTPC" class="form-control"  value="<?php echo $obj->getVesselParticularData('SUMMER_3','vessel_master_1',$vessel_imo_id);?>" readonly />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                              <div class="box-body no-padding">
                               <table class="table table-striped">
                                <thead>
                                <tr>
                                <td width="31%" align="left" ></td>
                                <td width="23%" align="right" valign="top">Full Speed</td>
                                <td width="23%" align="right" valign="top">Eco Speed 1</td>
                                <td width="23%" align="right" valign="top">Eco Speed 2</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <td align="left" >Ballast Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
                                <td align="right" valign="top"><input type="text" name="txtBFullSpeed" id="txtBFullSpeed" class="form-control" autocomplete="off" value="<?php echo $myrows['B_FULL_SPEED'];?>" /></td>
                                <td align="right" valign="top" ><input type="text" name="txtBEcoSpeed1" id="txtBEcoSpeed1" class="form-control" autocomplete="off" value="<?php echo $myrows['B_ECO_SPEED1'];?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtBEcoSpeed2" id="txtBEcoSpeed2" class="form-control" autocomplete="off" value="<?php echo $myrows['B_ECO_SPEED2'];?>" /></td>
                                </tr>
                                <tr>
                                <td align="left" class="text" >Laden Speed <span style="font-size:10px; font-style:italic;">(Knots)</span></td>
                                <td align="right" valign="top"><input type="text" name="txtLFullSpeed" id="txtLFullSpeed" class="form-control" autocomplete="off" value="<?php echo $myrows['L_FULL_SPEED'];?>" /></td>
                                <td align="right" valign="top" ><input type="text" name="txtLEcoSpeed1" id="txtLEcoSpeed1" class="form-control" autocomplete="off" value="<?php echo $myrows['L_ECO_SPEED1'];?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtLEcoSpeed2" id="txtLEcoSpeed2" class="form-control" autocomplete="off" value="<?php echo $myrows['L_ECO_SPEED2'];?>" /></td>
                                </tr>
                                </tbody>
                                </table>
                                </div>
                             
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                        FO Consumption MT/Day
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 FO Grade
                                    <address>
                                     <input type="text" name="txtFOGrade" id="txtFOGrade" class="form-control" autocomplete="off" value="<?php echo $myrows['FO_GRADE'];?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div> 
                              <div class="box-body no-padding">
                               <table class="table table-striped">
                                <thead>
                                <tr>
                                <td width="31%" align="left" ></td>
                                <td width="23%" align="right" valign="top">Full Speed</td>
                                <td width="23%" align="right" valign="top">Eco Speed 1</td>
                                <td width="23%" align="right" valign="top">Eco Speed 2</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <td align="left" >Ballast Passage</td>
                                <td align="right" valign="top"><input type="text" name="txtFOBPassageFS" id="txtFOBPassageFS" class="form-control" autocomplete="off" value="<?php echo $myrows['BP_FO_FULL_SPEED'];?>" /></td>
                                <td align="right" valign="top" ><input type="text" name="txtFOBPassageES1" id="txtFOBPassageES1" class="form-control" autocomplete="off" value="<?php echo $myrows['BP_FO_ECO_SPEED1'];?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtFOBPassageES2" id="txtFOBPassageES2" class="form-control" autocomplete="off" value="<?php echo $myrows['BP_FO_ECO_SPEED2'];?>" /></td>
                                </tr>
                                <tr>
                                <td align="left" class="text" >Laden Passage</td>
                                <td align="right" valign="top"><input type="text" name="txtFOLPassageFS" id="txtFOLPassageFS" class="form-control" autocomplete="off" value="<?php echo $myrows['LP_FO_FULL_SPEED'];?>" /></td>
                                <td align="right" valign="top" ><input type="text" name="txtFOLPassageES1" id="txtFOLPassageES1" class="form-control" autocomplete="off" value="<?php echo $myrows['LP_FO_ECO_SPEED1'];?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtFOLPassageES2" id="txtFOLPassageES2" class="form-control" autocomplete="off" value="<?php echo $myrows['LP_FO_ECO_SPEED2'];?>" /></td>
                                </tr>
                                </tbody>
                                </table>
                                </div>
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 In Port - Idle
                                    <address>
                                   <input type="text" name="txtFOIPIdleFS" id="txtFOIPIdleFS" class="form-control" autocomplete="off" value="<?php echo $myrows['PI_FO_FULL_SPEED'];?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    In Port - Working
                                    <address>
                                        <input type="text" name="txtFOIPWorkingFS" id="txtFOIPWorkingFS" class="form-control" autocomplete="off" value="<?php echo $myrows['PW_FO_FULL_SPEED'];?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Others
                                    <address>
                                        <input type="text" name="txtFOOthersFS" id="txtFOOthersFS" class="form-control" autocomplete="off" value="<?php echo $myrows['OTH_FO_FULL_SPEED'];?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             
                             <div class="row">
                                <div class="col-xs-12">
                                    <h2 class="page-header">
                                        DO Consumption per MT/Day
                                    </h2>                            
                                </div><!-- /.col -->
                            </div>
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 DO Grade
                                    <address>
                                     <input type="text" name="txtDOGrade" id="txtDOGrade" class="form-control" autocomplete="off" value="<?php echo $myrows['DO_GRADE'];?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div> 
                             <div class="box-body no-padding">
                               <table class="table table-striped">
                                <thead>
                                <tr>
                                <td width="31%" align="left" ></td>
                                <td width="23%" align="right" valign="top">Full Speed</td>
                                <td width="23%" align="right" valign="top">Eco Speed 1</td>
                                <td width="23%" align="right" valign="top">Eco Speed 2</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <td align="left" >Ballast Passage</td>
                                <td align="right" valign="top"><input type="text" name="txtDOBPassageFS" id="txtDOBPassageFS" class="form-control" autocomplete="off" value="<?php echo $myrows['BP_DO_FULL_SPEED'];?>" /></td>
                                <td align="right" valign="top" ><input type="text" name="txtDOBPassageES1" id="txtDOBPassageES1" class="form-control" autocomplete="off" value="<?php echo $myrows['BP_DO_ECO_SPEED1'];?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtDOBPassageES2" id="txtDOBPassageES2" class="form-control" autocomplete="off" value="<?php echo $myrows['BP_DO_ECO_SPEED2'];?>" /></td>
                                </tr>
                                <tr>
                                <td align="left" class="text" >Laden Passage</td>
                                <td align="right" valign="top"><input type="text" name="txtDOLPassageFS" id="txtDOLPassageFS" class="form-control" autocomplete="off" value="<?php echo $myrows['LP_DO_FULL_SPEED'];?>" /></td>
                                <td align="right" valign="top" ><input type="text" name="txtDOLPassageES1" id="txtDOLPassageES1" class="form-control" autocomplete="off" value="<?php echo $myrows['LP_DO_ECO_SPEED1'];?>" /></td>
                                <td align="right" valign="top"><input type="text" name="txtDOLPassageES2" id="txtDOLPassageES2" class="form-control" autocomplete="off" value="<?php echo $myrows['LP_DO_ECO_SPEED2'];?>" /></td>
                                </tr>
                                </tbody>
                                </table>
                                </div>
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 In Port - Idle
                                    <address>
                                    <input type="text" name="txtDOIPIdleFS" id="txtDOIPIdleFS" class="form-control" autocomplete="off" value="<?php echo $myrows['PI_DO_FULL_SPEED'];?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    In Port - Working
                                    <address>
                                        <input type="text" name="txtDOIPWorkingFS" id="txtDOIPWorkingFS" class="form-control" autocomplete="off" value="<?php echo $myrows['PW_DO_FULL_SPEED'];?>" />
                                    </address>
                                </div><!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    Others
                                    <address>
                                        <input type="text" name="txtDOOthersFS" id="txtDOOthersFS" class="form-control" autocomplete="off" value="<?php echo $myrows['OTH_DO_FULL_SPEED'];?>" />
                                    </address>
                                </div><!-- /.col -->
                             </div>
                             
                             <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                 Remarks
                                    <address>
                                    <textarea class="form-control areasize" name="txtRemarks" id="txtRemarks" cols="90%" rows="5"><?php echo $myrows["REMARKS"];?></textarea>
                                    </address>
                                </div><!-- /.col -->
                              </div>
                            
                            
                            <?php if($myrows['SUBMITID'] != 2){?>
                           <div class="box-footer" align="right"> 
                            <button type="submit" class="btn btn-primary btn-flat" onclick="return getSubmit3(1);">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary btn-flat" onclick="return getSubmit3(2);">Submit & Close</button>
                <input type="hidden" name="action3" id="action3" value="submit" />
                         </div> 
                    <?php }?>
                  
                  <?php }?>
                  </form>
								
              
				
				
					
				
				
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src='../../js/jquery.autosize.js'></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/datepicker/datepicker.css" rel="stylesheet" type="text/css" />
<script src="../../js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript" src="../../js/jquery.numeric.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#txtDate,#txtDPLayCanFinishes,#txtDateofDelivery").datepicker({
    format: 'dd-mm-yyyy'
});

$(".areasize").autosize({append: "\n"});

});


function getValidate()
{
	if($("#txtTTLShippingCost").val() > 0)
	{
		getFinalCalculation();
		return true;
	}
	else
	{
		jAlert('Not saved empty cost sheet', 'Alert');
		return false;
	}
}

function getBackgroundData()
{
	if($("#selFType").val() == 1)
	{
		location.href = "voyage_estimation.php?mappingid=<?php echo $mappingid;?>&cost_sheet_id=<?php echo $cost_sheet_id;?>";
	}
	if($("#selFType").val() == 2)
	{
		location.href = "voyage_estimation1.php?mappingid=<?php echo $mappingid;?>&cost_sheet_id=<?php echo $cost_sheet_id;?>";
	}
}

function showCapField()
{
	if($("#rdoCap1").is(":checked"))
	{
		$("#txtGCap").attr({'disabled':''});
		$("#txtBCap").attr({'disabled':'disabled'});
		getTotalDWT();
	}
	if($("#rdoCap2").is(":checked"))
	{
		$("#txtGCap").attr({'disabled':'disabled'});
		$("#txtBCap").attr({'disabled':''});
		getTotalDWT1();
	}
}

function getTotalDWT()
{
	if($("#rdoDWT1").is(":checked")){var dwt = $("#txtDWTS").val();}
	if($("#rdoDWT2").is(":checked")){var dwt = $("#txtDWTT").val();}
	
	if($("#rdoCap1").is(":checked"))
	{
		if($("#txtGCap").val() == ""){var gcap = 0;}else{var gcap = $("#txtGCap").val();}
		if($("#txtSF").val() != "" || $("#txtSF").val() != 0)
		{
			var ttl = parseFloat(gcap) / parseFloat($("#txtSF").val());
			if(ttl > parseFloat(dwt))
			{
				//jAlert('Loadable Qty MT > DWT. Please recheck CBM and SF', 'Alert');
				$("#txtLoadable").val(dwt);
			}
			else
			{
				$("#txtLoadable").val(ttl.toFixed(2));
			}
		}
	}
}

function getTotalDWT1()
{
	if($("#rdoDWT1").is(":checked")){var dwt = $("#txtDWTS").val();}
	if($("#rdoDWT2").is(":checked")){var dwt = $("#txtDWTT").val();}
	
	if($("#rdoCap2").is(":checked"))
	{
		if($("#txtBCap").val() == ""){var bcap = 0;}else{var bcap = $("#txtBCap").val();}
		if($("#txtSF").val() != "" || $("#txtSF").val() != 0)
		{
			var ttl = parseFloat(bcap) / parseFloat($("#txtSF").val());
			if(ttl > parseFloat(dwt))
			{
				//jAlert('Loadable Qty MT > DWT. Please recheck CBM and SF', 'Alert');
				$("#txtLoadable").val(dwt);
			}
			else
			{
				$("#txtLoadable").val(ttl.toFixed(2));
			}
		}
	}
}

function showMMarketField()
{
	if($("#rdoMMarket1").is(":checked"))
	{
		$("#txtMTCPDRate").attr({'disabled':''});
		$("#txtMLumpsum").attr({'disabled':'disabled'});
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtTTLShippingCost").val("0.00");
		$("#txtMTCPDRate").focus();
		//getAgreedFreight();
	}
	if($("#rdoMMarket2").is(":checked"))
	{
		$("#txtMTCPDRate").attr({'disabled':'disabled'});
		$("#txtMLumpsum").attr({'disabled':''});
		$("#txtMTCPDRate,#txtMLumpsum").val("");
		$("#txtTTLShippingCost").val("0.00");
		$("#txtMLumpsum").focus();
		//getFreightLumsump();
	}
}

function showDWTField()
{
	if($("#rdoDWT1").is(":checked"))
	{
		$("#txtDWTS").attr({'disabled':''});
		$("#txtDWTT").attr({'disabled':'disabled'});
		getTotalDWT();
		getTotalDWT1();
	}
	if($("#rdoDWT2").is(":checked"))
	{
		$("#txtDWTS").attr({'disabled':'disabled'});
		$("#txtDWTT").attr({'disabled':''});
		getTotalDWT();
		getTotalDWT1();
	}
}

function showQtyField()
{
	if($("#rdoQty1").is(":checked"))
	{
		$("#txtDFQMT").attr({'readonly':''});
		$("#txtAddnlQMT").attr({'readonly':'readonly'});
		$("#txtDFQMT,#txtAddnlQMT").val("0.00");
		$("#txtDFQMT").focus();
	}
	if($("#rdoQty2").is(":checked"))
	{
		$("#txtDFQMT").attr({'readonly':'readonly'});
		$("#txtAddnlQMT").attr({'readonly':''});
		$("#txtDFQMT,#txtAddnlQMT").val("0.00");
		$("#txtAddnlQMT").focus();
	}
}

function getCalculate()
{
	if($("#txtCQMT").val() == ""){var c_qty = 0;}else{var c_qty = $("#txtCQMT").val();}
	if($("#txtAQMT").val() == ""){var a_qty = 0;}else{var a_qty = $("#txtAQMT").val();}
	var calc = parseFloat(c_qty) - parseFloat(a_qty);
	if(calc >= 0){
	$("#txtDFQMT").val(calc.toFixed(2));
	$("#txtAddnlQMT").val(0);
	$("#txtHidAddnlQMT").val(0);
	}
	if(calc < 0){
	$("#txtDFQMT").val(0);
	$("#txtAddnlQMT").val(Math.abs(calc.toFixed(2)));
	$("#txtHidAddnlQMT").val(calc.toFixed(2));
	}
}

function getLOadPortQty()
{
	if($("#selLPCName").val() != "")
	{
		if($("#txtCQMT").val() != "")
		{
			$("#txtLPQMT").val(parseFloat($("#txtCQMT").val()) - parseFloat($("[id^=txtLpQMT_]").sum()));
			getLoadPortCalculation();
		}
		else
		{
			$("#txtLPQMT").val(0);
		}
	}
	else
	{
		$("#txtLPQMT,#txtLPRMTDay,#txtLPWorkDay").val("");
	}
}

function getLoadPortCalculation()
{
	if($("#txtLPQMT").val() != "" && $("#txtLPRMTDay").val() != "") 
	{
		var value = ($("#txtLPQMT").val() / $("#txtLPRMTDay").val());
		$("#txtLPWorkDay").val(value.toFixed(2));
	}
	else
	{
		$("#txtLPWorkDay").val('0.00');
	}
}

function addLoadPortDetails()
{
	if($("#selLoadPort").val() != "" && $("#selLPCName").val() != "" && $("#txtLPQMT").val() != "" && $("#txtLPRMTDay").val() != "" && $("#txtLPWorkDay").val() != "" && $("#txtLPPortCosts").val() != "")
	{
		var id = $("#load_portID").val();
		id = (id - 1) + 2;
		$("#LProw_Empty").remove();
		var load_port = document.getElementById("selLoadPort").selectedIndex;
		var load_port_text = document.getElementById("selLoadPort").options[load_port].text;
		
		var cargo = document.getElementById("selLPCName").selectedIndex;
		var cargo_text = document.getElementById("selLPCName").options[cargo].text;
				
		$('<tr id="lp_Row_'+id+'"><td align="center" class="input-text" ><a href="#lp'+id+'" onclick="removeLoadPort('+id+');" ><img src="../../images/icon_delete.gif" height="14" width="12" /></a></td><td align="left" class="input-text" >'+load_port_text+'<input type="hidden" name="txtLoadPort_'+id+'" id="txtLoadPort_'+id+'" class="input" size="10" value="'+$("#selLoadPort").val()+'"/></td><td align="left" class="input-text" >'+cargo_text+'<input type="hidden" name="txtLPCID_'+id+'" id="txtLPCID_'+id+'" class="input" size="10" value="'+$("#selLPCName").val()+'"/></td><td align="left" class="input-text" >'+$("#txtLPQMT").val()+'<input type="hidden" name="txtLpQMT_'+id+'" id="txtLpQMT_'+id+'" class="input" size="10" value="'+$("#txtLPQMT").val()+'"/></td><td align="left" class="input-text" >'+$("#txtLPRMTDay").val()+'<input type="hidden" name="txtLpRate_'+id+'" id="txtLpRate_'+id+'" class="input" size="10" value="'+$("#txtLPRMTDay").val()+'"/></td><td align="left" class="input-text" >'+$("#txtLPWorkDay").val()+'<input type="hidden" name="txtLpBLWorkDays_'+id+'" id="txtLpBLWorkDays_'+id+'" class="input" size="10" value="'+$("#txtLPWorkDay").val()+'"/></td><td align="left" class="input-text" >'+$("#txtLPPortCosts").val()+'<input type="hidden" name="txtLPPCosts_'+id+'" id="txtLPPCosts_'+id+'" class="input" size="10" value="'+$("#txtLPPortCosts").val()+'"/></td></tr>').appendTo("#tblLoadPort");
		
		if($("#txtLPPortCosts").val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtLPPortCosts").val();}
		/*var loadid = 0;
		$('[id^=txtLpBLNet_]').each(function(index) {
					var loadid = this.value;
		});
		if(loadid == 0)
		{
			var per_mt = $("#txtCQMT").val();
		}
		else
		{
			var per_mt = loadid;
		}*/
		
		//autocomplete="off" onkeyup="getPortCostSum('+id+','+lp+');"
		
		var per_mt = $("#txtCQMT").val();
		var lpcostMT = parseFloat(lp_cost) / parseFloat(per_mt);
		var lp = "'LP'";			
		$('<tr id="oscLProw_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >Load Port   '+load_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtLPOSCCost_'+id+'" id="txtLPOSCCost_'+id+'" class="input-text" size="10" readonly="true" value="'+$("#txtLPPortCosts").val()+'" /></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtLPOSCCostMT_'+id+'" id="txtLPOSCCostMT_'+id+'" class="input-text" size="10" readonly="true" value="'+lpcostMT.toFixed(2)+'" /></td></tr><tr id="oscLProw1_'+id+'" height="5"><td colspan="10" align="left" class="text" valign="top" ></td></tr>').appendTo("#tbPortCosts");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum())/ parseFloat(per_mt);
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
		$('<tr id="ddswLProw_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >Load Port   '+load_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtddswLPCost_'+id+'" id="txtddswLPCost_'+id+'" class="input-text" size="10" readonly="true" value="0.00"/></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtddswLPCostMT_'+id+'" id="txtddswLPCostMT_'+id+'" class="input-text" size="10" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDSW");
		
		$('<tr id="ddshipLProw_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >Load Port   '+load_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtddshipLPCost_'+id+'" id="txtddshipLPCost_'+id+'" class="input-text" size="10" readonly="true" value="0.00"/></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtddshipLPCostMT_'+id+'" id="txtddshipLPCostMT_'+id+'" class="input-text" size="10" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDShipper");
		
		$("#load_portID").val(id);
		$("#selLoadPort,#selLPCName,#txtLPQMT,#txtLPRMTDay,#txtLPBLQGross,#txtLPBLQNet,#txtLPBLDate,#txtLPWorkDay,#txtLPPortCosts").val("");
		getFinalCalculation();

	}
	else
	{
		jAlert('Please fill all the records for Load Port', 'Alert');
	}
}

function removeLoadPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#lp_Row_"+var1).remove();
			$("#ddswLProw_"+var1).remove();		
			$("#ddshipLProw_"+var1).remove();
			$("#oscLProw_"+var1).remove();
			$("#oscLProw1_"+var1).remove();
			/*var loadid = 0;
			$('[id^=txtLpBLNet_]').each(function(index) {
						var loadid = this.value;
			});
			if(loadid == 0)
			{
				var per_mt = $("#txtCQMT").val();
			}
			else
			{
				var per_mt = loadid;
			}*/
			var per_mt = $("#txtCQMT").val();
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum())/ parseFloat(per_mt);
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			getFinalCalculation();
		}
		else{return false;}
		});	
}

function addDisPortDetails()
{
	if($("#selDisPort").val() != "" && $("#selDPCName").val() != "" && $("#txtDPQMT").val() != "" && $("#txtDPRMTDay").val() != "" && $("#txtDPWorkDay").val() != "" && $("#txtDPPortCosts").val() != "")
	{
		var id = $("#dis_portID").val();
		id = (id - 1) + 2;
		$("#DProw_Empty").remove();
		var dis_port = document.getElementById("selDisPort").selectedIndex;
		var dis_port_text = document.getElementById("selDisPort").options[dis_port].text;
		
		var cargo = document.getElementById("selDPCName").selectedIndex;
		var cargo_text = document.getElementById("selDPCName").options[cargo].text;
		
		$('<tr id="dp_Row_'+id+'"><td align="center" class="input-text" ><a href="#dp'+id+'" onclick="removeDisPort('+id+');" ><img src="../../images/icon_delete.gif" height="14" width="12" /></a></td><td align="left" class="input-text" >'+dis_port_text+'<input type="hidden" name="txtDisPort_'+id+'" id="txtDisPort_'+id+'" class="input" size="10" value="'+$("#selDisPort").val()+'"/></td><td align="left" class="input-text" >'+cargo_text+'<input type="hidden" name="txtDPCID_'+id+'" id="txtDPCID_'+id+'" class="input" size="10" value="'+$("#selDPCName").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDPQMT").val()+'<input type="hidden" name="txtDpQMT_'+id+'" id="txtDpQMT_'+id+'" class="input" size="10" value="'+$("#txtDPQMT").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDPRMTDay").val()+'<input type="hidden" name="txtDpRate_'+id+'" id="txtDpRate_'+id+'" class="input" size="10" value="'+$("#txtDPRMTDay").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDPWorkDay").val()+'<input type="hidden" name="txtDpBLWorkDays_'+id+'" id="txtDpBLWorkDays_'+id+'" class="input" size="10" value="'+$("#txtDPWorkDay").val()+'"/></td><td align="left" class="input-text" >'+$("#txtDPPortCosts").val()+'<input type="hidden" name="txtDPPCosts_'+id+'" id="txtDPPCosts_'+id+'" class="input" size="10" value="'+$("#txtDPPortCosts").val()+'"/></td></tr>').appendTo("#tblDisPort");
		
		if($("#txtDPPortCosts").val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPPortCosts").val();}
		/*var loadid = 0;
		$('[id^=txtLpBLNet_]').each(function(index) {
					var loadid = this.value;
		});
		if(loadid == 0)
		{
			var per_mt = $("#txtCQMT").val();
		}
		else
		{
			var per_mt = loadid;
		}*/
		//autocomplete="off" onkeyup="getPortCostSum('+id+','+dp+');"
		
		var per_mt = $("#txtCQMT").val();
		var dpcostMT = parseFloat(dp_cost) / parseFloat(per_mt);
		var dp = "'DP'";
		$('<tr id="oscDProw_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >Discharge Port   '+dis_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtDPOSCCost_'+id+'" id="txtDPOSCCost_'+id+'" class="input-text" size="10" readonly="true"  value="'+$("#txtDPPortCosts").val()+'"  /></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtDPOSCCostMT_'+id+'" id="txtDPOSCCostMT_'+id+'" class="input-text" size="10" readonly="true" value="'+dpcostMT.toFixed(2)+'"  /></td></tr><tr id="oscDProw1_'+id+'" height="5"><td colspan="10" align="left" class="text" valign="top" ></td></tr>').appendTo("#tbPortCosts");
		
		var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum())/ parseFloat(per_mt);
		$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum().toFixed(2));
		$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
		
		$('<tr id="ddswDProw_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >Discharge Port   '+dis_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtddswDPCost_'+id+'" id="txtddswDPCost_'+id+'" class="input-text" size="10" readonly="true" value="0.00" /></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtddswDPCostMT_'+id+'" id="txtddswDPCostMT_'+id+'" class="input-text" size="10" readonly="true" value="0.00" /></td></tr>').appendTo("#tbDDSW");
		
		$('<tr id="DDReceiDProw_'+id+'"><td width="0%" align="left" class="input-text" valign="top" ></td><td width="35%" align="left" class="input-text"  valign="top" >Discharge Port   '+dis_port_text+'</td><td width="13%" align="left" class="input-text"  valign="top" ></td><td width="12%" align="left" class="input-text" valign="top"></td><td width="14%" align="left" class="input-text" valign="top"><input type="text"  name="txtDDReceiDPCost_'+id+'" id="txtDDReceiDPCost_'+id+'" class="input-text" size="10" readonly="true" value="0.00" /></td><td width="13%" align="left" class="input-text" valign="top"><input type="text" name="txtDDReceiDPCostMT_'+id+'" id="txtDDReceiDPCostMT_'+id+'" class="input-text" size="10" readonly="true" value="0.00"  /></td></tr>').appendTo("#tbDDReceiver");
				
		$("#dis_portID").val(id);
		$("#selDisPort,#selDPCName,#txtDPQMT,#txtDPRMTDay,#txtDPBLQGross,#txtDPBLQNet,#txtDPBLDate,#txtDPWorkDay,#txtDPPortCosts").val("");
		getFinalCalculation();
	}
	else
	{
		jAlert('Please fill all the records for Discharge Port', 'Alert');
	}
}

function getPortCostSum(var1,port)
{
	/*var loadid = 0;
	$('[id^=txtLpBLNet_]').each(function(index) {
				var loadid = this.value;
	});
	if(loadid == 0)
	{
		var per_mt = $("#txtCQMT").val();
	}
	else
	{
		var per_mt = loadid;
	}*/
	var per_mt = $("#txtCQMT").val();
	if(port == 'LP')
	{
		if($("#txtLPOSCCost_"+var1).val() == ""){var lp_cost = 0;}else{var lp_cost = $("#txtLPOSCCost_"+var1).val();}
		var lpcostMT = parseFloat(lp_cost) / parseFloat(per_mt);
		$("#txtLPOSCCostMT_"+var1).val(lpcostMT.toFixed(2));
	}
	else if(port == 'DP')
	{
		if($("#txtDPOSCCost_"+var1).val() == ""){var dp_cost = 0;}else{var dp_cost = $("#txtDPOSCCost_"+var1).val();}
		var dpcostMT = parseFloat(dp_cost) / parseFloat(per_mt);
		$("#txtDPOSCCostMT_"+var1).val(dpcostMT.toFixed(2));
	}		
	var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum())/ parseFloat(per_mt);
	$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum().toFixed(2));
	$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
	getFinalCalculation();
}

function removeDisPort(var1)
{
	jConfirm('Are you sure you want to delete this entry permanently ?', 'Confirmation', function(r) {
		if(r){ 
			$("#dp_Row_"+var1).remove();
			$("#ddswDProw_"+var1).remove();
			$("#DDReceiDProw_"+var1).remove();
			$("#oscDProw_"+var1).remove();
			$("#oscDProw1_"+var1).remove();
			/*var loadid = 0;
			$('[id^=txtLpBLNet_]').each(function(index) {
						var loadid = this.value;
			});
			if(loadid == 0)
			{
				var per_mt = $("#txtCQMT").val();
			}
			else
			{
				var per_mt = loadid;
			}*/
			var per_mt = $("#txtCQMT").val();
			var ttl_portcostsMT = parseFloat($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum())/ parseFloat(per_mt);
			$("#txtTTLPortCosts").val($("[id^=txtLPOSCCost_],[id^=txtDPOSCCost_]").sum().toFixed(2));
			$("#txtTTLPortCostsMT").val(ttl_portcostsMT.toFixed(2));
			getFinalCalculation();
		}
		else{return false;}
		});	
}

function getDisPortQty()
{
	if($("#selDPCName").val() != "")
	{
		if($("#txtCQMT").val() != "")
		{
			$("#txtDPQMT").val(parseFloat($("#txtCQMT").val()) - parseFloat($("[id^=txtDpQMT_]").sum()));
			getDisPortCalculation();
		}
		else
		{
			$("#txtDPQMT").val(0);
		}
	}
	else
	{
		$("#txtDPQMT,#txtDPRMTDay,#txtDPWorkDay").val("");
	}
}

function getDisPortCalculation()
{
	if($("#txtDPQMT").val() != "" && $("#txtDPRMTDay").val() != "") 
	{
		var value = ($("#txtDPQMT").val() / $("#txtDPRMTDay").val());
		$("#txtDPWorkDay").val(value.toFixed(2));
	}
	else
	{
		$("#txtDPWorkDay").val('0.00');
	}
}

function getFinalCalculation()
{
	/*var loadid = 0;
	$('[id^=txtLpBLNet_]').each(function(index) {
				var loadid = this.value;
	});
	if(loadid == 0)
	{
		var per_mt = $("#txtCQMT").val();
	}
	else
	{
		var per_mt = loadid;
	}*/
	var per_mt = $("#txtCQMT").val();
	if($("#rdoMMarket1").is(":checked"))
	{
		if($("#txtMTCPDRate").val() == ""){var agr_gross_fr = 0;}else{var agr_gross_fr = $("#txtMTCPDRate").val();}
		//if($("#txtAQMT").val() == ""){var actual_qty = 0;}else{var actual_qty = $("#txtAQMT").val();}
		
		var gross_fr = parseFloat(agr_gross_fr) * parseFloat(per_mt);
		$("#txtFrAdjUsdGF").val(gross_fr.toFixed(2));
		
		var gross_fr_mt = parseFloat(gross_fr) / parseFloat(per_mt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		//.......................................
		if($("#txtDFQMT").val() == "" || $("#txtDFQMT").val() == 0)
		{
			var df_qty = 0;
			$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
			
		}
		else
		{
			var df_qty = $("#txtDFQMT").val();
			var dead_fr = parseFloat(agr_gross_fr) * parseFloat(df_qty);
			$("#txtFrAdjUsdDF").val(dead_fr.toFixed(2));
			
			var dead_fr_mt = parseFloat(dead_fr) / parseFloat(df_qty);
			$("#txtFrAdjUsdDFMT").val(dead_fr_mt.toFixed(2));
		}
		//.......................................
		if($("#txtAddnlCRate").val() == ""){var addnl_rate = 0;}else{var addnl_rate = $("#txtAddnlCRate").val();}
		//if($("#txtAddnlQMT").val() == ""){var addnl_qty = 0;}else{var addnl_qty = $("#txtAddnlQMT").val();}
		if($("#txtAddnlQMT").val() == "" || $("#txtAddnlQMT").val() == 0 )
		{
			var addnl_qty = 0;
			$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		}
		else
		{
			var addnl_qty = $("#txtAddnlQMT").val();
			var addnl_fr = parseFloat(addnl_rate) * parseFloat(addnl_qty);
			$("#txtFrAdjUsdAF").val(addnl_fr.toFixed(2));
			
			var addnl_fr_mt = parseFloat(addnl_fr) / parseFloat(addnl_qty);
			$("#txtFrAdjUsdAFMT").val(addnl_fr_mt.toFixed(2));
		}
		//.............................................
		
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum().toFixed(2));
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / (parseFloat(per_mt) + parseFloat(df_qty) + parseFloat(addnl_qty));
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));
	}
	
	if($("#rdoMMarket2").is(":checked"))
	{
		$("#txtFrAdjUsdGF").val($("#txtMLumpsum").val());
		var gross_fr_mt = parseFloat($("#txtFrAdjUsdGF").val()) / parseFloat(per_mt);
		$("#txtFrAdjUsdGFMT").val(gross_fr_mt.toFixed(2));
		
		$("#txtFrAdjUsdDF,#txtFrAdjUsdDFMT").val("0.00");
		$("#txtFrAdjUsdAF,#txtFrAdjUsdAFMT").val("0.00");
		
		$("#txtFrAdjUsdTF").val($("#txtFrAdjUsdGF,#txtFrAdjUsdDF,#txtFrAdjUsdAF").sum());
		var ttl_fr_mt = parseFloat($("#txtFrAdjUsdTF").val()) / parseFloat(per_mt);
		$("#txtFrAdjUsdTFMT").val(ttl_fr_mt.toFixed(2));

	}
	
	if($("#txtFrAdjUsdTF").val() == ""){var ttl_fr = 0;}else{var ttl_fr = $("#txtFrAdjUsdTF").val();}
	if($("#txtFrAdjPerAC").val() == ""){var ac_per = 0;}else{var ac_per = $("#txtFrAdjPerAC").val();}
	
	var address_commission = (parseFloat(ttl_fr) * parseFloat(ac_per)) / 100;
	$("#txtFrAdjUsdAC").val(address_commission.toFixed(2));
	var address_commission_mt = parseFloat($("#txtFrAdjUsdAC").val()) / parseFloat(per_mt);
	$("#txtFrAdjUsdACMT").val(address_commission_mt.toFixed(2));
	
	if($("#txtFrAdjPerAgC").val() == ""){var ag_per = 0;}else{var ag_per = $("#txtFrAdjPerAgC").val();}
	var agent_commission = (parseFloat(ttl_fr) * parseFloat(ag_per)) / 100;
	$("#txtFrAdjUsdAgC").val(agent_commission.toFixed(2));
	
	var net_fr = parseFloat(ttl_fr) - parseFloat($("#txtFrAdjUsdAC,#txtFrAdjUsdAgC").sum());
	$("#txtFrAdjUsdFP,#txtTTLORCFP").val(net_fr.toFixed(2));
	
	var net_fr_mt = parseFloat(net_fr) / parseFloat(per_mt);
	$("#txtTTLORCFPCostMT").val(net_fr_mt.toFixed(2));
	
	$("#txtTTLShippingCost").val($("#txtTTLORCFP,[id^=txtOSCAbs_],[id^=txtOMCAbs_],#txtTTLPortCosts").sum().toFixed(2));
	var ttl_shipping_mt = parseFloat($("#txtTTLShippingCost").val()) / parseFloat(per_mt);
	$("#txtTTLShippingCostMT").val(ttl_shipping_mt.toFixed(2));
}

function getOSCostCalculate1(var1)
{
	/*var loadid = 0;
	$('[id^=txtLpBLNet_]').each(function(index) {
				var loadid = this.value;
	});
	if(loadid == 0)
	{
		var per_mt = $("#txtCQMT").val();
	}
	else
	{
		var per_mt = loadid;
	}*/
	var per_mt = $("#txtCQMT").val();
	if($("#txtOSCAbs_"+var1).val() == ""){var osc_abs = 0;}else{var osc_abs = $("#txtOSCAbs_"+var1).val();}
	var calc = parseFloat(osc_abs) / parseFloat(per_mt);
	$("#txtOSCCostMT_"+var1).val(calc.toFixed(2));
}

function getOMCCalculate(var1)
{
	/*var loadid = 0;
	$('[id^=txtLpBLNet_]').each(function(index) {
				var loadid = this.value;
	});
	if(loadid == 0)
	{
		var per_mt = $("#txtCQMT").val();
	}
	else
	{
		var per_mt = loadid;
	}*/
	var per_mt = $("#txtCQMT").val();
	if($("#txtOMCAbs_"+var1).val() == ""){var omc_abs = 0;}else{var omc_abs = $("#txtOMCAbs_"+var1).val();}
	var calc = parseFloat(omc_abs) / parseFloat(per_mt);
	$("#txtOMCCostMT_"+var1).val(calc.toFixed(2));		
}

function getValue()
{
	$("#txtOSCAbs_1").val($("#txtFrAdjUsdAgC").val());
	/*var loadid = 0;
	$('[id^=txtLpBLNet_]').each(function(index) {
				var loadid = this.value;
	});
	if(loadid == 0)
	{
		var per_mt = $("#txtCQMT").val();
	}
	else
	{
		var per_mt = loadid;
	}*/
	var per_mt = $("#txtCQMT").val();
	if($("#txtOSCAbs_1").val() == ""){var osc_abs = 0;}else{var osc_abs = $("#txtOSCAbs_1").val();}
	var calc = parseFloat(osc_abs) / parseFloat(per_mt);
	$("#txtOSCCostMT_1").val(calc.toFixed(2));
	getFinalCalculation();
}
</script>
    </body>
</html>
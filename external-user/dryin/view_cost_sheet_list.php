<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();
$mappingid  = $_REQUEST['mappingid'];
$page  = $_REQUEST['page'];
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->confirmFinalCostSheet();
	header('Location : ./view_cost_sheet_list.php?msg='.$msg."&mappingid=".$mappingid."&page=".$page);
 }
$pagename = basename($_SERVER['PHP_SELF'])."?mappingid=".$mappingid."&page=".$page;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> In Ops at a glance added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating In Ops at a glance.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
				<?php if($page == 1){$page_link = "in_ops_at_glance.php";}else if($page == 2){$page_link = "vessel_in_post_ops.php";}else {$page_link = "vessel_in_history.php";}?>
				<div align="right"><a href="<?php echo $page_link;?>"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				<h3 style=" text-align:center;">ESTIMATED COST SHEETS</h3>
				<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                 
				<div style="height:10px;">
				<input type="hidden" name="action" value="submit" /><input type="hidden" name="txtMappingid2" id="txtMappingid2" value="" />
				</div>				
                
                
				
                
                  <?php $obj->displayCostSheetList($mappingid,$page);?>
               
				</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
 <script type="text/javascript">
 $(document).ready(function(){

});
$(function() {
	$("#cost_sheet_list").dataTable();

});

function getValidate()
{
	var arr = Array();var i=0;
	$('[id^=rdoFCA_]').each(function(index) {
				var rowid = this.id;//alert(rowid);
				var lasrvar1 = rowid.split('_')[1];
				if($("#rdoFCA_"+lasrvar1).is(":checked"))
					{
						arr[i] = 1;
					}
					i++;
			});//alert(arr.length);
			if(arr.length > 0)
				{
					return true;
				}
				else
				{
					jAlert('Please select atleast one cost sheet.', 'Alert');
					return false;
				}	
}

</script>
		
</body>
</html>
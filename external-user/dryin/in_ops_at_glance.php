<?php 
session_start();
require_once("../../includes/display_internal_user_dryin.inc.php");
require_once("../../includes/functions_internal_user_dryin.inc.php");
$obj = new data();
$connect = $obj->funConnect();
$display = new display();
$display->logout_iu();

if (@$_REQUEST['action'] == 'submit')
 {
 	if($_REQUEST['txtStatus'] == "")
	{
		if($_REQUEST['txtMappingid2'] == "")
		{
			$msg = $obj->insertActualCostSheetName();
		}
		else
		{
			$msg = $obj->deleteInOpsEntry($_REQUEST['txtMappingid2']);
		}
	}
	else if($_REQUEST['txtStatus'] == "1")
	{
		$msg = $obj->convertToPostOps($_REQUEST['txtMappingid2']);
	}
	else if($_REQUEST['txtStatus'] == "2")
	{
		//$msg = $obj->deletePdfDetails($_REQUEST['txtMappingid2'],$_REQUEST['txtFileName']);
	}
	
	header('Location : ./in_ops_at_glance.php?msg=1');
 }
 
if (@$_REQUEST['action1'] == 'submit1')
{
	if (@$_REQUEST['txtuid'] == 1)
	{
		$msg = $obj->insertPerfAtSeaUploadDetails();
		header('Location : ./in_ops_at_glance.php?msg='.$msg);
	}
	else if (@$_REQUEST['txtuid'] == 2)
	{
		$msg = $obj->insertPerfAtPortUploadDetails();
		header('Location : ./in_ops_at_glance.php?msg='.$msg);
	}
}
if(isset($_REQUEST['vesselid'])){
$vesselid = $_REQUEST['vesselid'];
}
else
{
$vesselid = "";
}
$pagename = basename($_SERVER['PHP_SELF']);
$msg = NULL;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<link href="../../css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(5); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-book"></i>&nbsp;Daily Tasks&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="../"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Daily Tasks&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;In Ops at a glance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<?php if(isset($_REQUEST['msg'])){
						$msg = $_REQUEST['msg'];
						if($msg == 0){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> In Ops at a glance added/updated successfully.
				</div>
				<?php }?>
				<?php if($msg == 1){?>
				<div class="alert alert-danger alert-dismissable">
					<i class="fa fa-ban"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sorry!</b> there was an error while adding/updating In Ops at a glance.
				</div>
				<?php }?>
				<?php if($msg == 2){?>
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Congratulations!</b> Status changed successfully.
				</div>
				<?php }}?>
				<div class="box box-primary">
				<h3 style=" text-align:center;">In Ops at a glance List</h3>
				
				<form name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>"/>
                 <div class="row invoice-info" style="margin-left:10px;margin-right:10px;">
                        <div class="col-sm-3 invoice-col">
                        Vessels in Nomination
                            <address>
                               <input type="checkbox" name="checkVesselbox1" id="checkVesselbox1"  checked disabled="disabled" value="1" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                        Vessels in Operations
                            <address>
                                <input type="checkbox" name="checkVesselbox2" id="checkVesselbox2" checked disabled="disabled" value="1" />
                             </address>
                        </div><!-- /.col -->
                        
						<div class="col-sm-3 invoice-col">
                        Vessels in Post Operations
                            <address>
                               <input type="checkbox" name="checkVesselbox3" id="checkVesselbox3" disabled="disabled" value="1" />
                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                        Vessels in History
                            <address>
                              <input type="checkbox" name="checkVesselbox4" id="checkVesselbox4" disabled="disabled" value="1" />
                            </address>
                        </div><!-- /.col -->
					</div>
				<div style="height:10px;">
				<input type="hidden" name="action" value="submit" /><input type="hidden" name="txtMappingid2" id="txtMappingid2" value="" />
				</div>				
                
                
				<div class="box-body table-responsive" style="overflow:auto;">
                <table id="apen_cargo_position_list" class="table table-bordered table-striped">
                <thead>
                <tr valign="top">
                <th align="left">CS View</th>
                <th align="left">Business Type / Nom ID</th>
                <th align="left">Material Name</th>
                <th align="left">Vessel</th>
                <th align="left">Fixture Type</th>
                <th align="left">BAF Calculator</th>
                <th align="left">Cost Sheet</th>
                <th align="left">PDA Request</th>
                <th align="left">Appoint Agent</th>	
                <th align="left">SOF</th>	
                <th align="left">Laytime</th>			
                <th align="left">Perf at Sea</th>
                <th align="left">Perf in Port</th>
                <th align="center">Payment Grid</th>
                <th align="center">Deactivate</th>
                <th align="center">Re&nbsp;-&nbsp;Del</th>
                <th align="center">Complete</th>
                </tr>
                </thead>
                <tbody>
		<?php $sql = "select * from mapping_master where MODULEID='".$_SESSION['moduleid']."'  AND MCOMPANYID='".$_SESSION['company']."' and STATUS=2 order by MAPPINGID"; 

			  $res = mysql_query($sql);
			  $rec = mysql_num_rows($res);
			  $i=1;
			  $submit = 0;
			  if($rec == 0)
			  {
			 	 
			  }
			  else{
			  while($rows = mysql_fetch_assoc($res))
			  {
			  ?>
			<tr id="in_row_<?php echo $i;?>">
			<td align="left" valign="middle"  style="font-size:10px;">
			<a href="view_cost_sheet_list.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1">View</a>
			<br/><br/>
			<?php if($obj->getEditNominationValues($rows['MAPPINGID']) == 0){?>
			<a href="edit_nominations1.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:red;" title="Click me" >Edit Nom</a>
			<?php }else{?>
			<a href="edit_nominations1.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:blue;" title="Click me" >Edit Nom</a>
			<?php }?>
			<br/><br/>
			<?php if($obj->getAllStandardsValues($rows['MAPPINGID']) == 0){ $sstatus = 1;?>
			<a href="check_standards1.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:blue;" title="Click me" >Check</a>
			<?php }else{ $sstatus = 0;?>
			<a href="check_standards1.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:red;" title="Click me" >Check</a>
			<?php }?>
			<br/><br/>
			<a href="documents.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:green;" title="Click me" >Docs</a>
			</td>
			
			<td align="left" valign="middle"  style="font-size:10px;"><?php echo $obj->getBusinessTypeBasedOnID($obj->getVesselIMOData($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'VESSEL_IMO_ID'),"BUSINESSTYPEID"))."<br><br>".$obj->getMappingData($rows['MAPPINGID'],"NOM_NAME").'<br><br><a href="tce_attachments.php?mappingid='.$rows['MAPPINGID'].'&page=1" style="color:green;" title="Click me" >TCE Attachments</a><br><br><a href="mtm.php?mappingid='.$rows['MAPPINGID'].'&cost_sheet_id='.$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO").'&page=1" style="color:green;" title="Click me" >MTM</a><br><br>';?></td>
			
			<td align="left" valign="middle"  style="font-size:9px;"><?php echo $obj->getCargoContarctForMapping($obj->getMappingData($rows['MAPPINGID'],"CARGO_IDS"),$obj->getMappingData($rows['MAPPINGID'],"CARGO_POSITION"));?></td>
			<td align="left" valign="middle"  style="font-size:9px;"><?php echo $obj->getVesselIMOData($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'VESSEL_IMO_ID'),"VESSEL_NAME");?></td>
			<?php if($obj->getFinalCostSheetDataRec($rows['MAPPINGID']) == 0){?>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<td align="left" valign="middle"  style="font-size:9px;"></td>
			<?php }else{?>
			<td align="left" valign="middle"  style="font-size:9px;"><?php echo $obj->getFreightFixtureBasedOnID($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID"));?>
			<br/><br/>
	<?php if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 1){?>
			<a href="check_list_tci.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:red;" title="Click me" >Check List</a>	
			<?php }else{?>
				<a href="check_list.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:red;" title="Click me" >Check List</a>	
			<?php }?>	
			<?php if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 2){?>
			<br/><br/>
			<a href="change_vessel.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:red;" title="Click me" >Change Vessel</a>
			<?php }?>
			</td>
			<td align="left" valign="middle"  style="font-size:9px;">
			<?php if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 2){
			if($obj->getVesselOpenEntryDeta($rows['OPEN_VESSEL_ID'],'COA_SPOT') == 2){?>
			<a href="baf_calculation.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:green;" title="Click me" >BAF Calculator</a>
			<?php }}?>
			</td>
			<td align="center" valign="middle"  style="font-size:9px;">
			<a href='view_cost_sheet1.php?mappingid=<?php echo $rows['MAPPINGID'];?>&cost_sheet_id=<?php echo $obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO");?>&page=1' >FCS</a><br/>
			<?php 
			$sql1 = "select * from cost_sheet_name_master where MAPPINGID='".$rows['MAPPINGID']."' and MODULEID='".$_SESSION['moduleid']."' and MCOMPANYID='".$_SESSION['company']."'";

			$res1 = mysql_query($sql1);
			$rec1 = mysql_num_rows($res1);
			if($rec1 > 0)
			{
				while($rows1 = mysql_fetch_assoc($res1))
				{
					if($rows1['PROCESS'] != "EST")
					{
						if($obj->getFreightEstimationStatus($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO")) == 1 )
						{
							$href = "./cost_sheet_tci.php?mappingid=".$rows['MAPPINGID']."&cost_sheet_id=".$rows1['COST_SHEETID']."&page=1";
						}
						else
						{
							$href = "./cost_sheet_vci.php?mappingid=".$rows['MAPPINGID']."&cost_sheet_id=".$rows1['COST_SHEETID']."&page=1";
						}
						echo "<a href='".$href."'>".str_replace(' ','&nbsp;',$rows1['SHEET_NAME'])."</a><br/>";
						$curr_cst = $rows1['COST_SHEETID'];
					}
					else
					{
						$curr_cst = $obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO");
					}
				}
			}
			
			if($obj->getCurrentCostSheetStatus($rows['MAPPINGID'],$curr_cst) == 2)
			{
			?>
			<button class="ox-button1" id="inner-login-button"  type="button" title="Add New CS" onClick="openWin1(<?php echo $rows['MAPPINGID'];?>);"><b><span id="d27e53" >A</span></b></button>
			<?php }else{?>
			<button class="ox-button1" id="inner-login-button"  type="button" title="Add New CS" onClick="msg();"><b><span id="d27e53" >A</span></b></button>
			<?php }?>
			</td>
			<td align="center" valign="middle"  style="font-size:9px;">
			<a href="agency_letter_generation.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1">Generate Agency Letter</a>
			</td>
			<td align="center" valign="middle"  style="font-size:9px;">
			<a href="nominate_agent.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1">Nominate Agent</a>
			</td>
			
			<td align="center" valign="middle"  style="font-size:9px;">
			<a href="sof.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1">SOF</a>		
			</td>
			
			<td align="center" valign="middle"  style="font-size:9px;">
			<?php 
			if($obj->getFreightEstimationStatus($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO")) == 2 )
			{?>
			<a href="laytime_calculation.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" style="color:blue;" title="Click me" >Calculations</a>
			<?php }?>
			</td>
			
			<?php if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 1){?>
			<td align="center" valign="middle"  style="font-size:9px;">
			<a href="#" title="Upload Text File" onClick="openWin(<?php echo $rows['MAPPINGID'];?>,<?php echo $rows['OPEN_VESSEL_ID']?>,1);">Upload</a><br /><br /><br />
			<a href="#" title="Bunker Voyage Report" onClick="getPdfForSea(<?php echo $rows['MAPPINGID'];?>,<?php echo $rows['OPEN_VESSEL_ID'];?>)">Report</a><br />
			</td>
			
			<td width="6%" align="center" valign="middle"  style="font-size:9px;">
			<a href="#" title="Upload Text File" onClick="openWin(<?php echo $rows['MAPPINGID'];?>,<?php echo $rows['OPEN_VESSEL_ID'];?>,2);">Upload</a><br /><br /><br />
			<a href="#" onClick="getPdfForPort(<?php echo $rows['MAPPINGID'];?>,<?php echo $rows['OPEN_VESSEL_ID'];?>)">Report</a><br />
			</td>
			<?php }else{?>
				<td align="center" valign="middle"  style="font-size:9px;"></td>
				<td width="6%" align="center" valign="middle"  style="font-size:9px;"></td>
			<?php }?>
			
			
			
			<td align="center" valign="middle"  style="font-size:9px;">
			<a href="payment_grid.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" >Payment</a><br /><br /><br />
			<?php if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 2){?>
			<a href="invoice.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" >Invoice</a>
			<?php }else{?>
			<a href="invoice_tc.php?mappingid=<?php echo $rows['MAPPINGID'];?>&page=1" >Invoice</a>
			<?php }?>
			</td>
			<td align="center" valign="middle"  style="font-size:10px;">
			<?php if(in_array("1",$obj->getInternalUserRights(1))){?>
			<a href="#A" title="Deactivate entry" onClick="getValidate2(<?php echo $rows['MAPPINGID'];?>,<?php echo $i;?>);"><i class="fa fa-times " style="color:red;"></i></a>
			<?php }?>
			</td>
			<td align="center" valign="middle"  style="font-size:10px;">
			<?php 
			if($obj->getFreightCostData($rows['MAPPINGID'],$obj->getCostSheetFixtureID($rows['MAPPINGID'],"SHEET_NO"),"FIXTURETYPEID") == 1)
			{
				$re_del_date = $obj->getReDelDate($rows['MAPPINGID']);
				$alarm = (int)$obj->getNomDetailsData($rows['MAPPINGID'],"ALARM");
				$cost_sheet_id = $obj->getLatestCostSheetID($rows['MAPPINGID']);
				$fcaid = $obj->getCurrentCostSheetFCAID($rows['MAPPINGID'],$cost_sheet_id);
				$ttl_days = (int)$obj->getFreightEstimationTotalRecords($fcaid,"TTL_DAYS");
	
				$redate = strtotime($re_del_date);
				$days = $ttl_days - $alarm;
				$date = strtotime("+".$days." day", $redate);
				$date1 = strtotime("+".$ttl_days." day", $redate);
				//echo date('Y-m-d', $date);
				if($re_del_date != "" && date('Y-m-d', $date) <= date("Y-m-d",time()))
				{
				$title = "  Vessel Name&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;".$obj->getVesselIMOData($obj->getVesselOpenEntryDeta($obj->getMappingData($rows['MAPPINGID'],"OPEN_VESSEL_ID"),"VESSEL_IMO_ID"),"VESSEL_NAME")."<br>  Re - Del&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;".date("d M Y",$date1);
			?>
			<a href="#$i" title="<?php echo $title;?>" id="alarm_<?php echo $i;?>"><button class="ox-button2" id="inner-login-button"  type="button"><b><span id="d27e53" style="background-color:red; color:#fff;" >&nbsp;Alert&nbsp;</span></b></button></a>
			<?php }}?>
			<?php }?>
			</td>
			<td align="center" valign="middle"  style="font-size:10px;">
			<?php if(in_array("1",$obj->getInternalUserRights(2))){?>
			<button class="ox-button" id="inner-login-button"  type="button" title="Push to Post Ops" onClick="getValidate3(<?php echo $rows['MAPPINGID'];?>);"><b><span id="d27e53">Post Ops</span></b></button>
			<?php }?>
			</td>
			</tr>
		<?php $i++;}}?>
            </tbody>
        </table>
                
				</form>
				</div>
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<script src="../../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="../../js/timer.js" type="text/javascript"></script>
<link href="../../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../../js/jquery.alerts.js"></script>
 <script type="text/javascript">
 $(document).ready(function(){

});
$(function() {
	$("#apen_cargo_position_list").dataTable();

});

function openWin2(mappinid)
{
	$('#basic-modal-content1').modal();
	$("#txtMappingid1").val(mappinid);
	$("#simplemodal-container").css({"height":"150px"});
	
}

function getValidate1()
{
	if($("#txtFile").val() != "")
	{
		return true;
	}
	else
	{
		jAlert('Please fill the file name', 'Alert');
		return false;
	}
}

function getValidate2(var1)
{
	jConfirm('Are you sure you want to cancel this entry ?', 'Confirmation', function(r) {
		if(r){ 
			$("#txtMappingid2").val(var1);
			document.frm1.submit();
		}
		else{return false;}
		});	
}

</script>
		
</body>
</html>
<?php
session_start();
require_once("../../includes/functions_internal_user_dryout.inc.php");
include_once("../../includes/fpdf.php");

@$i = $_REQUEST['id'];
$ret = "";
switch ($i)
{
 
  case 1:
        $ret = create_generate_agency_letter_pdf();
				break;
  case 2:
        $ret = getBunkeringAgentPDF();
				break;

	default:
		    //$ret = funError();
				//echo $ret;
				break; 				
}



function getBunkeringAgentPDF()
{
	$obj = new data();
	$obj->funConnect();
	if(isset($_REQUEST['gen_agency_id'])){$g_agency_id = $_REQUEST['gen_agency_id']; }
	$port_type 						= $_REQUEST['port_type'];
	$randomno                       = $_REQUEST['randomno'];
	$sql 							= "select * from generate_agency_letter where GEN_AGENCY_ID='".$g_agency_id."'";
	$res 							= mysql_query($sql);
	$rows 							= mysql_fetch_assoc($res);
	
	$sql2 							= "select * from generate_agency_letter_slave2 where GEN_AGENCY_ID='".$g_agency_id."'";
	$res2 							= mysql_query($sql2);
	$rows2 							= mysql_fetch_assoc($res2);
	
	$vslid 							= $obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID");
	$_SESSION['vslName'] 			= $obj->getVesselIMOData($vslid,"VESSEL_NAME");
	$_SESSION['portname'] 			= $obj->getPortName($rows['PORTID']);
	$_SESSION['email'] 				= $rows['EMAIL_ADDRESS'];
	$_SESSION['vslconInfo'] 		= $rows['VSL_CONTACT_DETAILS'];
	$_SESSION['vslinfo'] 			= $rows['VSL_DETAILS'];
	$_SESSION['about'] 				= $rows['VSL_ABOUT'];
	$_SESSION['hsfoQty'] 			= $rows['BA_HSFO_QTY'];
	$_SESSION['supplier'] 			= $rows['BA_SUPPLIER_T_B_AD'];
	$_SESSION['necessary'] 			= $rows['BA_NECESSARY_GUID'];
	
	$_SESSION['bunkeringport'] 		= $obj->getPortNameBasedOnCode($rows2['BUNKERPORT']);
	
	
	$cost_sheet_id 					= $obj->getLatestCostSheetID($rows['COMID']);
							  		  $obj->viewFreightCostEstimationTempleteRecordsNew($cost_sheet_id);
	$_SESSION['quantity'] 			= $rows['QTY'];
	$_SESSION['cargo_name']         = $obj->getMaterialDescBasedOnMaterialCode($obj->getCargoIDBasedOnMaterialCode($obj->getCompareEstimateData($rows['COMID'],"MATERIALID")));
	$agent_code						= $_REQUEST['agent_code'];
	$_SESSION['agent_name']      	= $obj->getVendorListNewData($agent_code,"NAME");
	
	if(@$_REQUEST['port_type'] =="TP"){
		$qtyCargo					= "...........";
	}else{
		$qtyCargo 					= $_SESSION['quantity']." MT (".$_SESSION['cargo_name'].").WP WOG UCAE.";
	}
	
	$charterer_name 		= "";
	
	if($obj->getFun31()==1)
	{
		$charterer_name = $obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($rows['COMID'],"QTY_VENDORID"));
	}
	else
	{
		$sql12 = "select * from freight_cost_estimete_slave8 where FCAID='".$obj->getFun1()."'";
		$res12 = mysql_query($sql12);
		$rec12 = mysql_num_rows($res12);
		while($rows2 = mysql_fetch_assoc($res12))
		{
			$charterer_name = $charterer_name.", ".$obj->getVendorListNewBasedOnID($rows2['VENDORID']);
		}
	}

    $eta_fixture   = $rows['ETA_DATE'];
	if($eta_fixture=="" || $eta_fixture=="0000-00-00 00:00:00")
	{
		$eta_fixture="";
	}
	else
	{
		$eta_fixture=date('d-m-Y H:i',strtotime($rows['ETA_DATE']));
	}

	class PDF extends FPDF
	{
		function Header()
		{
			$obj = new data();
			$obj->funConnect();
			$this->SetFillColor(255,255,255);
			$image_path1 = '../../img/slogo.jpg';
			$this->Image($image_path1,83,5,40,18);
			$this->SetTextColor(27,119,166);
			$this->AddFont("daxcondensedlight",'',"daxcondensedlight.php");
			$this->SetFont('daxcondensedlight','',25);
			$this->SetX(20);
			$this->SetY(7);
			$this->SetX(21);
			//$this->Cell(170,6,"SOCATRA ",0,1,'C',0);
			$this->Ln(1);
			$this->SetFont('Arial','',10);
			$this->SetX(20);
			$this->Cell(170,6,"",0,1,'C',0);
			$this->SetTextColor(177,175,175);
		    $this->Ln(12);
			$this->Cell(0,1,"________________________________________________________________________________________________________________________",0,1,'C','1');
			$this->Ln(2);
		}
			
		function Footer()
		{
			$this->SetY(-20);
			$this->SetFillColor(255,255,255);
			$this->SetTextColor(177,175,175);
			$this->SetFont('Arial','',10);
			$this->SetX(0);
			$this->Cell(190,1,"______________________________________________________________________________________________________________________",0,1,'C','1');
			$this->Ln(1);
			$this->AddFont("daxcondensedlight",'',"daxcondensedlight.php");
			$this->SetFont('daxcondensedlight','',8);
			$this->SetTextColor(027,119,166);
			$this->Ln(1);
			if($_SESSION['company'] == 1)
			{
				
				$this->Cell(190,4,"9 Temasek Boulevard, 31F Suntec Tower 2, Singapore 038989",0,1,'C');
				$this->Cell(190,4,"Tel: +65 6542 0733  email: marketing@sevenoceansconsulting.com",0,1,'C');
				$this->Cell(190,4,"www.sevenoceansconsulting.com",0,1,'C');
			}
			else if($_SESSION['company'] == 2)
			{
				
				$this->Cell(190,4,"9 Temasek Boulevard, 31F Suntec Tower 2, Singapore 038989",0,1,'C');
				$this->Cell(190,4,"Tel: +65 6542 0733  email: marketing@sevenoceansconsulting.com",0,1,'C');
				$this->Cell(190,4,"www.sevenoceansconsulting.com",0,1,'C');
			}
		}
	}
	
	$pdf = new PDF();
	$pdf->AddPage('P');
	$pdf->SetDisplayMode('real','single');
	$pdf->SetTitle('Generate Agency Letter Report Pdf');
	$pdf->SetAuthor('Seven Oceans Holdings');
	$pdf->SetSubject('Generate Agency Letter Report Pdf');
	$pdf->SetKeywords('Designed and Developed By Seven Oceans Holdings pvt. Ltd.');
	
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetTextColor(0,0,0);
	
	$pdf->SetFont('daxcondensedlight','',11);
	$pdf->SetFillColor(0,0,0);
	$pdf->Ln(2);
	$pdf->Cell(190,6,"Letter to Agents for ".$_SESSION['vslName']." at ".$_SESSION['bunkeringport']." for bunkering ",0,1,'C');
	
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(0,0,0);
	$pdf->Ln(2);
	$pdf->Cell(190,5,"To : ",0,1,'L');
	
	$pdf->Cell(190,5,"".$_SESSION['agent_name'],0,1,'L');
	$arr1 = $arr2 = $arr3 = array();	
	$addr_arr = explode(",",$obj->getVendorListNewData($rows['VENDORID'],"STREET_1")); 
	
	for($i=0; $i<=count($addr_arr); $i++){
		$addr = trim($addr_arr[$i]);	
		$pdf->Row1(array("".$addr));
	}
	
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetTextColor(0,0,0);
	
	$pdf->Ln(-6);
	$pdf->Cell(190,6,"".$obj->getVendorListNewData($rows['VENDORID'],"STREET_2"),0,1,'L');
	
	$pdf->Ln(-1);
	$pdf->Cell(190,5,"From : ",0,1,'L');
	$pdf->Cell(190,5,"PROGRESS CHARTERING",0,1,'L');
	
	$pdf->Ln(1);
	$pdf->Cell(25,6,"Good day,",0,1,'L');
	
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1(array("As owners/disponent owners of the captioned vessel, we are pleased to consign the vessel to your agency during her bunker supply at ".$_SESSION['bunkeringport'].". Vessel ETA at ".$_SESSION['bunkeringport']." around ".$eta_fixture." LT."));
	$pdf->Ln(2);
	
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	
	$pdf->Ln(2);
	$pdf->Row1(array("1)Please declare the vessel to port and contact master for pre-arrival formalities without delay."));
	$pdf->Ln(2);
	$pdf->Row1(array("2) Vessel particulars are stated below:"));
	
	$pdf->Ln(1);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(190,4,"VESSEL NAME : ".strtoupper($_SESSION['vslName']),0,1,'L');
	$pdf->Cell(190,4,"FLAG : ".strtoupper($obj->getCountryNameOnId($obj->getVesselIMOData($vslid,"FLAG"))),0,1,'L');
	$pdf->Cell(190,4,"CLASS : ".strtoupper($obj->getClaSocNameOnId($obj->getVesselIMOData($vslid,"CLA_SOC_ID"))),0,1,'L');
	$pdf->Cell(190,4,"BUILT (WHEN/WHERE) : ".$obj->getVesselIMOData($vslid,"YEARBUILT")."/".strtoupper($obj->getCountryNameOnId($obj->getVesselParticularData('COUNTRY_ID','vessel_master_1',$vslid)))."/".strtoupper($obj->getVesselParticularData('YARD_NAME','vessel_master_1',$vslid)),0,1,'L');
	$pdf->Cell(190,4,"IMO NUMBER : ". $obj->getVesselIMOData($vslid,"IMO_NO")."",0,1,'L');
	$pdf->Cell(190,4,"PORT OF REGISTRY : ".strtoupper($obj->getPortName($obj->getVesselParticularData('PORT_ID','vessel_master_1',$vslid))),0,1,'L');
	$pdf->Cell(190,4,"SUMMER: DEADWEIGHT / DISPLACEMENT / DRAFT / TPC : ".strtoupper($obj->getVesselIMOData($vslid,"DWT"))." T / ".$obj->getVesselParticularData('DISPLACEMENT','vessel_master_1',$vslid)." T / ".$obj->getVesselIMOData($vslid,"DRAFTM")." M / ".$obj->getVesselParticularData('SUMMER_3','vessel_master_1',$vslid)." T",0,1,'L');
	
	$tonnage = $obj->getVesselIMOData($vslid,"GRT_NRT");
	$tonnage1 = $obj->getVesselIMOData($vslid,"NRT");
	if($obj->getVesselIMOData($vslid,"BUSINESSTYPEID")==1)
	{
		$pdf->Cell(190,4,"CARGO HOLD CAPACITY (GRAINS) : ".$obj->getVesselIMOData($vslid,"GRAIN")." CBM",0,1,'L'); 
		$pdf->Cell(190,4,"NO.OF HOLDS / NO. OF HATCHES : ". $obj->getVesselIMOData($vslid,"NOH")."/".$obj->getVesselIMOData($vslid,"NOHA"),0,1,'L');
	}
	if($obj->getVesselIMOData($vslid,"BUSINESSTYPEID")==3)
	{
		$pdf->Cell(190,4,"CARGO TANK CAPACITY (98PCT) : ".$obj->getVesselIMOData($vslid,"GAS_TANK_CAPACITY")." CBM",0,1,'L'); 
	}
	if($obj->getVesselIMOData($vslid,"BUSINESSTYPEID")==2)
	{
		$pdf->Cell(190,4,"CARGO TANK CAPACITY (TANKER) : ".$obj->getVesselIMOData($vslid,"TANKER_CAPACITY")." CBM",0,1,'L');
		$pdf->Cell(190,4,"NO. OF CARGO PUMPS / NO. OF GRADES(DOUBLE V/V SEG) : ".$obj->getVesselIMOData($vslid,"TANKER_CARGO_PUMP")."/".$obj->getVesselIMOData($vslid,"NO_OF_GRADE"),0,1,'L');
	}
	$pdf->Ln(1);
	$pdf->Cell(190,4,"GROSS TONNAGE : ".$tonnage."",0,1,'L');
	$pdf->Cell(190,4,"NET TONNAGE : ".$tonnage1."",0,1,'L');
	$pdf->Cell(190,4,"PANAMA TONNAGE : ".$obj->getVesselParticularData('GT_PANAMA','vessel_master_1',$vslid)."",0,1,'L');
	$pdf->Cell(190,4,"SUEZ CANAL TONNAGE : ".$obj->getVesselParticularData('GT_SUEZ','vessel_master_1',$vslid)."",0,1,'L');
	$pdf->Cell(190,4,"LENGTH (O.A.) : ".$obj->getVesselIMOData($vslid,"LOA")." M",0,1,'L');
	$pdf->Cell(190,4,"LENGTH (P.P) : ".$obj->getVesselParticularData('LBW','vessel_master_1',$vslid)." M",0,1,'L');
	$pdf->Cell(190,4,"BREADTH(MLD.) : ".$obj->getVesselIMOData($vslid,"EXT_BREADTH")." T",0,1,'L');
	$pdf->Cell(190,4,"DEPTH (MLD.) : ".$obj->getVesselParticularData('DEPTH_MODULE','vessel_master_1',$vslid)." M",0,1,'L');
	
	$pdf->Ln(2);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->Cell(190,4,"Vsl communication details: ",0,1,'L');
	$pdf->Ln(1);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->Cell(190,4,"CALL SIGN :       ". $obj->getVesselParticularData('CALL_SIGN','vessel_master_6',$vslid)."",0,1,'L');
	$pdf->Cell(190,4,"MMSI NO :        ". $obj->getVesselParticularData('MMSI_NUMBER','vessel_master_6',$vslid)."",0,1,'L');
	$pdf->Cell(190,4,"E-MAIL:            ".$obj->getVesselParticularData('EMAIL_ADDRESS','vessel_master_6',$vslid),0,1,'L');
	$pdf->Cell(190,4,"INM-F TEL :       ".$obj->getVesselParticularData('TELEX_NUMBER','vessel_master_6',$vslid),0,1,'L');
	$pdf->Cell(190,4,"INM-F FAX :      ".$obj->getVesselParticularData('FAX_NUMBER','vessel_master_6',$vslid),0,1,'L');
	$pdf->Cell(190,4,"INM-C :            ".$obj->getVesselParticularData('INMARSAT_NUMBER','vessel_master_6',$vslid),0,1,'L');
	
	
	$pdf->Ln(2);
	$pdf->Row1(array("3) Please contact vessel master directly for regular updates on ETA. Keep us in copy."));
	
	$pdf->Ln(2);
	$pdf->Row1(array("Bunker supply details �"));
	$pdf->Ln(2);
	
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$arr1=array('L','L','L','L');
	$arr2=array(50,50,50,40);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row(array('FUEL GRADE/SPEC','SUPPLIER','PHYSICAL','QUANTITY(MT)'));
	
	$sql2 	= "select * from generate_agency_letter_slave2 where GEN_AGENCY_ID='".$g_agency_id."'";
	$res2 	= mysql_query($sql2);
	while($rows2 = mysql_fetch_assoc($res2))
	{
		$pdf->SetFont('daxcondensedlight','',10);
		$pdf->Row(array($rows2['GRADE'],$rows2['SUPPLIER'],$rows2['PHYSICAL'],$rows2['QUANTITY']));
	}
	
	
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	
	$pdf->Ln(2);
	$pdf->Row1(array("4) Notices: Once bunker supplier details are received, please send relevant notices to bunker suppliers with copy to us. The barge schedule and activity should also be shared with us regularly."));
	
	$pdf->Ln(2);
	$pdf->Row1(array("5)Please appoint bunker surveyor stated below at ".$_SESSION['bunkeringport']." Our operations department will contact the surveyor for necessary guidelines."));
	
	$pdf->Ln(2);
	$pdf->Row1(array("  Bunker Surveyor (Name) :".$rows['BUNKER_SURVEYOR']));
	$pdf->Row1(array("  Bunker Surveyor (Company and Contact) :".$rows['BUNKER_SURVEYORCOM']));
	
	
	$pdf->Ln(2);
	$pdf->Row1(array("6) Please advise bunker survey fees in the PDA/FDA submission."));
	
	$pdf->Ln(2);
	$pdf->Row1(array("7) Please send all bunker related vouchers, receipts and survey report to our operations department, within 30 days. Meanwhile, PDFs of these to be sent to us via email, upon completion."));
	
	$pdf->Ln(2);
	$pdf->Row1(array("8) Details of boarding officer, if any, to be shared with us."));
	
	$pdf->Ln(2);
	$pdf->Row1(array("Please confirm safe receipt of our message by return email."));
	$pdf->Row1(array("Many thanks and we look forward to a speedy and cost-effective turnaround of the vessel under your agency."));
	
	$pdf->Ln(2);
	$pdf->Row1(array("Kind regards,"));
	$pdf->Row1(array($obj->getCompanyNameBasedOnID($_SESSION['company'],"COMPANY_NAME")));
	
	
	$pdf->Ln(6);
	$arr1 = $arr2 = $arr3 = array();	
	$pdf->SetFont('daxcondensedlight','',11);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L');
	$arr2=array(50,50,90);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1(array("Username: ".$rows['USERNAME'],"Password: ".$rows['PASSWORD'],""));
	$pdf->Ln(2);
	$pdf->SetTextColor(027,119,166);
	$link='https://soh1.sevenoceanscloud.com/sohvctc/agentlogin.html';
	$pdf->SetLink($link);
	$pdf->Write(4,'Click here to login',$link);
	

$filename = "Letter to Agents - Bunker Stemmed ( ".$_SESSION['vslName']." - ".$obj->getVendorListNewBasedOnID($rows['VENDORID'])." ) ".date("d-m-Y").".pdf";
$pdf->Output($filename,'D');

}


function create_generate_agency_letter_pdf()
{
	$obj = new data();
	$obj->funConnect();
	
	if(isset($_REQUEST['gen_agency_id'])){$g_agency_id = $_REQUEST['gen_agency_id']; }
	$randomno                   = $_REQUEST['randomno'];
	$comid 						= $_REQUEST['comid'];
	$port_type 					= $_REQUEST['port_type'];
	
	$sql 							= "select * from generate_agency_letter where GEN_AGENCY_ID='".$g_agency_id."'";
	$res 							= mysql_query($sql);
	$rows 							= mysql_fetch_assoc($res);
	$id 							= $obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID");
	$_SESSION['vslName'] 			= $obj->getVesselIMOData($obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID"),"VESSEL_NAME");
	$_SESSION['portname'] 			= $obj->getPortName($rows['PORTID']);
	$_SESSION['kind_attan_to']		= $rows['KIND_ATTN_TO'];
	$agent_code						= $_REQUEST['agent_code'];
	$_SESSION['agent_name']      	= $obj->getVendorListNewData($agent_code,"NAME");
	
	$_SESSION['pda_ini_pay_per'] 	= $rows['PAD_INI_PAY_PERSENT'];
	$_SESSION['pda_fin_pay_per'] 	= $rows['PAD_FIN_PAY_PERSENT'];
	
	$_SESSION['vslconInfo'] 		= $rows['VSL_CONTACT_DETAILS'];
	$_SESSION['vslinfo'] 			= $rows['VSL_DETAILS'];
	$_SESSION['about'] 				= $rows['VSL_ABOUT'];
	
	if($rows['TERMO_OF_TOLERANCE']!="")
	{
	  $_SESSION['termOfTolerance'] =" ".$rows['TERMO_OF_TOLERANCE'];
	}
	else
	{
	  $_SESSION['termOfTolerance'] ="";
	}
	if($rows['CARGO_PACKING_DESC']!="")
	{
	  $_SESSION['cargoPackDesc'] =" ".$rows['CARGO_PACKING_DESC'];
	}
	else
	{
	  $_SESSION['cargoPackDesc'] ="";
	}
	$_SESSION['tolerancesub'] 	    = $rows['TOLERANCE_SUB'];
	$_SESSION['tolerancePerSub'] 	= $rows['TOLERANCE_PERCENT_SUB'];
	$_SESSION['toleranceAdd'] 	    = $rows['TOLERANCE_ADD'];
	$_SESSION['tolerancePerAdd'] 	= $rows['TOLERANCE_PERCENT_ADD'];
	
	
	number_format(($rows['AGREED_GROSS_FREIGHT_LOCAL']),2,'.','');
	if($rows['TOLERANCE_PERCENT_SUB']>0 && $rows['TOLERANCE_PERCENT_ADD']>0)
	{$_SESSION['tolerancePerSub']= " -".number_format($rows['TOLERANCE_PERCENT_SUB'],1,'.','')."%/";}
	else if(($rows['TOLERANCE_PERCENT_SUB']>0 && $rows['TOLERANCE_PERCENT_ADD']==0) || ($rows['TOLERANCE_PERCENT_SUB']>0 && $rows['TOLERANCE_PERCENT_ADD']==""))
	{$_SESSION['tolerancePerSub']= " -".number_format($rows['TOLERANCE_PERCENT_SUB'],1,'.','')."% ";}
	else
	{$_SESSION['tolerancePerSub']= "";}
	if($rows['TOLERANCE_PERCENT_ADD']>0 && $rows['TOLERANCE_PERCENT_SUB']>0)
	{$_SESSION['tolerancePerAdd']= "+".number_format($rows['TOLERANCE_PERCENT_ADD'],1,'.','')."% ";}
	else if(($rows['TOLERANCE_PERCENT_ADD']>0 && $rows['TOLERANCE_PERCENT_SUB']==0) || ($rows['TOLERANCE_PERCENT_ADD']>0 && $rows['TOLERANCE_PERCENT_SUB']==""))
	{$_SESSION['tolerancePerAdd']= " +".number_format($rows['TOLERANCE_PERCENT_ADD'],1,'.','')."% ";}
	else
	{$_SESSION['tolerancePerAdd']= "";}
	
	$cost_sheet_id 					= $obj->getLatestCostSheetID($rows['COMID']);
	
									  $obj->viewFreightCostEstimationTempleteRecordsNew($cost_sheet_id);
	$_SESSION['quantity'] 			= $rows['QTY'];
	$_SESSION['cargo_name']			= $obj->getCargoNameListForMultipleNamemultiple($obj->getCompareEstimateData($rows['COMID'],"CARGO_ID"));
	
	$vlsImoId 						= $obj->getCompareEstimateData($rows['COMID'],"VESSEL_IMO_ID");
	$charterer_name 				= "";
	

if($obj->getFun155()==1)
{
	$charterer_name = $obj->getVendorListNewBasedOnID($obj->getCompareEstimateData($rows['COMID'],"QTY_VENDORID"));
}
else
{
	$sql12 = "select * from freight_cost_estimete_slave8 where FCAID='".$obj->getFun1()."'";
	$res12 = mysql_query($sql12);
	$rec12 = mysql_num_rows($res12);
	while($rows2 = mysql_fetch_assoc($res12))
	{
		$charterer_name = $charterer_name.", ".$obj->getVendorListNewBasedOnID($rows2['VENDORID']);
	}
}

$eta_fixture   = $obj->getCheckListLoadPortRecordsDataBasedOnMappingIDEntryName($rows['COMID'],"ACTUAL ARRIVAL","ENTITY_VALUE",$port_type,$rows['PORTID'],$randomno);
if($eta_fixture=="")
{
	$eta_fixture="";
}
else
{
	$eta_fixture=date('d-m-Y H:i',strtotime($eta_fixture));
}

$sql1 = "select * from generate_agency_letter_slave1 where GEN_AGENCY_ID=".$g_agency_id." order by SLAVEID asc";

$result1 = mysql_query($sql1);
$rows1 = mysql_fetch_assoc($result1);
$op_emailds = $rows1['EMAILID'];

/********************FOR LOAD PORT***************************/

if($port_type == "LP")
{
	class PDF extends FPDF
	{
		function Header()
		{
			$obj = new data();
			$obj->funConnect();
			$this->SetFillColor(255,255,255);
			$image_path1 = '../../img/slogo.jpg';
			$this->Image($image_path1,83,5,40,18);
			$this->SetTextColor(27,119,166);
			$this->AddFont("daxcondensedlight",'',"daxcondensedlight.php");
			$this->SetFont('daxcondensedlight','',25);
			$this->SetX(20);
			$this->SetY(7);
			$this->SetX(21);
			//$this->Cell(170,6,"SOCATRA ",0,1,'C',0);
			$this->Ln(1);
			$this->SetFont('Arial','',10);
			$this->SetX(20);
			$this->Cell(170,6,"",0,1,'C',0);
			$this->SetTextColor(177,175,175);
		    $this->Ln(12);
			$this->Cell(0,1,"________________________________________________________________________________________________________________________",0,1,'C','1');
			$this->Ln(2);
		}
			
		function Footer()
		{
			$this->SetY(-20);
			$this->SetFillColor(255,255,255);
			$this->SetTextColor(177,175,175);
			$this->SetFont('Arial','',10);
			$this->SetX(0);
			$this->Cell(190,1,"______________________________________________________________________________________________________________________",0,1,'C','1');
			$this->Ln(1);
			$this->AddFont("daxcondensedlight",'',"daxcondensedlight.php");
			$this->SetFont('daxcondensedlight','',8);
			$this->SetTextColor(027,119,166);
			$this->Ln(1);
			if($_SESSION['company'] == 1)
			{
				
				$this->Cell(190,4,"9 Temasek Boulevard, 31F Suntec Tower 2, Singapore 038989",0,1,'C');
				$this->Cell(190,4,"Tel: +65 6542 0733  email: marketing@sevenoceansconsulting.com",0,1,'C');
				$this->Cell(190,4,"www.sevenoceansconsulting.com",0,1,'C');
			}
			else if($_SESSION['company'] == 2)
			{
				
				$this->Cell(190,4,"9 Temasek Boulevard, 31F Suntec Tower 2, Singapore 038989",0,1,'C');
				$this->Cell(190,4,"Tel: +65 6542 0733  email: marketing@sevenoceansconsulting.com",0,1,'C');
				$this->Cell(190,4,"www.sevenoceansconsulting.com",0,1,'C');
			}
		}
	}	
	
	$pdf = new PDF();
	$pdf->AddPage('P');
	$pdf->SetDisplayMode('real','single');
	$pdf->SetTitle('Generate PDA Request Letter Report Pdf');
	$pdf->SetAuthor('Seven Oceans Holdings');
	$pdf->SetSubject('Generate PDA Request Letter Report Pdf');
	$pdf->SetKeywords('Designed and Developed By Seven Oceans Holdings pvt. Ltd.');
	
	$pdf->Ln(5);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(0,0,0);
	$pdf->Cell(190,6,"Please copy : opex@socatra.com",0,1,'L',0);
	
	$pdf->Ln(2);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(0,0,0);
	$pdf->Cell(190,6,"To :".$_SESSION['agent_name']." (".$_SESSION['portname'].") /att. ".$rows1['ENTITY_NAME']." (".$rows1['EMAILID'].")",0,1,'L',0);
	
	$pdf->Ln(2);
	$pdf->Cell(190,5,"From : Socatra Bordeaux",0,1,'L',0);
	
	$pdf->Ln(5);
	$pdf->Cell(25,5,"Good day,",0,1,'L');
	$pdf->Cell(25,5,"Dear Sirs,",0,1,'L');
	
	$pdf->Ln(1);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	
	$pdf->Ln(1);
	$pdf->Cell(190,4,"Re. ".strtoupper($_SESSION['vslName']),0,1,'L',1);
	
	
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	
	$pdf->Ln(1);
	$pdf->Row1(array("We are working on a possible loading at yours of ".$_SESSION['quantity']." MT ".$_SESSION['cargo_name']."."));
	$pdf->Ln(5);
	$pdf->Row1(array("Would you please revert with your best Performa D/A on basis following vessel�s particulars:"));
	
	$pdf->Ln(1);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	
	$pdf->Cell(190,4,"LOA(M) : ".$obj->getVesselIMOData($vlsImoId,"LOA"),0,1,'L',1);
	$pdf->Cell(190,4,"BOA(M) : ".$obj->getVesselParticularData('BEAM','vessel_master_tankers',$vlsImoId),0,1,'L',1);
	$pdf->Cell(190,4,"MAX S.DRAFT(M) : ".$obj->getVesselIMOData($vlsImoId,"DRAFTM"),0,1,'L',1);

	$pdf->Cell(190,4,"DWT(MT) : ".$obj->getVesselIMOData($vlsImoId,"DWT"),0,1,'L',1);
	$pdf->Cell(190,4,"GRT : ".$obj->getVesselIMOData($vlsImoId,"GRT_NRT"),0,1,'L',1);
	$pdf->Cell(190,4,"NRT : ".$obj->getVesselIMOData($vlsImoId,"NRT"),0,1,'L',1);
	
	$pdf->Ln(5);
	$pdf->SetFont('daxcondensedlight','',10);
	$arr1 = $arr2 = $arr3 = $arr4 = $arr5 = $arr6 = $arr7 = $arr8 = $arr9 = $arr10 = array();
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1(array("Thanks to quote an �all in� agency fees."));
	$pdf->Ln(5);
	$pdf->Row1(array("In addition, please advise the usual port restrictions for such type of tanker vessel."));
	
	$pdf->Ln(2);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	
	$pdf->Row1(array("Best regards,"));
	$pdf->Ln(6);
	$pdf->Row1(array("Socatra Comops"));
	$pdf->Row1(array("As agent to Ship-owners"));
	$pdf->Row1(array($obj->getCompanyNameBasedOnID($obj->getUserDetailBaseOnId($rows['LOGINID'],'MCOMPANYID'),'COMPANY_NAME')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'CONTACT_PERSON')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'ADDRESS')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'PHONE_NO')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'EMAILID')));
	
}

/********************FOR DISCHARGE PORT**********************/

else if($port_type == "DP")
{
	class PDF extends FPDF
	{
		function Header()
		{
			$obj = new data();
			$obj->funConnect();
			$this->SetFillColor(255,255,255);
			$image_path1 = '../../img/slogo.jpg';
			$this->Image($image_path1,83,5,40,18);
			$this->SetTextColor(27,119,166);
			$this->AddFont("daxcondensedlight",'',"daxcondensedlight.php");
			$this->SetFont('daxcondensedlight','',25);
			$this->SetX(20);
			$this->SetY(7);
			$this->SetX(21);
			//$this->Cell(170,6,"SOCATRA ",0,1,'C',0);
			$this->Ln(1);
			$this->SetFont('Arial','',10);
			$this->SetX(20);
			$this->Cell(170,6,"",0,1,'C',0);
			$this->SetTextColor(177,175,175);
		    $this->Ln(12);
			$this->Cell(0,1,"________________________________________________________________________________________________________________________",0,1,'C','1');
			$this->Ln(2);
		}
			
		function Footer()
		{
			$this->SetY(-20);
			$this->SetFillColor(255,255,255);
			$this->SetTextColor(177,175,175);
			$this->SetFont('Arial','',10);
			$this->SetX(0);
			$this->Cell(190,1,"______________________________________________________________________________________________________________________",0,1,'C','1');
			$this->Ln(1);
			$this->AddFont("daxcondensedlight",'',"daxcondensedlight.php");
			$this->SetFont('daxcondensedlight','',8);
			$this->SetTextColor(027,119,166);
			$this->Ln(1);
			if($_SESSION['company'] == 1)
			{
				
				$this->Cell(190,4,"9 Temasek Boulevard, 31F Suntec Tower 2, Singapore 038989",0,1,'C');
				$this->Cell(190,4,"Tel: +65 6542 0733  email: marketing@sevenoceansconsulting.com",0,1,'C');
				$this->Cell(190,4,"www.sevenoceansconsulting.com",0,1,'C');
			}
			else if($_SESSION['company'] == 2)
			{
				
				$this->Cell(190,4,"9 Temasek Boulevard, 31F Suntec Tower 2, Singapore 038989",0,1,'C');
				$this->Cell(190,4,"Tel: +65 6542 0733  email: marketing@sevenoceansconsulting.com",0,1,'C');
				$this->Cell(190,4,"www.sevenoceansconsulting.com",0,1,'C');
			}
		}
	}	
	
	$pdf = new PDF();
	$pdf->AddPage('P');
	$pdf->SetDisplayMode('real','single');
	$pdf->SetTitle('Generate PDA Request Letter Report Pdf');
	$pdf->SetAuthor('Seven Oceans Holdings');
	$pdf->SetSubject('Generate PDA Request Letter Report Pdf');
	$pdf->SetKeywords('Designed and Developed By Seven Oceans Holdings pvt. Ltd.');
	
	$pdf->Ln(5);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(0,0,0);
	$pdf->Cell(190,6,"Please copy : opex@socatra.com",0,1,'L',0);
	
	$pdf->Ln(2);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(0,0,0);
	$pdf->Cell(190,6,"To :".$_SESSION['agent_name']." (".$_SESSION['portname'].") /att. ".$rows1['ENTITY_NAME']." (".$rows1['EMAILID'].")",0,1,'L',0);
	
	$pdf->Ln(2);
	$pdf->Cell(190,5,"From : Socatra Bordeaux",0,1,'L',0);
	
	$pdf->Ln(5);
	$pdf->Cell(25,5,"Good day,",0,1,'L');
	$pdf->Cell(25,5,"Dear Sirs,",0,1,'L');
	
	$pdf->Ln(1);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	
	$pdf->Ln(1);
	$pdf->Cell(190,4,"Re. ".strtoupper($_SESSION['vslName']),0,1,'L',1);
	
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	
	$pdf->Ln(1);
	$pdf->Row1(array("We are working on a possible discharging at yours of ".$_SESSION['quantity']." MT ".$_SESSION['cargo_name']."."));
	$pdf->Ln(5);
	$pdf->Row1(array("Would you please revert with your best Performa D/A on basis following vessel�s particulars:"));
	
	$pdf->Ln(1);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	
	$pdf->Cell(190,4,"LOA(M) : ".$obj->getVesselIMOData($vlsImoId,"LOA"),0,1,'L',1);
	$pdf->Cell(190,4,"BOA(M) : ".$obj->getVesselParticularData('BEAM','vessel_master_tankers',$vlsImoId),0,1,'L',1);
	$pdf->Cell(190,4,"MAX S.DRAFT(M) : ".$obj->getVesselIMOData($vlsImoId,"DRAFTM"),0,1,'L',1);
	$pdf->Cell(190,4,"DWT(MT) : ".$obj->getVesselIMOData($vlsImoId,"DWT"),0,1,'L',1);
	$pdf->Cell(190,4,"GRT : ".$obj->getVesselIMOData($vlsImoId,"GRT_NRT"),0,1,'L',1);
	$pdf->Cell(190,4,"NRT : ".$obj->getVesselIMOData($vlsImoId,"NRT"),0,1,'L',1);
	
	$pdf->Ln(5);
	$pdf->SetFont('daxcondensedlight','',10);
	$arr1 = $arr2 = $arr3 = $arr4 = $arr5 = $arr6 = $arr7 = $arr8 = $arr9 = $arr10 = array();
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1(array("Thanks to quote an �all in� agency fees."));
	$pdf->Ln(5);
	$pdf->Row1(array("In addition, please advise the usual port restrictions for such type of tanker vessel."));
	
	$pdf->Ln(2);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	
	$pdf->Row1(array("Best regards,"));
	$pdf->Ln(6);
	$pdf->Row1(array("Socatra Comops"));
	$pdf->Row1(array("As agent to Ship-owners"));
	
	$pdf->Row1(array($obj->getCompanyNameBasedOnID($obj->getUserDetailBaseOnId($rows['LOGINID'],'MCOMPANYID'),'COMPANY_NAME')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'CONTACT_PERSON')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'ADDRESS')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'PHONE_NO')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'EMAILID')));
}

else if($port_type == "TP")
{
	class PDF extends FPDF
	{
		function Header()
		{
			$obj = new data();
			$obj->funConnect();
			$this->SetFillColor(255,255,255);
			$image_path1 = '../../img/slogo.jpg';
			$this->Image($image_path1,83,5,40,18);
			$this->SetTextColor(27,119,166);
			$this->AddFont("daxcondensedlight",'',"daxcondensedlight.php");
			$this->SetFont('daxcondensedlight','',25);
			$this->SetX(20);
			$this->SetY(7);
			$this->SetX(21);
			//$this->Cell(170,6,"SOCATRA ",0,1,'C',0);
			$this->Ln(1);
			$this->SetFont('Arial','',10);
			$this->SetX(20);
			$this->Cell(170,6,"",0,1,'C',0);
			$this->SetTextColor(177,175,175);
		    $this->Ln(12);
			$this->Cell(0,1,"________________________________________________________________________________________________________________________",0,1,'C','1');
			$this->Ln(2);
		}
			
		function Footer()
		{
			$this->SetY(-20);
			$this->SetFillColor(255,255,255);
			$this->SetTextColor(177,175,175);
			$this->SetFont('Arial','',10);
			$this->SetX(0);
			$this->Cell(190,1,"______________________________________________________________________________________________________________________",0,1,'C','1');
			$this->Ln(1);
			$this->AddFont("daxcondensedlight",'',"daxcondensedlight.php");
			$this->SetFont('daxcondensedlight','',8);
			$this->SetTextColor(027,119,166);
			$this->Ln(1);
			if($_SESSION['company'] == 1)
			{
				
				$this->Cell(190,4,"9 Temasek Boulevard, 31F Suntec Tower 2, Singapore 038989",0,1,'C');
				$this->Cell(190,4,"Tel: +65 6542 0733  email: marketing@sevenoceansconsulting.com",0,1,'C');
				$this->Cell(190,4,"www.sevenoceansconsulting.com",0,1,'C');
			}
			else if($_SESSION['company'] == 2)
			{
				
				$this->Cell(190,4,"9 Temasek Boulevard, 31F Suntec Tower 2, Singapore 038989",0,1,'C');
				$this->Cell(190,4,"Tel: +65 6542 0733  email: marketing@sevenoceansconsulting.com",0,1,'C');
				$this->Cell(190,4,"www.sevenoceansconsulting.com",0,1,'C');
			}
		}
	}	
	
	$pdf = new PDF();
	$pdf->AddPage('P');
	$pdf->SetDisplayMode('real','single');
	$pdf->SetTitle('Generate PDA Request Letter Report Pdf');
	$pdf->SetAuthor('Seven Oceans Holdings');
	$pdf->SetSubject('Generate PDA Request Letter Report Pdf');
	$pdf->SetKeywords('Designed and Developed By Seven Oceans Holdings pvt. Ltd.');
	
	$pdf->Ln(5);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(0,0,0);
	$pdf->Cell(190,6,"Please copy : opex@socatra.com",0,1,'L',0);
	
	$pdf->Ln(2);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(0,0,0);
	$pdf->Cell(190,6,"To :".$_SESSION['agent_name']." (".$_SESSION['portname'].") /att. ".$rows1['ENTITY_NAME']." (".$rows1['EMAILID'].")",0,1,'L',0);
	
	$pdf->Ln(2);
	$pdf->Cell(190,5,"From : Socatra Bordeaux",0,1,'L',0);
	
	$pdf->Ln(5);
	$pdf->Cell(25,5,"Good day,",0,1,'L');
	$pdf->Cell(25,5,"Dear Sirs,",0,1,'L');
	
	$pdf->Ln(1);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	
	$pdf->Ln(1);
	$pdf->Cell(190,4,"Re. ".strtoupper($_SESSION['vslName']),0,1,'L',1);
	
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	
	$pdf->Ln(1);
	$pdf->Row1(array("We are working on a possible transit at yours of ".$_SESSION['quantity']." MT ".$_SESSION['cargo_name']."."));
	$pdf->Ln(5);
	$pdf->Row1(array("Would you please revert with your best Performa D/A on basis following vessel�s particulars:"));
	
	$pdf->Ln(1);
	$pdf->SetFont('daxcondensedlight','',10);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetTextColor(000,000,000);
	$pdf->SetDrawColor(255,255,255);
	
	$pdf->Cell(190,4,"LOA(M) : ".$obj->getVesselIMOData($vlsImoId,"LOA"),0,1,'L',1);
	$pdf->Cell(190,4,"BOA(M) : ".$obj->getVesselParticularData('BEAM','vessel_master_tankers',$vlsImoId),0,1,'L',1);
	$pdf->Cell(190,4,"MAX S.DRAFT(M) : ".$obj->getVesselIMOData($vlsImoId,"DRAFTM"),0,1,'L',1);
	$pdf->Cell(190,4,"DWT(MT) : ".$obj->getVesselIMOData($vlsImoId,"DWT"),0,1,'L',1);
	$pdf->Cell(190,4,"GRT : ".$obj->getVesselIMOData($vlsImoId,"GRT_NRT"),0,1,'L',1);
	$pdf->Cell(190,4,"NRT : ".$obj->getVesselIMOData($vlsImoId,"NRT"),0,1,'L',1);
	
	$pdf->Ln(5);
	$pdf->SetFont('daxcondensedlight','',10);
	$arr1 = $arr2 = $arr3 = $arr4 = $arr5 = $arr6 = $arr7 = $arr8 = $arr9 = $arr10 = array();
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1(array("Thanks to quote an �all in� agency fees."));
	$pdf->Ln(5);
	$pdf->Row1(array("In addition, please advise the usual port restrictions for such type of tanker vessel."));
	
	$pdf->Ln(2);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L');
	$arr2=array(190);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	
	$pdf->Row1(array("Best regards,"));
	$pdf->Ln(6);
	$pdf->Row1(array("Socatra Comops"));
	$pdf->Row1(array("As agent to Ship-owners"));
	
	$pdf->Row1(array($obj->getCompanyNameBasedOnID($obj->getUserDetailBaseOnId($rows['LOGINID'],'MCOMPANYID'),'COMPANY_NAME')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'CONTACT_PERSON')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'ADDRESS')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'PHONE_NO')));
	$pdf->Row1(array($obj->getUserDetailBaseOnId($rows['LOGINID'],'EMAILID')));
	
}

	$pdf->Ln(6);
	$arr1 = $arr2 = $arr3 = array();	
	$pdf->SetFont('daxcondensedlight','',11);
	$pdf->SetTextColor(0,0,0);
	$arr1=array('L','L','L');
	$arr2=array(50,50,90);
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row1(array("Username: ".$rows['USERNAME'],"Password: ".$rows['PASSWORD'],""));
	$pdf->Ln(2);
	$pdf->SetTextColor(027,119,166);
	$link='https://www.socatra-soh.com/agentlogin.html';
	$pdf->SetLink($link);
	$pdf->Write(4,'Click here to login',$link);

$filename = "PDA Request Letter(".$_SESSION['vslName']."-".$obj->getVendorListNewBasedOnID($rows['VENDORID']).") ".date("d-m-Y").".pdf";
$pdf->Output($filename,'D');

}

?>
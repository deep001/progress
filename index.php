<?php 
session_start();
require_once "app/class/config.php"; 
require_once "app/class/csrf.php";
$getcsrf = Csrf::getCsrfToken(); // get csrf token to prevent hacking
$_SESSION['csrf'] = $getcsrf;
?> 

<!DOCTYPE html>
<html class="bg-blue">
    <head>
        <meta charset="UTF-8">
        <title>:: Welcome to Seven Oceans Commercials ::</title>
		<link rel="shortcut icon" href="favicon.ico">

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        
        <?php require_once Config::path()->INCLUDE_PATH.'/fronthead.php'; ?>

       
    </head> 
    <body class="bg-white" style="background-color:#FFFFFF;" >
        <div class="form-box" id="login-box" align="center" style="margin-top:70px;" >
			<img src="./img/slogo.png">
			<!--<table width="100%">
			<tr>
			<td><img src="./img/logo.png"></td>
		<td style="float:right"><label style="letter-spacing:0.7px;">Seven Oceans' Commercial</label><br><label style="letter-spacing:0.7px; font-style:italic; float:right">Enterprise</label></td>
			</tr>
			</table>-->
			
            <div style="height:15px;"></div>
            <div class="header">Seven Oceans Commercials</div>
          <!--  <form method="post" name="login-form" id="login-form" enctype="multipart/form-data" action="checklogin.php"> -->
          <form method="post" name="login-form" id="login-form" enctype="multipart/form-data" action="checklogin.php">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="txtUser" id="txtUser" class="form-control" placeholder="User ID" autocomplete="off"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="txtPass" id="txtPass" class="form-control" placeholder="Password"/>
                    </div>   
                    <input type="hidden" name="csrf-token" value="<?php echo $_SESSION['csrf']; ?>">       
                    <div class="form-group">
                        <!--<input type="checkbox" name="remember_me"/> Remember me-->
                    </div>
                </div>
                <div class="footer" style="border:1px solid #eaeaec;">                                                               
                    <input type="submit" class="btn bg-blue btn-block" name="signin" value = "Sign me in">
                </div>
            </form>
			<div><a href="brokerlogin.html" style="float:left; text-decoration:none; margin-top:2px;" title="Broker Login">Broker's Login</a><a href="agentlogin.html" style="float:right; text-decoration:none; margin-top:2px;" title="Agent Login">Agent's Login</a></div>
			
			<div style="height:15px;"></div>
			
			<div class="margin text-right">
              <label style="letter-spacing:0.7px;">Seven Oceans Commercials</label><br>
			  <label style="letter-spacing:0.7px; font-style:italic;">Cloud & Enterprise</label>

            </div>
			<div class="margin text-center">
               &nbsp;

            </div>
			<div class="margin text-center">
               &nbsp;<!--<span>A Seven Oceans Holdings production</span>-->

            </div>
			<div class="margin text-center">
               &nbsp;<!--<span>A Seven Oceans Holdings production</span>-->

            </div>
        </div>
 <div class="footer" align="right">                                                               
	<img src="img/Logo2.jpg" height="14" width="14">&nbsp;<a href="http://sevenoceansconsulting.com" target="_blank" style="font-weight:bold;">A Seven Oceans Holdings production</a>&nbsp;&nbsp;&nbsp;&nbsp;
</div>
<?php require_once Config::path()->INCLUDE_PATH.'/frontscript.php'; ?>
</body>
</html>

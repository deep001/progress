<?php 
session_start();
require_once("../includes/display_admin.inc.php");
require_once("../includes/functions_admin.inc.php");
$obj = new data();
$display = new display();
$display->logout_a();
$id = $_REQUEST['id'];
if (@$_REQUEST['action'] == 'submit')
 {
	
 	$msg = $obj->updateUserRightsRecords();
	header('Location:./shipping_user.php?msg='.$msg);
 }
 $obj->viewUserRecords($id);
$pagename = basename($_SERVER['PHP_SELF'])."?id=".$id;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
	vertical-align: top; 
	transition: height 0.2s;
	-webkit-transition: height 0.2s; 
	-moz-transition: height 0.2s; 
}

form.cmxform label.error, label.error {
	/* remove the next line when you have trouble in IE6 with labels in list */
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:10px;
	color: red;
	font-style:normal;
	font-weight:lighter;
	margin:5px;
	vertical-align:top;
}
tr td:nth-child(2) {
    font-weight:700 !important;
	font-size:13px !important;
}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(2); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <i class="fa fa-group"></i>&nbsp;Shipping User&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Shipping User</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
					<!--   content put here..................-->
					<div align="right"><a href="shipping_user.php"><button class="btn btn-info btn-flat">Back</button></a></div>
					<div style="height:10px;">&nbsp;</div>
					<div class="box box-primary">
						<!-- form start -->
						<form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
							<div class="box-body">
								
								<div class="row">
									<div class="col-xs-12">
										<h2 class="page-header">
										  USER DETAILS
										</h2>                            
									</div><!-- /.col -->
								</div>
								<div class="row">
									<div class="col-xs-12">
										<div class="box box-primary">
											<div class="box-body no-padding">
												<table class="table table-striped">
												   <thead>
														<tr>
															<th width="20%" align="center" valign="middle">&nbsp;</th>
															<th width="40%" align="center" valign="middle">&nbsp;</th>
															<th width="40%" align="left" valign="middle">&nbsp;</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td></td>
															<td>Company Name</td>
															<td><label style='color:#dc631e;font-size:12px;' ><?php echo strtoupper($obj->getCompanyData($obj->getFun1(),"COMPANY_NAME"));?></label></td>
														</tr>
														<tr>
															<td></td>
															<td>Name</td>
															<td><?php echo $obj->getFun5();?></td>
														</tr>
														<tr>
															<td></td>
															<td>Address</td>
															<td><?php echo $obj->getFun2();?></td>
														</tr>
														<tr>
															<td></td>
															<td>Phone Number</td>
															<td><?php echo $obj->getFun3();?></td>
														</tr>
														<tr>
															<td></td>
															<td>E-mail ID</td>
															<td><?php echo $obj->getFun4();?></td>
														</tr>
														<tr>
															<td></td>
															<td>User Name</td>
															<td><?php echo $obj->getFun7();?></td>
														</tr>
														<tr>
															<td></td>
															<td>Password</td>
															<td><?php echo $obj->getFun8();?></td>
														</tr>
														<tr>
															<td></td>
															<td>Confirm Password</td>
															<td><?php echo $obj->getFun8();?></td>
														</tr>
													</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>                          
									</div><!-- /.col -->
								</div>
								
								<div class="row">
									<div class="col-xs-12">
										<h2 class="page-header">
										  EDIT RIGHTS
										</h2>                            
									</div><!-- /.col -->
								</div>
								<div class="row">
									<div class="col-xs-12">
										<div class="box box-primary">
											<div class="box-body no-padding">
												<table class="table table-striped">
												   <thead>
														<tr>
															<th width="20%" align="center" valign="middle">&nbsp;</th>
															<th width="40%" align="center" valign="middle">Screens</th>
															<th width="40%" align="left" valign="middle">Rights</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td></td>
															<td>Nom ID deactivation in "In Ops"</td>
															<td>
															<?php if(in_array("1",$obj->getInternalUserRights($id,5,1))){?>
																<input type="checkbox" name="checkbox_1" id="checkbox_1" checked="checked" value="1" />
															<?php }else{?>
																<input type="checkbox" name="checkbox_1" id="checkbox_1" value="1" />
															<?php }?>
															</td>
														</tr>
														<tr>
															<td></td>
															<td>"In Ops" to "Post Ops" confirmation</td>
															<td>
															<?php if(in_array("1",$obj->getInternalUserRights($id,5,2))){?>
															<input type="checkbox" name="checkbox_2" id="checkbox_2" checked="checked" value="1" />
															<?php }else{?>
															<input type="checkbox" name="checkbox_2" id="checkbox_2" value="1" />
															<?php }?>
															</td>
														</tr>
														
														<tr>
															<td></td>
															<td>"Post Ops" to "History" confirmation</td>
															<td>
															<?php if(in_array("1",$obj->getInternalUserRights($id,5,3))){?>
																<input type="checkbox" name="checkbox_3" id="checkbox_3" checked="checked" value="1" />
															<?php }else{?>
																<input type="checkbox" name="checkbox_3" id="checkbox_3" value="1" />
															<?php }?>
															</td>
														</tr>
														
														<tr>
															<td></td>
															<td>Shipping User's Masters Tab</td>
															<td>
															<?php if(in_array("1",$obj->getInternalUserRights($id,5,4))){?>
																<input type="checkbox" name="checkbox_4" id="checkbox_4" checked="checked" value="1" />
															<?php }else{?>
															<input type="checkbox" name="checkbox_4" id="checkbox_4" value="1" />
															<?php }?>
															</td>
														</tr>
														
													</tbody>
												</table>
											</div><!-- /.box-body -->
										</div>                          
									</div><!-- /.col -->
								</div>
							</div><!-- /.box-body -->
							<div class="box-footer" align="center">
								<button type="submit" class="btn btn-primary btn-flat" onClick="return getValidate();">Submit</button><input type="hidden" name="action" value="submit" />
							</div>
						</form>
					</div>
				<!--   content ends here..................-->
			</section><!-- /.content -->
		</aside><!-- /.right-side -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../css/chosen.css" rel="stylesheet" type="text/css" />
<script src="../js/chosen.jquery.js" type="text/javascript"></script>
<script src='../js/jquery.autosize.js'></script>
<link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.alerts.js"></script>
<script src="../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#selCModule,#selRTo").chosen(); 
$('#txtAddress').autosize({append: "\n"});
});
/************************************/
</script>
    </body>
</html>
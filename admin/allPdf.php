<?php
session_start();
require_once("../includes/functions_admin.inc.php");
include_once("../includes/fpdf.php");
@$i = $_REQUEST['id'];
$ret = "";
switch ($i)
{
  case 1:
        $ret = create_audit_trail_pdf();
				break;
  case 2:
        $ret = create_access_log_pdf();
				break;
	default:
		    //$ret = funError();
				//echo $ret;
				break; 				
}

/*******************************************Access Log PDF************************************************/

function create_access_log_pdf()
{
	$obj = new data();
	$obj->funConnect();
	
	$loginid = $_REQUEST['loginid'];
	
	$sql1  = "SELECT CONTACT_PERSON FROM login WHERE LOGINID=".$loginid;
	$res1  = mysql_query($sql1);
	$rows1 = mysql_fetch_assoc($res1);
	
	$_SESSION['name'] = $rows1['CONTACT_PERSON'];
	
	class PDF extends FPDF
	{
		function Header()
		{
				$obj = new data();
				$obj->funConnect();
				
				$this->SetFillColor(255,255,255);
				$image_path1 = '../img/logo1.jpg';
				$this->Image($image_path1,87,3,30,15);
				$this->SetFont('Arial','',10);
				$this->SetTextColor(220, 99, 30);
				$this->SetXY(140,13);
				$this->Cell(0,5,"Access Log (".$_SESSION['name'].")",0,1,'R',1);	
				$this->SetFont('Arial','',10);
				$this->SetFillColor(255,255,255);
				$this->SetTextColor(177,175,175);
				$this->Cell(190,1,"_________________________________________________________________________________________________",0,1,'L','1');
				$this->Ln(2);
			}
			
			function Footer()
			{
				$this->SetY(-20);
				//SELECT Arial italic 8
				$this->SetFillColor(255,255,255);
				$this->SetTextColor(177,175,175);
				$this->SetFont('Arial','',10);
				$this->Cell(190,1,"_________________________________________________________________________________________________",0,1,'L','1');
				$this->Ln(1);
				$this->SetFont('Arial','',6);
				
				if($_SESSION['company'] == 1)
				{
					$this->Cell(190,4,"Alpina Chartering ApS",0,1,'C');
					$this->Cell(190,4," Skomagergade 5-7, DK-4000 Roskilde,Denmark",0,1,'C');
					$this->Cell(190,4,"Tel: +45 55 77 77 77 email: chartering@alpina.dk",0,1,'C');
					$this->Cell(190,4,"",0,1,'C');
				}
				else if($_SESSION['company'] == 2)
				{
					$this->Cell(190,4,"Alpina Chartering ApS",0,1,'C');
					$this->Cell(190,4," Skomagergade 5-7, DK-4000 Roskilde,Denmark",0,1,'C');
					$this->Cell(190,4,"Tel: +45 55 77 77 77 email: chartering@alpina.dk",0,1,'C');
					$this->Cell(190,4,"",0,1,'C');
				}
			}
		}

		$pdf = new PDF();
		$pdf->AddPage('P');
		$pdf->SetDisplayMode('real','single');
		$pdf->SetTitle('Access Log Pdf');
		$pdf->SetAuthor('Seven Oceans Holdings');
		$pdf->SetSubject('Access Log Pdf');
		$pdf->SetKeywords('Designed and Developed By Seven Oceans Holdings pvt. Ltd.');
		
		$pdf->Ln(5);
		
		$arr1 = $arr2 = $arr3 = array();	
		$pdf->SetFont('Arial','B',7);
		$pdf->SetFillColor(27,119,166);
		$pdf->SetTextColor(255,255,255);
		$pdf->SetDrawColor(241,241,241);
		$arr1=array('L','L','C','L');
		$arr2=array(50,50,50,42);
		$arr3=array("Log In","Log Out","Logged in Duration(HH:MM)","User Type");
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		$pdf->Row2($arr3);

		$sql = "SELECT * FROM access_log_master WHERE LOGINID='".$loginid."'";
		$res = mysql_query($sql);
		$rec = mysql_num_rows($res);
		
		$i = 0;
		if($rec == 0)
		{
			$arr1 = $arr2 = $arr3 = array();	
			$i=$i+1;
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(244,244,244);
		}
		else
		{
			$pdf->SetFillColor(255,255,255);
		}
		
			$pdf->SetFont('Arial','',7);
			$pdf->SetTextColor(0,0,0);
			$arr1=array('C');
			$arr2=array(192);
			$arr3=array("sorry, currently there are zero records");
			$pdf->SetAligns($arr1);
			$pdf->SetWidths($arr2);
			$pdf->Row2($arr3);
		}
		else
		{
		while($rows = mysql_fetch_assoc($res))
		{
			if(date("d-M-Y h:i",strtotime($rows['OUT_DATE'])) == '01-Jan-1970 12:00')
			{ 
				$date = ''; $diff = '';
			}
			else 
			{
				$date = date("d-M-Y h:i",strtotime($rows['OUT_DATE']));
				$diff = strtotime($rows['OUT_DATE'])-strtotime($rows['IN_DATE']);
			}
			
			$arr1 = $arr2 = $arr3 = array();	
			$i=$i+1;
			if($i % 2 == 0)
			{
				$pdf->SetFillColor(244,244,244);
			}
			else
			{
				$pdf->SetFillColor(255,255,255);
			}
			
			$pdf->SetFont('Arial','',7);
			$pdf->SetTextColor(0,0,0);
			$arr1=array('L','L','C','L');
			$arr2=array(50,50,50,42);
			$arr3=array(date("d-M-Y h:i",strtotime($rows['IN_DATE'])),$date,date("H:i",$diff),$rows['USER_TYPE']);
			$pdf->SetAligns($arr1);
			$pdf->SetWidths($arr2);
			$pdf->Row2($arr3);
		}
	}
	
	$filename = "Access Log (".$_SESSION['name'].") ".date("d-M-Y",time()).".pdf";
	$pdf->Output($filename,'D');

}

/*******************************************Audit Trial PDF************************************************/

function create_audit_trail_pdf()
{
	$obj = new data();
	$obj->funConnect();
	
	$loginid = $_REQUEST['loginid'];
	class PDF extends FPDF
	{
		function Header()
		{
			$obj = new data();
			$obj->funConnect();
			
			$this->SetFillColor(255,255,255);
			$image_path1 = '../img/logo1.jpg';
			$this->Image($image_path1,87,3,30,15);
			$this->SetFont('Arial','',10);
			$this->SetTextColor(220, 99, 30);
			$this->SetXY(140,13);
			$this->Cell(0,5,"Audit Trail",0,1,'R',1);	
			$this->SetFont('Arial','',10);
			$this->SetFillColor(255,255,255);
			$this->SetTextColor(177,175,175);
			$this->Cell(190,1,"_________________________________________________________________________________________________",0,1,'L','1');
			$this->Ln(2);
		}
			
		function Footer()
		{
			$this->SetY(-20);
			//SELECT Arial italic 8
			$this->SetFillColor(255,255,255);
			$this->SetTextColor(177,175,175);
			$this->SetFont('Arial','',10);
			$this->Cell(190,1,"_________________________________________________________________________________________________",0,1,'L','1');
			$this->Ln(1);
			$this->SetFont('Arial','',6);
			
			if($_SESSION['company'] == 1)
			{
				$this->Cell(190,4,"Alpina Chartering ApS",0,1,'C');
				$this->Cell(190,4," Skomagergade 5-7, DK-4000 Roskilde,Denmark",0,1,'C');
				$this->Cell(190,4,"Tel: +45 55 77 77 77 email: chartering@alpina.dk",0,1,'C');
				$this->Cell(190,4,"",0,1,'C');
			}
			else if($_SESSION['company'] == 2)
			{
				$this->Cell(190,4,"Alpina Chartering ApS",0,1,'C');
				$this->Cell(190,4," Skomagergade 5-7, DK-4000 Roskilde,Denmark",0,1,'C');
				$this->Cell(190,4,"Tel: +45 55 77 77 77 email: chartering@alpina.dk",0,1,'C');
				$this->Cell(190,4,"",0,1,'C');
			}
		}
	}
	$pdf = new PDF();
	$pdf->AddPage('P');
	$pdf->SetDisplayMode('real','single');
	$pdf->SetTitle('Audit Trail Pdf');
	$pdf->SetAuthor('Seven Oceans Holdings');
	$pdf->SetSubject('Audit Trail Pdf');
	$pdf->SetKeywords('Designed and Developed By Seven Oceans Holdings pvt. Ltd.');
	
	$pdf->Ln(5);

	$arr1 = $arr2 = $arr3 = array();	
	$pdf->SetFont('Arial','B',7);
	$pdf->SetFillColor(27,119,166);
	$pdf->SetTextColor(255,255,255);
	$pdf->SetDrawColor(241,241,241);
	$arr1=array('L','L','L','L','L');
	$arr2=array(40,50,36,35,30);
	$arr3=array("Name","Company Name","Username","Password","Update On Date");
	$pdf->SetAligns($arr1);
	$pdf->SetWidths($arr2);
	$pdf->Row2($arr3);
	
	$sql = "SELECT * FROM audit_trail WHERE LOGINID='".$loginid."'";
	$res = mysql_query($sql);
	$rec = mysql_num_rows($res);
	
	$sql1 = "SELECT * FROM login WHERE LOGINID='".$loginid."'";
	$res1 = mysql_query($sql1);
	$rows1 = mysql_fetch_assoc($res1);
	
	$i = 0;
	if($rec == 0)
	{
		$arr1 = $arr2 = $arr3 = array();	
		$i=$i+1;
		if($i % 2 == 0)
		{
			$pdf->SetFillColor(244,244,244);
		}
		else
		{
			$pdf->SetFillColor(255,255,255);
		}
		
		$pdf->SetFont('Arial','',7);
		$pdf->SetTextColor(0,0,0);
		$arr1=array('C');
		$arr2=array(191);
		$arr3=array("sorry, currently there are zero records");
		$pdf->SetAligns($arr1);
		$pdf->SetWidths($arr2);
		$pdf->Row2($arr3);
	}
	else
	{
		while($rows = mysql_fetch_assoc($res))
		{
			$arr1 = $arr2 = $arr3 = array();	
			$i=$i+1;
			if($i % 2 == 0)
			{
				$pdf->SetFillColor(244,244,244);
			}
			else
			{
				$pdf->SetFillColor(255,255,255);
			}
			
			$pdf->SetFont('Arial','',7);
			$pdf->SetTextColor(0,0,0);
			$arr1=array('L','L','L','L','L');
			$arr2=array(40,50,36,35,30);
			$arr3=array($rows1['CONTACT_PERSON'],$obj->getCompanyData($rows1['MCOMPANYID'],"COMPANY_NAME"),$rows['USERNAME'],$rows['PASSWORD'],date("d-M-Y",strtotime($rows['DATE_ON'])));
			$pdf->SetAligns($arr1);
			$pdf->SetWidths($arr2);
			$pdf->Row2($arr3);
		}
	}
	$filename = "Audit Trail (".$rows1['CONTACT_PERSON'].") ".date("d-M-Y",time()).".pdf";
	$pdf->Output($filename,'D');
}

?>
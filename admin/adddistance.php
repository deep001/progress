<?php 
session_start();
require_once("../includes/display_admin.inc.php");
require_once("../includes/functions_admin.inc.php");
$obj = new data();
$display = new display();
$display->logout_a();
if (@$_REQUEST['action'] == 'submit')
 {
 	$msg = $obj->InsertDistanceMasterRecords();
	header('Location:./distance_master.php?msg='.$msg);
 }
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php $display->title(); ?></title>
<?php $display->favicon(); ?>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<?php $display->css(); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
<![endif]-->
<style>
.animated {
				vertical-align: top; 
				transition: height 0.2s;
				-webkit-transition: height 0.2s; 
				-moz-transition: height 0.2s; 
			}

form.cmxform label.error, label.error {
		/* remove the next line when you have trouble in IE6 with labels in list */
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:10px;
		color: red;
		font-style:normal;
		font-weight:lighter;
		margin:5px;
		vertical-align:top;
		}
</style>
</head>
    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <?php $display->header_tag(); ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
            <?php $display->leftmenu(4); ?>
			<aside class="right-side">                
                <!-- Content Header (Page header) -->
                 <section class="content-header">
                    <h1>
                        <i class="fa fa-folder-open"></i>&nbsp;Masters&nbsp;&nbsp;&nbsp;&nbsp;<small><div class="headline text-center" id="time" style="color:#000; font-weight:bold; font-size:12px;"></div></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                        <li class="active">Masters&nbsp;&nbsp;&nbsp;>&nbsp;&nbsp;&nbsp;Distance</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				<!--   content put here..................-->
				<div align="right"><a href="distance_master.php"><button class="btn btn-info btn-flat">Back</button></a></div>
				<div style="height:10px;">&nbsp;</div>
				
				<!--<div class="col-md-6">-->
				<div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">ADD NEW DISTANCE</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" name="frm1" id="frm1" enctype="multipart/form-data" action="<?php echo $pagename;?>" method="post">
                                    <div class="box-body">
										<div class="form-group">
                                            <label for="selFPort" >From Port</label>
											<select  name="selFPort" class="form-control" id="selFPort" onchange="getData();">
											<?php 
											$obj->getPortListCountryWise();
											?>
											</select>                                         
                                        </div>
										<div class="form-group">
                                            <label for="selTPort" >To Port</label>
											<select  name="selTPort" class="form-control" id="selTPort" onchange="getData();">
											<?php 
											//$obj->getPortListCountryWise();
											?>
											</select>                                         
                                        </div>
										<div class="form-group">
                                            <label for="selRoute" >Route (via)</label>
											<select  name="selRoute" class="form-control" id="selRoute" onchange="getData();">
											<?php 
											$obj->getDistanceRoute();
											?>
											</select>                                         
                                        </div>
										<div class="form-group">
                                            <label for="txtShortDis">Shortest (nm)</label>
                                            <input type="text" class="form-control" id="txtShortDis" name="txtShortDis" placeholder="Shortest (nm)" autocomplete="off"><span id="loader1" ></span>
                                        </div>
										<div class="form-group">
                                            <label for="txt250nmDis">Piracy Avoidance 1 (nm)</label>
                                            <input type="text" class="form-control" id="txt250nmDis" name="txt250nmDis" placeholder="Piracy Avoidance 1 (nm)" autocomplete="off"><span id="loader2" ></span>
                                        </div>
										<div class="form-group">
                                            <label for="txt600nmDis">Piracy Avoidance 2 (nm)</label>
                                            <input type="text" class="form-control" id="txt600nmDis" name="txt600nmDis" placeholder="Piracy Avoidance 2 (nm)" autocomplete="off"><span id="loader3" ></span>
                                        </div>
										<div class="form-group">
                                            <label for="txtLongDis">Piracy Avoidance Longest (nm)</label>
                                            <input type="text" class="form-control" id="txtLongDis" name="txtLongDis" placeholder="Piracy Avoidance Longest (nm)" autocomplete="off"><span id="loader4" ></span>
                                        </div>
										
										<div class="form-group">
                                            <label for="selStatus" >Status</label>
											<select  name="selStatus" class="form-control" id="selStatus" >
											<?php 
											$obj->getUserStatusList();
											?>
											</select>                                         
                                        </div>
										
                                    </div><!-- /.box-body -->

                                    <div class="box-footer" align="center">
                                        <button type="submit" class="btn btn-primary btn-flat">Submit</button><input type="hidden" name="action" value="submit" />
                                    </div>
                                </form>
                            </div>
					<!--</div>			-->
				<!--   content ends here..................-->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
			
        </div><!-- ./wrapper -->
<?php $display->footer(); ?>
<?php $display->js(); ?>
<link href="../css/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" type="text/javascript" src="../js/jquery.alerts.js"></script>
<script src="../js/timer.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){ 
$("#selTPort").html($("#selFPort").html());
$("#frm1").validate({
	rules: {
	selFPort:"required",
	selTPort:{required :true},
	txtShortDis:{required :true ,number:true},
	txt250nmDis:{required :true ,number:true},
	txt600nmDis:{required :true ,number:true},
	txtLongDis:{required :true ,number:true},
	selStatus:"required"
	},
messages: {	
	selFPort:"*",
	selTPort:{required :"*"},
	txtShortDis:{required :"*" },
	txt250nmDis:{required :"*" },
	txt600nmDis:{required :"*" },
	txtLongDis:{required :"*" },
	selStatus:"*"
	},
submitHandler: function(form)  {
		jAlert('<b style="font-size:25px;">Please wait . . .</b><br><br><img src="../img/loading.gif"  />', 'Alert');
		$("#popup_container").css({"background": "rgba(255,255,255,0.1)","border": "10px solid rgba(255,255,255,0.1)","width": "100%"});
		$("#popup_content").css({"background":"none","text-align":"center"});
		$("#popup_ok,#popup_title").hide();  
		form.submit();
	}
});

});

function getData()
{
	if($('#selFPort').val() != "" && $('#selTPort').val() != "")
	{
		$("#txtShortDis,#txt250nmDis,#txt600nmDis,#txtLongDis,#selStatus").val("");
		$("#loader1,#loader2,#loader3,#loader4").show();
		$("#loader1,#loader2,#loader3,#loader4").html('<img src="../img/ajax-loader2.gif" />');
		$.post("options.php?id=2",{selFPort:""+$("#selFPort").val()+"",selTPort:""+$("#selTPort").val()+"",selRoute:""+$("#selRoute").val()+""}, function(html) {
		var var1 = html.split("#");
		if(html != '')
		{
			$('#txtShortDis').val(var1[0]);
			$('#txt250nmDis').val(var1[1]);
			$('#txt600nmDis').val(var1[2]);
			$('#txtLongDis').val(var1[3]);
			$('#selStatus').val(var1[4]);
			$("#loader1,#loader2,#loader3,#loader4").hide();
		}
		else
		{
			$("#txtShortDis,#txt250nmDis,#txt600nmDis,#txtLongDis").val(html);
			$("#selStatus").val("");
			$("#loader1,#loader2,#loader3,#loader4").hide();
		}
		});
	}
	else
	{
		$("#txtShortDis,#txt250nmDis,#txt600nmDis,#txtLongDis").val("");
		$("#selStatus").val("");
	}
}

</script>
    </body>
</html>